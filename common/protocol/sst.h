/**********************************************************************************\
  Name:         SST_handler.h
  Created by:   Brian Lee                May/11/2007
  Modified by:  Noel Wu                  May/12/2008

  Contents:   SST protocol for temperature sensor array (ADT7486A)

  Version:    Rev 1.2

  $Id: SST_handler.h 236 2008-07-14 04:40:40Z midas $
\**********************************************************************************/


#ifndef _MSCB_PROTOCOL_SST
#define _MSCB_PROTOCOL_SST

#include "../mscbemb.h"

// --------------------------------------------------------
// SST Protocol related definitions and function prototypes
// --------------------------------------------------------

#define SST_T_BIT 16  //Time Bit for both the Address and Message (AT and MT)
#define SST_T_BIT_HALF	8
#define SST_T_H1 12   //T_BIT * 0.75  //Logic High time bit
#define SST_T_H0 4    //T_BIT * 0.25  //Logic Low time bit
#define SST_T_H0_DOUBLE 8
	
#define SST_CLEAR_DELAY 2 //in milliseconds, not exactly measured, an assumed one that works


#define SST_SUCCESS								 0
#define SST_FCS_WERROR						-1			//FCS mismatch after writing to SST device
#define SST_FCS_RERROR						-2			//FCS mismatch after reading the temperature 
#define SST_CLIENT_ABORT					-3			//When client wants to abort the communication

void SST_Init(unsigned char SST_LINE);
void SST_Clear(unsigned char SST_LINE);

signed char SST_Cmd(unsigned char SST_LINE, 
	unsigned char addr, 
	unsigned char command,
	unsigned char writeLength, 
	unsigned char readLength, 
	unsigned char* writeData, 
	unsigned char* readData);

#endif // MSCB_PROTOCOL_SST
