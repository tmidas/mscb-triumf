/**********************************************************************************\
Name:			    spi16.c
Author:   	  Bryerton Shaw 	
Created: 		  March 11, 2008
Modified:	    August 19, 2010 by Bryerton Shaw
Description:	Simple SPI 16 and 32 bit protocol

$Id$
\**********************************************************************************/

//-------------------------------------------------------------------
//Include files
//-------------------------------------------------------------------
#include "spi16.h"

// SPI bus Global Variables
sbit SPI_CLK  = MSCB_SPI_CLK;
sbit SPI_MISO = MSCB_SPI_MISO;
sbit SPI_MOSI = MSCB_SPI_MOSI;

//-------------------------------------------------------------------
void SPI16_Init() {

}

/*-------------------------------------------------------------------*
* SPI_write                         *
* Sends up to 16-bits of datatosend on the pin defined as MOSI and  *
* returns the data read from MISO               *
*                       *
* length = number of bits to send/receive         *
*-------------------------------------------------------------------*/
unsigned int SPI16_Write(unsigned int datatosend, signed char length)
{
  signed char i;
  unsigned int din;
  unsigned int datareceived = 0;

  if(length > 16) {
    length = 16;
  }

  for(i=(length-1); i>=0; i--) {
    din = SPI_MISO;
    datareceived |= (din << i);
    SPI_MOSI = (datatosend >> i) & 0x1;
    delay_us(SPI_DELAY);
    SPI_CLK = 0;
    delay_us(SPI_DELAY);
    SPI_CLK = 1;
    watchdog_refresh(0);
  }

  return datareceived;

}

/*-------------------------------------------------------------------*
* SPI_write                         *
* Sends up to 32-bits of datatosend on the pin defined as MOSI
* no data returned by data read from MISO               *
*                       *
* length = number of bits to send         *
*-------------------------------------------------------------------*/
unsigned int SPI32_Write(unsigned long int datatosend, signed char length)
{
  signed char i;

  if(length > 32) {
    length = 32;
  }

  for(i=(length-1); i>=0; i--) {
    SPI_MOSI = (datatosend >> i) & 0x1;
    delay_us(SPI_DELAY);
    SPI_CLK = 0;
    delay_us(SPI_DELAY);
    SPI_CLK = 1;
    watchdog_refresh(0);
  }

  return length;

}

