/**********************************************************************************\
  Name:         temp36_SST_handler.c
  Created by:   Brian Lee                May/11/2007
  Modified by:  Noel Wu						  May/12/2008
	Modified by: 	Bryerton Shaw				Aug/27/2010
  Contents:     SST protocol for general MSCB-embedded experiments
  Last updated: Jun/08/2007

  $Id: SST_handler.c 832 2009-08-14 08:30:40Z midas $
\**********************************************************************************/

// --------------------------------------------------------
//  Include files
// --------------------------------------------------------

#include "sst.h"

sbit SST1 = MSCB_SST1; //This variable chooses between SST lines (e.g. SST1, SST2, etc)

#ifdef MORE_THAN_ONE_LINE
sbit SST2 = MSCB_SST2;
#endif

static void SST_WriteByte(unsigned char datByte, unsigned char SST_LINE);
static unsigned char SST_ReadByte(unsigned char SST_LINE);
static unsigned char FCS_Step(unsigned int msg, unsigned char fcs)  reentrant;

static void SST_DrvLow(unsigned char SST_LINE);
static void SST_DrvHigh(unsigned char SST_LINE);
static bit SST_DrvClientResponse(unsigned char SST_LINE);

/* CRC-8 Table for Polynomial x^8 + x^2 + x^1 + 1 (used for FCS in SST Protocol) */
unsigned char code FCS_data[] = {
  0x00, 0x07, 0x0e, 0x09, 0x1c, 0x1b, 0x12, 0x15,
  0x38, 0x3f, 0x36, 0x31, 0x24, 0x23, 0x2a, 0x2d,
  0x70, 0x77, 0x7e, 0x79, 0x6c, 0x6b, 0x62, 0x65,
  0x48, 0x4f, 0x46, 0x41, 0x54, 0x53, 0x5a, 0x5d,
  0xe0, 0xe7, 0xee, 0xe9, 0xfc, 0xfb, 0xf2, 0xf5,
  0xd8, 0xdf, 0xd6, 0xd1, 0xc4, 0xc3, 0xca, 0xcd,
  0x90, 0x97, 0x9e, 0x99, 0x8c, 0x8b, 0x82, 0x85,
  0xa8, 0xaf, 0xa6, 0xa1, 0xb4, 0xb3, 0xba, 0xbd,
  0xc7, 0xc0, 0xc9, 0xce, 0xdb, 0xdc, 0xd5, 0xd2,
  0xff, 0xf8, 0xf1, 0xf6, 0xe3, 0xe4, 0xed, 0xea,
  0xb7, 0xb0, 0xb9, 0xbe, 0xab, 0xac, 0xa5, 0xa2,
  0x8f, 0x88, 0x81, 0x86, 0x93, 0x94, 0x9d, 0x9a,
  0x27, 0x20, 0x29, 0x2e, 0x3b, 0x3c, 0x35, 0x32,
  0x1f, 0x18, 0x11, 0x16, 0x03, 0x04, 0x0d, 0x0a,
  0x57, 0x50, 0x59, 0x5e, 0x4b, 0x4c, 0x45, 0x42,
  0x6f, 0x68, 0x61, 0x66, 0x73, 0x74, 0x7d, 0x7a,
  0x89, 0x8e, 0x87, 0x80, 0x95, 0x92, 0x9b, 0x9c,
  0xb1, 0xb6, 0xbf, 0xb8, 0xad, 0xaa, 0xa3, 0xa4,
  0xf9, 0xfe, 0xf7, 0xf0, 0xe5, 0xe2, 0xeb, 0xec,
  0xc1, 0xc6, 0xcf, 0xc8, 0xdd, 0xda, 0xd3, 0xd4,
  0x69, 0x6e, 0x67, 0x60, 0x75, 0x72, 0x7b, 0x7c,
  0x51, 0x56, 0x5f, 0x58, 0x4d, 0x4a, 0x43, 0x44,
  0x19, 0x1e, 0x17, 0x10, 0x05, 0x02, 0x0b, 0x0c,
  0x21, 0x26, 0x2f, 0x28, 0x3d, 0x3a, 0x33, 0x34,
  0x4e, 0x49, 0x40, 0x47, 0x52, 0x55, 0x5c, 0x5b,
  0x76, 0x71, 0x78, 0x7f, 0x6a, 0x6d, 0x64, 0x63,
  0x3e, 0x39, 0x30, 0x37, 0x22, 0x25, 0x2c, 0x2b,
  0x06, 0x01, 0x08, 0x0f, 0x1a, 0x1d, 0x14, 0x13,
  0xae, 0xa9, 0xa0, 0xa7, 0xb2, 0xb5, 0xbc, 0xbb,
  0x96, 0x91, 0x98, 0x9f, 0x8a, 0x8d, 0x84, 0x83,
  0xde, 0xd9, 0xd0, 0xd7, 0xc2, 0xc5, 0xcc, 0xcb,
  0xe6, 0xe1, 0xe8, 0xef, 0xfa, 0xfd, 0xf4, 0xf3,
};


//
//----------------------------------------------------------------------------------
/** 
Sends commands to the device. Not all the functions are currently implemented
Ths is the cmd list...
* <center> test purpose.
*  \image html temp.jpg
* </center> 

@attention	Conversion time 12,38,38ms round-robin for all 3 channels of the device.
						Internal averaging, doesn't require one at this level. If anyway desided 
			>100ms< needs to be added if polling on single channel!
@param addr 				Address of the client which Originator wants to talk to
@param command			Command (see ADT7486A_tsensor.h file or ADT7486A manual)
@param writeLength	Length of the data Originator wants to send 
@param readLength 	Length of the data Originator wants to receive

@param *fdata 					Temperature 
@return SUCCESS, status from ADT7486A_Read()
*/	
signed char SST_Cmd(unsigned char SST_LINE, 
	unsigned char addr, 
	unsigned char command,
	unsigned char writeLength, 
	unsigned char readLength, 
	unsigned char* writeData, 
	unsigned char* readData) {
							
	unsigned char writeFCS_Org; 	
	unsigned char writeFCS_Client;
	unsigned char writeFCS_Org_Temp = 0;
	unsigned char i;
	unsigned char readFCS_Client;

	//Calculate originator's side write FCS
	writeFCS_Org = FCS_Step(command, FCS_Step(readLength, FCS_Step(writeLength, FCS_Step(addr, 0x00)))); 

	if(writeLength > 1) { 
		for(i = (writeLength - 2); i != 0; --i ) {
			writeFCS_Org_Temp = FCS_Step(*(writeData+i-1), writeFCS_Org_Temp);
		}
		writeFCS_Org = FCS_Step(writeFCS_Org, writeFCS_Org_Temp);
	}

	//Start the message
	SST_DrvLow(SST_LINE); 				// Sending two zero (the address timing negotiation bits)
	SST_DrvLow(SST_LINE);
	SST_WriteByte(addr,SST_LINE);	// Target address
	SST_DrvLow(SST_LINE); 			 	// Send the message timing negotiation bit
	SST_WriteByte(writeLength,SST_LINE);	// WriteLength
	SST_WriteByte(readLength,SST_LINE);	 	// ReadLength

	// Send CMD
	SST_WriteByte(command,SST_LINE);
			
	// Send additional bytes if necessary 
	for(i = (writeLength-1); i != 0; --i) {
		SST_WriteByte(*(writeData+i-1), SST_LINE); 
	}

	// Get the client FCS to compare against the originator
	writeFCS_Client = SST_ReadByte(SST_LINE);

	if(writeFCS_Org != writeFCS_Client) {
		//WriteFCS check (needs to match to continue)
		return SST_FCS_WERROR;
	} 

	if(writeFCS_Org == ~writeFCS_Client) {
		// Client is requesting a message abort		
		return SST_CLIENT_ABORT; 
	} 

	// Read Data
	for(i = (readLength); i != 0; --i) {
		*(readData+i-1) = SST_ReadByte(SST_LINE);
	}
  
	readFCS_Client = SST_ReadByte(SST_LINE);

	// We'll re-use this variable for the readFCS_Client check
	writeFCS_Client = 0x0;

	for(i = (readLength); i != 0; --i) {
			writeFCS_Client = FCS_Step(*(readData+i-1), writeFCS_Client);
	}

	// Client FC check
	if(writeFCS_Client != readFCS_Client) {
		return SST_FCS_RERROR;		 // No match 
	}

	return SST_SUCCESS;	
}

//
//----------------------------------------------------------------------------------
/**
Single calculation for 8-bit cyclic redundancy checksum
usually used in FORWARD recursive format (to save code-space)
Reentrant function 
/code

/endcode
@param msg 
@param fcs
@return FCS value  (CRC-8 code)                 							
*/
unsigned char FCS_Step(unsigned int msg, unsigned char fcs) reentrant {
   return FCS_data[(msg ^ fcs)];
}

//
//----------------------------------------------------------------------------------
/**
Initialize the SST bus by issuing a CLEAR
*/
void SST_Init(unsigned char SST_LINE) {	
	//Clear the SST signals (drive Low for a certain period)
	SST_Clear(SST_LINE);
}

//
//----------------------------------------------------------------------------------
/**
Drives SST line to low for a reasonably long time which gives enough time for
the SST to go idle. 
*/
void SST_Clear(unsigned char SST_LINE) {
  // Test this, high low high low
  if(SST_LINE==1)
     SST1 = 0;
#ifdef MORE_THAN_ONE_LINE
  else if(SST_LINE==2)					
  	 SST2 = 0;
#endif
  delay_ms(SST_CLEAR_DELAY); //clear to 0's
}

//
//----------------------------------------------------------------------------------
/**
Drives SST line to a logic "high" or "1", which is defined as driving SST
high 3/4 cycle of t_Bit and the rest of it low
*/
static void SST_DrvHigh(unsigned char SST_LINE) {
  // Drive high for 0.75 * T_BIT
  if(SST_LINE==1) {
     SST1 = 1;
     delay_us(SST_T_H1);
     SST1 = 0; //drive the rest of T_BIT low
     delay_us(SST_T_H0);
  }
#ifdef MORE_THAN_ONE_LINE
  else if(SST_LINE==2) {
     SST2 = 1;
     delay_us(SST_T_H1);
     SST2 = 0; //drive the rest of T_BIT low
     delay_us(SST_T_H0);
  }
#endif
}

//
//----------------------------------------------------------------------------------
/**
To drive SST line to a logic "low" or "0", which is defined as driving SST
high for 1/4 cycle of t_Bit and the rest of it low.
*/
void SST_DrvLow(unsigned char SST_LINE) {
  // Drive high for 0.25 * T_BIT
  if(SST_LINE==1) {
     SST1 = 1;
     delay_us(SST_T_H0);
     SST1 = 0; //drive the rest of T_BIT low
     delay_us(SST_T_H1);
  }
#ifdef MORE_THAN_ONE_LINE
  else if(SST_LINE==2) {
     SST2 = 1;
     delay_us(SST_T_H0);
     SST2 = 0; //drive the rest of T_BIT low
     delay_us(SST_T_H1);
  }   
#endif
}

//
//----------------------------------------------------------------------------------
/**
Get the comparator bit value (Client response)
@return Read Bit from Client's Response, Only 1 or 0 (gone through comparator)
*/
static bit SST_DrvClientResponse(unsigned char SST_LINE) {
	bit result;

  // Drive SST to logic "0" (high 1/4, low 3/4) to let Client
  // drive SST to its desired state
  if(SST_LINE==1) {
     SST1 = 1; //Drv high (originator drives SST bus high to toggle client's response
     delay_us (1);
     SST1 = 0;

     delay_us(SST_T_BIT_HALF); //delay for half of T_BIT time

#if defined(MCU_C8051F120)
#ifdef CMB 
	  SFRPAGE  = CPT1_PAGE;
#elif defined(FEB64)	
	  SFRPAGE  = CPT1_PAGE;
#elif defined(LPB)	
	  SFRPAGE  = CPT1_PAGE;
#else
    SFRPAGE  = CPT0_PAGE;
#endif	
#endif

		result = SST_ClientResponse1;
		delay_us(SST_T_H0_DOUBLE);
  }
#ifdef MORE_THAN_ONE_LINE
  else if(SST_LINE==2) {
     SST2 = 1; //Drv high (originator drives SST bus high to toggle client's response
     delay_us (1);
     SST2 = 0;

     delay_us(SST_T_BIT_HALF); //delay for half of T_BIT time

#if defined(MCU_C8051F120)
#ifdef FEB64
     SFRPAGE  = CPT0_PAGE;
#elif defined(MORE_THAN_ONE_LINE)
     SFRPAGE  = CPT1_PAGE;
#endif	
#endif
     result = SST_ClientResponse2;
     delay_us(SST_T_H0_DOUBLE);
  } // Client1
#endif

	return result;
}

//
//----------------------------------------------------------------------------------
/**
Write a single byte to the SST device.
@param datByte
*/
void SST_WriteByte(unsigned char datByte, unsigned char SST_LINE) {
  signed char i;

  for(i = 7; i >= 0; i--) {
		if(datByte & (1 << i)) {
			SST_DrvHigh(SST_LINE);
		} else {
			SST_DrvLow(SST_LINE);
    }
  }
}

//
//----------------------------------------------------------------------------------
/**
Read a single byte from the SST device.
@return Device Client byte response
*/
unsigned char SST_ReadByte(unsigned char SST_LINE) {
  unsigned char dataReceived = 0;
  signed char j;

  for(j = 7; j >= 0; j--) {
    // Get single bit from comparator
    dataReceived |= ((unsigned char)SST_DrvClientResponse(SST_LINE) << j);
  }

  return dataReceived;
}