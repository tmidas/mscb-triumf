/**********************************************************************************\
  Name:			SPIBus_handler.h
  Author:   	Bryerton Shaw 	
  Created: 		March 11, 2008
  Description:	simple SPI bus protocol
  $Id$
\**********************************************************************************/


#ifndef  SPIBUS_HANDLER_H
#define  SPIBUS_HANDLER_H

#include "../../common/mscbemb.h"

// SPI bus Constants
// SPI interface constantsr a
// SPI_DELAY = SPI Clock Period / 2
#define SPI_DELAY        100


void 	SPI16_Init(void);
unsigned int SPI16_Write(unsigned int datatosend, signed char length);
unsigned int SPI32_Write(unsigned long int ldata, signed char length); 

#endif // SPIBUS_HANDLER_H

