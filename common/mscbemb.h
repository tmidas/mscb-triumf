/********************************************************************\

  Name:         mscbemb.h
  Created by:   Stefan Ritt

  Contents:     Midas Slow Control Bus protocol commands

  $Id: mscbemb.h 83 2009-06-25 05:28:31Z midas $

\********************************************************************/

#ifndef _MSCB_EMB_H
#define _MSCB_EMB_H

// String Tokenize trick
#define TOKEN_STRING(X) #X
#define INCLUDE_STR_(X) TOKEN_STRING(X)
#define INCLUDE_STR(X) INCLUDE_STR_(X)

/*--------------------------------*/
// Project Include
#ifndef MSCB_SETTINGS
#error "MSCB_SETTINGS must be defined as a relative path from the MSCB/common directory to the project directory"
#error "Example: define(MSCB_SETTINGS="../project/power_switch/settings.h")"
#else 
#include INCLUDE_STR(MSCB_SETTINGS)
#endif // MSCB_SETTINGS

/*--------------------------------*/
// Processor Includes
#if   defined(MCU_C8051F410) 
#include "../mcu/F410/f410.h"
#elif defined(MCU_C8051F310)
#include "../mcu/f310/f310.h"
#elif defined(MCU_C8051F120)
#include "../mcu/f120/f120.h"
#elif
#error "No MCU_x defined!"
#endif

/*---- CPU specific items ------------------------------------------*/

/* default flags */
#define USE_WATCHDOG
#define HAVE_EEPROM

/*---- MSCB commands -----------------------------------------------*/

#define PROTOCOL_VERSION 5
#define INTERCHAR_DELAY 20      // 20us between characters

/* Version history:

1.0 Initial
1.1 Add unit prefix, implement upgrade command
1.2 Add CMD_ECHO, CMD_READ with multiple channels
1.3 Add "set node name"
1.4 Remove CMD_xxxs_CONF
1.5 Return 0x0A for protected pages on upload
1.6 Upload subpages of 60 bytes with ACK
1.7 Upload with simplified CRC code
1.8 Add 20us delay betwee characters for repeater
*/

#define CMD_ADDR_NODE8  0x09
#define CMD_ADDR_NODE16 0x0A
#define CMD_ADDR_BC     0x10
#define CMD_ADDR_GRP8   0x11
#define CMD_ADDR_GRP16  0x12
#define CMD_PING8       0x19
#define CMD_PING16      0x1A

#define CMD_INIT        0x20
#define CMD_GET_INFO    0x28
#define CMD_SET_ADDR    0x33
#define CMD_SET_NAME    0x37
#define CMD_SET_BAUD    0x39

#define CMD_FREEZE      0x41
#define CMD_SYNC        0x49
#define CMD_SET_TIME    0x4E
#define CMD_UPGRADE     0x50
#define CMD_USER        0x58

#define CMD_ECHO        0x61
#define CMD_TOKEN       0x68
#define CMD_GET_UPTIME  0x70

#define CMD_ACK         0x78

#define CMD_WRITE_NA    0x80
#define CMD_WRITE_ACK   0x88

#define CMD_FLASH       0x98

#define CMD_READ        0xA0

#define CMD_WRITE_BLOCK 0xB5
#define CMD_READ_BLOCK  0xB9

#define GET_INFO_GENERAL   0
#define GET_INFO_VARIABLES 1

#define ADDR_SET_NODE      1
#define ADDR_SET_HIGH      2
#define ADDR_SET_GROUP     3

/*---- MSCB upgrade commands ---------------------------------------*/

#define UCMD_ECHO          1
#define UCMD_ERASE         2
#define UCMD_PROGRAM       3
#define UCMD_VERIFY        4
#define UCMD_READ          5
#define UCMD_REBOOT        6
#define UCMD_RETURN        7

/*---- flags from the configuration and status register (CSR) ------*/

#define CSR_DEBUG       (1<<0)
#define CSR_LCD_PRESENT (1<<1)
#define CSR_SYNC_MODE   (1<<2)
#define CSR_FREEZE_MODE (1<<3)
#define CSR_WD_RESET    (1<<2)

/*---- baud rates used ---------------------------------------------*/

#define BD_2400            1
#define BD_4800            2
#define BD_9600            3
#define BD_19200           4
#define BD_28800           5
#define BD_38400           6
#define BD_57600           7
#define BD_115200          8
#define BD_172800          9
#define BD_345600         10

/*---- info structures ---------------------------------------------*/
#define NaN       0xFFFFFFFF    /* Not a number (error) */
#define plusINF   0x7F800000    /* Positive overflow    */
#define minusINF  0xFF800000    /* Negative overflow    */

union f  {
  float          f;          /* Floating-point value */
  unsigned long ul;          /* Unsigned long value */
};

typedef struct {
   unsigned char protocol_version;
   unsigned char node_status;
   unsigned char n_variables;
   unsigned short node_address;
   unsigned short group_address;
   unsigned short watchdog_resets;
   char node_name[16];
} MSCB_INFO;

typedef struct {
   unsigned char width;         // width in bytes
   unsigned char unit;          // physical units UNIT_xxxx
   unsigned char prefix;        // unit prefix PRFX_xxx
   unsigned char status;        // status (not yet used)
   unsigned char flags;         // flags MSCBF_xxx
   char name[8];                // name
   void *ud;                    // point to user data buffer

#if defined(SCS_1000) || defined(SCS_1001) || defined(SCS_2000)
   float min, max, delta;       // limits for button control
   unsigned short node_address; // address for remote node on subbus
   unsigned char  channel;      // address for remote channel subbus
#endif
} MSCB_INFO_VAR;

#define MSCBF_FLOAT    (1<<0)   // channel in floating point format
#define MSCBF_SIGNED   (1<<1)   // channel is signed integer
#define MSCBF_DATALESS (1<<2)   // channel doesn't contain data
#define MSCBF_HIDDEN   (1<<3)   // used for internal config parameters
#define MSCBF_REMIN    (1<<4)   // get variable from remote node on subbus
#define MSCBF_REMOUT   (1<<5)   // send variable to remote node on subbus
#define MSCBF_INVALID  (1<<6)   // cannot read remote variable

/* physical units */

#define PRFX_PICO       -12
#define PRFX_NANO        -9
#define PRFX_MICRO       -6
#define PRFX_MILLI       -3
#define PRFX_NONE         0
#define PRFX_KILO         3
#define PRFX_MEGA         6
#define PRFX_GIGA         9
#define PRFX_TERA        12

#define UNIT_UNDEFINED    0

// SI base units
#define UNIT_METER        1
#define UNIT_GRAM         2
#define UNIT_SECOND       3
#define UNIT_MINUTE       4
#define UNIT_HOUR         5
#define UNIT_AMPERE       6
#define UNIT_KELVIN       7
#define UNIT_CELSIUS      8
#define UNIT_FARENHEIT    9

// SI derived units

#define UNIT_HERTZ       20
#define UNIT_PASCAL      21
#define UNIT_BAR         22
#define UNIT_WATT        23
#define UNIT_VOLT        24
#define UNIT_OHM         25
#define UNIT_TESLA       26
#define UNIT_LITERPERSEC 27
#define UNIT_RPM         28
#define UNIT_FARAD       29
#define UNIT_JOULE       30

// computer units

#define UNIT_BOOLEAN     50
#define UNIT_BYTE        52
#define UNIT_WORD        53
#define UNIT_DWORD       54
#define UNIT_ASCII       55
#define UNIT_STRING      56
#define UNIT_BAUD        57

// others

#define UNIT_PERCENT     90
#define UNIT_PPM         91
#define UNIT_COUNT       92
#define UNIT_FACTOR      93

/*------------------------------------------------------------------*/

typedef struct {                // system info stored in EEPROM
   unsigned int node_addr;
   unsigned int group_addr;
   unsigned int svn_revision;
   char node_name[16];
} SYS_INFO;

#define LED_OFF !LED_ON

#if defined(LED_7)
#define N_LED 8
#elif defined(LED_6)
#define N_LED 7
#elif defined(LED_5)
#define N_LED 6
#elif defined(LED_4)
#define N_LED 5
#elif defined(LED_3)
#define N_LED 4
#elif defined(LED_2)
#define N_LED 3
#elif defined(LED_1)
#define N_LED 2
#elif defined(LED_0)
#define N_LED 1
#else
#define N_LED 0
#endif

/* map SBUF0 & Co. to SBUF */
#if !defined(MCU_C8051F020) && !defined(MCU_C8051F120) && !defined(MCU_C8051F310) && !defined(MCU_C8051F320) && !defined(MCU_C8051F410)
#define SCON0    SCON 
#define SBUF0    SBUF
#define TI0      TI
#define RI0      RI
#define RB80     RB8
#define ES0      ES
#define PS0      PS
#endif

#if defined(MCU_C8051F020) || defined(MCU_C8051F120)
#define PS0      PS
#endif

#if defined(MCU_C8051F310) || defined(MCU_C8051F320) || defined(MCU_C8051F410)
#define EEPROM_OFFSET 0x3A00 // 0x3A00-0x3BFF = 0.5kB, 0x3C00 cannot be erased!
#define N_EEPROM_PAGE      1 // 1 page  @ 512 bytes
#elif defined(MCU_C8051F120)
#define EEPROM_OFFSET 0xE000 // 0xE000-0xEFFF = 4kB
#define N_EEPROM_PAGE      8 // 8 pages @ 512 bytes
#else
#define EEPROM_OFFSET 0x6000 // 0x6000-0x6FFF = 4kB
#define N_EEPROM_PAGE      8 // 8 pages @ 512 bytes
#endif


/* UART1 specific definitions */
#if defined(SCS_210) | defined(SCS_220)
#define UART1_DEVICE                     // use direct device communication
#endif

#if defined(UART1_DEVICE) && defined(HAVE_LCD)
char putchar1(char c);                   // putchar cannot be used with LCD support
#endif

#ifndef UART1_DEVICE
#if defined(SCS_1000) || defined(SCS_1001) || defined(SCS_2000)
#define UART1_MSCB                       // UART1 connected as master to MSCB slave bus
#endif
#endif

/*---- Delay macro to be used in interrupt routines etc. -----------*/

#if defined(SUBM_260)
#define DELAY_US(_us) { \
   unsigned char _i,_j; \
   for (_i = (unsigned char) _us; _i > 0; _i--) \
      for (_j=2 ; _j>0 ; _j--) \
         _nop_(); \
}
#elif defined(SCS_210)
#define DELAY_US(_us) { \
   unsigned char _i,_j; \
   for (_i = (unsigned char) _us; _i > 0; _i--) { \
      _nop_(); \
      for (_j=3 ; _j>0 ; _j--) \
         _nop_(); \
   } \
}
#elif defined(MCU_C8051F120)
#define DELAY_US(_us) { \
   unsigned char _i,_j; \
   for (_i = (unsigned char) _us; _i > 0; _i--) \
      for (_j=19 ; _j>0 ; _j--) \
         _nop_(); \
}
#define DELAY_US_REENTRANT(_us) { \
   unsigned char _i,_j; \
   for (_i = (unsigned char) _us; _i > 0; _i--) \
      for (_j=3 ; _j>0 ; _j--) \
         _nop_(); \
}
#elif defined(MCU_C8051F310)
#define DELAY_US(_us) { \
   unsigned char _i,_j; \
   for (_i = (unsigned char) _us; _i > 0; _i--) { \
      _nop_(); \
      for (_j=3 ; _j>0 ; _j--) \
         _nop_(); \
   } \
}
#elif defined(MCU_C8051F410)
#define DELAY_US(_us) { \
   unsigned char _i,_j; \
   for (_i = (unsigned char) _us; _i > 0; _i--) { \
      _nop_(); \
      for (_j=3 ; _j>0 ; _j--) \
         _nop_(); \
   } \
}
#elif defined(MCU_C8051F320)
#define DELAY_US(_us) { \
   unsigned char _i,_j; \
   for (_i = (unsigned char) _us; _i > 0; _i--) { \
      _nop_(); \
      for (_j=1 ; _j>0 ; _j--) \
         _nop_(); \
   } \
}
#else
#define DELAY_US(_us) { \
   unsigned char _i; \
   for (_i = (unsigned char) _us; _i > 0; _i--) { \
      _nop_(); \
      _nop_(); \
   } \
}
#endif

#ifndef ENABLE_INTERRUPTS
#define ENABLE_INTERRUPTS { EA = 1; }
#endif

#ifndef DISABLE_INTERRUPTS
#define DISABLE_INTERRUPTS { EA = 0; }
#endif

/*---- function declarations ---------------------------------------*/

void watchdog_refresh(unsigned char from_interrupt) reentrant;
void watchdog_enable(unsigned char watchdog_timeout);
void watchdog_disable(void);
void yield(void);
void led_set(unsigned char led, unsigned char flag) reentrant;
void led_blink(unsigned char led, unsigned char n, int interval) reentrant;
void led_mode(unsigned char led, unsigned char flag) reentrant;
void delay_us(unsigned int us);
void delay_ms(unsigned int ms);

unsigned char crc8(unsigned char *buffer, int len) reentrant;
unsigned char crc8_add(unsigned char crc, unsigned int c);

void lcd_setup();
void lcd_clear();
void lcd_cursor(unsigned char flag);
void lcd_goto(char x, char y);
void lcd_putc(char c);
void lcd_puts(char *str);
char scs_lcd1_read();

char getchar_nowait(void);
unsigned char gets_wait(char *str, unsigned char size, unsigned char timeout);
void flush(void);

void eeprom_flash(void);
unsigned char eeprom_retrieve(unsigned char flag);
void setup_variables(void);

void uart_init(unsigned char port, unsigned char baud);

unsigned char uart1_send(char *buffer, int size, unsigned char bit9);
unsigned char uart1_receive(char *buffer, int size);
void send_remote_var(unsigned char i);
unsigned char ping(unsigned short addr);

void sysclock_init(void);
void sysclock_reset(void);
unsigned long time(void);
unsigned long uptime(void);

unsigned char user_func(unsigned char *data_in, unsigned char *data_out);

void set_n_sub_addr(unsigned char n);
unsigned char cur_sub_addr(void);

void rtc_init(void);
unsigned char rtc_present(void);
void rtc_write(unsigned char d[6]);
void rtc_read(unsigned char d[6]);
void rtc_write_item(unsigned char item, unsigned char d);
void rtc_conv_date(unsigned char d[6], char *str);
void rtc_conv_time(unsigned char d[6], char *str);
void rtc_print(void);
void rtc_write_byte(unsigned char adr, unsigned char d);

/*----------------*/
// Protocol Includes
#ifdef MSCB_SHT7x
#include "protocol/sht7x.h"
#endif 

#ifdef MSCB_SST
#include "protocol/sst.h"
#endif

#ifdef MSCB_SPI16
#include "protocol/spi16.h"
#endif

/*----------------*/
// Device Includes

#ifdef MSCB_DEVICE_TEMP_LTC2605
#include "../device/temp/LTC2605CGN.h"
#endif

#ifdef MSCB_DEVICE_ADC_LTC2489
#include "../device/adc/LTC2489.h"
#endif

#ifdef MSCB_DEVICE_ADC_LTC2485
#include "../device/adc/LTC2485.h"
#endif

#ifdef MSCB_DEVICE_ADC_AD7991
#include "../device/adc/AD7991.h"
#endif

#ifdef MSCB_DEVICE_ADC_LTC2493
#include "../device/adc/LTC2493.h"
#endif

#ifdef MSCB_DEVICE_ADC_LTC2495
#include "../device/adc/LTC2495.h"
#endif

#ifdef MSCB_DEVICE_ADC_LTC2499
#include "../device/adc/LTC2499.h"
#endif

#ifdef MSCB_DEVICE_ADC_ADS1225
#include "../device/adc/ADS1225.h"
#endif

#ifdef MSCB_DEVICE_ADC_ADS1240
#include "../device/adc/ADS1240.h"
#endif

#ifdef MSCB_DEVICE_IO_PCA9535
#include "../device/io/PCA9535.h"
#endif

#ifdef MSCB_DEVICE_IO_PCA9536
#include "../device/io/PCA9536.h"
#endif

#ifdef MCSB_DEVICE_IO_AD5248
#include "../device/io/AD5248.h"
#endif

#ifdef MSCB_DEVICE_IO_DS1023
#include "../device/io/DS1023.h"
#endif

#ifdef MSCB_DEVICE_DAC_LTC2605
#include "../device/dac/ltc2605cgn.h"
#endif

#ifdef MSCB_DEVICE_DAC_AD5383
#include "../device/dac/ad5383.h"
#endif

#ifdef MSCB_DEVICE_DAC_LTC2606
#include "../device/dac/ltc2606cgn.h"
#endif

#ifdef MSCB_DEVICE_DAC_LTC2607
#include "../device/dac/ltc2607.h"
#endif

#ifdef MSCB_DEVICE_DAC_LTC2609
#include "../device/dac/ltc2609cgn.h"
#endif

#ifdef MSCB_DEVICE_DAC_LTC2655
#include "../device/dac/ltc2655.h"
#endif

#ifdef MSCB_DEVICE_TUI_LTC2990
#include "../device/temp/ltc2990tui.h"
#endif

#ifdef MSCB_DEVICE_TUI_LTC2991
#include "../device/temp/ltc2991tui.h"
#endif

#ifdef MSCB_DEVICE_ADC_ADM1176
#include "../device/adc/adm1176.h"
#endif

#ifdef MSCB_DEVICE_TEMP_ADT7483
#include "../device/temp/adt7483.h"
#endif

#ifdef MSCB_DEVICE_HUMIDITY_SHT7x
#include "../device/humidity/sht7x_sensor.h"
#endif

#ifdef MSCB_DEVICE_ADC_INTERNAL
#include "../device/adc/adc_internal.h"
#endif

#ifdef MSCB_DEVICE_PCA_INTERNAL
#include "../device/io/pca_internal.h"
#endif

#ifdef MSCB_DEVICE_TEMP_ADT7486A
#include "../device/temp/adt7486a.h"
#endif

#ifdef MCSB_DEVICE_IO_AD5535
#include "../device/dac/AD5535.h"
#endif

#ifdef MCSB_DEVICE_IO_AD5248
#include "../device/io/AD5248.h"
#endif

#endif // _MSCB_EMB_H
