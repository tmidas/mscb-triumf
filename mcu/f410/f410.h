// $Id$

#ifndef _MCSB_MCU_C8051F410_H
#define _MSCB_MCU_C8051F410_H

#include  "c8051f410.h"

#ifdef MSCB_SMBus_HW
#include "protocol/smbus.h"
#endif

#define ADC_INTERNAL_TEMP_CH	0x18  // Temperature channel

#endif
