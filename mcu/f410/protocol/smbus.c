/**********************************************************************************\
Name:			    smbus.c
Author:   	  Bryerton Shaw 	
Created: 		  March 11, 2008
Modified:	    August 19, 2010 by Bryerton Shaw
Description:	SMBus protocol using interrupts

$Id: SMBus310_handler.c 73 2009-01-30 05:16:24Z midas $
\**********************************************************************************/

//-------------------------------------------------------------------
//Include files
//-------------------------------------------------------------------
#include "smbus.h"

// SMBus Global Variables
static volatile unsigned char *pSMB_DATA_OUT;
static volatile unsigned char xdata SMB_DATA_OUT_LEN;
static volatile unsigned char *pSMB_DATA_IN;
static volatile unsigned char xdata SMB_DATA_IN_LEN;
static volatile unsigned char xdata SMB_TARGET;
static volatile unsigned char xdata SMB_MAX_POLLS;
static volatile bit SMB_BUSY;
static volatile bit SMB_RW;
static volatile bit SMB_ACKPOLL;
static volatile bit SMB_SUCCESS;

static volatile unsigned char xdata _smb_failed_polls;

//
//-------------------------------------------------------------------
void SMBus_SetACKPoll(unsigned char poll_state, unsigned char max_polls) {
  SMB_ACKPOLL = poll_state;
  SMB_MAX_POLLS = max_polls;
}

//
//-------------------------------------------------------------------
void SMBus_SetSlaveAddr(unsigned char slaveAddr) {
  SMB_TARGET = slaveAddr;
}

//
//-------------------------------------------------------------------
void SMBus_SetTXBuffer(unsigned char *pData, unsigned char dataLen) {
  pSMB_DATA_OUT = pData;
  SMB_DATA_OUT_LEN = dataLen;  
}

//
//-------------------------------------------------------------------
void SMBus_SetRXBuffer(unsigned char *pData, unsigned char dataLen) {
  pSMB_DATA_IN = pData; 
  SMB_DATA_IN_LEN = dataLen;
}

//
//-------------------------------------------------------------------
void SMBus_Wait(void) {
  while(SMB_BUSY);
}

//
//-------------------------------------------------------------------
void SMBus_Clear(void) reentrant {
  pSMB_DATA_OUT = 0;
  pSMB_DATA_IN = 0;
  SMB_DATA_IN_LEN = 0;
  SMB_DATA_OUT_LEN = 0;
  SMB_BUSY = 0;
  SMB_ACKPOLL = SMB_DISABLE_ACKPOLL;
  SMB_TARGET = 0;
  SMB_MAX_POLLS = 0;
  _smb_failed_polls = 0;
  SMB_SUCCESS = 0;
}

//
//-------------------------------------------------------------------
unsigned char SMBus_Start(void) {
  unsigned char retVal;

  if(SMB_DATA_OUT_LEN > 0) {
    SMB_RW = SMB_WRITE;
  } else if(SMB_DATA_IN_LEN > 0) {
    SMB_RW = SMB_READ;
  } else {
    return 0;
  }
  
  SMB_BUSY = 1;
  STA = 1;

  SMBus_Wait();

  retVal = SMB_SUCCESS;

  SMBus_Clear();

  return retVal;
}

//
//-------------------------------------------------------------------
/**
Initializing the SMBus
*/
void SMBus_Init(void) {
  static bit init = 0;

  if(!init) {
    init = 1;

    // Configuring the Timer3 Registers
    TMR3CN = 0x00;	// Turn Clock off and SYSCLK / 12

    TMR3RLL = 0x00; /* TIMER 3 RELOAD LOW BYTE */
    TMR3RLH = 0x00; /* TIMER 3 RELOAD HIGH BYTE */
    TMR3L = 0x00;   //Timer3 Low Bytes
    TMR3H = 0x00;   //Timer3 High Bytes
    TMR3CN = 0x04;  // Enable Timer3

    SMB0CF = 0x5D;   // SMbus Setup Hold Extension/Timeout Detection/Free Timeout Detection Enable
    // Timer1 Overflow is set as the clock source
    SMB0CF |= 0x80;  // Enable SMBus after all setting done.

    EIE1 |= 0x81;	   // Enable SMBus interrupts/Enable Timer3 interrupts
    
    SMBus_SetACKPoll(SMB_DISABLE_ACKPOLL, 0);
    SMBus_Clear();    
  }
}

//
//-------------------------------------------------------------------
void SMBus_ISR(void) interrupt INTERRUPT_SMBUS0 {
  // transfers
  static unsigned char data_in;
  static unsigned char data_out;

  switch (SMB0CN & 0xF0)	    // Status vector
  {
    // Master Transmitter/Receiver: START condition transmitted.
  case SMB_MTSTA:
    SMB0DAT = (SMB_TARGET << 1) | SMB_RW; // Load address of the target slave and R/W bit
    STA = 0;	// Clear START bit
    data_in = 0;
    data_out = 0;
    break;

    // Master Transmitter: Data byte transmitted
  case SMB_MTDB:
    if (ACK) {				     // Slave ACK?
      if (data_out < SMB_DATA_OUT_LEN) {	 	      						 		 
        if (SMB_RW==SMB_WRITE) {// If this transfer is a WRITE,
          SMB0DAT = pSMB_DATA_OUT[data_out++]; // send data byte
        }
      } else if(data_in < SMB_DATA_IN_LEN) {
        SMB_RW = SMB_READ;
        STA = 1;      
      } else {
        STO = 1;
        SMBus_Clear();
        SMB_SUCCESS = 1;
      }
    } else if (SMB_ACKPOLL && _smb_failed_polls < SMB_MAX_POLLS) {			 				  
      STO = 0;
      STA = 1;  
      _smb_failed_polls++;
    } else {	// If slave NACK, and we are not expecting anything to read
      STO = 1;
      SMBus_Clear();										  
    }
    break;

    // Master Receiver: byte received
  case SMB_MRDB:
    if(!ACK && SMB_ACKPOLL ) {
      if(_smb_failed_polls < SMB_MAX_POLLS) { 
        STO = 0;
        STA = 1;        
       _smb_failed_polls++;
       } else {
         STO = 1;
         SMBus_Clear();
       }
    } else {
      pSMB_DATA_IN[data_in++] = SMB0DAT;
      if ( data_in >= SMB_DATA_IN_LEN) { 		     						  
        ACK = 0;          // Send NACK to indicate last byte
        STO = 1;          // Send STOP to terminate transfer
        SMBus_Clear();
        SMB_SUCCESS = 1;
      } else {
        ACK = 1;
      }
    }
    break;

  default:
    STA = 0;
    STO = 1;
    ACK = 0;
    break;
  } // end switch
	
  SI=0; // clear interrupt flag
}

//
//-------------------------------------------------------------------
void Timer3_ISR(void) interrupt INTERRUPT_TIMER3 {
  TMR3CN &= ~0xC0;	// Clear Timer3 interrupt-pending flag
  SMBus_Clear();
}	
