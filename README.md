This mscb-Triumf package requires custom electronics hardware based on the C8051 micro-controllers (uC), appropriate cross-compiler and the Midas Data Acquisition package.

This repository provides a variation of the original **mscb** packaged from Stefan Ritt (PSI) for a better project organization.
Recently, the **mscb** from PSI has been updated to closely match the Triumf project structure.

**mscb** stands for **Midas Slow Control Bus**. It provides a software framework for operation and communication to electronics boards controlled by a C8051 uC.