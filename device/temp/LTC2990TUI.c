/********************************************************************\
  Name:         LTC2990TUI.c
  Contents:     Temp, Volt, Current monitoring.
  $Id$
\********************************************************************/

#include "LTC2990TUI.h"

//
//------------------------------------------------------------------------
void LTC2990TUI_Init(void) {
#ifdef MSCB_DEVICE_TUI_LTC2990_DISABLE
#else
	SMBus_Init(); // SMBus initialization
#endif // MSCB_DEVICE_TUI_LTC2990_DISABLE
}

//
//------------------------------------------------------------------------
/*
void LTC2990TUI_ConfigStart(unsigned char addr, unsigned char mode) {
#ifdef MSCB_DEVICE_TUI_LTC2990_DISABLE
  addr = 0;
#else
  unsigned char cmd;

  cmd = mode; 	 

  SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&cmd, 1);
  SMBus_SetRXBuffer(0, 0);
  SMBus_Start();
#endif // MSCB_DEVICE_TUI_LTC2990_DISABLE
}

//
//------------------------------------------------------------------------
unsigned char LTC2990TUI_ReadConversion(unsigned char addr, unsigned char channel, signed long *pResult) {
#ifdef MSCB_DEVICE_TUI_LTC2990_DISABLE
  addr = 0;
  channel = 0;
  *pResult = 0;
  return 0;
#else
  unsigned char cmd;
  signed int Result;
  unsigned char validRange = LTC2990TUI_VALIDRANGE;

  cmd = channel;

  Result = 0;
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&cmd, 1);
  SMBus_SetRXBuffer((unsigned char*)&Result, 2);
  SMBus_Start();

	if((Result & 0x8000) == 0x0000) { validRange = LTC2990TUI_INVALID;  }
  *pResult = Result;

	Result &= 0x7FFF;
  *pResult = Result;

  // Sign extend if necessary
  if((*pResult & 0x4000) == 0x4000) {
    *pResult |= 0xC000;
  }

	return validRange;
#endif // MSCB_DEVICE_TUI_LTC2990_DISABLE
}

//
//------------------------------------------------------------------------
unsigned char LTC2990TUI_ReadRegister(unsigned char addr, unsigned char reg) {
#ifdef MSCB_DEVICE_TUI_LTC2990_DISABLE
  addr = 0;
  reg = 0;
  return 0;
#else
  unsigned char result = 0xff;
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&reg, 1);
  SMBus_SetRXBuffer(&result, 1);
  SMBus_Start();

  return result;

#endif // MSCB_DEVICE_TUI_LTC2990_DISABLE
}
*/

//
//------------------------------------------------------------------------
unsigned char LTC2990TUI_ReadTemperature(unsigned char addr, unsigned char reg, signed int *pResult) {
#ifdef MSCB_DEVICE_TUI_LTC2990_TEMPERATURE_DISABLE
  addr = 0;
  reg = 0;
  return 0;
#else
  unsigned char validRange = LTC2990TUI_VALIDRANGE;
  unsigned int result = 0xffff;
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&reg, 1);
  SMBus_SetRXBuffer((unsigned char *)(&result), 2);
  SMBus_Start();

 	if((result & 0x8000) == 0x0000) { validRange = LTC2990TUI_INVALID;  }
 	if(result & 0x4000) { validRange = LTC2990TUI_TEMPSHORT;  }
 	if(result & 0x2000) { validRange = LTC2990TUI_TEMPOPEN;  }

  *pResult = result & 0x1FFF;  // 13 bits signed
  if(*pResult & 0x1000) { *pResult += 0xE000; }

  return (validRange);

#endif // MSCB_DEVICE_TUI_LTC2990_DISABLE
}

//
//------------------------------------------------------------------------
unsigned char LTC2990TUI_ReadVoltage(unsigned char addr, unsigned char reg, signed int *pResult) {
#ifdef MSCB_DEVICE_TUI_LTC2990_DISABLE
  addr = 0;
  reg = 0;
  return 0;
#else
  unsigned char validRange = LTC2990TUI_VALIDRANGE;
  signed int result;
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&reg, 1);
  SMBus_SetRXBuffer((unsigned char *)&result, 2);
  SMBus_Start();

 	if((result & 0x8000) == 0x0000) { validRange = LTC2990TUI_INVALID;  }

  // Check Data valid
  result &= 0x7FFF;  // 14 bits + sign bit
  *pResult = result;

  // Sign extend if necessary
  if(*pResult & 0x4000) { *pResult += 0x8000; }
 
  // [ADC's?] zero offset compensation;
  // should be accurate to �0.75% (-0.1% measured on node addr 200).
  // There is some temperature dependance, less than few ADC counts.
  // Vertilon HV card SN#076700020 has -0.8% error on HV_SENSE.
  // The actual HV output is very accurate, i.e -3.2V at 1000V
  // (10 MOhm DVM load, 118 �A current). Current accuracy was
  // not tested, only zero offset.
  *pResult += 5;

  return (validRange);

#endif // MSCB_DEVICE_TUI_LTC2990_DISABLE
}

//
//------------------------------------------------------------------------
void LTC2990TUI_WriteRegister(unsigned char addr, unsigned char cmd, unsigned char value) {
#ifdef MSCB_DEVICE_TUI_LTC2990_DISABLE
  return;
#else
  unsigned char buffer[2];

  buffer[0] = cmd;
  buffer[1] = value;

	// Wait for the SMBus to clear
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(buffer, 2);
  SMBus_SetRXBuffer(0, 0);
  SMBus_Start();
#endif // MSCB_DEVICE_TUI_LTC2990_DISABLE
}
