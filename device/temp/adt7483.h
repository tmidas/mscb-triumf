/********************************************************************\
  Name:         ADT7483.h
  Created by:   Bryerton Shaw					Aug/20/2010			
  Modified by:  

  Contents:     This temperature sensor uses interrupt based SMB protocol.
  $Id$
\********************************************************************/

#ifndef _MSCB_DEVICE_TEMP_ADT7483_H
#define _MSCB_DEVICE_TEMP_ADT7483_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_TEMP_ADT7483_DISABLE
#warning "ADT7483 is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the ADT7483 driver to function!"
#endif

// Missing Offset and Limit Registers - Unused at this time

// Read Registers
#define ADT7483_RREG_LTEMP 	 0x00 
#define ADT7483_RREG_R1TEMPH 0x01
#define ADT7483_RREG_R1TEMPL 0x10
#define ADT7483_RREG_STATUS1 0x02
#define ADT7483_RREG_CONF1   0x03
#define ADT7483_RREG_CHSEL   0x04
#define ADT7483_RREG_R2TEMPH 0x30
#define ADT7483_RREG_R2TEMPL 0x33
#define ADT7483_RREG_STATUS2 0x23
#define ADT7483_RREG_CONF2   0x24
#define ADT7483_RREG_MFID    0xFE
#define ADT7483_RREG_REV     0xFF

// Write Registers
#define ADT7483_WREG_CONF1   0x09
#define ADT7483_WREG_CHSEL   0x0A
#define ADT7483_WREG_ONESHOT 0x0F
#define ADT7483_WREG_CONF2   0x24

#define ADT7483_CH_READ_ALL     0x00
#define ADT7483_CH_READ_LOCAL   0x01
#define ADT7483_CH_READ_REMOTE1 0x02
#define ADT7483_CH_READ_REMOTE2 0x03

#define ADT7483_CONV_TIME_16_S        0x0
#define ADT7483_CONV_TIME_8_S         0x1
#define ADT7483_CONV_TIME_4_S         0x2
#define ADT7483_CONV_TIME_2_S         0x3
#define ADT7483_CONV_TIME_1_S         0x4
#define ADT7483_CONV_TIME_500_MS      0x5
#define ADT7483_CONV_TIME_250_MS      0x6
#define ADT7483_CONV_TIME_125_MS      0x7
#define ADT7483_CONV_TIME_62_5_MS     0x8
#define ADT7483_CONV_TIME_31_25_MS    0x9
#define ADT7483_CONV_TIME_CONTINUOUS  0xA

#define ADT7483_CONV_TIME_DEFAULT ADT7483_CONV_TIME_125_MS // Internal default
#define ADT7483_EXTENDED_TEMP_RANGE   0x04
#define ADT7483_THERM2                0x10
//
//------------------------------------------------------------------------
void ADT7483_Init(void);

//
//------------------------------------------------------------------------
float ADT7483_ReadTemp(unsigned char addr, unsigned char channel);

//
//------------------------------------------------------------------------
unsigned char ADT7483_GetRegister(unsigned char addr, unsigned char reg_addr);

//
//------------------------------------------------------------------------
void ADT7483_WriteRegister(unsigned char addr, unsigned char reg_addr, unsigned char value); 


#endif // _MSCB_DEVICE_TEMP_ADT7483_H 