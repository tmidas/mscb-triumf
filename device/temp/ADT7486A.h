/**********************************************************************************\
  Name:         temp36_ADT7486A_tsensor.h
  Created by:   Brian Lee						     May/11/2007
  Modified by:  Bahman Sotoodian					  March/03/2008
  Modified by:  Noel Wu                        May/12/2008

  Contents:     Temperature sensor array (ADT7486A) handler for feb64 board

  $Id$
\**********************************************************************************/

#ifndef  _MSCB_DEVICE_TEMP_ADT7486A_H
#define  _MSCB_DEVICE_TEMP_ADT7486A_H

#include "../../common/mscbemb.h"

// --------------------------------------------------------
// ADT7486A related definitions and function prototypes
// --------------------------------------------------------

#define ADT7486A_LOCAL_CONVERSION_TIME  12 // Delay in ms
#define ADT7486A_REMOTE_CONVERSION_TIME 38 // Delay in ms

// Command Map (ADT7486A) - Including WL and RL
// Format is as follows:
// (1) CommandName 
// (2) writeLength
// (3) readLength
// (4) command
// (5) data (if applicable, defined by user), 
//                    				 		 cmd   wLen  rLen  MSB   LSB  
#define ADT7486A_CMD_Ping          0x00, 0x00, 0x00, 0x00, 0x00		
#define ADT7486A_CMD_GetIntTemp    0x00, 0x01, 0x02 		
#define ADT7486A_CMD_GetExt1Temp   0x01, 0x01, 0x02   	
#define ADT7486A_CMD_GetExt2Temp   0x02, 0x01, 0x02 		
#define ADT7486A_CMD_GetAllTemps   0x00, 0x01, 0x06						
#define ADT7486A_CMD_SetExt1Offset 0xE0, 0x03, 0x00
#define ADT7486A_CMD_GetExt1Offset 0xE0, 0x01, 0x02 		
#define ADT7486A_CMD_SetExt2Offset 0xE1, 0x03, 0x00
#define ADT7486A_CMD_GetExt2Offset 0xE1, 0x01, 0x02 
#define ADT7486A_CMD_ResetDevice   0xF6, 0x01, 0x00
#define ADT7486A_CMD_GetDIB_08byte 0xF7, 0x01, 0x08
#define ADT7486A_CMD_GetDIB_16byte 0xF7, 0x01, 0x10 


#define ADT7486A_CMD_INT	0x00
#define ADT7486A_CMD_EXT1	0x01
#define ADT7486A_CMD_EXT2	0x02

// Defining the type of error that we have encounted during the 
// temperature reading  
#define ADT7486A_SUCCESS       0
#define ADT7486A_DIODE_ERROR   -1.0f // If temperature returns this value, diode is open or shorted.

void ADT7486A_Init(unsigned char SST_LINE);

signed char ADT7486A_GetAllTemp(unsigned char SST_LINE, unsigned char addr, float temp[3]);
signed char ADT7486A_GetTemp(unsigned char SST_LINE, unsigned char addr, unsigned char cmd, float *temp);

#endif // _MSCB_DEVICE_TEMP_ADT7486A_H
