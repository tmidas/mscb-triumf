/********************************************************************\
  Name:         LTC2990TUI_adc.h
  Created by:   Bahman Sotoodian							
  Modified by:  Bryerton Shaw         Aug/19/2010	
  Date:         Apr/21/2008
                16bit deltasigma+PGA 8/16 channel
  Contents:     This ADC uses interrupt based SMB protocol.
  $Id$
\********************************************************************/

#ifndef _MSCB_DEVICE_LTC2990TUI_H
#define _MSCB_DEVICE_LTC2990TUI_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_TUI_LTC2990_DISABLE
#warning "LTC2990TUI is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the LTC2990TUI driver to function!"
#endif

#define LTC2990TUI_STATUS        0x00
#define LTC2990TUI_CONTROL       0x01
#define LTC2990TUI_TRIGGER       0x02
#define LTC2990TUI_INTTEMP       0x04   // MSB..LSB
#define LTC2990TUI_VOLT1         0x06   // MSB..LSB
#define LTC2990TUI_VOLT2         0x08   // MSB..LSB
#define LTC2990TUI_VOLT3         0x0A   // MSB..LSB
#define LTC2990TUI_VOLT4         0x0C   // MSB..LSB
#define LTC2990TUI_VCC           0x0E   // MSB..LSB
#define LTC2990TUI_CTL_DIFF12T2  0x05   // CM[2..0]:2, M[4..3]:1     

#define LTC2990TUI_TEMPCOEFF     0.0625
#define LTC2990TUI_SGL_ACQ       0x40
#define LTC2990TUI_RPT_ACQ       0x00
#define LTC2990TUI_SGL_V1V2TR2   0x58
#define LTC2990TUI_RPT_V1V2TR2   0x18
#define LTC2990TUI_TEMPOPEN         3
#define LTC2990TUI_TEMPSHORT        2
#define LTC2990TUI_INVALID          1
#define LTC2990TUI_VALIDRANGE       0

//
//------------------------------------------------------------------------
void LTC2990TUI_Init(void);
void LTC2990TUI_ConfigStart(unsigned char addr, unsigned char mode);
void LTC2990TUI_WriteRegister(unsigned char addr, unsigned char cmd, unsigned char value);
unsigned char LTC2990TUI_ReadTemperature(unsigned char addr, unsigned char reg, signed int *pResult);
unsigned char LTC2990TUI_ReadVoltage(unsigned char addr, unsigned char reg, signed int *pResult);
unsigned char LTC2990TUI_ReadRegister(unsigned char addr, unsigned char reg);
unsigned char LTC2990TUI_ReadConversion(unsigned char addr, unsigned char channel, signed long *pResult);

#endif // _MSCB_DEVICE_ADC_LTC2990TUI_H 