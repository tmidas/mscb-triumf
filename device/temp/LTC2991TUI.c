/********************************************************************\
  Name:         LTC2991TUI.c
  Contents:     Octal I2C Voltage, Current,and Temperature Monitor
  $Id$
\********************************************************************/

#include "LTC2991TUI.h"

//
//------------------------------------------------------------------------
void LTC2991TUI_Init(void) {
#ifdef MSCB_DEVICE_TUI_LTC2991_DISABLE
#else
	SMBus_Init(); // SMBus initialization
#endif // MSCB_DEVICE_TUI_LTC2991_DISABLE
}

//
//------------------------------------------------------------------------
/*
void LTC2991TUI_ConfigStart(unsigned char addr, unsigned char mode) {
#ifdef MSCB_DEVICE_TUI_LTC2991_DISABLE
  addr = 0;
#else
  unsigned char cmd;

  cmd = mode; 	 

  SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&cmd, 1);
  SMBus_SetRXBuffer(0, 0);
  SMBus_Start();
#endif // MSCB_DEVICE_TUI_LTC2991_DISABLE
}

//
//------------------------------------------------------------------------
unsigned char LTC2991TUI_ReadConversion(unsigned char addr, unsigned char channel, signed long *pResult) {
#ifdef MSCB_DEVICE_TUI_LTC2991_DISABLE
  addr = 0;
  channel = 0;
  *pResult = 0;
  return 0;
#else
  unsigned char cmd;
  signed int Result;
  unsigned char validRange = LTC2991TUI_VALIDRANGE;

  cmd = channel;

  Result = 0;
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&cmd, 1);
  SMBus_SetRXBuffer((unsigned char*)&Result, 2);
  SMBus_Start();

	if((Result & 0x8000) == 0x0000) { validRange = LTC2991TUI_INVALID;  }
  *pResult = Result;

	Result &= 0x7FFF;
  *pResult = Result;

  // Sign extend if necessary
  if((*pResult & 0x4000) == 0x4000) {
    *pResult |= 0xC000;
  }

	return validRange;
#endif // MSCB_DEVICE_TUI_LTC2991_DISABLE
}

//
//------------------------------------------------------------------------
unsigned char LTC2991TUI_ReadRegister(unsigned char addr, unsigned char reg) {
#ifdef MSCB_DEVICE_TUI_LTC2991_DISABLE
  addr = 0;
  reg = 0;
  return 0;
#else
  unsigned char result = 0xff;
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&reg, 1);
  SMBus_SetRXBuffer(&result, 1);
  SMBus_Start();

  return result;

#endif // MSCB_DEVICE_TUI_LTC2991_DISABLE
}
*/

//
//------------------------------------------------------------------------
unsigned char LTC2991TUI_ReadTemperature(unsigned char addr, unsigned char reg, signed int *pResult) {
#ifdef MSCB_DEVICE_TUI_LTC2991_DISABLE
  addr = 0;
  reg = 0;
  return 0;
#else
  unsigned char validRange = LTC2991TUI_VALIDRANGE;
  unsigned int result = 0xffff;
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&reg, 1);
  SMBus_SetRXBuffer((unsigned char *)&result, 2);
  SMBus_Start();

 	if((result & 0x8000) == 0x0000) { validRange = LTC2991TUI_INVALID;  }
 	if(result & 0x4000) { validRange = LTC2991TUI_TEMPSHORT;  }
 	if(result & 0x2000) { validRange = LTC2991TUI_TEMPOPEN;  }

  *pResult = result & 0x1FFF;  // 13 bits signed
  if(*pResult & 0x1000) { *pResult += 0xE000; }

  return (validRange);

#endif // MSCB_DEVICE_TUI_LTC2991_DISABLE
}

//
//------------------------------------------------------------------------
unsigned char LTC2991TUI_ReadVoltage(unsigned char addr, unsigned char reg, signed int *pResult) {
#ifdef MSCB_DEVICE_TUI_LTC2991_DISABLE
  addr = 0;
  reg = 0;
  return 0;
#else
  unsigned char validRange = LTC2991TUI_VALIDRANGE;
  signed int result;
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&reg, 1);
  SMBus_SetRXBuffer((unsigned char *)&result, 2);
  SMBus_Start();

 	if((result & 0x8000) == 0x0000) { validRange = LTC2991TUI_INVALID;  }

  // Check Data valid
  result &= 0x7FFF;  // 14 bits + sign bit
  *pResult = result;

  // Sign extend if necessary
  if(*pResult & 0x4000) { *pResult += 0x8000; }
 
  return (validRange);

#endif // MSCB_DEVICE_TUI_LTC2991_DISABLE
}

//
//------------------------------------------------------------------------
void LTC2991TUI_WriteRegister(unsigned char addr, unsigned char cmd, unsigned char value) {
#ifdef MSCB_DEVICE_TUI_LTC2991_DISABLE
  return;
#else
  unsigned char buffer[2];

  buffer[0] = cmd;
  buffer[1] = value;

	// Wait for the SMBus to clear
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(buffer, 2);
  SMBus_SetRXBuffer(0, 0);
  SMBus_Start();
#endif // MSCB_DEVICE_TUI_LTC2991_DISABLE
}
