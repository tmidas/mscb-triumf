/********************************************************************\
  Name:         LTC2991TUI.h
  Contents:     Octal I2C Voltage, Current,and Temperature Monitor
  $Id$
\********************************************************************/

#ifndef _MSCB_DEVICE_LTC2991TUI_H
#define _MSCB_DEVICE_LTC2991TUI_H

#include "../../common/mscbemb.h"

#ifdef MSCB_DEVICE_TUI_LTC2991_DISABLE
#warning "LTC2991TUI is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the LTC2991TUI driver to function!"
#endif

#define LTC2991TUI_STATUS        0x00
#define LTC2991TUI_TRIGGER       0x01
#define LTC2991TUI_V1234CTL      0x06   // MSB..LSB
#define LTC2991TUI_V5678CTL      0x07   // MSB..LSB
#define LTC2991TUI_PWM           0x08   // MSB..LSB
#define LTC2991TUI_VOLT1         0x0A   // MSB..LSB
#define LTC2991TUI_VOLT2         0x0C   // MSB..LSB
#define LTC2991TUI_VOLT3         0x0E   // MSB..LSB
#define LTC2991TUI_VOLT4         0x10   // MSB..LSB
#define LTC2991TUI_VOLT5         0x12   // MSB..LSB
#define LTC2991TUI_VOLT6         0x14   // MSB..LSB
#define LTC2991TUI_VOLT7         0x16   // MSB..LSB
#define LTC2991TUI_VOLT8         0x18   // MSB..LSB
#define LTC2991TUI_TINTERNAL     0x1A   // MSB..LSB
#define LTC2991TUI_VCC           0x1C   // MSB..LSB

#define LTC2991TUI_TEMPCOEFF     0.0625
#define LTC2991TUI_V3478FILTER   0x80
#define LTC2991TUI_T3478KELVIN   0x40
#define LTC2991TUI_V3478TEMP     0x20
#define LTC2991TUI_V3478MODE     0x10

#define LTC2991TUI_V1256FILTER   0x08
#define LTC2991TUI_T1256KELVIN   0x04
#define LTC2991TUI_V1256TEMP     0x02
#define LTC2991TUI_V1256MODE     0x01

#define LTC2991TUI_TEMPOPEN         3
#define LTC2991TUI_TEMPSHORT        2
#define LTC2991TUI_INVALID          1
#define LTC2991TUI_VALIDRANGE       0

//
//------------------------------------------------------------------------
void LTC2991TUI_Init(void);
void LTC2991TUI_ConfigStart(unsigned char addr, unsigned char mode);
void LTC2991TUI_WriteRegister(unsigned char addr, unsigned char cmd, unsigned char value);
unsigned char LTC2991TUI_ReadTemperature(unsigned char addr, unsigned char reg, signed int *pResult);
unsigned char LTC2991TUI_ReadVoltage(unsigned char addr, unsigned char reg, signed int *pResult);
unsigned char LTC2991TUI_ReadRegister(unsigned char addr, unsigned char reg);
unsigned char LTC2991TUI_ReadConversion(unsigned char addr, unsigned char channel, signed long *pResult);

#endif // _MSCB_DEVICE_ADC_LTC2991TUI_H 