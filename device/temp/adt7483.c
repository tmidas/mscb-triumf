/********************************************************************\

  Name:        ADT7483.c
  Created by:	 Bryerton Shaw                Aug/19/2010
  Modified by: 

  Contents: ADT7483 Temperature sensor user interface
  				
  $Id$

\********************************************************************/

#include "ADT7483.h"

unsigned char ADT7483_GetRegister(unsigned char addr, unsigned char reg_addr) {
#ifdef MSCB_DEVICE_TEMP_ADT7483_DISABLE
#else
  unsigned char value;

	// Wait for the SMBus to clear
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&reg_addr, 1);
  SMBus_SetRXBuffer(&value, 1);
  SMBus_Start();

  return value;
#endif // MSCB_DEVICE_TEMP_ADT7483_DISABLE
}

void ADT7483_WriteRegister(unsigned char addr, unsigned char reg_addr, unsigned char value) {
#ifdef MSCB_DEVICE_TEMP_ADT7483_DISABLE
#else
  unsigned char buffer[2];

  buffer[0] = reg_addr;
  buffer[1] = value;

	// Wait for the SMBus to clear
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(buffer, 2);
  SMBus_SetRXBuffer(0, 1);
  SMBus_Start();
#endif // MSCB_DEVICE_TEMP_ADT7483_DISABLE
}

//
//------------------------------------------------------------------------
void ADT7483_Init(void) {
#ifdef MSCB_DEVICE_TEMP_ADT7483_DISABLE
#else
	SMBus_Init(); // SMBus initialization
#endif // MSCB_DEVICE_TEMP_ADT7483_DISABLE
}

//
//------------------------------------------------------------------------
float ADT7483_ReadTemp(unsigned char addr, unsigned char channel) {
#ifdef MSCB_DEVICE_TEMP_ADT7483_DISABLE
#else
  unsigned char status;
  unsigned int  temp;
  union f convert;

  switch(channel) {
    case ADT7483_CH_READ_LOCAL:
      status = ADT7483_GetRegister(addr, ADT7483_RREG_CONF1);
      if (status & ADT7483_EXTENDED_TEMP_RANGE) { 
      return (ADT7483_GetRegister(addr, ADT7483_RREG_LTEMP) - 64.);
     } else {
      return ADT7483_GetRegister(addr, ADT7483_RREG_LTEMP);
     }
    case ADT7483_CH_READ_REMOTE1:
      status = ADT7483_GetRegister(addr, ADT7483_RREG_STATUS1);
      if(status & 0x4) {
        temp = 0xFFFF;
      } else {
        temp  = ADT7483_GetRegister(addr, ADT7483_RREG_R1TEMPL); // Must be read first
        temp |= ADT7483_GetRegister(addr, ADT7483_RREG_R1TEMPH) << 8;
      }
      break;

    case ADT7483_CH_READ_REMOTE2:
      status = ADT7483_GetRegister(addr, ADT7483_RREG_STATUS2);
      if(status & 0x4 ) { 
        temp = 0xFFFF; 
      } else {
        temp  = ADT7483_GetRegister(addr, ADT7483_RREG_R2TEMPL); // Must be read first
        temp |= ADT7483_GetRegister(addr, ADT7483_RREG_R2TEMPH) << 8;
      }
      break;
   
    default:
      temp = 0xFFFF; // Just fall thru
  }

  if(temp != 0xFFFF) {
    convert.f = (float)(temp >> 8) + ((float)((temp & 0xC0) >> 6) * 0.25f);
    status = ADT7483_GetRegister(addr, ADT7483_RREG_CONF1);
    if (status & ADT7483_EXTENDED_TEMP_RANGE) convert.f -= 64.; 
  } else {
    convert.f = -1.0f;
  }

  return convert.f;
#endif // MSCB_DEVICE_TEMP_ADT7483_DISABLE
}
