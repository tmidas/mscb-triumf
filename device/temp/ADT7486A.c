/**********************************************************************************\
Name: 				ADT7486A_tsensor.c
Created by: 	Brian Lee 							 May/11/2007
Modified By:	Bahman Sotoodian					 March/03/2008
Modified by:   Noel Wu                        May/12/2008
Contents: 		Temperature sensor array (ADT7486A) handler

$Id$
\**********************************************************************************/

// --------------------------------------------------------
//	Include files
// --------------------------------------------------------

#include "ADT7486A.h"

//
//----------------------------------------------------------------------------------
/**
Initialize the SST communication protocol
*/
void ADT7486A_Init(unsigned char SST_LINE) {	
	SST_Init(SST_LINE);
}

signed char ADT7486A_GetAllTemp(unsigned char SST_LINE, unsigned char addr, float temp[3]) {
	signed char status;
	signed short xdata temp_raw[3];
	unsigned char i;

	status = SST_Cmd(SST_LINE, addr, ADT7486A_CMD_GetAllTemps, 0, (unsigned char*)temp_raw);

	for(i = 0; i < 3; ++i) {
		if(temp_raw[i] != 0x8000) {
			// HACK: Put in place because if you allow interrupts while SST runs
			// It will occasionally return zero instead of a valid temperature. 
			if(temp_raw[i] == 0) {
				status = SST_FCS_RERROR;
			}
			// END HACK
			temp[i] = (float)temp_raw[i] / 64.0f;
		} else {
			temp[i] = ADT7486A_DIODE_ERROR;
		}
	}

	return status;
}
/*
signed char ADT7486A_GetTemp(unsigned char SST_LINE, unsigned char addr, unsigned char cmd, float *temp) {
	signed char status;
	signed short xdata temp_raw;

	status = SST_Cmd(SST_LINE, addr, cmd, 0x01, 0x02, 0, (unsigned char*)&temp_raw);

	if(temp_raw != 0x8000) {
		// HACK: Put in place because if you allow interrupts while SST runs
		// It will occasionally return zero instead of a valid temperature. 
		if(temp_raw == 0) {
			status = SST_FCS_RERROR;
		}
		// END HACK
		*temp = (float)temp_raw / 64.0f;
	} else {
		*temp = ADT7486A_DIODE_ERROR;
	}

	return status;
}
*/