/**********************************************************************************\
  Name:         LTC2655.h
  Created by:   
  Modified by:  


  Contents:     Quad DAC 16/12bits

  $Id$
\**********************************************************************************/

#ifndef _MSCB_DEVICE_DAC_LTC2655_H
#define _MSCB_DEVICE_DAC_LTC2655_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_DAC_LTC2655_DISABLE
#warning "LTC2655 is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the LTC2655 driver to function!"
#endif

//Defining each DAC address
#define LTC2655_DAC_A	  0x00
#define LTC2655_DAC_B	  0x01
#define LTC2655_DAC_C	  0x02
#define LTC2655_DAC_D   0x03
#define LTC2655_ALL_DAC 0x0f

//Defining the DAC commands
#define LTC2655_WREG_n					  0x00
#define LTC2655_UPDATE_REG_n 		  0x10
#define LTC2655_WREG_n_UPDATE_All	0x20
#define LTC2655_WREG_n_UPDATE_n		0x30
#define LTC2655_POWER_DOWN_n			0x40
#define LTC2655_NO_OPERATION			0xf0

void LTC2655_Init(void);
void LTC2655_Write(unsigned char addr, unsigned char cmd, unsigned int dac_value);

#endif // _MSCB_DEVICE_DAC_LTC2655_H
