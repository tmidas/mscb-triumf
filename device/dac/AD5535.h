/**********************************************************************************\
  Name:         AD5535.h
  Created by:   
  Modified by:  

  Contents:     32 channels DAC 14bits

  $Id$
\**********************************************************************************/

#ifndef _MSCB_DEVICE_DAC_AD5535_H
#define _MSCB_DEVICE_DAC_AD5535_H

#include "../../common/mscbemb.h"

// AD5535 registers
// none

#ifdef MCSB_DEVICE_DAC_AD5535_DISABLE
#warning "AD5535 is disabled."
#endif 

void AD5535_Init(void);
void AD5535_Write(unsigned char channel, unsigned long int value);

#endif // _MSCB_DEVICE_DAC_AD5535_H
