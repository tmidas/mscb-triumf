/********************************************************************\

  Name:        LTC2605CGN.c
  Created by:	 Bahman Sotoodian							Apr/18/2008
  Modified by: Bryerton Shaw                Aug/19/2010
               Rail-to-Rail 16bit Octal DAC
  Contents:    LTC2605CGN DAC user interface
  				
  $Id$

\********************************************************************/

#include "LTC2605CGN.h"

//
//------------------------------------------------------------------------
void LTC2605_Init(void) {
#ifdef MCSB_DEVICE_DAC_LTC2605_DISABLE
#else
	SMBus_Init(); // SMBus initialization
#endif
}

//
//------------------------------------------------------------------------
void LTC2605_Write(unsigned char addr, unsigned char cmd, unsigned int dac_value) {
#ifdef MCSB_DEVICE_DAC_LTC2605_DISABLE
#else
  unsigned char buffer[3];

  buffer[0] = cmd;
  buffer[1] = (unsigned char)(dac_value >> 8);
  buffer[2] = (unsigned char)(dac_value & 0x00FF);

	// Wait for the SMBus to clear
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(buffer, 3);
  SMBus_SetRXBuffer(0, 0);
  SMBus_Start();
#endif
}
