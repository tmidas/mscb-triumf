/**********************************************************************************\
  Name:         LTC2605CGN.h
  Created by:   Brian Lee						     May/11/2007
  Modified by:  Bryerton Shaw					  August/19/2010
                Rail-to-Rail 16bit Octal DAC
  Contents:     This DAC uses interrupt based SMB protocol.

  $Id$
\**********************************************************************************/

#ifndef _MSCB_DEVICE_DAC_LTC2605_H
#define _MSCB_DEVICE_DAC_LTC2605_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_DAC_LTC2605_DISABLE
#warning "LTC2605 is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the LTC2605 driver to function!"
#endif

//Defining each DAC address
#define LTC2605_DAC_A	  0x00
#define LTC2605_DAC_B	  0x01
#define LTC2605_DAC_C	  0x02
#define LTC2605_DAC_D   0x03
#define LTC2605_DAC_E	  0x04
#define LTC2605_DAC_F	  0x05
#define LTC2605_DAC_G   0x06
#define LTC2605_DAC_H 	0x07
#define LTC2605_ALL_DAC 0x0f

//Defining the DAC commands
#define LTC2605_WREG_n					  0x00
#define LTC2605_UPDATE_REG_n 		  0x10
#define LTC2605_WREG_n_UPDATE_All	0x20
#define LTC2605_WREG_n_UPDATE_n		0x30
#define LTC2605_POWER_DOWN_n			0x40
#define LTC2605_NO_OPERATION			0xf0

void LTC2605_Init(void);
void LTC2605_Write(unsigned char addr, unsigned char cmd, unsigned int dac_value);

#endif // _MSCB_DEVICE_DAC_LTC2605_H