/**********************************************************************************\
  Name:         AD5383.h
  Created by:   
  Modified by:  


  Contents:     32 channels DAC 12bits

  $Id$
\**********************************************************************************/

#ifndef _MSCB_DEVICE_DAC_AD5383_H
#define _MSCB_DEVICE_DAC_AD5383_H

#include "../../common/mscbemb.h"

// AD5383 registers
#define AD5383_SFR       0
#define AD5383_GAIN      1
#define AD5383_OFFSET    2
#define AD5383_DATA      3

// AD5383 SFR Addresses
#define AD5283_NOP         0x0
#define AD5283_CLRCODE     0x1
#define AD5283_SOFTCLR     0x2
#define AD5283_SOFTPD      0x8
#define AD5283_SOFTPOWERUP 0x9
#define AD5283_CR          0xC
#define AD5283_CHMON       0xA
#define AD5283_SOFTRESET   0xF

#ifdef MCSB_DEVICE_DAC_AD5383_DISABLE
#warning "AD5383 is disabled."
#endif 

void AD5383_Init(void);
void AD5383_Write(unsigned char addr, unsigned char cmd, unsigned int dac_value);

#endif // _MSCB_DEVICE_DAC_AD5383_H
