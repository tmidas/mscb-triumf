/********************************************************************\

  Name:         LTC2655.c
  Created by:	 
  Modified by: 

  Contents: LTC2655 Quad DAC 16/12bits user interface
  				
  $Id$

\********************************************************************/

#include "LTC2655.h"

//
//------------------------------------------------------------------------
void LTC2655_Init(void) {
#ifdef MCSB_DEVICE_DAC_LTC2655_DISABLE
#else
	SMBus_Init(); // SMBus initialization
#endif
}

//
//------------------------------------------------------------------------
void LTC2655_Write(unsigned char addr, unsigned char cmd, unsigned int dac_value) {
#ifdef MCSB_DEVICE_DAC_LTC2655_DISABLE
#else
  unsigned char buffer[3];

  buffer[0] = cmd;
  buffer[1] = (unsigned char)(dac_value >> 8);
  buffer[2] = (unsigned char)(dac_value & 0xFF);

	// Wait for the SMBus to clear
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(buffer, 3);
  SMBus_SetRXBuffer(0, 0);
  SMBus_Start();
#endif
}
