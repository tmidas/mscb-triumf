/**********************************************************************************\
  Name:         LTC2606CGN.h
  Created by:   Brian Lee						     May/11/2007
  Modified by:  Bryerton Shaw					  August/19/2010
                Rail-to-Rail 16bit single DAC 
  Contents:     This DAC uses interrupt based SMB protocol.

  $Id$
\**********************************************************************************/

#ifndef _MSCB_DEVICE_DAC_LTC2606_H
#define _MSCB_DEVICE_DAC_LTC2606_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_DAC_LTC2606_DISABLE
#warning "LTC2606 is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the LTC2606 driver to function!"
#endif

//Defining each DAC address
#define LTC2606_DAC  	  0x00

#define LTC2606_ALL_DAC 0x0f

//Defining the DAC commands
#define LTC2606_WREG_n				0x00
#define LTC2606_UPDATE_REG_n 		0x10
#define LTC2606_WREG_n_UPDATE_All	0x20
#define LTC2606_WREG_n_UPDATE_n		0x30
#define LTC2606_POWER_DOWN_n		0x40
#define LTC2606_NO_OPERATION		0xf0

void LTC2606_Init(void);
void LTC2606_Write(unsigned char addr, unsigned char cmd, unsigned int dac_value);

#endif // _MSCB_DEVICE_DAC_LTC2606_H
