/********************************************************************\
  Name:         AD5383.c
  Created by:	 
  Modified by: 

  Contents: AD5383 32 DAC 12bits SPI interface
  				
  $Id$
\********************************************************************/

#include "AD5383.h"
#include "../../common/mscbemb.h"

sbit SPI_CLK  = MSCB_SPI_CLK;
sbit SPI_MISO = MSCB_SPI_MISO;
sbit SPI_MOSI = MSCB_SPI_MOSI;

//
//------------------------------------------------------------------------
void AD5383_Init(void) {
#ifdef MCSB_DEVICE_DAC_AD5383_DISABLE
#else
	SPI16_Init(); // SPI Bus initialization
  SPI_MISO = 1;
#endif
}

//
//------------------------------------------------------------------------
void AD5383_write(unsigned char reg, unsigned char channel, unsigned int value) {
#ifdef MCSB_DEVICE_DAC_AD5383_DISABLE
#else
  SPI_CLK = 1;
//  DAC_SYNCn = 0;

  SPI16_Write(0x0, 3);     // 3-bit command... write to A regs (b000)
  SPI16_Write(channel, 5); // 5-bit channel address
  SPI16_Write(reg, 2);     // 2-bit register select
  SPI16_Write(value, 12);  // 12-bit data word
  SPI16_Write(0x0, 2);     // 2-bit trailer (b00)

//  DAC_SYNCn = 1;
  delay_us(SPI_DELAY);
#endif
}

