/********************************************************************\
  Name:         AD5535.c
  Created by:	 
  Modified by: 

  Contents: AD5535 32 DAC 14bits SPI interface
  				
  $Id$
\********************************************************************/

#include "AD5535.h"
#include "../../common/protocol/spi16.h"
#include "../../common/mscbemb.h"

sbit SPI_CLK  = MSCB_SPI_CLK;
sbit SPI_MOSI = MSCB_SPI_MOSI;
sbit SPI_SYNC = MSCB_SPI_SYNC;

//
//------------------------------------------------------------------------
void AD5535_Init(void) {
#ifdef MCSB_DEVICE_DAC_AD5535_DISABLE
#else
	SPI16_Init(); // SPI Bus initialization
  SPI_SYNC = 1;
  delay_us(100); 
  SPI_SYNC = 0;
#endif
}

//
//------------------------------------------------------------------------
void AD5535_Write(unsigned char channel, unsigned long int value) {
#ifdef MCSB_DEVICE_DAC_AD5535_DISABLE
#else
  unsigned long int lch, ldata;
  
  lch = channel;
  SPI_SYNC = 1;
  ldata = (lch << 14) | (value & 0x3FFF); 
  SPI32_Write(ldata, 19);   // directly 19 bit channel/data
  delay_us(100);
  SPI_SYNC = 0;
#endif
}

