/**********************************************************************************\
  Name:         LTC2609CGN.h
  Created by:   Brian Lee						     May/11/2007
  Modified by:  Bryerton Shaw					  August/19/2010


  Contents:     Quad DAC 16bits

  $Id$
\**********************************************************************************/

#ifndef _MSCB_DEVICE_DAC_LTC2609_H
#define _MSCB_DEVICE_DAC_LTC2609_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_DAC_LTC2609_DISABLE
#warning "LTC2609 is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the LTC2609 driver to function!"
#endif

//Defining each DAC address
#define LTC2609_DAC_A	  0x00
#define LTC2609_DAC_B	  0x01
#define LTC2609_DAC_C	  0x02
#define LTC2609_DAC_D   0x03
#define LTC2609_ALL_DAC 0x0f

//Defining the DAC commands
#define LTC2609_WREG_n					  0x00
#define LTC2609_UPDATE_REG_n 		  0x10
#define LTC2609_WREG_n_UPDATE_All	0x20
#define LTC2609_WREG_n_UPDATE_n		0x30
#define LTC2609_POWER_DOWN_n			0x40
#define LTC2609_NO_OPERATION			0xf0

void LTC2609_Init(void);
void LTC2609_Write(unsigned char addr, unsigned char cmd, unsigned int dac_value);

#endif // _MSCB_DEVICE_DAC_LTC2609_H
