/********************************************************************\
  Name:         LTC2499.h
  Created by:   Bahman Sotoodian							
  Modified by:  Bryerton Shaw         Aug/19/2010	
  Date:         Apr/21/2008
                24bit deltasigma+PGA 8/16 channel
  Contents:     This ADC uses interrupt based SMB protocol.
  $Id$
\********************************************************************/

#ifndef _MSCB_DEVICE_ADC_LTC2499_H
#define _MSCB_DEVICE_ADC_LTC2499_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_ADC_LTC2499_DISABLE
#warning "LTC2499 is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the LTC2499 driver to function!"
#endif

#define LTC2499_ADDR_GLOBAL 0x77 // Use this to sync multiple LTC2499s

#define LTC2499_CMD_IGNORE 	0x00 // Don't care/ignore following data
#define LTC2499_CMD_ENABLE	0xA0 // Select an Input Channel command
#define LTC2499_CONF_ENABLE	0x80 // Select an Configuration command
#define LTC2499_CONF_F5060	0x00 // Select Freq Filter
#define LTC2499_CONF_SPDAC  0x00 // Select Auto-Calibration (not 2x speed)
#define LTC2499_CONF_SPEED  0x04 // Select 2x speed (Gain selection is changed)

#define LTC2499_GAIN1   0x00 // Select Gain x1
#define LTC2499_GAIN4   0x01 // Select Gain x4
#define LTC2499_GAIN8   0x02 // Select Gain x8
#define LTC2499_GAIN16  0x03 // Select Gain x16
#define LTC2499_GAIN32  0x04 // Select Gain x32
#define LTC2499_GAIN64  0x05 // Select Gain x64
#define LTC2499_GAIN128 0x06 // Select Gain x128
#define LTC2499_GAIN264 0x07 // Select Gain x264

#define LTC2499_CMD_SGL			0x10 // Single Ended (non-differential)
#define LTC2499_CMD_DIF			0x00 // Differential Ended
#define LTC2499_CMD_ODD     0x08 // Odd/Signed

#define LTC2499_OVERRANGE   1
#define LTC2499_UNDERRANGE  2
#define LTC2499_VALIDRANGE  0

//
//------------------------------------------------------------------------
void LTC2499_Init(void);

//
//------------------------------------------------------------------------
void LTC2499_SetMode(unsigned char mode);

//
//------------------------------------------------------------------------
void LTC2499_StartConversion(unsigned char addr, unsigned char mode, unsigned char cmd2, unsigned char channel);

//
//------------------------------------------------------------------------
unsigned char LTC2499_ReadConversion(unsigned char addr, unsigned char mode , unsigned char cmd2, unsigned char channel, signed long *pResult);

#endif // _MSCB_DEVICE_ADC_LTC2499_H 