/********************************************************************\
  Name:         ADM1176.c
  Contents:     Hot Swap Controller and I2C Power Monitor
  $Id$
\********************************************************************/

#include <string.h>
#include "ADM1176.h"

//
//------------------------------------------------------------------------
void ADM1176_Init(void) {
#ifdef MSCB_DEVICE_ADC_ADM1176_DISABLE
#else
	SMBus_Init(); // SMBus initialization
#endif // MSCB_DEVICE_ADC_ADM1176_DISABLE
}

//
//------------------------------------------------------------------------
unsigned char ADM1176_ReadStatus(unsigned char addr, unsigned char* status) {
#ifdef MSCB_DEVICE_ADC_ADM1176_DISABLE
  addr = 0;
  return 0;
#else
  unsigned char Cmd = 0x40;
  
 	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&Cmd, 1);
  SMBus_SetRXBuffer(status, 1);
  return SMBus_Start();

#endif // MSCB_DEVICE_ADC_ADM1176_DISABLE
}

//
//------------------------------------------------------------------------
unsigned char ADM1176_CombineConversion(unsigned char addr, unsigned char cmd, unsigned char *pResult) {
  unsigned char retVal;
#ifdef MSCB_DEVICE_ADC_ADM1176_DISABLE
  addr = 0;
  cmd = 0;
  retVal = 0;
  return 0;
#else
  unsigned char Result[3] = {0};
  
	SMBus_Wait();
  SMBus_SetACKPoll(1, 3);          // Retry until conversion is done!
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&cmd, 1);
  SMBus_SetRXBuffer(&Result, 3);
  retVal = SMBus_Start();

  memcpy(pResult, Result, sizeof(Result));

	return retVal;
#endif // MSCB_DEVICE_ADC_ADM1176_DISABLE
}

/*

//
//------------------------------------------------------------------------
unsigned char ADM1176_ReadConversion(unsigned char addr, unsigned char *pResult) {
#ifdef MSCB_DEVICE_ADC_ADM1176_DISABLE
  addr = 0;
  return 0;
#else
  unsigned char Result[3] = {0};
  
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetRXBuffer(&Result, 3);
  SMBus_Start();

  memcpy(pResult, Result, sizeof(Result));

	return 0;
#endif // MSCB_DEVICE_ADC_ADM1176_DISABLE
}

//
//------------------------------------------------------------------------
void ADM1176_WriteRegister(unsigned char addr, unsigned char cmd) {
#ifdef MSCB_DEVICE_ADC_ADM1176_DISABLE
  return;
#else

	// Wait for the SMBus to clear
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&cmd, 1);
  SMBus_Start();
  return;
#endif
}

//
//------------------------------------------------------------------------
void ADM1176_WriteERegister(unsigned char addr, unsigned char onoff) {
#ifdef MSCB_DEVICE_ADC_ADM1176_DISABLE
  return;
#else
  unsigned char ecmd[2];

  ecmd[0] = 0x83;
  ecmd[1] = onoff;
	// Wait for the SMBus to clear
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&ecmd, 2);
  SMBus_Start();
  return;
#endif // MSCB_DEVICE_ADC_ADM1176_DISABLE
}

*/