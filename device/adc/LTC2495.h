/********************************************************************\
  Name:         LTC2495_adc.h
  Created by:   Bahman Sotoodian							
  Modified by:  Bryerton Shaw         Aug/19/2010	
  Date:         Apr/21/2008
                16bit deltasigma+PGA 8/16 channel
  Contents:     This ADC uses interrupt based SMB protocol.
  $Id$
\********************************************************************/

#ifndef _MSCB_DEVICE_ADC_LTC2495_H
#define _MSCB_DEVICE_ADC_LTC2495_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_ADC_LTC2495_DISABLE
#warning "LTC2495 is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the LTC2495 driver to function!"
#endif

#define LTC2495_ADDR_GLOBAL 0x77 // Use this to sync multiple LTC2495s

#define LTC2495_CMD_IGNORE 	0x00 // Don't care/ignore following data
#define LTC2495_CMD_ENABLE	0xA0 // Select an Input Channel command
#define LTC2495_CONF_ENABLE	0x80 // Select an Configuration command
#define LTC2495_CONF_F5060	0x00 // Select Freq Filter
#define LTC2495_CONF_SPDAC  0x00 // Select Auto-Calibration (not 2x speed)
#define LTC2495_CONF_SPEED  0x04 // Select 2x speed (Gain selection is changed)

#define LTC2495_GAIN1   0x00 // Select Gain x1
#define LTC2495_GAIN4   0x01 // Select Gain x4
#define LTC2495_GAIN8   0x02 // Select Gain x8
#define LTC2495_GAIN16  0x03 // Select Gain x16
#define LTC2495_GAIN32  0x04 // Select Gain x32
#define LTC2495_GAIN64  0x05 // Select Gain x64
#define LTC2495_GAIN128 0x06 // Select Gain x128
#define LTC2495_GAIN264 0x07 // Select Gain x264

#define LTC2495_CMD_SGL			0x10 // Single Ended (non-differential)
#define LTC2495_CMD_DIF			0x00 // Differential Ended
#define LTC2495_CMD_ODD     0x08 // Odd/Signed

#define LTC2495_OVERRANGE   1
#define LTC2495_UNDERRANGE  2
#define LTC2495_VALIDRANGE  0

//
//------------------------------------------------------------------------
void LTC2495_Init(void);

//
//------------------------------------------------------------------------
void LTC2495_SetMode(unsigned char mode);

//
//------------------------------------------------------------------------
void LTC2495_StartConversion(unsigned char addr, unsigned char mode, unsigned char cmd2, unsigned char channel);

//
//------------------------------------------------------------------------
unsigned char LTC2495_ReadConversion(unsigned char addr, unsigned char mode , unsigned char cmd2, unsigned char channel, signed long *pResult);

#endif // _MSCB_DEVICE_ADC_LTC2495_H 