/** 
 * $URL: $
 * $Id$
 *
 * @file 
 * @ingroup MSCB_DEVICE_ADC_ADS1225
 * @author Bryerton Shaw
 *
 * @defgroup MSCB_DEVICE_ADC_ADS1225 ADS1225
 * @ingroup MSCB_DEVICE_ADC
 * @brief Texas Instruments 24-bit ADC	
 * @details This device is setup to use its own SPI routines
 * @todo Migrate code to use MSCB software/hardware SPI routines
 * @{
 */

#include "ADS1225.h"

sbit SCLK		  = ADS1225_SCLK_PIN;
sbit START		= ADS1225_START_PIN;
sbit DOUT		  = ADS1225_DOUT_PIN;
sbit TEMP_EN	= ADS1225_TEMP_EN_PIN;
sbit BUFF_EN	= ADS1225_BUFF_EN_PIN;
sbit MODE		  = ADS1225_MODE_PIN;

//
//--------------------------------------------
static void _SPI_Clock(unsigned char count) {
	unsigned char i;

	for(i=0; i < count; ++i) {
		SCLK = 1;
		delay_us(10);
		SCLK = 0;
		delay_us(10);
	}
}

//
//--------------------------------------------
static unsigned char _SPI_ReadByte(void) {
	signed char i;
	unsigned char value;

	for(i = 7, value = 0; i >= 0; --i) {
		SCLK = 1;
		delay_us(1);
		value |= (unsigned char)(DOUT) << i;
		SCLK = 0;
		delay_us(1);
	}

	return value;
}

//
//--------------------------------------------
void ADS1225_Init(void) {
	SCLK  = 0;
	START = 0;
	BUFF_EN = 0;
	TEMP_EN = 0;
	MODE = 0;

	while(!DOUT);
	while(DOUT);

	_SPI_ReadByte();
	_SPI_ReadByte();
	_SPI_ReadByte();

	// Force DOUT to stay high until next conversion is finished
	_SPI_Clock(1);		
}

//
//--------------------------------------------
signed long ADS1225_SingleConversion(void) {
	signed long value;
	// Start Conversion

  //	BUFF_EN = 1;

	START = 1;
	delay_us(20);
	START = 0;
	delay_us(20);

	// Wait for conversion to finish
	while(DOUT);

  //	BUFF_EN = 0;

	delay_us(1);

	// Clock in Data
	value = (signed long)_SPI_ReadByte() << 16;

	// Sign extend if necessary
	if(value & 0x00800000) { value |= 0xFF000000; }

	value |= (signed long)_SPI_ReadByte() << 8;
	value |= (signed long)_SPI_ReadByte();	

	// Force DOUT to stay high until next conversion is finished
	_SPI_Clock(1);

	return value;
}

//
//--------------------------------------------
signed long ADS1225_ReadTemp(void) {
	signed long value;
  
  // Enable Temperature Sensor
  TEMP_EN = 1;
  
 	// Wait for conversion to finish
	delay_us(20);

	START = 1;
	delay_us(20);
	START = 0;
	delay_us(20);

	// Wait for conversion to finish
	while(DOUT);

	delay_us(1);

	// Clock in Data
	value = (signed long)_SPI_ReadByte() << 16;

	// Sign extend if necessary
	if(value & 0x00800000) { value |= 0xFF000000; }

	value |= (signed long)_SPI_ReadByte() << 8;
	value |= (signed long)_SPI_ReadByte();	

	// Force DOUT to stay high until next conversion is finished
	_SPI_Clock(1);

  TEMP_EN = 0;
	delay_us(20);

	return value;

}

#ifdef EXPERIMENTAL

// UNTESTED
//
//--------------------------------------------
void ADS1225_SelfCalibrate(void) {
	// Start Conversion
	START = 1;

	// Wait for conversion to finish
	while(DOUT);

	// Discard data
	_SPI_ReadByte();
	_SPI_ReadByte();
	_SPI_ReadByte();
		
	// Activate Self-Calibration
	_SPI_Clock(2);

	// Wait for Self-Calibration to finish
	while(DOUT);		
}


// UNTESTED
//
//--------------------------------------------
signed long ADS1225_PollConversion(void) {
	signed long value;

	// Start Conversion
	START = 1;
	delay_us(20);

	// Wait for conversion to finish
	while(DOUT);

	// Clock in Data
	value = (signed long)_SPI_ReadByte() << 16;

	// Sign extend if necessary
	if(value & 0x00800000) { value |= 0xFF000000; }

	value |= (signed long)_SPI_ReadByte() << 8;
	value |= (signed long)_SPI_ReadByte();	

	// Force DOUT to stay high until next conversion is finished
	_SPI_Clock(1);

	return value;
}

//
//--------------------------------------------
void ADS1225_SetMode(unsigned char value) {
	if((value == ADS1225_MODE_HIGHRES) || (value == ADS1225_MODE_HIGHSPEED)) {
		MODE = value;
	} 
}


#endif // EXPERIMENTAL

/** 
 * @}
 */