/********************************************************************\
  Name:         LTC2495_adc.c
  Created by:   Bahman Sotoodian								
  Date:         Apr/21/2008
                16bit deltasigma+PGA 8/16 channel
  Contents:     8ch 16bit ADC using interrupt based SMB protocol.
  $Id$
\********************************************************************/

#include "LTC2495.h"

//
//------------------------------------------------------------------------
void LTC2495_Init(void) {
#ifdef MCSB_DEVICE_ADC_LTC2495_DISABLE
#else
	SMBus_Init(); // SMBus initialization
#endif // MCSB_DEVICE_ADC_LTC2495_DISABLE
}

//
//------------------------------------------------------------------------
void LTC2495_StartConversion(unsigned char addr, unsigned char mode, unsigned char conv_conf, unsigned char channel) {
#ifdef MCSB_DEVICE_ADC_LTC2495_DISABLE
  addr = 0;
  channel = 0;
#else
  unsigned char cmd[2];
  if (mode &	LTC2495_CMD_SGL) {  // != 0 -> SGL
    cmd[0] = LTC2495_CMD_ENABLE | mode | (channel >> 1);
    cmd[1] = LTC2495_CONF_ENABLE | conv_conf; 	 
    if(channel & 0x01) {  // Add odd channel
       cmd[0] |= 0x08;
    }
  } else {  // == 0  -> Differential 
    cmd[0] = LTC2495_CMD_ENABLE | mode | (channel);
    cmd[1] = LTC2495_CONF_ENABLE | conv_conf; 	 
  }

	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&cmd, 2);
  SMBus_SetRXBuffer(0, 0);
  SMBus_Start();
#endif // MCSB_DEVICE_ADC_LTC2495_DISABLE
}

//
//------------------------------------------------------------------------
unsigned char LTC2495_ReadConversion(unsigned char addr,  unsigned char mode , unsigned char conv_conf, unsigned char channel, signed long *pResult) {
#ifdef MCSB_DEVICE_ADC_LTC2495_DISABLE
  addr = 0;
  channel = 0;
  *pResult = 0;
  return 0;
#else
  unsigned char cmd[2];
  unsigned char validRange = LTC2495_VALIDRANGE;
  signed long Result;

  if (mode &	LTC2495_CMD_SGL) {  // != 0 -> SGL
    cmd[0] = LTC2495_CMD_ENABLE | mode | (channel >> 1);
    cmd[1] = LTC2495_CONF_ENABLE | conv_conf; 	 
    if(channel & 0x01) {  // Add odd channel
       cmd[0] |= 0x08;
    }
  } else {  // == 0  -> Differential 
    cmd[0] = LTC2495_CMD_ENABLE | mode | (channel);
    cmd[1] = LTC2495_CONF_ENABLE | conv_conf; 	 
  }

  Result = 0;
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&cmd, 2);
  SMBus_SetRXBuffer((unsigned char*)&Result, 3);
  SMBus_Start();

	if((Result & 0xC0000000) == 0xC0000000) { validRange = LTC2495_OVERRANGE;  }
  if((Result & 0xC0000000) == 0x00000000) { validRange = LTC2495_UNDERRANGE; }
	
  // Shift results so we are dealing with real values
  // The last 6 bits are always zero, so they can be truncated
  // pResult is 32 bits long, so we need to truncate 8 + 6=14 
	Result = (Result >> 14) & 0x1FFFF;
  *pResult = Result;

  // Sign extend if necessary
  if((*pResult & 0x10000) == 0x10000) {
    *pResult |= 0xFFFF0000;
  }

	return validRange;
#endif // MCSB_DEVICE_ADC_LTC2495_DISABLE
}
