/********************************************************************\
  Name:         AD7991.h
  Contents:     24-Bit 2-/4-Channel
                DS ADC with Easy Drive Input Current
                Cancellation and I2C Interface.
  $Id$
\********************************************************************/

#ifndef _MSCB_DEVICE_ADC_AD7991_H
#define _MSCB_DEVICE_ADC_AD7991_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_ADC_AD7991_DISABLE
#warning "AD7991 is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the AD7991 driver to function!"
#endif

#define AD7991_VIN3_REF  0x8
#define AD7991_VDD_REF   0x0

//
//------------------------------------------------------------------------
void AD7991_Init(void);
void AD7991_SetConversion(unsigned char addr, unsigned char channel);
unsigned char AD7991_ReadConversion(unsigned char addr, unsigned char mode, unsigned char channel, unsigned int *pResult);

#endif // _MSCB_DEVICE_ADC_AD7991_H 