/********************************************************************\
  Name:         ADM1176.h
  Contents:     Hot Swap Controller and I2C Power Monitor
  $Id$
\********************************************************************/

#ifndef _MSCB_DEVICE_ADC_ADM1176_H
#define _MSCB_DEVICE_ADC_ADM1176_H

#include "../../common/mscbemb.h"

#ifdef MSCB_DEVICE_ADC_ADM1176_DISABLE
#warning "ADM1176 is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the ADM1176 driver to function!"
#endif


//
//------------------------------------------------------------------------
void ADM1176_Init(void);

//unsigned char ADM1176_ReadConversion(unsigned char addr, unsigned char *pResult);
unsigned char ADM1176_ReadStatus(unsigned char addr, unsigned char *status);
//void ADM1176_WriteERegister(unsigned char addr, unsigned char onoff);
//void ADM1176_WriteRegister(unsigned char addr, unsigned char cmd);
unsigned char ADM1176_CombineConversion(unsigned char addr, unsigned char cmd, unsigned char *pResult);
#endif // _MSCB_DEVICE_ADC_ADM1176_H 