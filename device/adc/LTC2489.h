/********************************************************************\
  Name:         LTC2489_adc.h
  Created by:   Bahman Sotoodian							
  Modified by:  Bryerton Shaw         Aug/19/2010	
  Date:         Apr/21/2008

  Contents:     This ADC uses interrupt based SMB protocol.
  $Id$
\********************************************************************/

#ifndef _MSCB_DEVICE_ADC_LTC2489_H
#define _MSCB_DEVICE_ADC_LTC2489_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_ADC_LTC2489_DISABLE
#warning "LTC2489 is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the LTC2489 driver to function!"
#endif

#define LTC2489_ADDR_GLOBAL 0x77 // Use this to sync multiple LTC2489s

#define LTC2489_CMD_IGNORE 	0x00 // Don't care/ignore following data
#define LTC2489_CMD_ENABLE	0xA0 // Select an Input Channel command
#define LTC2489_CMD_SGL			0x10 // Single Ended (non-differential)
#define LTC2489_CMD_ODD     0x08 // Odd/Signed

#define LTC2489_OVERRANGE   1
#define LTC2489_UNDERRANGE  2
#define LTC2489_VALIDRANGE  0

//
//------------------------------------------------------------------------
void LTC2489_Init(void);

//
//------------------------------------------------------------------------
void LTC2489_SetMode(unsigned char mode);

//
//------------------------------------------------------------------------
void LTC2489_StartConversion(unsigned char addr, unsigned char channel);

//
//------------------------------------------------------------------------
unsigned char LTC2489_ReadConversion(unsigned char addr, unsigned char channel, signed long *pResult); 

#endif // _MSCB_DEVICE_ADC_LTC2489_H 