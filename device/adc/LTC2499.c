/********************************************************************\
  Name:         LTC2499.c
  Created by:   PAA, DB  Sep 2012						
                24bit deltasigma+PGA 8/16 channel
  Contents:     16ch 24bit ADC using interrupt based SMB protocol.
  $Id$
\********************************************************************/

#include "LTC2499.h"

//
//------------------------------------------------------------------------
void LTC2499_Init(void) {
#ifdef MCSB_DEVICE_ADC_LTC2499_DISABLE
#else
	SMBus_Init(); // SMBus initialization
#endif // MCSB_DEVICE_ADC_LTC2499_DISABLE
}

//
//------------------------------------------------------------------------
void LTC2499_StartConversion(unsigned char addr, unsigned char mode, unsigned char conv_conf, unsigned char channel) {
#ifdef MCSB_DEVICE_ADC_LTC2499_DISABLE
  addr = 0;
  channel = 0;
#else
  unsigned char cmd[2];
  if (mode & LTC2499_CMD_SGL) {  // != 0 -> SGL
    cmd[0] = LTC2499_CMD_ENABLE | mode | (channel >> 1);
    cmd[1] = LTC2499_CONF_ENABLE | conv_conf; 	 
    if(channel & 0x01) {  // Add odd channel
       cmd[0] |= 0x08;
    }
  } else {  // == 0  -> Differential 
    cmd[0] = LTC2499_CMD_ENABLE | mode | (channel);
    cmd[1] = LTC2499_CONF_ENABLE | conv_conf; 	 
  }

  SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&cmd, 2);
  SMBus_SetRXBuffer(0, 0);
  SMBus_Start();
#endif // MCSB_DEVICE_ADC_LTC2499_DISABLE
}

//
//------------------------------------------------------------------------
unsigned char LTC2499_ReadConversion(unsigned char addr,  unsigned char mode , unsigned char conv_conf, unsigned char channel, signed long *pResult) {
#ifdef MCSB_DEVICE_ADC_LTC2499_DISABLE
  addr = 0;
  channel = 0;
  *pResult = 0;
  return 0;
#else
  unsigned char cmd[2];
  unsigned char validRange = LTC2499_VALIDRANGE;
  signed long Result;

  if (mode & LTC2499_CMD_SGL) {  // != 0 -> SGL
    cmd[0] = LTC2499_CMD_ENABLE | mode | (channel >> 1);
//    cmd[1] = LTC2499_CONF_ENABLE | conv_conf; 	 
    if(channel & 0x01) {  // Add odd channel
       cmd[0] |= 0x08;
    }
  } else {  // == 0  -> Differential 
    cmd[0] = LTC2499_CMD_ENABLE | mode | (channel);
 //   cmd[1] = LTC2499_CONF_ENABLE | conv_conf; 	 
  }

  Result = 0;
  SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&cmd, 1);
  SMBus_SetRXBuffer((unsigned char*)&Result, 4);
  SMBus_Start();

  if((Result & 0xC0000000) == 0xC0000000) { validRange = LTC2499_OVERRANGE;  }
  if((Result & 0xC0000000) == 0x00000000) { validRange = LTC2499_UNDERRANGE; }
	
  // Shift results so we are dealing with real values
  // The last 6 bits are always zero, so they can be truncated
  // pResult is 32 bits long, so we need to truncate 6=
  Result = (Result >> 6) & 0x1FFFFFF;
  *pResult = Result;

  // Sign extend if necessary
  if((*pResult & 0x1000000) == 0x1000000) {
    *pResult |= 0xFF000000;
  }

	return validRange;
#endif // MCSB_DEVICE_ADC_LTC2499_DISABLE
}
