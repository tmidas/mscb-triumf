/********************************************************************\
  Name:         LTC2493.h
  Contents:     24-Bit 2-/4-Channel
                DS ADC with Easy Drive Input Current
                Cancellation and I2C Interface.
  $Id$
\********************************************************************/

#ifndef _MSCB_DEVICE_ADC_LTC2493_H
#define _MSCB_DEVICE_ADC_LTC2493_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_ADC_LTC2493_DISABLE
#warning "LTC2493 is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the LTC2493 driver to function!"
#endif

#define LTC2493_ADDR_GLOBAL 0x77 // Use this to sync multiple LTC2493s

#define LTC2493_CMD_IGNORE 	0x00 // Don't care/ignore following data
#define LTC2493_CMD_ENABLE	0xA0 // Select an Input Channel command
#define LTC2493_CONF_ENABLE	0x80 // Select an Configuration command
#define LTC2493_CONF_F5060	0x00 // Select Freq Filter
#define LTC2493_CONF_SPDAC  0x00 // Select Auto-Calibration (not 2x speed)
#define LTC2493_CONF_SPEED  0x04 // Select 2x speed (Gain selection is changed)

#define LTC2493_CMD_SGL		0x10 // Single Ended (non-differential)
#define LTC2493_CMD_DIF		0x00 // Differential Ended
#define LTC2493_CMD_ODD   0x08 // Odd/Signed

#define LTC2493_OVERRANGE   1
#define LTC2493_UNDERRANGE  2
#define LTC2493_VALIDRANGE  0

//
//------------------------------------------------------------------------
void LTC2493_Init(void);

//
//------------------------------------------------------------------------
void LTC2493_SetMode(unsigned char mode);

//
//------------------------------------------------------------------------
void LTC2493_StartConversion(unsigned char addr, unsigned char mode, unsigned char cmd2, unsigned char channel);

//
//------------------------------------------------------------------------
unsigned char LTC2493_ReadConversion(unsigned char addr, unsigned char mode , unsigned char cmd2, unsigned char channel, signed long *pResult);

#endif // _MSCB_DEVICE_ADC_LTC2493_H 