/********************************************************************\
  Name:         LTC2489_adc.c
  Created by:   Bahman Sotoodian								
  Date:         Apr/21/2008

  Contents:     This ADC uses interrupt based SMB protocol.
  $Id$
\********************************************************************/

#include "LTC2489.h"

//
//------------------------------------------------------------------------
void LTC2489_Init(void) {
#ifdef MSCB_DEVICE_ADC_LTC2489_DISABLE
#else
	SMBus_Init(); // SMBus initialization
#endif // MSCB_DEVICE_ADC_LTC2489_DISABLE
}

//
//------------------------------------------------------------------------
void LTC2489_StartConversion(unsigned char addr, unsigned char channel) {
#ifdef MSCB_DEVICE_ADC_LTC2489_DISABLE
  addr = 0;
  channel = 0;
#else
  unsigned char cmd = LTC2489_CMD_ENABLE | LTC2489_CMD_SGL | (channel >> 1);
	
	if(channel & 0x01) {
		cmd |= 0x08;
	} 

	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&cmd, 1);
  SMBus_SetRXBuffer(0, 0);
  SMBus_Start();
#endif // MSCB_DEVICE_ADC_LTC2489_DISABLE
}

//
//------------------------------------------------------------------------
unsigned char LTC2489_ReadConversion(unsigned char addr, unsigned char channel, signed long *pResult) {
#ifdef MSCB_DEVICE_ADC_LTC2489_DISABLE
  addr = 0;
  channel = 0;
  *pResult = 0;
  return 0;
#else
	 unsigned char cmd = LTC2489_CMD_ENABLE | LTC2489_CMD_SGL | (channel >> 1);
	 unsigned char validRange = LTC2489_VALIDRANGE;

	if(channel & 0x01) {
		cmd |= 0x08;
	} 	

	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&cmd, 1);
  SMBus_SetRXBuffer((unsigned char*)pResult, 3);
  SMBus_Start();

	if((*pResult & 0xC0000000) == 0xC0000000) { validRange = LTC2489_OVERRANGE;  }
  if((*pResult & 0xC0000000) == 0x00000000) { validRange = LTC2489_UNDERRANGE; }
	
  // Shift results so we are dealing with real values
  // The last 6 bits are always zero, so they can be truncated
  // pResult is 32 bits long, so we need to truncate 8 + 6 == 14
	*pResult = (*pResult >> 14) & 0x1FFFF;

  // Sign extend if necessary
  if((*pResult & 0x10000) == 0x10000) {
    *pResult |= 0xFFFF0000;
  }

	return validRange;
#endif // MSCB_DEVICE_ADC_LTC2489_DISABLE
}
