/** 
 * $URL: $
 * $Id$
 *
 * @file 
 * @author Bryerton Shaw
 * 
 * @ingroup MSCB_DEVICE_ADC_ADS1225
 * @{
 */

#ifndef _MSCB_DEVICE_ADC_ADS1225_H
#define _MSCB_DEVICE_ADC_ADS1225_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_ADC_ADS1225_DISABLE
#warning "ADS1225 is disabled."
#endif 

/** @def ADS1225_SCLK_PIN 
 * Defines the SCLK pin used by the ADS1225
 */
#ifndef ADS1225_SCLK_PIN
#error "ADS1225_SCLK_PIN must be defined!"
#endif

/** @def ADS1225_START_PIN 
 * Defines the START pin used by the ADS1225
 */
#ifndef ADS1225_START_PIN
#error "ADS1225_START_PIN must be defined!"
#endif

/** @def ADS1225_DOUT_PIN 
 * Defines the DOUT pin used by the ADS1225
 */
#ifndef ADS1225_DOUT_PIN
#error "ADS1225_DOUT_PIN must be defined!"
#endif 

/** @def ADS1225_TEMP_EN_PIN 
 * Defines the TEMP_EN pin used by the ADS1225
 */
#ifndef ADS1225_TEMP_EN_PIN
#error "ADS1225_TEMP_EN_PIN must be defined!"
#endif 

/** @def ADS1225_BUFF_EN_PIN 
 * Defines the BUFF_EN pin used by the ADS1225
 */
#ifndef ADS1225_BUFF_EN_PIN
#error "ADS1225_BUFF_EN_PIN must be defined!"
#endif

/** @def ADS1225_MODE_PIN 
 * Defines the MODE pin used by the ADS1225
 */
#ifndef ADS1225_MODE_PIN
#error "ADS1225_MODE_PIN must be defined!"
#endif

/** @def ADS1225_MODE_HIGHRES 
 * Tells the ADS1225 to use the High Resolution mode
 */
#define ADS1225_MODE_HIGHRES	0

/** @def ADS1225_SCLK_PIN 
 * Tells the ADS1225 to use the High Speed mode
 */
#define ADS1225_MODE_HIGHSPEED	1


/**
 * @brief	Initializes ADS1225
 * @details	Sets up ADS1225 for use. Resets device to default state.
 */
void ADS1225_Init(void);

/**
 * @brief	Performs self calibration
 * @details	Runs the self-calibration command
 */
void ADS1225_SelfCalibrate(void);

/**
 * @brief	Performs a single conversion
 * @details	Starts a conversion then returns a result.
 */
signed long ADS1225_SingleConversion(void);

/**
 * @brief	Performs a single conversion for temperature
 * @details	Starts a conversion then returns a result.
 */
signed long ADS1225_ReadTemp(void);

/**
 * @brief	Performs poll conversion
 * @details	Gets the result of the last conversion, if in polled mode.
 */
signed long ADS1225_PollConversion(void);

#endif // ADS1225_H

/**
 * @}
 */