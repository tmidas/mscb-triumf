/** 
 * $URL: $
 * $Id$
 *
 * @file 
 * @ingroup MSCB_DEVICE_ADC_ADS1240
 * @author Bryerton Shaw
 *
 * @defgroup MSCB_DEVICE_ADC_ADS1240 ADS1240
 * @ingroup MSCB_DEVICE_ADC
 * @brief Texas Instruments 24-bit ADC	
 * @details This device is setup to use its own SPI routines
 * @todo Migrate code to use MSCB software/hardware SPI routines
 * @{
 */

#include "ADS1240.h"

#define ADS124_TOSC	1

sbit DIN	= ADS1240_DIN_PIN; 
sbit DOUT	= ADS1240_DOUT_PIN; 
sbit SCLK	= ADS1240_SCLK_PIN;
sbit DATARDYn = ADS1240_DATARDYn_PIN;
sbit CS = ADS1240_CS_PIN;


static unsigned char Tosc = ADS124_TOSC;

//
//------------------------------------------
void ADS1240_Init(void) {
	SCLK = 1; 
  CS = 0;
}

//
//------------------------------------------
static unsigned char _SPI_ReadByte(void) {
	signed char i;
	unsigned char value;

	for(i = 7, value = 0; i >= 0; --i) {
		SCLK = 0;
		delay_us(10);     // delay between DIN and DOUT
		value |= (unsigned char)(DOUT) << i;
		SCLK = 1;
		delay_us(10);
	}
  
  
  return value;

}

//
//------------------------------------------
static void _SPI_WriteByte(unsigned char value) {
	signed char i;

	for(i = 7; i >= 0; --i) {
		SCLK = 0;
		DIN = (value >> i) & 0x01;
		delay_us(10);
		SCLK = 1;
		delay_us(10);
	}

	DIN = 0;

}

//
//------------------------------------------
void ADS1240_WriteRegister(unsigned char reg, unsigned char d) {

	_SPI_WriteByte(ADS1240_CMD_WREG | reg);
	_SPI_WriteByte(0);
	_SPI_WriteByte(d);

}

//
//------------------------------------------
unsigned char ADS1240_ReadRegister(unsigned char reg) {
	unsigned char d;

	_SPI_WriteByte(ADS1240_CMD_RREG | reg); 
	_SPI_WriteByte(0);

//  delay_us(50);

	d = _SPI_ReadByte();

	return d;
}

//
//------------------------------------------
void ADS1240_Wait(void) {
	unsigned char d;

  while(DATARDYn == 1);
  while(DATARDYn == 0);

//	do {
//		d = ADS1240_ReadRegister(ADS1240_REG_ACR); 
//	} while(d & ADS1240_ACR_DRDY != 0);

}

//
//------------------------------------------
void ADS1240_WaitWhileNotReady(void) {
	unsigned char d;

	do {
		d = ADS1240_ReadRegister(ADS1240_REG_ACR); 
    delay_us(10);
	} while(d & ADS1240_ACR_DRDY != 0);

}

//
//------------------------------------------
void ADS1240_WaitWhileReady(void) {
	unsigned char d;

	do {
		d = ADS1240_ReadRegister(ADS1240_REG_ACR); 
    delay_us(10);
	} while(d & ADS1240_ACR_DRDY == 0);

}

//
//------------------------------------------
void ADS1240_SelfCalibrate(void) {

	// Select Register to Write
	_SPI_WriteByte(ADS1240_CMD_SELFCAL);

	delay_ms(100);
}

//
//------------------------------------------
signed long ADS1240_ReadConversion(void) {
	signed long raw_value;

	delay_us(10);
	_SPI_WriteByte(ADS1240_CMD_RDATA);


	ADS1240_Wait();

//  delay_ms(2);

	raw_value  = (signed long)(_SPI_ReadByte()) << 16;
	raw_value |= (signed long)(_SPI_ReadByte()) << 8;
	raw_value |= (signed long)(_SPI_ReadByte());


	return raw_value;
}

//
//------------------------------------------
void ADS1240_SelectChannel(unsigned char channel) {
  

  ADS1240_WriteRegister(ADS1240_REG_MUX, channel);
  
}


//
//------------------------------------------
void ADS1240_Reset(void) {

  CS = 0;
  delay_us(1);

	 _SPI_WriteByte(ADS1240_CMD_RESET);

  delay_us(1);

}
//
//------------------------------------------
void ADS1240_Wakeup(void) {

  CS = 0;
  delay_us(1);
	 _SPI_WriteByte(ADS1240_CMD_WAKEUP);


  delay_us(1);

}
//
//------------------------------------------
void ADS1240_ReadCont(void) {
 
  CS = 0;
  delay_us(1);

	 _SPI_WriteByte(ADS1240_CMD_RDATAC);


  delay_us(1);
}

/**
 * @}
 */