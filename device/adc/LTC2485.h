/********************************************************************\
  Name:         LTC2485.h
  Created by:   Bahman Sotoodian							
  Modified by:  Bryerton Shaw         Aug/19/2010	
  Date:         Apr/21/2008
                24bit deltasigma 1 channel
  Contents:     This ADC uses interrupt based SMB protocol.
  $Id$
\********************************************************************/

#ifndef _MSCB_DEVICE_ADC_LTC2485_H
#define _MSCB_DEVICE_ADC_LTC2485_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_ADC_LTC2485_DISABLE
#warning "LTC2485 is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the LTC2485 driver to function!"
#endif

#define LTC2485_ADDR_GLOBAL 0x77 // Use this to sync multiple LTC2485s

#define LTC2485_CMD_EXT_INPUT 	0x00 //
#define LTC2485_CMD_INT_TEMP    0x40 // 

#define LTC2485_OVERRANGE   1
#define LTC2485_UNDERRANGE  2
#define LTC2485_VALIDRANGE  0

//
//------------------------------------------------------------------------
void LTC2485_Init(void);

//
//------------------------------------------------------------------------
void LTC2485_StartConversion(unsigned char addr, unsigned char command);

//
//------------------------------------------------------------------------
unsigned char LTC2485_ReadConversion(unsigned char addr, unsigned char command, signed long *pResult); 

#endif // _MSCB_DEVICE_ADC_LTC2485_H 