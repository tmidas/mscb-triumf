/********************************************************************\
  Name:         LTC2485.c
  Created by:   Bahman Sotoodian								
  Date:         Apr/21/2008
                24bit deltasigma 1 channel
  Contents:     This ADC uses interrupt based SMB protocol.
  $Id$
\********************************************************************/

#include "LTC2485.h"

//
//------------------------------------------------------------------------
void LTC2485_Init(void) {
#ifdef MSCB_DEVICE_ADC_LTC2485_DISABLE
#else
	SMBus_Init(); // SMBus initialization
#endif // MSCB_DEVICE_ADC_LTC2485_DISABLE
}

//
//------------------------------------------------------------------------
unsigned char LTC2485_ReadConversion(unsigned char addr, unsigned char command, signed long *pResult) {
#ifdef MSCB_DEVICE_ADC_LTC2485_DISABLE
  addr = 0;
  *pResult = 0;
  return 0;
#else
	 unsigned char cmd = command;
	 unsigned char validRange = LTC2485_VALIDRANGE;

  SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&cmd, 1);
  SMBus_SetRXBuffer((unsigned char*)pResult, 4);
  SMBus_Start();

	if((*pResult & 0xC0000000) == 0xC0000000) { validRange = LTC2485_OVERRANGE;  }
  if((*pResult & 0xC0000000) == 0x00000000) { validRange = LTC2485_UNDERRANGE; }
	
 	*pResult ^= 0x80000000;

	return validRange;
#endif // MSCB_DEVICE_ADC_LTC2485_DISABLE
}
