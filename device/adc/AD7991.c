/********************************************************************\
  Name:         AD7991.c
  Contents:     ADC 12-Bit 4 Channel
                 I2C Interface.
  $Id$
\********************************************************************/

#include "AD7991.h"

//
//------------------------------------------------------------------------
void AD7991_Init(void) {
#ifdef MCSB_DEVICE_ADC_AD7991_DISABLE
#else
	SMBus_Init(); // SMBus initialization
#endif // MCSB_DEVICE_ADC_AD7991_DISABLE
}

//
//------------------------------------------------------------------------
char lchmap[4] = {0x10, 0x20, 0x40, 0x80};
unsigned char AD7991_ReadConversion(unsigned char addr, unsigned char mode, unsigned char channel, unsigned int *pResult) {
#ifdef MCSB_DEVICE_ADC_AD7991_DISABLE
  addr = 0;
  channel = 0;
  return 0;
#else
  unsigned char cmd=0;
  unsigned int Result;
  
  if (mode && (channel == 3)) return 0;

  if (mode == AD7991_VIN3_REF) {
    cmd |= mode;
  }
  cmd |= lchmap[channel];
  Result = 0;
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&cmd, 1);
  SMBus_SetRXBuffer((unsigned char*)&Result, 2);
  SMBus_Start();

  
  *pResult = (Result & 0xFFF);

	return ((Result >> 8) & 0x3); // Channel readout
#endif // MCSB_DEVICE_ADC_AD7991_DISABLE
}
