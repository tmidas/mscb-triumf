/********************************************************************\

  Name:         adc_internal.h
  Created by:     							


  Contents:     Internal ADCs

  $Id$

\********************************************************************/

#ifndef  _MSCB_DEVICE_ADC_INTERNAL_H
#define  _MSCB_DEVICE_ADC_INTERNAL_H

#include "../../common/mscbemb.h"

#ifdef MSCB_DEVICE_ADC_INTERNAL_DISABLE
#warning "Internal ADC is disabled."
#endif // MSCB_DEVICE_ADC_INTERNAL_DISABLE

// Number of active internal ADCs

void adc_internal_init(unsigned char ref0cn);
unsigned int adc_read(unsigned char channel, unsigned char gain);

#endif // _MSCB_DEVICE_ADC_INTERNAL_H
