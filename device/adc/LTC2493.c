/********************************************************************\
  Name:         LTC2493.c
  Contents:     24-Bit 2-/4-Channel
                DS ADC with Easy Drive Input Current
                Cancellation and I2C Interface.
  $Id$
\********************************************************************/

#include "LTC2493.h"

//
//------------------------------------------------------------------------
void LTC2493_Init(void) {
#ifdef MCSB_DEVICE_ADC_LTC2493_DISABLE
#else
	SMBus_Init(); // SMBus initialization
#endif // MCSB_DEVICE_ADC_LTC2493_DISABLE
}

//
//------------------------------------------------------------------------
void LTC2493_StartConversion(unsigned char addr, unsigned char mode, unsigned char conv_conf, unsigned char channel) {
#ifdef MCSB_DEVICE_ADC_LTC2493_DISABLE
  addr = 0;
  channel = 0;
#else
  unsigned char cmd[2]={0,0};
  unsigned char oa;
  // channel# uses the ODD/SIGN for channel selection
  switch(channel) {
  case 0:
    oa = 0x0;
  break;
  case 1:
    oa = 0x8;
  break;
  case 2:
    oa = 0x1;
  break;
  case 3:
    oa = 0x9;
  break;
  }

  cmd[0] = LTC2493_CMD_ENABLE | mode | oa;
  cmd[1] = LTC2493_CONF_ENABLE | conv_conf; 	 

  SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&cmd, 2);
  SMBus_SetRXBuffer(0, 0);
  SMBus_Start();
#endif // MCSB_DEVICE_ADC_LTC2493_DISABLE
}

//
//------------------------------------------------------------------------
unsigned char LTC2493_ReadConversion(unsigned char addr,  unsigned char mode , unsigned char conv_conf, unsigned char channel, signed long *pResult) {
#ifdef MCSB_DEVICE_ADC_LTC2493_DISABLE
  addr = 0;
  channel = 0;
  *pResult = 0;
  return 0;
#else
  unsigned char cmd[2]={0,0};
  unsigned char validRange = LTC2493_VALIDRANGE;
  signed long Result;

  unsigned char oa;
  // channel# uses the ODD/SIGN for channel selection
  switch(channel) {
  case 0:
    oa = 0x0;
  break;
  case 1:
    oa = 0x8;
  break;
  case 2:
    oa = 0x1;
  break;
  case 3:
    oa = 0x9;
  break;
  }

  cmd[0] = LTC2493_CMD_ENABLE | mode | oa;
  cmd[1] = LTC2493_CONF_ENABLE | conv_conf; 	 

  Result = 0;
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&cmd, 2);
  SMBus_SetRXBuffer((unsigned char*)&Result, 4);
  SMBus_Start();

	if((Result & 0xC0000000) == 0xC0000000) { validRange = LTC2493_OVERRANGE;  }
  if((Result & 0xC0000000) == 0x00000000) { validRange = LTC2493_UNDERRANGE; }
	
  // Shift results so we are dealing with real values
  // The last 6 bits are always zero, so they can be truncated
  // pResult is 32 bits long, so we need to truncate 8 + 6=14 
	Result = (Result >> 6) & 0x1FFFFFF;
  *pResult = Result;

  {
    // Sign extend if necessary
    if((*pResult & 0x1000000) == 0x1000000) {
       *pResult |= 0xFF000000;
    }
  }

	return validRange;
#endif // MCSB_DEVICE_ADC_LTC2493_DISABLE
}
