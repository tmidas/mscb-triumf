/**
 * $URL: $
 * $Id$
 * 
 * @file 
 * @author Bryerton Shaw
 * 
 * @ingroup MSCB_DEVICE_ADC_ADS1240
 * @{
 */

#ifndef _MSCB_DEVICE_ADC_ADS1240_H
#define _MSCB_DEVICE_ADC_ADS1240_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_ADC_ADS1240_DISABLE
#warning "ADS1240 is disabled."
#endif 

#ifndef ADS1240_SCLK_PIN
#error "ADS1240_SCLK_PIN must be defined!"
#endif

#ifndef ADS1240_DIN_PIN
#error "ADS1240_DIN_PIN must be defined!"
#endif

#ifndef ADS1240_DOUT_PIN
#error "ADS1240_DOUT_PIN must be defined!"
#endif 

#ifndef ADS1240_CS_PIN
#error "ADS1240_CS_PIN must be defined!"
#endif 

#define ADS1240_REG_SETUP	0x00
#define ADS1240_REG_MUX		0x01
#define ADS1240_REG_ACR		0x02
#define ADS1240_REG_ODAC	0x03
#define ADS1240_REG_DIO		0x04
#define ADS1240_REG_DIR		0x05
#define ADS1240_REG_IOCON	0x06
#define ADS1240_REG_OCR0	0x07
#define ADS1240_REG_OCR1	0x08
#define ADS1240_REG_OCR2	0x09
#define ADS1240_REG_FSR0	0x0A
#define ADS1240_REG_FSR1	0x0B
#define ADS1240_REG_FSR2	0x0C
#define ADS1240_REG_DOR2	0x0D
#define ADS1240_REG_DOR1	0x0E
#define ADS1240_REG_DOR0	0x0F

#define ADS1240_MUX_P_CH0	0x00
#define ADS1240_MUX_P_CH1	0x10
#define ADS1240_MUX_P_CH2	0x20
#define ADS1240_MUX_P_CH3	0x30
#define ADS1240_MUX_P_CH4	0x40
#define ADS1240_MUX_P_CH5	0x50
#define ADS1240_MUX_P_CH6	0x60
#define ADS1240_MUX_P_CH7	0x70
#define ADS1240_MUX_P_COM	0x80
#define ADS1240_MUX_P_RES	0xFF
#define ADS1240_MUX_N_CH0	0x00
#define ADS1240_MUX_N_CH1	0x01
#define ADS1240_MUX_N_CH2	0x02
#define ADS1240_MUX_N_CH3	0x03
#define ADS1240_MUX_N_CH4	0x04
#define ADS1240_MUX_N_CH5	0x05
#define ADS1240_MUX_N_CH6	0x06
#define ADS1240_MUX_N_CH7	0x07
#define ADS1240_MUX_N_COM	0x08
#define ADS1240_MUX_N_RES	0xFF

#define ADS1240_ACR_DRDY	0x80
#define ADS1240_ACR_POL		0x40
#define ADS1240_ACR_SPEED	0x20
#define ADS1240_ACR_BUFEN	0x10
#define ADS1240_ACR_BORDER	0x08
#define ADS1240_ACR_RANGE	0x04
#define ADS1240_ACR_DR1		0x02
#define ADS1240_ACR_DR0		0x01

#define ADS1240_CMD_RDATA	0x01
#define ADS1240_CMD_RDATAC	0x03
#define ADS1240_CMD_STOPC	0x0F
#define ADS1240_CMD_RREG	0x10 // 0001 rrrr xxxx nnnn r=register, n=count, x=dontcare
#define ADS1240_CMD_WREG	0x50 // 0101 rrrr xxxx nnnn r=register, n=count, x=dontcare
#define ADS1240_CMD_SELFCAL	0xF0
#define ADS1240_CMD_SELFOCAL	0xF1
#define ADS1240_CMD_SELFGCAL	0xF2
#define ADS1240_CMD_SYSOCAL	0xF3
#define ADS1240_CMD_SYSGCAL	0xF4
#define ADS1240_CMD_WAKEUP	0xFB
#define ADS1240_CMD_DSYNC	0xFC
#define ADS1240_CMD_SLEEP	0xFD
#define ADS1240_CMD_RESET	0xFE

void ADS1240_Test(unsigned char d1);

void ADS1240_Init(void);
signed long ADS1240_ReadConversion(void);
void ADS1240_SelectChannel(unsigned char channel);
void ADS1240_WriteRegister(unsigned char reg, unsigned char d);
unsigned char ADS1240_ReadRegister(unsigned char reg);
void ADS1240_SelfCalibrate(void);
void ADS1240_Reset(void);
void ADS1240_Wakeup(void);
void ADS1240_ReadCont(void);
#endif // ADS1240_H

/**
 * @}
 */

