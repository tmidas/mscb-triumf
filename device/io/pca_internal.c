/********************************************************************\
  Name:         pca_internal.c
  Created by:   

  Contents:     Internal PCA functions for F12x-13x
                Midas Slow Control Bus protocol

  $Id$

\********************************************************************/

#include "pca_internal.h"

/*---- PCA initilalization -----------------------------------------*/
void pca_Init(void) {
#ifdef MSCB_DEVICE_INTERNAL_PCA_DISABLE
#else
		PCA0CN   = 0x00; // Disable PCA Run Control 
		PCA0MD   = 0x04;  // Sysclk (default CKCON [2MHz])
		PCA0CPL0 = 0x01;
		PCA0CPM0 = 0x46; // ECM, TOG, PWM
		PCA0CPH0 = 0x01;   // (for ~1MHz)		       

    // PCA setup for Frequency Output Mode on P0.2 CEX0
		P0MDOUT  |= 0x04; // Add pin .2 (CHG_PMP_CLK) to PushPull
		XBR1     |= 0x01; // Route CEX0(CHG_PMP_CLK) to its pin
		XBR1     |= 0x40; // Enable Xbar
#endif
}

//
//------------------------------------------------------------------------
void pca_Operation(unsigned char mode) {
#ifdef MSCB_DEVICE_INTERNAL_PCA_DISABLE
#else
  if (mode == PCA_INTERNAL_OFF) {
		PCA0CN   = 0x00 ;   //Turn off the PCA counter

	} else if (mode == PCA_INTERNAL_ON) {
		PCA0CN   = 0x40 ;   //Turn on the PCA counter
    }
#endif 
}

//
//------------------------------------------------------------------------
void pca_FreqChange(unsigned char freq) {
#ifdef MSCB_DEVICE_INTERNAL_PCA_DISABLE
#else
  // CEX0 freq = 2MHz/(2xPCA0CPH0)
		PCA0CPH0 = freq;   // (1 ==> 1MHz)		       
#endif 
}

