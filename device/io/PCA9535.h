/********************************************************************\

  Name:         PCA9535.h
  Created by:   Bahman Sotoodian 								Feb/11/2008
  Modified by:  Bryerton Shaw										Aug/19/2010

  Contents:     PCA9535 I/O register user interface based on 36

  $Id$

\********************************************************************/

#ifndef  _MSCB_DEVICE_IO_PCA9535_H
#define  _MSCB_DEVICE_IO_PCA9535_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_IO_PCA9535_DISABLE
#warning "PCA9535 is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the PCA9536 driver to function!"
#endif

// Command Bytes
#define PCA9535_INPUT  	      0x00
#define PCA9535_OUTPUT        0x02
#define PCA9535_POLINV			  0x04
#define PCA9535_CONFIG			  0x06

// Bit Meanings
#define PCA9535_READ				  1
#define PCA9535_WRITE			    0

#define PCA9535_INVERT			  0xFFFF
#define PCA9535_NO_INVERT		  0x0000
#define PCA9535_ALL_INPUT		  0xFFFF
#define PCA9535_ALL_OUTPUT		0x0000

void PCA9535_Init(void);
void PCA9535_WriteWord(unsigned char addr, unsigned char reg, unsigned int dataWord);

#endif // _PCA9535_H
