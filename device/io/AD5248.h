/********************************************************************\

  Name:         AD5248.h
  Created by:   P.-A. Amaudruz
  Modified by:  
  Contents:     AD5248 256 digital resistor

  $Id$

\********************************************************************/

#ifndef  _MSCB_DEVICE_IO_AD5248_H
#define  _MSCB_DEVICE_IO_AD5248_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_IO_AD5248_DISABLE
#warning "AD5248 is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the AD5248 driver to function!"
#endif

// Command Bytes

// Bit Meanings
#define AD5248_READ				  1
#define AD5248_WRITE			  0

void AD5248_Init(void);
void AD5248_Write(unsigned char addr, unsigned char selectPort, unsigned char dataByte);
void AD5248_Read(unsigned char addr, unsigned char selectPort, unsigned char* dataBytes, unsigned char dataLen);

#endif // _AD5248_H
