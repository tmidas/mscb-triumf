/********************************************************************\

  Name:         PCA9536.h
  Created by:   Bahman Sotoodian 								Feb/11/2008
  Modified by:  Bryerton Shaw										Aug/19/2010

  Contents:     PCA9536 I/O register user interface

  $Id$

\********************************************************************/

#ifndef  _MSCB_DEVICE_IO_PCA9536_H
#define  _MSCB_DEVICE_IO_PCA9536_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_IO_PCA9536_DISABLE
#warning "PCA9536 is disabled."
#endif 

#ifndef MSCB_SMBus_HW 
#error "MSCB_SMBus_HW must be defined for the PCA9536 driver to function!"
#endif

// Command Bytes
#define PCA9536_INPUT		      0x00
#define PCA9536_OUTPUT			  0x01
#define PCA9536_POLINV			  0x02
#define PCA9536_CONFIG			  0x03

// Bit Meanings
#define PCA9536_READ				  1
#define PCA9536_WRITE			    0

#define PCA9536_INVERT			  1
#define PCA9536_NO_INVERT		  0
#define PCA9536_ALL_INPUT		  0xFF
#define PCA9536_ALL_OUTPUT		0x00

void PCA9536_Init(void);
void PCA9536_WriteByte(unsigned char addr, unsigned char selectPort, unsigned char dataByte);
void PCA9536_WriteWord(unsigned char addr, unsigned char selectPort, unsigned int dataWord);
void PCA9536_Read(unsigned char addr, unsigned char selectPort, unsigned char* dataBytes, unsigned char dataLen);

#endif // _PCA9536_H
