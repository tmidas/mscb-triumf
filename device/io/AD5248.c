/********************************************************************\

  Name:         AD5248.c
  Created by:   P.-A. Amaudruz
  Modified by:	

  Contents:     AD5248 256 digital resistor

  $Id$

\********************************************************************/

#include "AD5248.h"

//
//------------------------------------------------------------------------
void AD5248_Init(void) {
#ifdef MCSB_DEVICE_IO_AD5248_DISABLE
#else
  SMBus_Init(); // SMBus initialization
#endif
}

//
//------------------------------------------------------------------------
void AD5248_Write(unsigned char addr, unsigned char selectPort, unsigned char dataByte) {
#ifdef MCSB_DEVICE_IO_AD5248_DISABLE
  addr = 0;
  selectPort = 0;
  dataByte = 0;
#else
  unsigned char buffer[2];

  buffer[0] = (selectPort<<7);
  buffer[1] = dataByte;

	// Wait for the SMBus to clear
  SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(buffer, 2);
  SMBus_SetRXBuffer(0, 0);
  SMBus_Start();
#endif
}

//
//------------------------------------------------------------------------
void AD5248_Read(unsigned char addr, unsigned char selectPort, unsigned char *pDataBytes, unsigned char dataLen) {
#ifdef MCSB_DEVICE_IO_AD5248_DISABLE
  addr = 0;
  selectPort = 0;
  pDataBytes = pDataBytes;
  dataLen = 0;
#else
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&selectPort, 1);
  SMBus_SetRXBuffer(pDataBytes, dataLen);
  SMBus_Start();
#endif
}
