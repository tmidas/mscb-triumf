/********************************************************************\

  Name:         PCA9536_io.c
  Created by:   Brian Lee
  Modified by:	 Bahman Sotoodian							Feb/11/2008
  Modified by:	 Bryerton Shaw								Mar/15/2008

  Contents:     PCA9536 I/O register user interface

  $Id$

\********************************************************************/

#include "PCA9536.h"

//
//------------------------------------------------------------------------
void PCA9536_Init(void) {
#ifdef MCSB_DEVICE_IO_PCA9536_DISABLE
#else
  SMBus_Init(); // SMBus initialization
#endif
}

//
//------------------------------------------------------------------------
void PCA9536_WriteByte(unsigned char addr, unsigned char selectPort, unsigned char dataByte) {
#ifdef MCSB_DEVICE_IO_PCA9536_DISABLE
  addr = 0;
  selectPort = 0;
  dataByte = 0;
#else
  unsigned char buffer[2];

  buffer[0] = selectPort;
  buffer[1] = dataByte;

	// Wait for the SMBus to clear
  SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(buffer, 2);
  SMBus_SetRXBuffer(0, 0);
  SMBus_Start();
#endif
}

#if 0
//
//------------------------------------------------------------------------
void PCA9536_WriteWord(unsigned char addr, unsigned char selectPort, unsigned int dataWord) {
#ifdef MCSB_DEVICE_IO_PCA9536_DISABLE
  addr = 0;
  selectPort = 0;
  dataWord = 0;
#else
	unsigned char buffer[3];
  
  buffer[0] = selectPort;
  buffer[1] = (dataWord >> 8);
  buffer[2] = (dataWord & 0x00FF);

	// Wait for the SMBus to clear
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(buffer, 3);
  SMBus_SetRXBuffer(0, 0);
  SMBus_Start();
#endif
}

#endif 

//
//------------------------------------------------------------------------
void PCA9536_Read(unsigned char addr, unsigned char selectPort, unsigned char *pDataBytes, unsigned char dataLen) {
#ifdef MCSB_DEVICE_IO_PCA9536_DISABLE
  addr = 0;
  selectPort = 0;
  pDataBytes = pDataBytes;
  dataLen = 0;
#else
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(&selectPort, 1);
  SMBus_SetRXBuffer(pDataBytes, dataLen);
  SMBus_Start();
#endif
}
