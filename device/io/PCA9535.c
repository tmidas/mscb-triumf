/********************************************************************\

  Name:         PCA9535_io.c
  Created by:   Brian Lee
  Modified by:	 Bahman Sotoodian							Feb/11/2008
  Modified by:	 Bryerton Shaw								Mar/15/2008

  Contents:     PCA9536 I/O register user interface based on 36

  $Id$

\********************************************************************/

#include "PCA9535.h"

//
//------------------------------------------------------------------------
void PCA9535_Init(void) {
#ifdef MCSB_DEVICE_IO_PCA9535_DISABLE
#else
  SMBus_Init(); // SMBus initialization
#endif
}

//
//------------------------------------------------------------------------
void PCA9535_WriteWord(unsigned char addr, unsigned char reg, unsigned int dataWord) {
#ifdef MCSB_DEVICE_IO_PCA9535_DISABLE
  addr = 0;
  reg = 0;
  dataWord = 0;
#else
	unsigned char buffer[3];
// Slave Addres / Command Byte / Data Port 0 / Data Port 1
//                    0x2          -> 2           -> 3
  buffer[0] = reg;
  buffer[1] = (dataWord & 0x00FF);
  buffer[2] = (dataWord >> 8);

	// Wait for the SMBus to clear
	SMBus_Wait();
  SMBus_SetSlaveAddr(addr);
  SMBus_SetTXBuffer(buffer, 3);
  SMBus_SetRXBuffer(0, 0);
  SMBus_Start();
#endif
}

