/********************************************************************\

  Name:         pca_internal.h
  Created by:     							

  Contents:     Internal PCA

  $Id$

\********************************************************************/

#ifndef _MSCB_DEVICE_INTERNAL_PCA_H
#define _MSCB_DEVICE_INTERNAL_PCA_H

#include "../../common/mscbemb.h"

#ifdef MCSB_DEVICE_INTERNAL_PCA_DISABLE
#warning "Internal_PCA is disabled."
#endif 

//
// mode refers to:
//  1 : turn OFF PCA
//  2 : turn  ON PCA
// 
#define PCA_INTERNAL_OFF           1
#define PCA_INTERNAL_ON            2

void pca_Init(void);
void pca_Operation(unsigned char mode);
void pca_FreqChange(unsigned char freq);

#endif // _MSCB_DEVICE_INTERNAL_PCA_H