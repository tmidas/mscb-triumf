     /**********************************************************************************\
  Name:			  Humidity_sensor.c
  Author:   	Bahman Sotoodian 	
  Created: 		March 28, 2008
  Description:	Humidity and Temperature measurements

  $Id$
\**********************************************************************************/


//---------------------------------------------------------------------------------
//Include files
//---------------------------------------------------------------------------------
#include "sht7x_sensor.h"
#include "../../common/protocol/SHT7x.h"

<<<<<<< HEAD
=======
sbit SHT7x_DATA1 = SHT_DATA1;
sbit SHT7x_SCK1  = SHT_SCK1;

#ifdef MORETHANONEHUM
// Defined in SHT7x.h
//sbit SHT7x_DATA2 = SHT_DATA2;
//sbit SHT7x_SCK2  = SHT_SCK2;
#endif

>>>>>>> 9a53618ae433902ee9be5ca11decadee806ad057
static void SHT7x_Correction (float *p_humidity ,float *p_temperature);
static signed char SHT7x_Measure(unsigned int *DataToSend, unsigned char flag, unsigned char *FCSoriginator, unsigned char *FCSclient, int humsen);
static unsigned char ReverseStatus (unsigned char value);
static unsigned char ReverseByte (unsigned char dataBeReversed);
static unsigned char SHT7x_StatusRead(unsigned char *StatusRead, int humsen);
static unsigned char SHT7x_StatusWrite(unsigned char StatusWrite, int humsen);

//Initializing the SHT7x protocol communication 
void HumiSensor_Init(int humsen){
	SHT7x_Init(humsen);
}

//----------------------------------------------------------------------------------
// 1. connection reset
// 2. measure humidity(12 bit) and temperature(14 bit)
// 3. calculate humidity [%RH] and temperature [C]
// 4. calculate dew point [C]
//----------------------------------------------------------------------------------------
unsigned char HumidSensor_Cmd (unsigned int *rhumidity,unsigned int *rtemperature, 
float *humidity, float *temperature,unsigned char *OrigFCS, unsigned char *DeviceFCS, int humsen){
	
	float         xdata temp_humi, temp_tempe;
	unsigned char xdata TempStatus,HumiStatus;
	unsigned int  xdata rtemp,rhumi;
	unsigned char xdata temp_FCSOrig,temp_FCSDevice;
	
	TempStatus = SHT7x_Measure(&rtemp, SHT7x_TEMP, &temp_FCSOrig, &temp_FCSDevice, humsen); //measure temperature
	HumiStatus = SHT7x_Measure(&rhumi, SHT7x_HUMI, &temp_FCSOrig, &temp_FCSDevice, humsen); //measure humidity
	
	*DeviceFCS = temp_FCSDevice;
	*OrigFCS = temp_FCSOrig;
	*rtemperature = rtemp;
	*rhumidity = rhumi;

  temp_humi  = (float) rhumi;	
  temp_tempe = (float) rtemp;
  SHT7x_Correction(&temp_humi,&temp_tempe); 			//calculate humidity, temperature
  *humidity = temp_humi;
  *temperature = temp_tempe;								

	if((TempStatus == SHT7x_ERROR) || (HumiStatus == SHT7x_ERROR)){ 
		SHT7x_ConnectionReset(humsen); //in case of an error: connection reset
		// delay_ms(800);
		return SHT7x_ERROR;
	} 

	//wait approx. 0.8s to avoid heating up SHTxx
  //delay_ms(800);
	return SHT7x_DONE;
}

//----------------------------------------------------------------------------------------
// Makes a measurement (Humidity/Temperature) with checksum
//----------------------------------------------------------------------------------------
static signed char SHT7x_Measure(unsigned int *DataToSend
                        , unsigned char flag
                        , unsigned char *FCSoriginator
                        , unsigned char *FCSclient
                        , int humsen)
{
	unsigned char xdata check_flag, status = SHT7x_DONE;
	unsigned char xdata FCSdevice, FCSorig, MSBdata, LSBdata;
	unsigned int  xdata measurements=0;
	unsigned long xdata SHT_time =0, temp_check=0;

	
	SHT7x_TransStart(humsen);						 //transmission start
	
	switch(flag) { 								 //send command to sensor
		case SHT7x_TEMP: 
			status = SHT7x_WriteByte(SHT7x_MEASURE_TEMP, humsen); 
			check_flag = SHT7x_MEASURE_TEMP;
			break;
		
		case SHT7x_HUMI : 
			status = SHT7x_WriteByte(SHT7x_MEASURE_HUMI, humsen); //if device does not ack, returns error
			check_flag = SHT7x_MEASURE_HUMI;
			break;
		
		default : 
			break;
	}
	
	SHT_time = uptime();

	do {
		if(humsen==1) {
			if(SHT7x_DATA1 == 0) {
				temp_check = 1; 
				break; //wait until sensor has finished the measurement
		 	}
		}
#ifdef MORETHANONEHUM
		else if(humsen==2) {
			if(SHT7x_DATA2 == 0) {
				temp_check = 1; 
				break; //wait until sensor has finished the measurement
		 	}			
		}
#endif
	}while ((uptime() - SHT_time) < SHT7x_TIME);
	

	if(temp_check == 0){ 
		SHT7x_ConnectionReset(humsen);
		return SHT7x_ERROR; 			  // timeout is reached
	}

	//14bit temperature measurements
	//12bit humidity measurements
	MSBdata  	= SHT7x_ReadByte(SHT7x_ACK, humsen);		  //read the first byte (MSB)
	LSBdata  	= SHT7x_ReadByte(SHT7x_ACK, humsen);      //read the second byte (LSB)
	FCSdevice = SHT7x_ReadByte(SHT7x_ACK, humsen);      //read the last byte (checksum)
	FCSdevice = ReverseByte (FCSdevice);
	
	//Calculate originator's side write FCS
  FCSorig = SHT7x_FCS_Step(LSBdata
          , SHT7x_FCS_Step(MSBdata
          , SHT7x_FCS_Step(check_flag
          , 0x00)));	

	*FCSclient = FCSdevice;
	*FCSoriginator = FCSorig;

	// Checking the checksum
	if (FCSdevice != FCSorig) {
		SHT7x_ConnectionReset(humsen);
		return SHT7x_ERROR;
  }

	measurements = (MSBdata	<<8) | (LSBdata);

	if(flag == SHT7x_TEMP)
		measurements &= 0x3fff;
	else
		measurements &= 0x0fff;
	
	*DataToSend =  measurements;
	return status;     
}

//-------------------------------------------------------------
//
//
static void SHT7x_Correction(float *p_humidity ,float *p_temperature)
{ 
	// Assuming VDD to be 3.5V but the actual voltage is 3.3V
	*p_temperature = (*p_temperature * SHT7x_D2) + SHT7x_D1 ; 	
	*p_humidity = (*p_humidity) * (*p_humidity * SHT7x_C3) + *p_humidity * SHT7x_C2 + SHT7x_C1; 	
	
	*p_humidity=(*p_temperature - 25)*(SHT7x_T1+ *p_humidity * SHT7x_T2)+ *p_humidity; //calc. Temperature compensated humidity [%RH]

	if(*p_humidity > 100)
		*p_humidity = 100; 		    // Cut if the value is outside of
	
	if(*p_humidity <0.1)
		*p_humidity = 0.1;        // the physical possible range

}


/*
//-------------------------------------------------------------
//
//
unsigned char SHT7x_StatusRead(unsigned char *StatusRead, int humsen)
{
	unsigned char status=1;

	SHT7x_TransStart(humsen);
	status = SHT7x_WriteByte(STATUS_REG_R, humsen);
  delay_ms(20);
	*StatusRead = SHT7x_ReadByte(NACK, humsen);
	return status;

}


//-------------------------------------------------------------
//
//
unsigned char SHT7x_StatusWrite(unsigned char StatusWrite, int humsen){
	unsigned char status;
	
	SHT7x_TransStart(humsen);
	status = SHT7x_WriteByte(STATUS_REG_W, humsen);
	status = SHT7x_WriteByte(StatusWrite, humsen);
	
	return status;
}
*/	
//-------------------------------------------------------------
//
//
static unsigned char ReverseByte (unsigned char dataBeReversed){

	unsigned char i,j,temp = 0;
	
	for ( i=0x80,j=7;i>=0x10;i/=2,j-=2 )
			temp |= ((dataBeReversed & i) >> j);		
	
	for ( i=0x08,j=1;i>=0x01;i/=2,j+=2 )
			temp |= ((dataBeReversed & i) << j);
	return temp;
}

 





