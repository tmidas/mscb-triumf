/**********************************************************************************\
  Name:			   Humidity_Sensor.h
  Author:   	 Bahman Sotoodian 	
  Created: 		 March 28, 2008
  Description: Humidity and Temperature measurements

  $Id$
\**********************************************************************************/

#ifndef _MSCB_DEVICE_HUMIDITY_SHT7x_SENSOR_H
#define _MSCB_DEVICE_HUMIDITY_SHT7x_SENSOR_H

#include "../../common/mscbemb.h"

//Defining the SHT7x instructions 		//adr  command  r/w											
#define SHT7x_STATUS_REG_W 0x06 					  //000	  0011 	 0
#define SHT7x_STATUS_REG_R 0x07 					  //000   0011 	 1
#define SHT7x_MEASURE_TEMP 0x03 					  //000   0001 	 1
#define SHT7x_MEASURE_HUMI 0x05 					  //000   0010 	 1
#define SHT7x_RESET 0x1e 						        //000   1111 	 0

#define SHT7x_TEMP 1
#define SHT7x_HUMI 2
#define SHT7x_ERROR 0
#define SHT7x_DONE  1
#define SHT7x_TIME  2

#define SHT7x_C1 -4.0 			// for 12 Bit humidity readings
#define SHT7x_C2 0.0405 		// for 12 Bit humidity readings
#define SHT7x_C3 -0.0000028 // for 12 Bit humidity readings
#define SHT7x_T1 0.01 			// compensation for humidity at high temp
#define SHT7x_T2 0.00008 		// compensation for humidity at high temp
#define SHT7x_D1 -39.66			// for 14 Bit @ 3.5V temp readings
#define SHT7x_D2 0.01			  // for 14 Bit @ 3.5V temp readings

void HumiSensor_Init(int humsen);
unsigned char HumidSensor_Cmd (unsigned int *rhumidity,unsigned int *rtemperature, float *humidity, float *temperature,unsigned char *OrigFCS, unsigned char *DeviceFCS, int humsen);
signed char SHT7x_Measure(unsigned int *DataToSend
                        , unsigned char flag
                        , unsigned char *FCSoriginator
                        , unsigned char *FCSclient
                        , int humsen);
void SHT7x_Correction(float *p_humidity ,float *p_temperature);
unsigned char ReverseByte (unsigned char dataBeReversed);

#endif // _MSCB_DEVICE_HUMIDITY_SHT7x_SENSOR_H

