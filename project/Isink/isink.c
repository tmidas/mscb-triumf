/********************************************************************\

Name:     isink.c
Author:   P-A Amaudruz
       Current sink board controlled by a DAC
       Project Geotomo, Cript
$Id$

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "isink.h" 
extern xdata SYS_INFO sys_info;

char    code  MSCB_node_name[] = "isink410";
char    xdata svn_rev_code[] = "$Rev$";
xdata         MSCB_USER_DATA user_data;

MSCB_INFO_VAR code vars[] = {
  4, UNIT_BYTE,     0, 0, 0,           "SerialN",    &user_data.sn,          // 0
  2, UNIT_BYTE,     0, 0, 0,           "Error",      &user_data.error,       // 1
  1, UNIT_BYTE,     0, 0, 0,           "Control",    &user_data.control,     // 2
  1, UNIT_BYTE,     0, 0, 0,           "Status",     &user_data.status,      // 3

  // LTC2607 Dual-DAC 16bits
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Vdac",     &user_data.vset,          // 4
  
  // F410 internal ADCs
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "uCT",        &user_data.ucTemp,      // 5

  // F410 internal ADCs Raw Data
  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,  "rTuC",       &user_data.rtemp,     // 6
  0
};

code MSCB_INFO_VAR *variables = vars;

/********************************************************************\

Application specific init and inout/output routines

\********************************************************************/

// Change register
volatile unsigned char bdata bChange = 0;
sbit fSdac0 = bChange ^ 0;
sbit fSdac1 = bChange ^ 1;
sbit fna3   = bChange ^ 2;
sbit fna4   = bChange ^ 3;
sbit fna5   = bChange ^ 4;

// Control register
unsigned char bdata rCTL = 0;
sbit cSw1     = rCTL ^ 0;

// Status Register
unsigned char bdata rSTAT = 0;
sbit sSw1     = rSTAT ^ 0;

// ERR Error Register
//The low and high bytes are switched in the bdata section of the memory
//This is the reason that the sbit declarations do not appear to match
//the documentation but they actually do.
unsigned int bdata rERR =0;
sbit eNa8  = rERR ^ 8;   //0x1  
sbit eNa9  = rERR ^ 9;   //0x2
sbit eNa10 = rERR ^ 10;  //0x4
sbit eNa11 = rERR ^ 11;  //0x8

sbit eNa12 = rERR ^ 12;  //0x10 
sbit eNa13 = rERR ^ 13;  //0x20 
sbit eNa16 = rERR ^ 14;  //0x40 
sbit eNa15 = rERR ^ 15;  //0x80 

sbit eNa0  = rERR ^ 0;   //0x100
sbit eNa1  = rERR ^ 1;   //0x200
sbit eNa2  = rERR ^ 2;   //0x400
sbit eNa3  = rERR ^ 3;   //0x800

sbit eNa4  = rERR ^ 4;   //0x1000
sbit eNa5  = rERR ^ 5;   //0x2000
sbit eNa6  = rERR ^ 6;   //0x4000
sbit eNa7  = rERR ^ 7;   //0x8000

// DAC update register
unsigned char bdata rDAC = 0xFF;
sbit DAC0     = rDAC ^ 0;
sbit DAC1     = rDAC ^ 1;

// Hardware bits
// P0 .7: Sp8  .6:Tx/En  .5:Rx,   .4:Tx,  .3:n/a, .2:n/a       .1: SCK, .0 SDA
// P1 .7:LED2  .6:LED1   .5:n/a   .4:n/a  .3:n/a  .2:n/a/Vref  .1:n/a   .0:n/a
// P2 .7:C2D   .6:n/a    .5:n/a  .4:n/a   .3:n/a  .2:n/a       .1:n/a   .0:n/a

/*---- User init function ------------------------------------------*/
void user_init(unsigned char init)
{
  char i;
  unsigned char temp;

  /* Format the SVN and store this code SVN revision into the system */
  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  sys_info.svn_revision = (svn_rev_code[6]-'0')*1000+
    (svn_rev_code[7]-'0')*100+
    (svn_rev_code[8]-'0')*10+
    (svn_rev_code[9]-'0');

  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  if (init) {
    // Initialize variables
    user_data.vset = 0;
    // Temperature initialization
    user_data.ucTemp = 0;
  }

  // Reference Voltage on P0.0 Enable
  //  REF0CN = 0x00;  // Int Bias off, Int Temp off, VREF input on

  // 0:analog 1:digital
  P0MDIN = 0xF7; // P0 all digital pins  Vrbk
  P1MDIN = 0xFF; // P1 all digital pins
  P2MDIN = 0xFF; // P2 all digital pins
 
  // 0: open-drain, 1:push-pull
  // P0 .7: Sp8  .6:Tx/En  .5:Rx,   .4:Tx,  .3:n/a, .2:n/a       .1: SCK, .0 SDA
  P0MDOUT = 0x61;  //   OD: Rx, SDA

  // 0: open-drain, 1:push-pull
  // P1 .7:LED2  .6:LED1   .5:n/a   .4:n/a  .3:n/a  .2:n/a/Vref  .1:n/a   .0:n/a
  P1MDOUT = 0xFF;  

  // 0: open-drain, 1:push-pull
  // P2 .7:C2D   .6:n/a    .5:n/a  .4:n/a   .3:n/a  .2:n/a       .1:n/a   .0:n/a
  P2MDOUT = 0xFF;  // 

  // Setting the cross bar
  P0SKIP = 0x0; 
  P1SKIP = 0x0; 
  P2SKIP = 0x0; 

  // Reset HV enable
  user_data.vset = 0;
  
  XBR0 |=0x04;	 // Enable SMBus pins P0 .0/SDA .1/SCL
  // CPLD clock input 25MHz
  XBR0 |=0x08;	 // Enable SYSCLK to Port 1.2

  rCTL  = 0;
  rSTAT = 0;
  rERR  = 0;
  publishCtlCsr();
   
  UpdateIO();

  LTC2607_Init();
  adc_internal_init(0x15);  //  0(1.5V)/1(2.2V)-0-TEMPE-0-REFBE

}

/*---- User write function -----------------------------------------*/
void user_write(unsigned char index) reentrant {

  // Set the appropriate DAC update register bit
  // Done this way so we if we can take being interrupted, and avoid a
  // read-write-modify setup. (if done using a byte)
  if (index == CTL_IDX){
    if (user_data.control & 0x01) { cSw1 = 1; } 
  } 
 
  if ((index >= FIRST_DAC) && (index < (FIRST_DAC + (N_DAC)))) {
    switch(index - FIRST_DAC) {
      case 0:
        DAC0 = 1;
        break;

      case 1:
        DAC1 = 1;
        break;
    }
  }

  return;
}

/*---- User read function ------------------------------------------*/

unsigned char user_read(unsigned char index)
{
  if(index);
  return 0;
}

/*---- User function called vid CMD_USER command -------------------*/

unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
  // echo input data
  data_out[0] = data_in[0];
  data_out[1] = data_in[1];
  return 2;
}

//
//-----------------------------------------------------------------------------
void publishCtlCsr(void) {
    DISABLE_INTERRUPTS;
    user_data.control = rCTL;
    user_data.status  = rSTAT;
    user_data.error   = rERR;
    ENABLE_INTERRUPTS;
}

//
//-------------------------------------------------------------
void setDAC(unsigned char ch, float v) {
  unsigned int ldac;

  ldac = (unsigned int) (v * VOLT2DAC);
 	LTC2607_Write(LTC2607_ADDR, LTC2607_WREG_n_UPDATE_n | ch, ldac);
}

//
//-------------------------------------------------------------
/*---- User loop function -----------------------------------*/
void user_loop(void)
{

//  UpdateDAC();
  // Command input (dac, on/off, rh)
  UpdateIO();

  // Vcc, uCTemp
  UpdateiADC();

  // Set Dac
  if (DAC1) setDAC(DAC1, user_data.vset);

  // Mark loop
  led_blink(LED_RED, 1, 50);
}

//
//-------------------------------------------------------------
void UpdateIO(void) {
  static char changed=0;
  if (cSw1) {
    if (sSw1) {
    } else {
    }
  }

  publishCtlCsr();
}

//
//-------------------------------------------------------------
void UpdateiADC(void) {
  unsigned int adc_raw;
  float xdata ucAdc;

  adc_raw = adc_read(ADC_INTERNAL_TEMP_CH, 1);
  ucAdc = (((float)(adc_raw)) * IADC2VOLT);
  ucAdc = (ucAdc - 0.9) / 0.00295;
  
  DISABLE_INTERRUPTS;
  user_data.rtemp  = adc_raw;
  user_data.ucTemp = ucAdc;
  ENABLE_INTERRUPTS;
}



