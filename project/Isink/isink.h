/********************************************************************\

  Name:     vertilon.h
  $Id$

\********************************************************************/

#ifndef _GENERIC_IO_H
#define _GENERIC_IO_H

#include "../../common/mscbemb.h"

//
#define CTL_IDX       2
#define FIRST_DAC     4

#define LTC2607_ADDR  0x10   // 16-bit Dual channel DAC
#define N_DAC         1
#define DACVREF       5.0f
#define VOLT2DAC      (65536.0f / DACVREF)

#define IVREF         2.23f
#define IADC2VOLT     (IVREF / 4096.0f)

// Global definition
// Global ON / OFF definition
#define ON     1
#define OFF    0
#define DONE   1
#define SET    1
#define OFF    0
#define CLEAR  0

#define LED_GREEN 1
#define LED_RED   0

//
//--- MSCB structure
typedef struct {
  // System
  unsigned long sn;
  unsigned int  error;
  unsigned char control;
  unsigned char status;
  
  // Functions
  float         vset;   // 1 ch DAC
  float         ucTemp;        // 410Temp

  // Raw data
  unsigned int rtemp;

} MSCB_USER_DATA;


void publishCtlCsr(void);
void publishErr(void);
void UpdateDAC(void);
void UpdateIO(void);
void UpdateiADC(void);
#endif // _GENERIC_IO.