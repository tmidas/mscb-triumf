/********************************************************************\

Name:     musr_spec 
Author:   PAA, DB   Sep/2012
$Id$
	LTC2499    16ch ADC  0x14 / 7
	LTC2485     1ch ADC  0x26 / 7
    ADT7483     2ch Temp 0x18 / 7
	LTC2605     8ch DAC  0x10 / 7
	LTC2606     1ch DAC  0x22 / 7
	Qpump PCA 
\
\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "musr_spec.h" 

extern xdata SYS_INFO sys_info;

char    code  MSCB_node_name[] = "MUSRSPEC";
char    xdata svn_rev_code[] = "$Rev: 226 $";
xdata         MSCB_USER_DATA user_data;
//-----------------------------------------------------------------------------
// User Data structure declaration
MSCB_INFO_VAR code vars[] = {
  4, UNIT_BYTE,     0, 0, 0,           "SerialN",   &user_data.SerialN,      // 0
  2, UNIT_BYTE,     0, 0, 0,           "Error"  ,   &user_data.error,        // 1
  1, UNIT_BYTE,     0, 0, 0,           "Control",   &user_data.control,      // 2
  1, UNIT_BYTE,     0, 0, 0,           "Status" ,   &user_data.status,       // 3

  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "vQpump",    &user_data.vQpump,       // 4  LTC2606
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "vBias",     &user_data.vbias,        // 5  LTC2485

  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Ich0"  ,   &user_data.ibias[0],     // 6 LTC2499
  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Ich1"  ,   &user_data.ibias[1],     // 7 
  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Ich2"  ,   &user_data.ibias[2],     // 8 
  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Ich3"  ,   &user_data.ibias[3],     // 9
  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Ich4"  ,   &user_data.ibias[4],     // 10
  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Ich5"  ,   &user_data.ibias[5],     // 11
  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Ich6"  ,   &user_data.ibias[6],     // 12
  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Ich7"  ,   &user_data.ibias[7],     // 13
  
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,        "vDAC0"   ,   &user_data.dac[0],      // 14  LTC2605
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,        "vDAC1"   ,   &user_data.dac[1],      // 15
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,        "vDAC2"   ,   &user_data.dac[2],      // 16
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,        "vDAC3"   ,   &user_data.dac[3],      // 17
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,        "vDAC4"   ,   &user_data.dac[4],      // 18
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,        "vDAC5"   ,   &user_data.dac[5],      // 19
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,        "vDAC6"   ,   &user_data.dac[6],      // 20
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,        "vDAC7"   ,   &user_data.dac[7],      // 21

  4, UNIT_BYTE,     0, 0, 0,                  "rADC0"   ,   &user_data.adc[0],      // 22  LTC2499
  4, UNIT_BYTE,     0, 0, 0,                  "rADC1"   ,   &user_data.adc[1],      // 23
  4, UNIT_BYTE,     0, 0, 0,                  "rADC2"   ,   &user_data.adc[2],      // 24
  4, UNIT_BYTE,     0, 0, 0,                  "rADC3"   ,   &user_data.adc[3],      // 25
  4, UNIT_BYTE,     0, 0, 0,                  "rADC4"   ,   &user_data.adc[4],      // 26
  4, UNIT_BYTE,     0, 0, 0,                  "rADC5"   ,   &user_data.adc[5],      // 27
  4, UNIT_BYTE,     0, 0, 0,                  "rADC6"   ,   &user_data.adc[6],      // 28
  4, UNIT_BYTE,     0, 0, 0,                  "rADC7"   ,   &user_data.adc[7],      // 29
  
  4, UNIT_BYTE,     0, 0, 0,                   "rBIAS"  ,   &user_data.qadc  ,      // 30

  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,        "CurGain",   &user_data.Igain,       // 31
    
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "T0int",     &user_data.intemp,      // 32
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "T00ext",    &user_data.temp[0],     // 33
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "T01ext",    &user_data.temp[1],     // 34

  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT|MSCBF_HIDDEN, "Och0"  ,   &user_data.obias[0],     // 35       
  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT|MSCBF_HIDDEN, "OCh1"  ,   &user_data.obias[1],     // 36
  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT|MSCBF_HIDDEN, "Och2"  ,   &user_data.obias[2],     // 37
  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT|MSCBF_HIDDEN, "Och3"  ,   &user_data.obias[3],     // 38
  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT|MSCBF_HIDDEN, "Och4"  ,   &user_data.obias[4],     // 49
  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT|MSCBF_HIDDEN, "Och5"  ,   &user_data.obias[5],     // 40
  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT|MSCBF_HIDDEN, "Och6"  ,   &user_data.obias[6],     // 41
  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT|MSCBF_HIDDEN, "Och7"  ,   &user_data.obias[7],     // 42
  1, UNIT_BYTE,    0, 0, MSCBF_HIDDEN,"Freq",      &user_data.freq,        // 43
  0
  };

code MSCB_INFO_VAR *variables = vars;

/********************************************************************\

Application specific init and inout/output routines

\********************************************************************/

// Change register
volatile unsigned char bdata bChange = 0;
sbit IO_Flag      = bChange ^ 0;
sbit QDAC         = bChange ^ 1;

// Control register
unsigned char bdata rCTL = 0;
sbit cQpup      = rCTL ^ 0;
sbit cOffEn     = rCTL ^ 1;
sbit cOffset    = rCTL ^ 2;
sbit cspare3    = rCTL ^ 3;
sbit cspare4    = rCTL ^ 4;
sbit cspare5    = rCTL ^ 5;
sbit cspare6    = rCTL ^ 6;
sbit CmSd       = rCTL ^ 7;

// Status Register
unsigned char bdata rCSR = 0;
sbit sQpup    = rCSR ^ 0;
sbit sOffEn   = rCSR ^ 1;
sbit sOffset  = rCSR ^ 2;
sbit sspare3  = rCSR ^ 3;
sbit sspare4  = rCSR ^ 4;
sbit sspare5  = rCSR ^ 5;
sbit sspare6  = rCSR ^ 6;
sbit sspare7  = rCSR ^ 7;

// DAC update register
unsigned char bdata rDAC = 0xFF;
sbit DAC0   = rDAC ^ 0;
sbit DAC1   = rDAC ^ 1;
sbit DAC2   = rDAC ^ 2;
sbit DAC3   = rDAC ^ 3;
sbit DAC4   = rDAC ^ 4;
sbit DAC5   = rDAC ^ 5;
sbit DAC6   = rDAC ^ 6;
sbit DAC7   = rDAC ^ 7;

// ESR Error Register
//The low and high bytes are switched in the bdata section of the memory
//This is the reason that the sbit declarations do not appear to match
//the documentation but they actually do.
unsigned int bdata rESR;
sbit eAlert     = rESR ^ 8;  //0x1
sbit eAdc       = rESR ^ 9;  //0x2

// unused bunch
sbit AV33mon   = rESR ^ 10; //0x4
sbit AV25mon   = rESR ^ 11; //0x8

sbit DV15mon   = rESR ^ 12; //0x10
sbit DV18mon   = rESR ^ 13; //0x20
sbit DV25mon   = rESR ^ 14; //0x40
sbit DV33mon   = rESR ^ 15; //0x80

sbit uCT       = rESR ^ 0;  //0x100
sbit FPGAssTT  = rESR ^ 1;  //0x200
sbit Vreg1ssTT = rESR ^ 2;  //0x400
sbit Vreg2ssTT = rESR ^ 3;  //0x800

sbit RdssT     = rESR ^ 4;  //0x1000
sbit EEPROM    = rESR ^ 5;  //0x2000
sbit AsumLock  = rESR ^ 6;  //0x4000 1= any of 4 not lock
sbit V4_OC     = rESR ^ 7;  //0x8000

// Hardware bits
sbit bAlert = P2 ^ 0;

/*---- User init function ------------------------------------------*/
void user_init(unsigned char init)
{
  char i;
  unsigned char temp;

  /* Format the SVN and store this code SVN revision into the system */
  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  sys_info.svn_revision = (svn_rev_code[6]-'0')*1000+
    (svn_rev_code[7]-'0')*100+
    (svn_rev_code[8]-'0')*10+
    (svn_rev_code[9]-'0');

  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  if (init) {
    // Initialize variables
    for (i=0;i<N_ADC;i++) user_data.adc [i] = 0;
    for (i=0;i<N_DAC;i++) user_data.dac [i] = 0.0;
    for (i=0;i<N_ADC;i++) user_data.ibias[i] = 0.0;
    for (i=0;i<N_ADC;i++) user_data.obias[i] = 0.0;
    user_data.vbias = 0.0;
    user_data.qadc  = 0;

    // Temperature initialization
    user_data.intemp = 0;
    memset (user_data.temp, 0, sizeof(user_data.temp));
  }

  //
  REF0CN = 0x00;  // Int Bias off, Int Temp off

  // 0:analog 1:digital
  P0MDIN = 0xFF; // P0 all digital pins
  P1MDIN = 0xFF; // P1 all digital pins 
  P2MDIN = 0xFF; // P2 all digital pins
 
  // 0: open-drain, 1:push-pull
  P0MDOUT = 0x94; // .7:RS485EN  .5:Rx .4:Tx .2:QPUMPCLK

  // 0: open-drain, 1:push-pull
  P1MDOUT = 0x00;  
  P1 = 0x00;

  // 0: open-drain, 1:push-pull
  P2MDOUT = 0x00;  // .0 ALERTn (temp)
  P2 = 0x00;

  // Setting the cross bar
  P0SKIP = 0x0; 
  P1SKIP = 0x0; 
  P2SKIP = 0x0; 

  XBR0 |=0x04;	 // Enable SMBus pins P0 .0/SDA .1/SCL

  /* set-up / initialize circuit components (order is important) */

  // Force IO update
  IO_Flag = 1;

  rCTL = 0;
  rCSR = 0;
  rESR = 0;

  UpdateIO();

  pca_Init();
  pca_FreqChange(freqTable[3]);  // Max frequency (1MHz)
  user_data.freq = freqTable[3];


  ADT7483_Init();  // Temperature
  LTC2499_Init();  // ADC 24b/8chDiff
  LTC2485_Init();  // ADC 24b/1ch

  LTC2605_Init();  // DAC 16b/1ch
  LTC2606_Init();  // DAC 16b/8ch


  // -50DegC to +150DegC  (non extended: 0DegC to +127DegC)
  ADT7483_WriteRegister(ADT7483_ADDR, ADT7483_WREG_CONF2, 0);
  ADT7483_WriteRegister(ADT7483_ADDR, ADT7483_WREG_CHSEL, ADT7483_CONV_TIME_125_MS);
  ADT7483_WriteRegister(ADT7483_ADDR, ADT7483_WREG_CONF1, ADT7483_EXTENDED_TEMP_RANGE);
//  ADT7483_WriteRegister(ADT7483_ADDR, ADT7483_WREG_CONF1, 

  // ADC Gain
  user_data.Igain = LTC2499_GAIN128;

  // Force qDAC setting
  QDAC = 1;
  // Force DAC vernier settings
  rDAC = 0xFF;

}

/*---- User write function -----------------------------------------*/
void user_write(unsigned char index) reentrant{

  if (index == CTL_IDX){ // Turn on/off Qpump
    if (user_data.control & 0x1) {
      cQpup = 1;
    }
    else if (user_data.control & 0x2) { // Enable Current offfset subtraction
      cOffEn = 1;
    }
    else if (user_data.control & 0x4) { // Copy current offset to offset array
      cOffset = 1;
    }
  }
  if ((index >= FIRST_DAC) && (index < (FIRST_DAC + (N_DAC)))) {
      rDAC |= (1 << (index - FIRST_DAC));
  } else if (index == FIRST_QDAC) {
      QDAC = 1;
  }
  if (index == FREQ_IDX)
      pca_FreqChange(user_data.freq);
//      pca_FreqChange(freqTable[user_data.freq]);

  return;
}

/*---- User read function ------------------------------------------*/
unsigned char user_read(unsigned char index)
{
  if(index);
  return 0;
}

/*---- User function called vid CMD_USER command -------------------*/
unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
  // echo input data
  data_out[0] = data_in[0];
  data_out[1] = data_in[1];
  return 2;
}

/*---- User loop function ------------------------------------------*/
void user_loop(void)
{
  char i;

  UpdateADC();
  UpdateDAC();
  UpdateIO();
//  delay_ms(100);
  UpdateQADC();
//  delay_ms(300);
  UpdateQDAC();
  UpdateTemperature();
//  led_blink(LED_GREEN, 1, 50);

  delay_ms(150); // ADC takes 149.9 ms to convert maximum.
}


//-----------------------------------------------------------------------------
//
void publishCtlCsr(void) {
    DISABLE_INTERRUPTS;
    user_data.control = rCTL;
    user_data.status  = rCSR;
    ENABLE_INTERRUPTS;
}

#if 0
//-----------------------------------------------------------------------------
//
void publishErr(bit errbit) {
    DISABLE_INTERRUPTS;
    errbit = SET;
    user_data.error   = rESR;
    ENABLE_INTERRUPTS;
}

//-----------------------------------------------------------------------------
//
void publishAll() {
  user_data.control = rCTL;
  user_data.status  = rCSR;
  user_data.error   = rESR;
  ENABLE_INTERRUPTS;
}
#endif

//
//-------------------------------------------------------------
void UpdateIO(void) {
  char i;

  if (cQpup) {
    if (sQpup) {
     pca_Operation(PCA_INTERNAL_OFF);
     cQpup = 0; sQpup = 0;
    } else {
     pca_Operation(PCA_INTERNAL_ON);
     cQpup = 0; sQpup = 1;
    }
  }

  if (cOffEn) {
    if (sOffEn) {
      sOffEn = 0; cOffEn = 0; 
    } else {
      sOffEn = 1; cOffEn = 0; 
    }
  }
  
  if (cOffset) {
    for (i=0;i<N_ADC;i++) {
      user_data.obias[i] = user_data.ibias[i];
    }
    sOffset = 1; cOffset = 0;
  }

  // Temperature alert
  if (bAlert) {  // bAlertN
    eAlert = 0;
  } else {
    eAlert = 1;    
  }
//  if (changed) {
//    publishErr(eAlert);
    publishCtlCsr();
//    publishAll();
//  }
}

//
//-------------------------------------------------------------
char chmap[8] = {0, 1, 2, 3, 4, 5, 6, 7};
void UpdateADC(void) {
#ifdef MSCB_DEVICE_ADC_LTC2499_DISABLE
#else
  static char adcChannel = N_ADC_CHANNEL + 1;
  char prevChannel;
  unsigned char range;
  static xdata signed long adc_raw;

  if(adcChannel == (N_ADC_CHANNEL + 1)) {
    adcChannel = CLEAR;
      LTC2499_StartConversion(LTC2499_ADDR, LTC2499_CMD_DIF, user_data.Igain, adcChannel);
  } else {
    prevChannel = adcChannel;
    adcChannel++;
    adcChannel %= N_ADC_CHANNEL;

    range = LTC2499_ReadConversion(LTC2499_ADDR, LTC2499_CMD_DIF, user_data.Igain , adcChannel, &adc_raw);
      
  {   switch(range) {
      case LTC2499_VALIDRANGE:
        adc_raw += ADC_ZERO_OFFSET;
        eAdc = 0;
        break;

      case LTC2499_OVERRANGE:
        eAdc = 1;
        adc_raw = 0xFFFFFFFF;
        break;

      case LTC2499_UNDERRANGE:
        eAdc = 1;
        adc_raw = 0xFFFFFFFF;
        break;
    }
  }  
    DISABLE_INTERRUPTS;
    if (prevChannel < 8) {
        user_data.ibias[chmap[prevChannel]] = ((float)(adc_raw) * (float)(A2VCONV24));
        user_data.ibias[chmap[prevChannel]] *= 1.0E5;  // 1.0E6 for 1K
//        if (sOffEn) user_data.ibias[chmap[prevChannel]] -= user_data.obias[chmap[prevChannel]];
    }

    user_data.adc[chmap[prevChannel]]  = adc_raw;  // +/- Range 
    ENABLE_INTERRUPTS;

  }
#endif // MSCB_DEVICE_ADC_LTC2489_DISABLE
}

//
//-------------------------------------------------------------
void UpdateDAC(void) {
#ifdef MSCB_DEVICE_DAC_LTC2605_DISABLE
#else
  char i;
  unsigned int dacval;

  // Set all necessary DAC values, if an interrupt occurs while this is going on
  // everything should be OK, at worst we may update a newer value.
  for (i=0;i<N_DAC;i++) {
    if (rDAC & (1<<i)) {
      led_blink(LED_RED, 1, 50);
	  dacval =  (user_data.dac[i] * V2DCONV12);
	  dacval <<= 4;
      LTC2605_Write(LTC2605_ADDR
                  , LTC2605_WREG_n_UPDATE_n | chmap[i], dacval);
                  ; rDAC &= ~(1<<i);
      }
 }
  
#endif // MSCB_DEVICE_DAC_LTC2605_DISABLE
}

//
//-------------------------------------------------------------
void UpdateQADC(void) {
#ifdef MSCB_DEVICE_ADC_LTC2485_DISABLE
#else
  unsigned long adc_raw;
  unsigned char vrange;
    vrange = LTC2485_ReadConversion(LTC2485_ADDR, LTC2485_CMD_EXT_INPUT, &adc_raw);
    if (vrange == LTC2485_VALIDRANGE) {
	  DISABLE_INTERRUPTS;
      user_data.vbias = (float)((adc_raw * A2VCONV31 * 101.00) - 1.73); //- 2.0652) ;
      user_data.qadc = adc_raw;  // +/- Range 
      ENABLE_INTERRUPTS;
     } else {
	  DISABLE_INTERRUPTS;
      user_data.qadc = adc_raw;  // +/- Range 
      ENABLE_INTERRUPTS;
	 }
#endif // MSCB_DEVICE_ADC_LTC2485_DISABLE
}

//
//-------------------------------------------------------------
void UpdateQDAC(void) {
#ifdef MSCB_DEVICE_DAC_LTC2606_DISABLE
#else
  unsigned long dac;
  if(QDAC) {
  if (user_data.vQpump < 0.) {
      DISABLE_INTERRUPTS;
      user_data.vQpump = 0.;
      ENABLE_INTERRUPTS;
      }
    if (user_data.vQpump > 4.4995) {
      DISABLE_INTERRUPTS;
      user_data.vQpump = 4.4995;
      ENABLE_INTERRUPTS;
      }
    dac = (unsigned long)(user_data.vQpump * V2DCONV216);
    LTC2606_Write(LTC2606_ADDR, LTC2606_WREG_n_UPDATE_n , dac);
    QDAC = 0;
  }

#endif // MSCB_DEVICE_DAC_LTC2606_DISABLE
}

//
//-------------------------------------------------------------
void UpdateTemperature(void) {
  float temp;

  temp = ADT7483_ReadTemp(ADT7483_ADDR, ADT7483_CH_READ_LOCAL);

  DISABLE_INTERRUPTS;
  user_data.intemp  = temp; 
  ENABLE_INTERRUPTS;
  
  temp = ADT7483_ReadTemp(ADT7483_ADDR, ADT7483_CH_READ_REMOTE1);

  DISABLE_INTERRUPTS;  
  user_data.temp[0] = temp;
  ENABLE_INTERRUPTS;

  temp = ADT7483_ReadTemp(ADT7483_ADDR, ADT7483_CH_READ_REMOTE2);
  
  DISABLE_INTERRUPTS;
  user_data.temp[1] = temp;  
  ENABLE_INTERRUPTS;
}