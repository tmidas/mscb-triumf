/********************************************************************\

Name:     i2cflasher.c
Author:   P-A Amaudruz
       I2C tool for flshing Griffin chip
       Project Griffin
$Id$

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "i2cflasher.h" 
extern xdata SYS_INFO sys_info;

char    code  MSCB_node_name[] = "i2cflasher410";
char    xdata svn_rev_code[] = "$Rev$";
xdata         MSCB_USER_DATA user_data;

MSCB_INFO_VAR code vars[] = {
  4, UNIT_BYTE,     0, 0, 0,           "SerialN",    &user_data.sn,          // 0
  2, UNIT_BYTE,     0, 0, 0,           "Error",      &user_data.error,       // 1
  1, UNIT_BYTE,     0, 0, 0,           "Control",    &user_data.control,     // 2
  1, UNIT_BYTE,     0, 0, 0,           "Status",     &user_data.status,      // 3

  2, UNIT_BYTE,     0, 0, 0,           "Dac0",       &user_data.Dac[0],     // 4 
  2, UNIT_BYTE,     0, 0, 0,           "Dac1",       &user_data.Dac[1],     // 5 
  2, UNIT_BYTE,     0, 0, 0,           "Dac2",       &user_data.Dac[2],     // 6 
  2, UNIT_BYTE,     0, 0, 0,           "Dac3",       &user_data.Dac[3],     // 7 
  
  // F410 internal ADCs
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "uCT",        &user_data.ucTemp,      // 18
  0
};

code MSCB_INFO_VAR *variables = vars;

/********************************************************************\

Application specific init and inout/output routines

\********************************************************************/

// Change register
volatile unsigned char bdata bChange = 0;
sbit fDac0 = bChange ^ 0;
sbit fDac1 = bChange ^ 1;
sbit fDac2 = bChange ^ 2;
sbit fDac3 = bChange ^ 3;
sbit fRange= bChange ^ 4;

// Control register
unsigned char bdata rCTL = 0;
sbit cRange    = rCTL ^ 0;  // Load pattern 
sbit cmTrig    = rCTL ^ 1;  // Manual Trigger LED
sbit cpTrig    = rCTL ^ 2;  // Enable Frq Trigger LED
sbit cxTrig    = rCTL ^ 3;  // Enable External Trigger LED

// Status Register
unsigned char bdata rSTAT = 0;
sbit sRange   = rSTAT ^ 0;
sbit smTrig   = rSTAT ^ 1;
sbit spTrig   = rSTAT ^ 2;
sbit sxTrig   = rSTAT ^ 3;

// ERR Error Register
//The low and high bytes are switched in the bdata section of the memory
//This is the reason that the sbit declarations do not appear to match
//the documentation but they actually do.
unsigned int bdata rERR = 0;
sbit eDac0 = rERR ^ 8;  //0x1  
sbit eDac1 = rERR ^ 9;  //0x2
sbit eDac2 = rERR ^ 10; //0x4
sbit eDac3 = rERR ^ 11; //0x8

sbit eNa12 = rERR ^ 12;  //0x10 
sbit eNa13 = rERR ^ 13;  //0x20 
sbit eNa16 = rERR ^ 14;  //0x40 
sbit eNa15 = rERR ^ 15;  //0x80 

sbit eNa0  = rERR ^ 0;   //0x100
sbit eNa1  = rERR ^ 1;   //0x200
sbit eNa2  = rERR ^ 2;   //0x400
sbit eNa3  = rERR ^ 3;   //0x800

sbit eNa4  = rERR ^ 4;   //0x1000
sbit eNa5  = rERR ^ 5;   //0x2000
sbit eNa6  = rERR ^ 6;   //0x4000
sbit eNa7  = rERR ^ 7;   //0x8000

sbit Rbit  = bRange;

// Hardware bits
// P0 .7:Sp11   .6:Tx/En   .5:Rx,     .4:Tx,   .3:Sp9    .2:Sp10   .1:SCL   .0 SDA
// P1 .7:Sp12   .6:Sp2     .5:LED1    .4:Sp1   .3:Sp15   .2:LED2   .1:Sp7    0:Sp6    
// P2 .7:C2D    .6:RBit    .5:CS0     .4:SCK   .3:MOSI   .2:MISO   .1:Sp5   .0:Sp4

/*---- User init function ------------------------------------------*/
void user_init(unsigned char init)
{
  char i;
  unsigned char temp;

  /* Format the SVN and store this code SVN revision into the system */
  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  sys_info.svn_revision = (svn_rev_code[6]-'0')*1000+
    (svn_rev_code[7]-'0')*100+
    (svn_rev_code[8]-'0')*10+
    (svn_rev_code[9]-'0');

  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

//  if (init) {
    // Initialize variables
    user_data.ucTemp = 0;
    user_data.Dac[0] = 0x7FFF;
    user_data.Dac[1] = 0x7FFF;
    user_data.Dac[2] = 0x7FFF;
    user_data.Dac[3] = 0x7FFF;
//  }

  // Reference Voltage on P0.0 Enable
  //  REF0CN = 0x00;  // Int Bias off, Int Temp off, VREF input on

  // 0:analog 1:digital
  P0MDIN = 0xFF; // P0 all digital pins 
  P1MDIN = 0xFF; // P1 all digital pins
  P2MDIN = 0xFF; // P2 all digital pins
 
  // 0: open-drain, 1:push-pull
// P0 .7:Sp11   .6:Tx/En   .5:Rx,     .4:Tx,   .3:Sp9    .2:Sp10   .1:SCL   .0 SDA
  P0MDOUT = ~(0x21);  //   OD: Rx, SDA

  // 0: open-drain, 1:push-pull
  // P1 .7:Sp12   .6:Sp2     .5:LED1    .4:Sp1   .3:Sp15   .2:LED2   .1:Sp7    0:Sp6    
  P1MDOUT = 0x00;  

  // 0: open-drain, 1:push-pull
// P2 .7:C2D    .6:RBit    .5:CS0     .4:SCK   .3:MOSI   .2:MISO   .1:Sp5   .0:Sp4
  P2MDOUT = 0xFF;  // 

  // Setting the cross bar
  P0SKIP = 0x0; 
  P1SKIP = 0x0; 
  P2SKIP = 0x0; 

  
  XBR0 |=0x04;	 // Enable SMBus pins P0 .0/SDA .1/SCL
  // CPLD clock input 25MHz
  // XBR0 |=0x08;	 // Enable SYSCLK to Port 1.2

  rCTL  = 0;
  rSTAT = 0;
  rERR  = 0;
   
  UpdateIO();

//  Rbit = 1;
  adc_internal_init(0x1C);  //  0(1.5V)/1(2.2V)--Vdd-TEMPE-0-nREFBE

  // Dac Digital 
  LTC2655_Init();

  // Update Dac to current value
  fDac0 = fDac1 = fDac2 = fDac3 = 1;

  UpdateDac();
}

/*---- User write function -----------------------------------------*/
void user_write(unsigned char index) reentrant {

  // Set the appropriate DAC update register bit
  // Done this way so we if we can take being interrupted, and avoid a
  // read-write-modify setup. (if done using a byte)
  if (index == CTL_IDX){
    if (user_data.control & 0x01) { cRange = 1; } 
    if (user_data.control & 0x02) { cmTrig = 1; } 
    if (user_data.control & 0x04) { cpTrig = 1; } 
    if (user_data.control & 0x08) { cxTrig = 1; } 
  } 
 
  if (index == DAC0) { fDac0 = 1; }
  if (index == DAC1) { fDac1 = 1; }
  if (index == DAC2) { fDac2 = 1; }
  if (index == DAC3) { fDac3 = 1; }

  return;
}

/*---- User read function ------------------------------------------*/

unsigned char user_read(unsigned char index)
{
  if(index);
  return 0;
}

/*---- User function called via CMD_USER command -------------------*/

unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
  // echo input data
  data_out[0] = data_in[0];
  data_out[1] = data_in[1];
  return 2;
}

//
//-----------------------------------------------------------------------------
void publishCtlCsr(void) {
    DISABLE_INTERRUPTS;
    user_data.control = rCTL;
    user_data.status  = rSTAT;
    user_data.error   = rERR;
    ENABLE_INTERRUPTS;
}

//
//-------------------------------------------------------------
unsigned char setDac(unsigned char chip, unsigned char ch, unsigned int v) {
  unsigned char value=0;

  LTC2655_Write(chip, ch, v);
  if (v != value) return 1;
  return 0;
}


//
//-------------------------------------------------------------
void UpdateDac(void) {

  if (fDac0 || fDac1 || fDac2 || fDac3) {
    led_blink(LED_RED, 1, 50);
  }
  
  // Set Dac
  if (fDac0) {
     fDac0 = 0;
     eDac0 = setDac(LTC2655_ADDR1, LTC2655_WREG_n_UPDATE_n | 0, user_data.Dac[0]);
  }
   if (fDac1) {
     fDac1 = 0;
     eDac1 = setDac(LTC2655_ADDR1, LTC2655_WREG_n_UPDATE_n | 1, user_data.Dac[1]);
  }
   if (fDac2) {
     fDac2 = 0;
     eDac2 = setDac(LTC2655_ADDR1, LTC2655_WREG_n_UPDATE_n | 2, user_data.Dac[2]);
  }
   if (fDac3) {
     fDac3 = 0;
     eDac3 = setDac(LTC2655_ADDR1, LTC2655_WREG_n_UPDATE_n | 3, user_data.Dac[3]);
  }

  DISABLE_INTERRUPTS;
  user_data.error   = rERR;
  ENABLE_INTERRUPTS;
}

//
//-------------------------------------------------------------
/*---- User loop function -----------------------------------*/
void user_loop(void)
{

  // Command input (dac, on/off, rh)
  UpdateIO();

  // uCTemp
  UpdateiADC();

  UpdateDac();

  delay_ms(1);

  // Mark loop
//  led_blink(LED_RED, 1, 50);
}

//
//-------------------------------------------------------------
void UpdateIO(void) {
  static char changed=0;

  // Toggle External
  if (cRange) { 
    cRange = 0;
    if (sRange) {
      Rbit = 0;
      sRange = 0;
    } else {
      Rbit = 1;
      sRange = 1;
    } 
  }

  publishCtlCsr();
}

//
//-------------------------------------------------------------
void UpdateiADC(void) {
  unsigned int adc_raw;
  float xdata ucAdc;

  adc_raw = adc_read(ADC_INTERNAL_TEMP_CH, 1);
  ucAdc = (((float)(adc_raw)) * IADC2VOLT);
  ucAdc = (ucAdc - 0.9) / 0.00295;
  
  DISABLE_INTERRUPTS;
  user_data.ucTemp = ucAdc;
  ENABLE_INTERRUPTS;
}



