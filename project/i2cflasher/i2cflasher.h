/********************************************************************\

  Name:     i2cflasher.h
  $Id$

\********************************************************************/

#ifndef _GENERIC_IO_H
#define _GENERIC_IO_H

#include "../../common/mscbemb.h"

//
#define CTL_IDX    2
#define DAC0       4
#define DAC1       5
#define DAC2       6
#define DAC3       7

#define IVREF         2.47f
#define IADC2VOLT     (IVREF / 4096.0f)

// DAC chip Address
#define LTC2655_ADDR1  0x10 

// Global definition
// Global ON / OFF definition
#define ON     1
#define OFF    0
#define DONE   1
#define SET    1
#define OFF    0
#define CLEAR  0

#define LED_GREEN 1
#define LED_RED   0

// Hardware bits
// P0 .7:Sp11   .6:Tx/En   .5:Rx,     .4:Tx,   .3:Sp9    .2:Sp10   .1:SCL   .0 SDA
// P1 .7:Sp12   .6:Sp2     .5:LED1    .4:Sp1   .3:Sp15   .2:LED2   .1:Sp7    0:Sp6    
// P2 .7:C2D    .6:RBit    .5:CS0     .4:SCK   .3:MOSI   .2:MISO   .1:Sp5   .0:Sp4

//
//--- MSCB structure
typedef struct {
  // System
  unsigned long sn;
  unsigned int  error;
  unsigned char control;
  unsigned char status;
  
  // Functions
  unsigned int  Dac[4];
  unsigned int  periodic;
  unsigned int  duration;
  float         ucTemp;        // 410Temp

} MSCB_USER_DATA;


void publishCtlCsr(void);
void publishErr(void);
void UpdateIO(void);
void UpdateDac(void);
void UpdateiADC(void);
#endif // _GENERIC_IO.