/********************************************************************\

  Name:     vertilon.h
  $Id$

\********************************************************************/

#ifndef _GENERIC_IO_H
#define _GENERIC_IO_H

#include "../../common/mscbemb.h"

//
#define CTL_IDX       2
#define FIRST_DAC     4
#define RHON         23

#define LTC2607_ADDR  0x10   // 16-bit Dual channel DAC
#define N_DAC         2
#define DACVREF       5.0f
#define HVMAX         1000.f
#define VOLT2DAC      (65535.0f / DACVREF)
#define HV2VOLT       (DACVREF / HVMAX)

#define IVREF         2.23f
#define CPU_ADC_CH    3
#define IADC2VOLT     (IVREF / 4096.0f)

#define N_TUI         4
#define LTC2990_ADDR  0x4C   // Temp, Voltage, Current monitoring
#define ADCVREF       5.0f
#define ADC2VOLT      (ADCVREF / 16384.f)  // 14bits + sign
#define VOLT2HV       (HVMAX / ADCVREF)
#define A2MA          1000.f * 0.1	// 5V/500uA

#define MAXHV           1000.f
#define MINHV            200.f
#define HV_VALIDRANGE   0
#define HV_INVALIDRANGE 1

// Global definition
// Global ON / OFF definition
#define ON     1
#define OFF    0
#define DONE   1
#define SET    1
#define OFF    0
#define CLEAR  0

#define LED_GREEN 1
#define LED_RED   0

#define  NAVGE  5 // # of elements to average
#define  NPOINT 10
typedef struct {
  unsigned char index, ten;
  float         array[NPOINT];
  float         sum;
  float         result;
} AVERAGE;

//
//--- MSCB structure
typedef struct {
  // System
  unsigned long sn;
  unsigned int  error;
  unsigned char control;
  unsigned char status;
  
  // Functions
  float         vset[N_DAC];   // 2 channels DAC
  float         uimon[N_TUI];  // U, I, extT, intT
  float         ucAdc;         // Vcc mon
  float         ucTemp;        // 410Temp

  float         ShtTemp;       // SHTTemp
  float         ShtHumid;      // SHTHumidity

  // Raw data
  unsigned int tui[N_TUI];
  unsigned int radc;
  unsigned int rtemp;
  unsigned int rShtTemp, rShtHumid;
  float        ramp_up, ramp_down;
  unsigned char  SHT_on; 

} MSCB_USER_DATA;

xdata         AVERAGE avge[NAVGE];

float xdata   vsetMirror;
float xdata   u_actual;
unsigned long xdata t_ramp;

void publishCtlCsr(void);
void publishErr(void);
void UpdateADC(void);
void UpdateiADC(void);
void UpdateDAC(void);
void UpdateIO(void);
void UpdateTUI(void);
void UpdateSHT(void);
void switchHV(unsigned char sw);
void setHV(unsigned char ch, float hv);
void rampHV(void);
void doAverage(unsigned char idx, float value);
unsigned char checkHV(void);
#endif // _GENERIC_IO.