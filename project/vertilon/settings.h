// $Id$


#ifndef _MSCB_PROJECT_H
#define _MSCB_PROJECT_H

#define LED_0 P2 ^ 0
#define LED_1 P2 ^ 2
#define LED_ON 1
#define MCU_C8051F410
#define RS485_ENABLE_PIN P0 ^ 7

#define SHT_DATA1    P1 ^ 4
#define SHT_SCK1     P1 ^ 5

// MSCB Constants
#define MSCB_NUM_SUB_NODES 1

// Used Protocols
#define MSCB_SMBus_HW
#define MSCB_SHT7x

// Used Devices
#define MSCB_DEVICE_DAC_LTC2607
#define MSCB_DEVICE_TUI_LTC2990
#define MSCB_DEVICE_ADC_INTERNAL
#define MSCB_DEVICE_HUMIDITY_SHT7x

// Disabled Devices
//#define MCSB_DEVICE_ADC_LTC2493_DISABLE
//#define MSCB_DEVICE_ADC_LTC2489_DISABLE 
//#define MSCB_DEVICE_DAC_LTC2605_DISABLE 
//#define MSCB_DEVICE_IO_PCA9536_DISABLE 
//#define MSCB_DEVICE_TEMP_ADT7483_DISABLE
//#define MSCB_DEVICE_INTERNAL_PCA_DISABLE
//#define MCSB_DEVICE_TUI_LTC2990_DISABLE
//#define MSCB_DEVICE_HUMIDITY_SHT7x_DISABLE
#endif // _MSCB_PROJECT_H
