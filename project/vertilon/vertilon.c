/********************************************************************\

Name:     vertilon.c
Author:   P-A Amaudruz
       Single channel HV control/monitor for the Vertilon board
       Project Geotomo, Cript
$Id$

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vertilon.h" 
extern xdata SYS_INFO sys_info;

char    code  MSCB_node_name[] = "Vertilon410";
char    xdata svn_rev_code[] = "$Rev$";
xdata         MSCB_USER_DATA user_data;

MSCB_INFO_VAR code vars[] = {
  4, UNIT_BYTE,     0, 0, 0,           "SerialN",    &user_data.sn,          // 0
  2, UNIT_BYTE,     0, 0, 0,           "Error",      &user_data.error,       // 1
  1, UNIT_BYTE,     0, 0, 0,           "Control",    &user_data.control,     // 2
  1, UNIT_BYTE,     0, 0, 0,           "Status",     &user_data.status,      // 3

  // LTC2607 Dual-DAC 16bits
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "HVSet1",     &user_data.vset[0],     // 4
  4, UNIT_VOLT,     0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Vspare", &user_data.vset[1],     // 5 

  // LTC2990 Quad-ADC 14bits
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "HVVmon",     &user_data.uimon[0],    // 6
  4, UNIT_AMPERE,   PRFX_MICRO, 0, MSCBF_FLOAT, "HVImon",  &user_data.uimon[1],    // 7 
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "Tremote",    &user_data.uimon[2],    // 8
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "Tlocal" ,    &user_data.uimon[3],    // 9
  
  // F410 internal ADCs
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "VCCmon",     &user_data.ucAdc,       // 10
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "uCT",        &user_data.ucTemp,      // 11

  // SHT75 RH/T chip
  4, UNIT_CELSIUS,   0, 0, MSCBF_FLOAT, "SHTtemp",   &user_data.ShtTemp,     // 12
  4, UNIT_PERCENT,   0, 0, MSCBF_FLOAT, "RHumid",    &user_data.ShtHumid,    // 13

  // LTC2990 Quad-ADC Raw Data
  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,  "rTUIv",    &user_data.tui[0],      // 14
  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,  "rTUTi",    &user_data.tui[1],      // 15
  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,  "rTUIt",    &user_data.tui[2],      // 16 

  // F410 internal ADCs Raw Data
  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,  "rVCCmon",    &user_data.radc,        // 17
  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,  "rTuC",       &user_data.rtemp,       // 18
  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,  "rSHTemp",    &user_data.rShtTemp,    // 19
  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,  "rRHumid",    &user_data.rShtHumid,   // 20
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,   "RampUp",     &user_data.ramp_up,     // 21
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,   "RampDown",   &user_data.ramp_down,   // 22
  1, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,  "SHTon",      &user_data.SHT_on,      // 23
  0
};

code MSCB_INFO_VAR *variables = vars;

/********************************************************************\

Application specific init and inout/output routines

\********************************************************************/

// Change register
volatile unsigned char bdata bChange = 0;
sbit fHumid   = bChange ^ 0;
sbit fLowHV   = bChange ^ 1;
sbit fHighHV  = bChange ^ 2;
sbit fsetHV   = bChange ^ 3;
sbit fRamp    = bChange ^ 4;

// Control register
unsigned char bdata rCTL = 0;
sbit cSw1     = rCTL ^ 0;
sbit cAvge    = rCTL ^ 1;
sbit cSpare2  = rCTL ^ 2;
sbit CSpare3  = rCTL ^ 3;  // 0x08
sbit CSpare4  = rCTL ^ 4;  // 0x10
sbit CSpare5  = rCTL ^ 5;
sbit CSpare6  = rCTL ^ 6;
sbit CSpare7  = rCTL ^ 7;

// Status Register
unsigned char bdata rSTAT = 0;
sbit sSw1      = rSTAT ^ 0;
sbit sAvge     = rSTAT ^ 1;
sbit sSpare2   = rSTAT ^ 2;
sbit sSpare3   = rSTAT ^ 3;
sbit sSpare4   = rSTAT ^ 4;
sbit sSpare5   = rSTAT ^ 5;
sbit sRampUp   = rSTAT ^ 6;
sbit sRampDwn  = rSTAT ^ 7;

// ERR Error Register
//The low and high bytes are switched in the bdata section of the memory
//This is the reason that the sbit declarations do not appear to match
//the documentation but they actually do.
unsigned int bdata rERR =0;
sbit eTUITinvalid = rERR ^ 8;  //0x1  
sbit eTUIUinvalid = rERR ^ 9;  //0x2
sbit eTUIIinvalid = rERR ^ 10; //0x4
sbit eTUIXinvalid = rERR ^ 11; //0x8

sbit eTUIXshort   = rERR ^ 12; //0x10 
sbit eTUIXopen    = rERR ^ 13; //0x20 
sbit eSpare6  = rERR ^ 14;  //0x40 
sbit eSpare7  = rERR ^ 15;  //0x80 

sbit eSpare8  = rERR ^ 0;   //0x100
sbit eSpare9  = rERR ^ 1;   //0x200
sbit eSpare10 = rERR ^ 2;   //0x400
sbit eSpare11 = rERR ^ 3;   //0x800

sbit eSpare12 = rERR ^ 4;   //0x1000
sbit eSpare13 = rERR ^ 5;   //0x2000
sbit eHVoff   = rERR ^ 6;   //0x4000
sbit eHVinvalid= rERR ^ 7;  //0x8000

// DAC update register
unsigned char bdata rDAC = 0xFF;
sbit DAC0     = rDAC ^ 0;
sbit DAC1     = rDAC ^ 1;
sbit HUMID    = rDAC ^ 2;

// Hardware bits
// P0 .7:Tx/En  .6:n/a,    .5:Rx,   .4:Tx,    .3:ExtVrbk,   .2:n/a,  .1: SCK, .0 SDA

// P1 .7:CPU sp1 .6:Rh/TVdd  .5:Rh/TClk .4:Rh/TData .3:HV_en  .2:Vref  .1:n/a, .0:CPUsp2
sbit bhvEn      = P1 ^ 3;
sbit bcpusp1    = P1 ^ 7;
sbit bcpusp2    = P1 ^ 0;

sbit bRhTVdd    = P1 ^ 6;
sbit bRhTClk    = P1 ^ 5;
sbit bRhTData   = P1 ^ 4;

// P2 .7:C2D    6:n/a  .5:n/a  .4:n/a  .3:n/a  .2:LED2  .1:n/a  .0:LED1
sbit bLED2      = P2 ^ 2;
sbit bLED1      = P2 ^ 0;

/*---- User init function ------------------------------------------*/
void user_init(unsigned char init)
{
  char i;
  unsigned char temp;

  /* Format the SVN and store this code SVN revision into the system */
  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  sys_info.svn_revision = (svn_rev_code[6]-'0')*1000+
    (svn_rev_code[7]-'0')*100+
    (svn_rev_code[8]-'0')*10+
    (svn_rev_code[9]-'0');

  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  if (init) {
    // Initialize variables
    for (i=0;i<N_DAC;i++) user_data.vset[i] = 0;
    for (i=0;i<N_TUI;i++) user_data.tui[i] = 0;
    user_data.ucTemp = 0;	// Temperature initialization
    user_data.ramp_up = MAXHV;
    user_data.ramp_down = MAXHV;

  }

  // Reference Voltage on P0.0 Enable
//  REF0CN = 0x00;  // Int Bias off, Int Temp off, VREF input on

  // 0:analog 1:digital
  P0MDIN = 0xF7; // P0 all digital pins  Vrbk
  P1MDIN = 0xFF; // P1 all digital pins
  P2MDIN = 0xFF; // P2 all digital pins
 
  // 0: open-drain, 1:push-pull
  // P0 .7:Tx/En  .6:n/a,    .5:Rx,   .4:Tx,    .3:ExtVrbk,   .2:n/a,  .1: SCK, .0 SDA
  P0MDOUT = 0xD2;  //   OD: Rx, SDA, ExtVrbk

  // 0: open-drain, 1:push-pull
  // P1 .7:CPU sp1 .6:Rh/TVdd  .5:Rh/TClk .4:Rh/TData .3:HV_en  .2:Vref  .1:n/a, .0:CPUsp2
  P1MDOUT = 0xEF;  //  RhTData 

  // 0: open-drain, 1:push-pull
  // P2 .7:C2D    6:n/a  .5:n/a  .4:n/a  .3:n/a  .2:LED2  .1:n/a  .0:LED1
  P2MDOUT = 0xFF;  // 

  // Setting the cross bar
  P0SKIP = 0x0; 
  P1SKIP = 0x0; 
  P2SKIP = 0x0; 

  // Reset HV enable
  for (i=0;i<N_DAC;i++) user_data.vset[i] = 0;
  bhvEn = 0;  
  vsetMirror = 0.0;
  
  memset ((char *) &(avge[0].index), 0, NAVGE * sizeof(AVERAGE));

  XBR0 |=0x04;	 // Enable SMBus pins P0 .0/SDA .1/SCL
  // CPLD clock input 25MHz
  XBR0 |=0x08;	 // Enable SYSCLK to Port 1.2

  rCTL  = 0;
  rSTAT = 0;
  rERR  = 0;
  publishCtlCsr();
   
  UpdateIO();

  LTC2607_Init();
  LTC2990TUI_Init();
  adc_internal_init(0x15);  //  0(1.5V)/1(2.2V)-0-TEMPE-0-REFBE

 	setHV(0, user_data.vset[0]);
  setHV(1, user_data.vset[1]);
  u_actual = 0;

// Humidity sensor initialization
  user_data.ShtTemp = 0;
  user_data.ShtHumid = 0;
  user_data.rShtTemp = 0;
  user_data.rShtHumid = 0;
}

/*---- User write function -----------------------------------------*/
void user_write(unsigned char index) reentrant {

  // Set the appropriate DAC update register bit
  // Done this way so we if we can take being interrupted, and avoid a
  // read-write-modify setup. (if done using a byte)
  if (index == CTL_IDX){
    if (user_data.control & 0x01) { cSw1 = 1; } 
    if (user_data.control & 0x02) { cAvge = 1; }
  } 
 
  if (index == RHON) { HUMID = 1; }

  if ((index >= FIRST_DAC) && (index < (FIRST_DAC + (N_DAC)))) {
    switch(index - FIRST_DAC) {
      case 0:
        DAC0 = 1;
        fsetHV = ON;
        break;

      case 1:
        DAC1 = 1;
        break;
    }
  }

  return;
}

/*---- User read function ------------------------------------------*/

unsigned char user_read(unsigned char index)
{
  if(index);
  return 0;
}

/*---- User function called vid CMD_USER command -------------------*/

unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
  // echo input data
  data_out[0] = data_in[0];
  data_out[1] = data_in[1];
  return 2;
}

//
//-----------------------------------------------------------------------------
void publishCtlCsr(void) {
    DISABLE_INTERRUPTS;
    user_data.control = rCTL;
    user_data.status  = rSTAT;
    user_data.error   = rERR;
    ENABLE_INTERRUPTS;
}

//
//-----------------------------------------------------------------------------
void publishErr(void) {
    DISABLE_INTERRUPTS;
    user_data.error   = rERR;
    ENABLE_INTERRUPTS;
}

//
//-------------------------------------------------------------
void doAverage(unsigned char idx, float value) {

  unsigned char index, i;
  avge[idx].index += 1;
  index = avge[idx].index;
  if ((index % 10) == 0) { avge[idx].index = 0; index = 0; avge[idx].ten = 1;}
//  avge[idx].sum -= avge[idx].array[index];
//  avge[idx].sum += value;
  avge[idx].array[index] = value;
  avge[idx].sum = 0.0;  for (i=0;i<NPOINT;i++) avge[idx].sum += avge[idx].array[i];
  avge[idx].result = avge[idx].sum / ((avge[idx].ten == 0) ? (index) : NPOINT);
}

//
//-------------------------------------------------------------
void switchHV(unsigned char sw) {

  // No ramping is needed when switching the voltage
  // (overshoot is 8.5%, rise time is around 50 ms).
  // To disable ramping completely, the default ramp
  //  rate is set to MAXHV (above 25 V/s ramping works
  //  in large voltage steps, so better not to use it).
  if (sw == ON) { cSw1 = 0; sSw1 = 1; setHV(0,0); bhvEn = 1; }
  else          { bhvEn = 0; cSw1 = 0; sSw1 = 0; }
  publishCtlCsr();

}

//
//-------------------------------------------------------------
unsigned char checkHV(void) {

  // Reset flags and error
  fsetHV = OFF;
  eHVinvalid = 0;

  // Check range (no action)
  if (user_data.vset[0] > MAXHV) {
    // Setting larger than limit, set error flag, restore previous vset point
    DISABLE_INTERRUPTS;
    user_data.vset[0] = vsetMirror;
    ENABLE_INTERRUPTS;
    fRamp = OFF;
    eHVinvalid = ON;
    publishErr();
    return HV_INVALIDRANGE;

  } else {   // Setting within ranges
    if (sSw1) {
      // HV enabled
      vsetMirror = user_data.vset[0];
      eHVinvalid = OFF;
      fRamp = ON;
    }
    publishErr();
    return HV_VALIDRANGE;
  }
}

//
//-------------------------------------------------------------
void rampHV(void)
{
  unsigned long delta;

  if (fRamp) { // Setup ramping
    fRamp = OFF;
    /* start ramping */
    if (user_data.vset[0] > u_actual && user_data.ramp_up > 0) {
      /* ramp up */
      sRampUp = 1;
    /* remember start time */
    t_ramp = time();
    }
    if (user_data.vset[0] < u_actual && user_data.ramp_down > 0) {
      /* ramp down */
      sRampDwn = 1;
    /* remember start time */
    t_ramp = time();
    }

  } else if (sRampUp) {  // Ramp up requested
     delta = time() - t_ramp;
     u_actual += (user_data.ramp_up) * (float) delta / 100.0;
     if (u_actual >= user_data.vset[0]) {
       /* finish ramping */
       u_actual = user_data.vset[0];
       sRampUp = 0;
     }
     setHV(0, u_actual);
     t_ramp = time();

  } else  if (sRampDwn) {  // Ramp down requested
     delta = time() - t_ramp;
     u_actual -= (user_data.ramp_down) * (float) delta / 100.0;
     if (u_actual <= user_data.vset[0]) {
       /* finish ramping */
       u_actual = user_data.vset[0];
       sRampDwn = 0;
     }
     setHV(0, u_actual);
     t_ramp = time();
  }

  publishCtlCsr();	// publish Sbits
}

//
//-------------------------------------------------------------
void setHV(unsigned char ch, float hv) {
  unsigned int ldac;

  ldac = (unsigned int) (hv * HV2VOLT * VOLT2DAC);
 	LTC2607_Write(LTC2607_ADDR, LTC2607_WREG_n_UPDATE_n | ch, ldac);
}

//
//-------------------------------------------------------------
/*---- User loop function -----------------------------------*/
void user_loop(void)
{

//  UpdateDAC();
  // Command input (dac, on/off, rh)
  UpdateIO();

  // Vcc, uCTemp
  UpdateiADC();

  // if new HV requested, check validity, turn on fRamp
  if (fsetHV) { checkHV(); }
  //setHV(1, user_data.vset[1]);  // testing channel 1

  // independent of on/off go in ramp
  rampHV();

  // HV U/I/T measurements
  UpdateTUI();

  // Give an extra chance for exteral Int.
  yield();

  // Rh/T measurments
  UpdateSHT();

  // Mark loop
  led_blink(LED_RED, 1, 50);
}

//
//-------------------------------------------------------------
void UpdateIO(void) {
  static char changed=0;
  if (cSw1) {
    if (sSw1) {
     u_actual = 0;
     switchHV(OFF); //cSw1 = 0; sSw1= 0; bhvEn = 0;
//	 fsetHV = OFF;
    } else {
     switchHV(ON); // cSw1 = 0; sSw1 = 1; bhvEn = 1;
     fsetHV = ON;  // Force checkHV for pending ramping
    }
  }

  if (cAvge) { 
    if (sAvge) {
     cAvge = 0; sAvge = 0; 
    } else {
     cAvge = 0; sAvge = 1;
    }
  }

  if(HUMID) {
    HUMID = 0;
  // 0: open-drain, 1:push-pull
  // P1 .7:CPU sp1 .6:Rh/TVdd  .5:Rh/TClk .4:Rh/TData .3:HV_en  .2:Vref  .1:n/a, .0:CPUsp2
    if (user_data.SHT_on) {
      bRhTVdd = 1;    // Device powered
      fHumid  = 1;
    } else {
      bRhTVdd = 0;
      fHumid = 0;
    }  
  }
  publishCtlCsr();
}

//
//-------------------------------------------------------------
void UpdateiADC(void) {
  unsigned int adc_raw;
  float xdata ucAdc;

  adc_raw = adc_read(CPU_ADC_CH, 1);
  ucAdc  = (((float)(adc_raw)) * IADC2VOLT);
  ucAdc  *= 17.0;  // Res 16 / 1

  DISABLE_INTERRUPTS;
  user_data.radc   = adc_raw;
  user_data.ucAdc  = ucAdc;
  ENABLE_INTERRUPTS;

  adc_raw = adc_read(ADC_INTERNAL_TEMP_CH, 1);
  ucAdc = (((float)(adc_raw)) * IADC2VOLT);
  ucAdc = (ucAdc - 0.9) / 0.00295;
  
  DISABLE_INTERRUPTS;
  user_data.rtemp  = adc_raw;
  user_data.ucTemp = ucAdc;
  ENABLE_INTERRUPTS;
}

//
//-------------------------------------------------------------
/*
void UpdateDAC(void) {
#ifdef MSCB_DEVICE_DAC_LTC2607_DISABLE
#else
  unsigned int ldac;

  if (DAC0 || DAC1 ) {
//    led_blink(LED_RED, 1, 150);
    memset ((char *) &(avge[0].index), 0, sizeof(AVERAGE));

  }
  // Set all necessary DAC values, if an interrupt occurs while this is going on
  // everything should be OK, at worst we may update a newer value.
  if(DAC0) {
    DAC0 = 0;
    setHV(0, user_data.vset[0]);
  }
  if(DAC1) {
    DAC1 = 0;
    setHV(1, user_data.vset[1]);
  }

#endif // MSCB_DEVICE_DAC_LTC2607_DISABLE
}
*/

//
//-------------------------------------------------------------
void UpdateTUI(void) {
#ifdef MCSB_DEVICE_TUI_LTC2990_DISABLE
#else
  char i, stat[N_TUI];
  signed int xdata raw[N_TUI];
//  float xdata x;

// Internal Temperature
//  LTC2990TUI_WriteRegister(LTC2990_ADDR, LTC2990TUI_CONTROL, LTC2990TUI_RPT_ACQ);
  LTC2990TUI_WriteRegister(LTC2990_ADDR, LTC2990TUI_CONTROL, LTC2990TUI_SGL_ACQ);
  LTC2990TUI_WriteRegister(LTC2990_ADDR, LTC2990TUI_TRIGGER, 1);
  delay_ms(250);
  i = LTC2990TUI_ReadTemperature(LTC2990_ADDR, LTC2990TUI_INTTEMP, &(raw[3]));
  if (i) { eTUITinvalid = 1; } else { eTUITinvalid = 0; }

  // Vmon, Imon, Remote Temp T3-T4 (board)
  //  LTC2990TUI_WriteRegister(LTC2990_ADDR, LTC2990TUI_CONTROL, LTC2990TUI_RPT_V1V2TR2);
  LTC2990TUI_WriteRegister(LTC2990_ADDR, LTC2990TUI_CONTROL, LTC2990TUI_SGL_V1V2TR2);
  LTC2990TUI_WriteRegister(LTC2990_ADDR, LTC2990TUI_TRIGGER, 1);
  delay_ms(250);
  i = LTC2990TUI_ReadVoltage(LTC2990_ADDR, LTC2990TUI_VOLT1, &(raw[0])); // V1
  if (i) { eTUIUinvalid = 1; } else { eTUIUinvalid = 0; }
  i = LTC2990TUI_ReadVoltage(LTC2990_ADDR, LTC2990TUI_VOLT2, &(raw[1])); // V2
  if (i) { eTUIIinvalid = 1; } else { eTUIIinvalid = 0; }
  i = LTC2990TUI_ReadTemperature(LTC2990_ADDR, LTC2990TUI_VOLT3, &(raw[2])); // V3-V4
  if (i) { rERR = (0x8<<i); } else { rERR &= ~0x38; }

  publishErr();

  DISABLE_INTERRUPTS;
  for (i=0;i<N_TUI;i++)  user_data.tui[i] = raw[i];  
  ENABLE_INTERRUPTS;

  doAverage(0, raw[0] * ADC2VOLT * VOLT2HV);
  doAverage(1, raw[1] * ADC2VOLT * A2MA);
  doAverage(2, raw[2] * LTC2990TUI_TEMPCOEFF);

  DISABLE_INTERRUPTS;
  user_data.uimon[0] = (sAvge) ? avge[0].result : raw[0] * ADC2VOLT * VOLT2HV;
  user_data.uimon[1] = (sAvge) ? avge[1].result : raw[1] * ADC2VOLT * A2MA;
  user_data.uimon[2] = (sAvge) ? avge[2].result : raw[2] * LTC2990TUI_TEMPCOEFF;
  user_data.uimon[3] = raw[3] * LTC2990TUI_TEMPCOEFF;
  ENABLE_INTERRUPTS;
#endif // MCSB_DEVICE_TUI_LTC2990_DISABLE
}

//
//-------------------------------------------------------------
void UpdateSHT(void) {
#ifdef MSCB_DEVICE_HUMIDITY_SHT7x_DISABLE
#else
  //Humidity variables
  static unsigned long xdata currentTime=0;
  static unsigned char xdata status;
  static float         xdata humidity, htemperature;
  static unsigned int  xdata rSHTtemp1, rSHThumi1;
  static unsigned char xdata FCSorig1, FCSdevi1;

  if(user_data.SHT_on) {
    if(fHumid) {
       // Initializing the SHTxx communication
      HumiSensor_Init(1);  
      fHumid = 0;
    }

//    led_blink(LED_GREEN, 1, 50);
    //Measuring the humidity and temperature
    if ((uptime() - currentTime) > 5) {
      status = HumidSensor_Cmd (&rSHThumi1
                               ,&rSHTtemp1
                              ,&humidity
                              ,&htemperature
                              ,&FCSorig1
                              ,&FCSdevi1
                              ,1);
        if (status == DONE){	 
          doAverage(3, humidity);
          doAverage(4, htemperature);
          DISABLE_INTERRUPTS;
          user_data.ShtHumid  = (sAvge) ? avge[3].result : humidity;
          user_data.ShtTemp   = (sAvge) ? avge[4].result : htemperature;
          user_data.rShtHumid = rSHThumi1;
          user_data.rShtTemp  = rSHTtemp1;
          ENABLE_INTERRUPTS;
        } else {
          DISABLE_INTERRUPTS;
          user_data.ShtHumid = -1;
          user_data.ShtTemp = -1;
          ENABLE_INTERRUPTS;
        }
        currentTime = uptime();
      } 
    } else {
      DISABLE_INTERRUPTS;
      user_data.ShtHumid = 0;
      user_data.ShtTemp = 0;
      user_data.rShtHumid = 0;
      user_data.rShtTemp = 0;
      ENABLE_INTERRUPTS;
    }
#endif // MSCB_DEVICE_HUMIDITY_SHT7x_DISABLE
}
