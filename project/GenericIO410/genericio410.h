/********************************************************************\

  Name:     Genericio410.h
  $Id$

\********************************************************************/

#ifndef _GENERIC_IO_H
#define _GENERIC_IO_H

#include "../../common/mscbemb.h"

//
//
#define N_MUX       4
#define N_SW        1
#define N_IO        6
#define N_VOLT      8
#define N_ADC       8
#define N_DAC       8
#define N_TEMP      6
#define FIRST_MUX   4
#define FIRST_SW    8
#define FIRST_IO    9
#define FIRST_VOLT  15
#define FIRST_ADC   23
#define FIRST_DAC   31
#define FIRST_TEMP  39
#define IDX_HUM_ON  44

#define N_ADC_CHANNEL   4              //Number of ADC Channels
#define VREF        2.500f
#define ADC_OFFSET  0.0f
#define ADC_ZERO_OFFSET 65536
#define ADC_GAIN	  (VREF / 131072.0f)

#define PCA_DAC03   0x01
#define PCA_DAC47   0x04
#define PCA_ADC03   0x08
#define PCA_ADC47   0x02

#define PCA9536_ADDR  0x41 // MUX
#define LTC2605_ADDR  0x10 // 16-bit 8 channel DAC
#define LTC2489_ADDR1 0x24 // 16-bit 4 channel ADC
#define LTC2489_ADDR2 0x17 // 16-bit 4 channel ADC
#define ADT7483_ADDR  0x18 // Temperature Sensor

// Global definition
// Global ON / OFF definition
#define ON     1
#define DONE   1
#define SET    1
#define OFF    0
#define CLEAR  0

#define LED_GREEN 0
#define LED_RED   1

//
//--- MSCB structure
typedef struct {
  // System
  unsigned long SerialN;
  unsigned int  error;
  unsigned char control;
  unsigned char status;
  
  // Functions
  unsigned char mux[N_MUX];
  unsigned char iosw;
  unsigned char io_read[N_IO];
  unsigned char io_write[N_IO];
  unsigned char io_mode[N_IO];
  float         volt[N_VOLT];
  unsigned long adc[N_ADC];
  unsigned int  dac[N_DAC];
  float         intemp;     // ADT7486A internal temperature [degree celsius]
  float         temp[2];    // ADT7486A external2 temperature [degree celsius]
  float         SHT_temp;
  float         SHT_humidity;
  unsigned char SHT_on;
} MSCB_USER_DATA;

void UpdatePCA(void);
void UpdateADC(void);
void UpdateDAC(void);
void UpdateIO(void);
void UpdateHumidity(void);
void UpdateTemperature(void);

#endif // _GENERIC_IO.H