/********************************************************************\

Name:     Genericio410
Author:   Pierre-Andr�
Date:     
$Id$


\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "genericio410.h" 

extern xdata SYS_INFO sys_info;

char          code  MSCB_node_name[] = "GENERICIO410";
char          xdata svn_rev_code[] = "$Rev$";
xdata MSCB_USER_DATA user_data;

MSCB_INFO_VAR code vars[] = {
  4, UNIT_BYTE,     0, 0, 0,           "SerialN",   &user_data.SerialN,     // 0
  1, UNIT_BYTE,     0, 0, 0,           "Error"  ,   &user_data.error,       // 1
  1, UNIT_BYTE,     0, 0, 0,           "Control",   &user_data.control,     // 2
  1, UNIT_BYTE,     0, 0, 0,           "Status" ,   &user_data.status,      // 3

  // Status Registers
  1, UNIT_BYTE,     0, 0, 0,           "D03P103" ,  &user_data.mux[0],      // 4
  1, UNIT_BYTE,     0, 0, 0,           "D47P147" ,  &user_data.mux[1],      // 5
  1, UNIT_BYTE,     0, 0, 0,           "A03P203" ,  &user_data.mux[2],      // 6
  1, UNIT_BYTE,     0, 0, 0,           "A47P247" ,  &user_data.mux[3],      // 7 

  // Actual IO mux switch register
  1, UNIT_BYTE,     0, 0, 0,           "io_mux" ,   &user_data.iosw,        // 8
  
  1, UNIT_BYTE,     0, 0, 0,           "P1rd"   ,   &user_data.io_read[0],  // 9 
  1, UNIT_BYTE,     0, 0, 0,           "P1wr"   ,   &user_data.io_write[0], // 10
  1, UNIT_BYTE,     0, 0, 0,           "P1out"  ,   &user_data.io_mode[0],  // 11
   
  1, UNIT_BYTE,     0, 0, 0,           "P2rd"   ,   &user_data.io_read[1],  // 12
  1, UNIT_BYTE,     0, 0, 0,           "P2wr"   ,   &user_data.io_write[1], // 13
  1, UNIT_BYTE,     0, 0, 0,           "P2out"  ,   &user_data.io_mode[1],  // 14
  
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt0"  ,   &user_data.volt[0],     // 15
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt1"  ,   &user_data.volt[1],     // 16
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt2"  ,   &user_data.volt[2],     // 17
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt3"  ,   &user_data.volt[3],     // 18
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt4"  ,   &user_data.volt[4],     // 19
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt5"  ,   &user_data.volt[5],     // 20
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt6"  ,   &user_data.volt[6],     // 21
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt7"  ,   &user_data.volt[7],     // 22
  
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC0"   ,   &user_data.adc[0],      // 23
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC1"   ,   &user_data.adc[1],      // 24
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC2"   ,   &user_data.adc[2],      // 25
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC3"   ,   &user_data.adc[3],      // 26
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC4"   ,   &user_data.adc[4],      // 27
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC5"   ,   &user_data.adc[5],      // 28
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC6"   ,   &user_data.adc[6],      // 29
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC7"   ,   &user_data.adc[7],      // 30
  
  2, UNIT_BYTE,     0, 0, 0,           "DAC0"   ,   &user_data.dac[0],      // 31
  2, UNIT_BYTE,     0, 0, 0,           "DAC1"   ,   &user_data.dac[1],      // 32
  2, UNIT_BYTE,     0, 0, 0,           "DAC2"   ,   &user_data.dac[2],      // 33
  2, UNIT_BYTE,     0, 0, 0,           "DAC3"   ,   &user_data.dac[3],      // 34
  2, UNIT_BYTE,     0, 0, 0,           "DAC4"   ,   &user_data.dac[4],      // 35
  2, UNIT_BYTE,     0, 0, 0,           "DAC5"   ,   &user_data.dac[5],      // 36
  2, UNIT_BYTE,     0, 0, 0,           "DAC6"   ,   &user_data.dac[6],      // 37
  2, UNIT_BYTE,     0, 0, 0,           "DAC7"   ,   &user_data.dac[7],      // 38
  
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "T0int",     &user_data.intemp,      // 39
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "T00ext",    &user_data.temp[0],     // 40
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "T01ext",    &user_data.temp[1],     // 41
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "SHTtemp",   &user_data.SHT_temp,     // 42 
  4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT, "SHThum",    &user_data.SHT_humidity, // 43
  1, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,"SHTon",     &user_data.SHT_on,      // 44
  0
};

code MSCB_INFO_VAR *variables = vars;

/********************************************************************\

Application specific init and inout/output routines

\********************************************************************/

// Change register
volatile unsigned char bdata bChange = 0;
sbit PCA9536_Flag = bChange ^ 0;
sbit IO_Flag      = bChange ^ 1;
sbit HUM_FLAG     = bChange ^ 2;

// Control register
unsigned char bdata rCTL = 0;
sbit C0       = rCTL ^ 0;
sbit C1       = rCTL ^ 1;
sbit C2       = rCTL ^ 2;
sbit C3       = rCTL ^ 3;
sbit CeeS     = rCTL ^ 4;
sbit CeeR     = rCTL ^ 5;
sbit CeeClr   = rCTL ^ 6;
sbit CmSd     = rCTL ^ 7;

// Status Register
unsigned char bdata rSTAT = 0;
sbit S0       = rSTAT ^ 0;
sbit S1       = rSTAT ^ 1;
sbit S2       = rSTAT ^ 2;
sbit S3       = rSTAT ^ 3;
sbit SeeS     = rSTAT ^ 4;
sbit SeeR     = rSTAT ^ 5;
sbit SsS      = rSTAT ^ 6;
sbit eAdc     = rSTAT ^ 7;

// DAC update register
unsigned char bdata rDAC = 0xFF;
sbit DAC0     = rDAC ^ 0;
sbit DAC1     = rDAC ^ 1;
sbit DAC2     = rDAC ^ 2;
sbit DAC3     = rDAC ^ 3;
sbit DAC4     = rDAC ^ 4;
sbit DAC5     = rDAC ^ 5;
sbit DAC6     = rDAC ^ 6;
sbit DAC7     = rDAC ^ 7;


/*---- User init function ------------------------------------------*/

void user_init(unsigned char init)
{
  char i;

  /* Format the SVN and store this code SVN revision into the system */
  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  sys_info.svn_revision = (svn_rev_code[6]-'0')*1000+
    (svn_rev_code[7]-'0')*100+
    (svn_rev_code[8]-'0')*10+
    (svn_rev_code[9]-'0');

  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  if (init) {
    // Initialize variables
    for (i=0;i<8;i++) {
      user_data.adc [i] = 0;
      user_data.dac [i] = 0;
      user_data.volt[i] = 0.0;
    }

    // Temperature initialization
    user_data.intemp = 0;
    memset (user_data.temp, 0, sizeof(user_data.temp));
  }

  // Reference Voltage on P0.0 Enable
  REF0CN = 0x00;  // Int Bias off, Int Temp off, VREF input on

  // 0:analog 1:digital
  P0MDIN = 0xFF; // P0 all digital pins
  P1MDIN = 0xFF; // P1 all digital pins 
  P2MDIN = 0xFF; // P2 all digital pins
 
  // 0: open-drain, 1:push-pull
  P0MDOUT = 0x50; // P0 .6/Ren, .4/Tx

  P1MDOUT = 0x00; 
  P2MDOUT = 0x00; 
  P1 = 0x00;
  P2 = 0x00;

  // Setting the cross bar
  P0SKIP = 0x0; 
  P1SKIP = 0x0; 
  P2SKIP = 0x0; 

  XBR0 |=0x04;	 // Enable SMBus pins P0 .0/SDA .1/SCL

  /* set-up / initialize circuit components (order is important) */

  // CONFIG : 0 for output
  PCA9536_Init();
  PCA9536_WriteByte (PCA9536_ADDR, PCA9536_CONFIG, PCA9536_ALL_OUTPUT);
  PCA9536_WriteByte (PCA9536_ADDR, PCA9536_POLINV, PCA9536_NO_INVERT);

  PCA9536_Flag = 1;
  UpdatePCA();

  // Force IO update
  IO_Flag = 1;
  UpdateIO();

  // Humidity sensor initialization
  if(user_data.SHT_on) {
    HUM_FLAG = 1;
  }
  UpdateHumidity();

  LTC2489_Init();
  LTC2605_Init();
  ADT7483_Init();

  ADT7483_WriteRegister(ADT7483_ADDR, ADT7483_WREG_CONF1, 0);
  ADT7483_WriteRegister(ADT7483_ADDR, ADT7483_WREG_CONF2, 0);  // 4 for extended range 
  ADT7483_WriteRegister(ADT7483_ADDR, ADT7483_WREG_CHSEL, ADT7483_CONV_TIME_125_MS);
}

/*---- User write function -----------------------------------------*/
void user_write(unsigned char index) reentrant{

  led_blink(LED_RED, 1, 150);

  // Set the appropriate DAC update register bit
  // Done this way so we if we can take being interrupted, and avoid a
  // read-write-modify setup. (if done using a byte)
  if ((index >= FIRST_DAC) && (index < (FIRST_DAC + (N_DAC)))) {
    switch(index - FIRST_DAC) {
      case 0:
        DAC0 = 1;
        break;

      case 1:
        DAC1 = 1;
        break;

      case 2:
        DAC2 = 1;
        break;

      case 3:
        DAC3 = 1;
        break;

      case 4:
        DAC4 = 1;
        break;

      case 5:
        DAC5 = 1;
        break;

      case 6:
        DAC6 = 1;
        break;

      case 7:
        DAC7 = 1;
        break;

    }
  } else if (index == FIRST_SW) {
    PCA9536_Flag = 1;
  } else if ((index >= FIRST_IO) && (index < (FIRST_IO + (N_IO)))) {
    IO_Flag = 1;
  } else if (index == IDX_HUM_ON) {
    HUM_FLAG = 1;
    IO_Flag = 1;
    PCA9536_Flag = 1;
  }

  return;
}

/*---- User read function ------------------------------------------*/

unsigned char user_read(unsigned char index)
{
  if(index);
  return 0;
}

/*---- User function called vid CMD_USER command -------------------*/

unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
  // echo input data
  data_out[0] = data_in[0];
  data_out[1] = data_in[1];
  return 2;
}

//
//----------------------------------------------------------------------------------
//Main user loop
/*---- User loop function ------------------------------------------*/
void user_loop(void)
{
  UpdatePCA();
  UpdateDAC();
  UpdateIO();
  UpdateADC();
  UpdateHumidity();
  UpdateTemperature();
  led_blink(LED_GREEN, 1, 50);
  delay_ms(150); // ADC takes 149.9 ms to convert maximum.
}

//
//---------------------------------------------------------------
void UpdateADC(void) {
#ifdef MCSB_DEVICE_ADC_LTC2489_DISABLE
#else
  static char adcChannel = N_ADC_CHANNEL + 1;
  char prevChannel;
  unsigned char range;
  static code unsigned char adc_channel[4] = { 1, 2, 0, 3 };
  static xdata signed long adc_raw;

  if(adcChannel == (N_ADC_CHANNEL + 1)) {
    adcChannel = CLEAR;
    LTC2489_StartConversion(LTC2489_ADDR_GLOBAL, adcChannel);
  } else {
    prevChannel = adcChannel;
    adcChannel++;
    adcChannel %= N_ADC_CHANNEL;

    range = LTC2489_ReadConversion(LTC2489_ADDR1, adcChannel, &adc_raw);
    switch(range) {
      case LTC2489_VALIDRANGE:
        adc_raw += ADC_ZERO_OFFSET;
        break;

      case LTC2489_OVERRANGE:
        eAdc = 1;
        adc_raw = 0xFFFFFFFF;
        break;

      case LTC2489_UNDERRANGE:
        eAdc = 1;
        adc_raw = 0;
        break;
    }
    
    DISABLE_INTERRUPTS;
    user_data.volt[adc_channel[prevChannel]] = (((float)(adc_raw + ADC_OFFSET)) * (float)(ADC_GAIN));
    user_data.adc[adc_channel[prevChannel]]  = adc_raw / 2;
    ENABLE_INTERRUPTS;

    range = LTC2489_ReadConversion(LTC2489_ADDR2, adcChannel, &adc_raw);
    switch(range) {
      case LTC2489_VALIDRANGE:
        adc_raw += ADC_ZERO_OFFSET;
        break;

      case LTC2489_OVERRANGE:
        eAdc = 1;
        adc_raw = 0xFFFFFFFF;
        break;

      case LTC2489_UNDERRANGE:
        eAdc = 1;
        adc_raw = 0;
        break;
    }
    
    DISABLE_INTERRUPTS;
    user_data.volt[adc_channel[prevChannel]+4] = (((float)(adc_raw) + ADC_OFFSET) * (float)(ADC_GAIN));
    user_data.adc[adc_channel[prevChannel]+4]  = adc_raw / 2;
    ENABLE_INTERRUPTS;
    
  }
#endif // MCSB_DEVICE_ADC_LTC2489_DISABLE
}

//
//---------------------------------------------------------------
void UpdateDAC(void) {
#ifdef MSCB_DEVICE_DAC_LTC2605_DISABLE
#else
  if (DAC0 || DAC1 || DAC2 || DAC3 || DAC4 || DAC5 || DAC6 || DAC7) {
    led_blink(LED_GREEN, 1, 150);
  }
  // Set all necessary DAC values, if an interrupt occurs while this is going on
  // everything should be OK, at worst we may update a newer value.
  if(DAC0) { LTC2605_Write(LTC2605_ADDR, LTC2605_WREG_n_UPDATE_n | 0, user_data.dac[0]); DAC0 = 0; }
  if(DAC1) { LTC2605_Write(LTC2605_ADDR, LTC2605_WREG_n_UPDATE_n | 1, user_data.dac[1]); DAC1 = 0; }
  if(DAC2) { LTC2605_Write(LTC2605_ADDR, LTC2605_WREG_n_UPDATE_n | 2, user_data.dac[2]); DAC2 = 0; }
  if(DAC3) { LTC2605_Write(LTC2605_ADDR, LTC2605_WREG_n_UPDATE_n | 3, user_data.dac[3]); DAC3 = 0; }
  if(DAC4) { LTC2605_Write(LTC2605_ADDR, LTC2605_WREG_n_UPDATE_n | 4, user_data.dac[4]); DAC4 = 0; }
  if(DAC5) { LTC2605_Write(LTC2605_ADDR, LTC2605_WREG_n_UPDATE_n | 5, user_data.dac[5]); DAC5 = 0; }
  if(DAC6) { LTC2605_Write(LTC2605_ADDR, LTC2605_WREG_n_UPDATE_n | 6, user_data.dac[6]); DAC6 = 0; }
  if(DAC7) { LTC2605_Write(LTC2605_ADDR, LTC2605_WREG_n_UPDATE_n | 7, user_data.dac[7]); DAC7 = 0; }
#endif // MSCB_DEVICE_DAC_LTC2605_DISABLE
}

//
//---------------------------------------------------------------
void UpdateIO(void) {
  DISABLE_INTERRUPTS;
  
  if (IO_Flag) {
    
    if(user_data.SHT_on) {
      user_data.io_mode[0] &= ~0x02; // Make sure P1.1 is open drain
      user_data.io_mode[0] |= (0x10 | 0x04 | 0x08); // P1.4, P1.2, P1.3 are P/P 
      user_data.io_write[0] &= ~0x04; // Make sure P1.2 is pulling low
      user_data.io_write[0] |= 0x08; // Make sure P1.3 is pulling high 
    }

    led_blink(LED_GREEN, 1, 150);

    P1MDOUT = user_data.io_mode[0];
    P2MDOUT = ((user_data.io_mode[1] & 0x80) | 
              ((user_data.io_mode[1] & 0x40) >> 1) |
              ((user_data.io_mode[1] & 0x20) >> 1) |
              ((user_data.io_mode[1] & 0x10) << 2) |
               (user_data.io_mode[1] & 0x08) | 
              ((user_data.io_mode[1] & 0x04) >> 1) |
              ((user_data.io_mode[1] & 0x02) >> 1) |
              ((user_data.io_mode[1] & 0x01) << 2));

    P1 = user_data.io_write[0];
    P2 = ((user_data.io_write[1] & 0x80) | 
         ((user_data.io_write[1] & 0x40) >> 1) |
         ((user_data.io_write[1] & 0x20) >> 1) |
         ((user_data.io_write[1] & 0x10) << 2) |
          (user_data.io_write[1] & 0x08) | 
         ((user_data.io_write[1] & 0x04) >> 1) |
         ((user_data.io_write[1] & 0x02) >> 1) |
         ((user_data.io_write[1] & 0x01) << 2));

    IO_Flag = 0;
  }

  user_data.io_read[0] = P1;
  user_data.io_read[1] = P2;

  user_data.io_read[1] = 
    ((user_data.io_read[1] & 0x80) | 
    ((user_data.io_read[1] & 0x40) >> 2) |
    ((user_data.io_read[1] & 0x20) << 1) |
    ((user_data.io_read[1] & 0x10) << 1) |
     (user_data.io_read[1] & 0x08) | 
    ((user_data.io_read[1] & 0x04) >> 2) |
    ((user_data.io_read[1] & 0x02) << 1) |
    ((user_data.io_read[1] & 0x01) << 1));

  ENABLE_INTERRUPTS;
}

//
//---------------------------------------------------------------
void UpdatePCA(void) {
#ifdef MCSB_DEVICE_IO_PCA9536_DISABLE
#else
  unsigned char temp;

  if (PCA9536_Flag) {
      led_blink(LED_GREEN, 1, 150);
    
      // Update PCA with value from iosw
      DISABLE_INTERRUPTS
       // if SHT is on, ensure we do not turn switch the DACs on!
      if(user_data.SHT_on) {
        user_data.iosw |= 0x0C; // Disable DACs
      } 
  
      // Convert into actual pin format...
      temp = (((user_data.iosw & 0x08)) |
              ((user_data.iosw & 0x04) >> 1) |
              ((user_data.iosw & 0x02) >> 1) |
               (user_data.iosw & 0x01) << 2);  
      ENABLE_INTERRUPTS;        

     
      PCA9536_WriteByte (PCA9536_ADDR, PCA9536_OUTPUT, temp);
      PCA9536_Flag = 0;
    }
    
    // Read back the value, and use that to propogate the user_data.mux fields
    PCA9536_Read (PCA9536_ADDR, PCA9536_INPUT, &temp ,1);

    DISABLE_INTERRUPTS;
    user_data.iosw = (temp & 0x08) | ((temp & 0x04) >> 2) | ((temp & 0x02) << 1) | ((temp & 0x01) << 1);    

    // Decode 4 bit word
    if(user_data.iosw & 0x8) { user_data.mux[0] = 0x0f; } else { user_data.mux[0] = 0xf0; }
    if(user_data.iosw & 0x4) { user_data.mux[1] = 0x0f; } else { user_data.mux[1] = 0xf0; }
    if(user_data.iosw & 0x2) { user_data.mux[2] = 0x0f; } else { user_data.mux[2] = 0xf0; }
    if(user_data.iosw & 0x1) { user_data.mux[3] = 0x0f; } else { user_data.mux[3] = 0xf0; }
    ENABLE_INTERRUPTS;
#endif // MCSB_DEVICE_IO_PCA9536_DISABLE
}

//
//---------------------------------------------------------------
void UpdateHumidity(void) {

  //Humidity variables
  static unsigned long xdata currentTime=0;
  static unsigned char xdata status;
  static float         xdata humidity, htemperature;
  static unsigned int  xdata rSHTtemp1, rSHThumi1;
  static unsigned char xdata FCSorig1, FCSdevi1;

  if(user_data.SHT_on) {
    if(HUM_FLAG) {
      // Initializing the SHTxx communication
      HumiSensor_Init(1);  
      HUM_FLAG = 0;
    }
    //Measuring the humidity and temperature
    if ((uptime() - currentTime) > 1) {
      led_blink(LED_RED, 1, 50);
      status = HumidSensor_Cmd (&rSHThumi1
                               ,&rSHTtemp1
                              ,&humidity
                              ,&htemperature
                              ,&FCSorig1
                              ,&FCSdevi1
                              ,1);
      if (status == DONE){	 
        DISABLE_INTERRUPTS;
        user_data.SHT_humidity = humidity;
        user_data.SHT_temp = htemperature;
        ENABLE_INTERRUPTS;
      } else {
        DISABLE_INTERRUPTS;
        user_data.SHT_humidity = -1;
        user_data.SHT_temp = -1;
        ENABLE_INTERRUPTS;
      }
      currentTime = uptime();
    } // uptime()-
  } else { // SHT off
    DISABLE_INTERRUPTS;
    user_data.SHT_humidity = 0;
    user_data.SHT_temp = 0;
    ENABLE_INTERRUPTS;
  }
}

//
//---------------------------------------------------------------
void UpdateTemperature(void) {
  float temp;

  temp = ADT7483_ReadTemp(ADT7483_ADDR, ADT7483_CH_READ_LOCAL);

  DISABLE_INTERRUPTS;
  user_data.intemp  = temp; 
  ENABLE_INTERRUPTS;
  
  temp = ADT7483_ReadTemp(ADT7483_ADDR, ADT7483_CH_READ_REMOTE1);

  DISABLE_INTERRUPTS;  
  user_data.temp[0] = temp;
  ENABLE_INTERRUPTS;

  temp = ADT7483_ReadTemp(ADT7483_ADDR, ADT7483_CH_READ_REMOTE2);
  
  DISABLE_INTERRUPTS;
  user_data.temp[1] = temp;  
  ENABLE_INTERRUPTS;
}
