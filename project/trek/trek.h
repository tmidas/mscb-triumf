/********************************************************************\

  Name:     trek.h
            on F410
  $Id$

\********************************************************************/

#ifndef _GENERIC_IO_H
#define _GENERIC_IO_H

#include "../../common/mscbemb.h"

//
//
#define N_ADC       8
#define N_DAC       8
#define N_TEMP      3
#define CTL_IDX     2
#define FIRST_QDAC  4
#define FIRST_IBAS  6
#define FIRST_ADC   22
#define FIRST_DAC   14
#define FIRST_TEMP  32
#define FREQ_IDX    43 
#define N_ADC_CHANNEL   8    // Number of ADC Channels

#define LTC2495_ADDR  0x14 // 16-bit 8 channel ADC
#define ADCVREF      4.9967f/2.0f
#define ADC_OFFSET   0.0f
#define ADC_ZERO_OFFSET 0;
#define A2VCONV16	   (ADCVREF / 65536.0f)
#define V2ACONV16	   (65536.0f / ADCVREF)
unsigned int code gaintable[]   = {1, 4, 8, 16, 32, 64, 128, 264};
unsigned int gain=0;

#define LTC2605_ADDR  0x10 // 16-bit 8 channel DAC
#define DACVREF      4.5f
#define V2DCONV16    (65536.0f / DACVREF)
#define D2VCONV16    (DACVREF / 65536.0f)

#define LTC2485_ADDR  0x26 // 24-bit 1 channel ADC
#define ADCVREF2     5.000f
#define A2VCONV24	   (ADCVREF2 / 2147483648.0f)
#
#define LTC2606_ADDR  0x22 // 16-bit 1 channel DAC
#define DACVREF2     5.00f
#define V2DCONV216   (65536.0f / DACVREF2)

#define ADT7483_ADDR  0x18 // Temperature Sensor

unsigned char freqTable[4] = {250, 150, 50 ,1};

// Global definition
// Global ON / OFF definition
#define ON     1
#define DONE   1
#define SET    1
#define OFF    0
#define CLEAR  0

#define LED_GREEN 1
#define LED_RED   0


//
//--- MSCB structure
typedef struct {
  // System
  unsigned long SerialN;
  unsigned int  error;
  unsigned char control;
  unsigned char status;
  float         vQpump;
  float         vbias;    
  float         ibias[N_ADC];
  unsigned long adc[N_ADC];
  float         dac[N_DAC];
  unsigned long qadc;
  unsigned int  Igain;
  float         intemp;     // ADT7486A internal temperature [degree celsius]
  float         temp[2];    // ADT7486A external2 temperature [degree celsius]
  float         obias[N_ADC];
  unsigned char freq;
} MSCB_USER_DATA;

void UpdateADC(void);
void UpdateDAC(void);
void UpdateQADC();
void UpdateQDAC();
void UpdateIO(void);
void UpdateTemperature(void);
void publishCtlCsr(void);
void publishErr(bit errbit);
void publishAll();

#endif // _GENERIC_IO.H