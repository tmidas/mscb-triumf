/********************************************************************\

Name:     muPet 
Author:   Pierre-Andr�
Date:     
$Id$


\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mupet.h" 

extern xdata SYS_INFO sys_info;

char    code  MSCB_node_name[] = "MUPET410";
char    xdata svn_rev_code[] = "$Rev$";
xdata         MSCB_USER_DATA user_data;

MSCB_INFO_VAR code vars[] = {
  4, UNIT_BYTE,     0, 0, 0,           "CurGain",   &user_data.Igain,       // 0
  1, UNIT_BYTE,     0, 0, 0,           "Error"  ,   &user_data.error,       // 1
  1, UNIT_BYTE,     0, 0, 0,           "Control",   &user_data.control,     // 2
  1, UNIT_BYTE,     0, 0, 0,           "Status" ,   &user_data.status,      // 3

  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Ich0"  ,   &user_data.volt[0],     // 4
  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Ich1"  ,   &user_data.volt[1],     // 5 
  4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Ich2"  ,   &user_data.volt[2],     // 6 
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Bias"  ,   &user_data.volt[3],     // 7 
  4, UNIT_VOLT,     0, 0, MSCBF_HIDDEN | MSCBF_FLOAT, "Spare4"  ,   &user_data.volt[4],     // 8 
  4, UNIT_VOLT,     0, 0, MSCBF_HIDDEN | MSCBF_FLOAT, "Spare5"  ,   &user_data.volt[5],     // 9 
  4, UNIT_VOLT,     0, 0, MSCBF_HIDDEN | MSCBF_FLOAT, "Spare6"  ,   &user_data.volt[6],     // 10
  4, UNIT_VOLT,     0, 0, MSCBF_HIDDEN | MSCBF_FLOAT, "Spare7"  ,   &user_data.volt[7],     // 11
  
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC0"   ,   &user_data.adc[0],      // 12
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC1"   ,   &user_data.adc[1],      // 13
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC2"   ,   &user_data.adc[2],      // 14
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC3"   ,   &user_data.adc[3],      // 15
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC4"   ,   &user_data.adc[4],      // 16
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC5"   ,   &user_data.adc[5],      // 17
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC6"   ,   &user_data.adc[6],      // 18
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC7"   ,   &user_data.adc[7],      // 19
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC8"   ,   &user_data.adc[8],      // 20
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC9"   ,   &user_data.adc[9],      // 21
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC10"   ,   &user_data.adc[10],      // 22
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC11"   ,   &user_data.adc[11],      // 23
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC12"   ,   &user_data.adc[12],      // 24
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC13"   ,   &user_data.adc[13],      // 25
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC14"   ,   &user_data.adc[14],      // 26
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC15"   ,   &user_data.adc[15],      // 27
  
  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "DAC0"   ,   &user_data.dac[0],      // 28
  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "DAC1"   ,   &user_data.dac[1],      // 29
  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "DAC2"   ,   &user_data.dac[2],      // 30
  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "BIAS"   ,   &user_data.dac[3],      // 31
  
  4, UNIT_VOLT,   0, 0, MSCBF_FLOAT, "vDAC0"   ,   &user_data.vdac[0],      // 32
  4, UNIT_VOLT,   0, 0, MSCBF_FLOAT, "vDAC1"   ,   &user_data.vdac[1],      // 33
  4, UNIT_VOLT,   0, 0, MSCBF_FLOAT, "vDAC2"   ,   &user_data.vdac[2],      // 34
  4, UNIT_VOLT,   0, 0, MSCBF_FLOAT, "vBIAS"   ,   &user_data.vdac[3],      // 35
  
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "T0int",     &user_data.intemp,      // 36
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "T00ext",    &user_data.temp[0],     // 37
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "T01ext",    &user_data.temp[1],     // 38
  1, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,"Freq",      &user_data.freq,        // 39
  0
};

code MSCB_INFO_VAR *variables = vars;

/********************************************************************\

Application specific init and inout/output routines

\********************************************************************/

// Change register
volatile unsigned char bdata bChange = 0;
sbit IO_Flag      = bChange ^ 0;

// Control register
unsigned char bdata rCTL = 0;
sbit cQpup    = rCTL ^ 0;
sbit cVref    = rCTL ^ 1;
sbit cBias    = rCTL ^ 2;
sbit C3       = rCTL ^ 3;
sbit CeeS     = rCTL ^ 4;
sbit CeeR     = rCTL ^ 5;
sbit CeeClr   = rCTL ^ 6;
sbit CmSd     = rCTL ^ 7;

// Status Register
unsigned char bdata rSTAT = 0;
sbit sQpup    = rSTAT ^ 0;
sbit sVref    = rSTAT ^ 1;
sbit sBias    = rSTAT ^ 2;
sbit sAlert   = rSTAT ^ 3;
sbit eAdc     = rSTAT ^ 4;
sbit eSpare1  = rSTAT ^ 5;
sbit eSpare2  = rSTAT ^ 6;
sbit eSpare3  = rSTAT ^ 7;

// DAC update register
unsigned char bdata rDAC = 0xFF;
sbit DAC0     = rDAC ^ 0;
sbit DAC1     = rDAC ^ 1;
sbit DAC2     = rDAC ^ 2;
sbit DAC3     = rDAC ^ 3;
sbit VDAC0    = rDAC ^ 4;
sbit VDAC1    = rDAC ^ 5;
sbit VDAC2    = rDAC ^ 6;
sbit VDAC3    = rDAC ^ 7;

// Hardware bits
sbit bAlert = P1 ^ 5;
sbit bBias  = P1 ^ 6;
sbit bVref  = P1 ^ 7;

/*---- User init function ------------------------------------------*/

void user_init(unsigned char init)
{
  char i;
  unsigned char temp;

  /* Format the SVN and store this code SVN revision into the system */
  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  sys_info.svn_revision = (svn_rev_code[6]-'0')*1000+
    (svn_rev_code[7]-'0')*100+
    (svn_rev_code[8]-'0')*10+
    (svn_rev_code[9]-'0');

  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  if (init) {
    // Initialize variables
    for (i=0;i<8;i++) {
      user_data.adc [i] = 0;
      user_data.dac [i] = 0;
      user_data.volt[i] = 0.0;
    }

    // Temperature initialization
    user_data.intemp = 0;
    memset (user_data.temp, 0, sizeof(user_data.temp));
  }

  // Reference Voltage on P0.0 Enable
  REF0CN = 0x00;  // Int Bias off, Int Temp off, VREF input on

  // 0:analog 1:digital
  P0MDIN = 0xFF; // P0 all digital pins
  P1MDIN = 0xFF; // P1 all digital pins 
  P2MDIN = 0xFF; // P2 all digital pins
 
  // 0: open-drain, 1:push-pull
  P0MDOUT = 0x94; // .7:RS485EN  .5:Rx .4:Tx .2:QPUMPCLK

  P1MDOUT = 0xC0;  // .7:BiasENn, .6:V10RefENn, .5:ALERTn
  P1 = 0xC0;
  P1MDOUT = 0xC0;  // .7:BiasENn, .6:V10RefENn, .5:ALERTn
  P1 = 0xC0;

  P2MDOUT = 0x00; 
  P2 = 0x00;

  // Setting the cross bar
  P0SKIP = 0x0; 
  P1SKIP = 0x0; 
  P2SKIP = 0x0; 

  XBR0 |=0x04;	 // Enable SMBus pins P0 .0/SDA .1/SCL

  /* set-up / initialize circuit components (order is important) */

  // Force IO update
  IO_Flag = 1;

  rCTL  = 0;
  rSTAT = 0;

  // Set Bias monitoring as default
  cBias = ON;
  UpdateIO();

  pca_Init();
  pca_FreqChange(freqTable[1]);  // Max frequency (1MHz)
  user_data.freq = freqTable[1];

  temp = 0;

  LTC2495_Init();
  LTC2609_Init();
  ADT7483_Init();

  // -50DegC to +150DegC  (non extended: 0DegC to +127DegC
  ADT7483_WriteRegister(ADT7483_ADDR, ADT7483_WREG_CONF2, 0);
  ADT7483_WriteRegister(ADT7483_ADDR, ADT7483_WREG_CHSEL, ADT7483_CONV_TIME_125_MS);
  ADT7483_WriteRegister(ADT7483_ADDR, ADT7483_WREG_CONF1, ADT7483_EXTENDED_TEMP_RANGE);

  // ADC Gain
  user_data.Igain = LTC2495_GAIN128;
}

/*---- User write function -----------------------------------------*/
void user_write(unsigned char index) reentrant{

//  led_blink(LED_RED, 1, 150);

  // Set the appropriate DAC update register bit
  // Done this way so we if we can take being interrupted, and avoid a
  // read-write-modify setup. (if done using a byte)
  if (index == CTL_IDX){
    if (user_data.control & 0x1) {
     cQpup = 1;
    } else if (user_data.control & 0x2) {
     cVref = 1;
    } else if (user_data.control & 0x4) {
     cBias = 1;
    }
  } else if ((index >= FIRST_DAC) && (index < (FIRST_DAC + (N_DAC)))) {
    switch(index - FIRST_DAC) {
      case 0:
        DAC0 = 1;
        break;

      case 1:
        DAC1 = 1;
        break;

      case 2:
        DAC2 = 1;
        break;

      case 3:
        DAC3 = 1;
        break;
    }
  } else if ((index >= FIRST_VDAC) && (index < (FIRST_VDAC + (N_DAC)))) {
    switch(index - FIRST_VDAC) {
      case 0:
        VDAC0 = 1;
        break;

      case 1:
        VDAC1 = 1;
        break;

      case 2:
        VDAC2 = 1;
        break;

      case 3:
        VDAC3 = 1;
        break;
    }
  }

  return;
}

/*---- User read function ------------------------------------------*/

unsigned char user_read(unsigned char index)
{
  if(index);
  return 0;
}

/*---- User function called vid CMD_USER command -------------------*/

unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
  // echo input data
  data_out[0] = data_in[0];
  data_out[1] = data_in[1];
  return 2;
}

//
//----------------------------------------------------------------------------------
//Main user loop
//Read 6 ADT7486A SST diode Temperature

/*---- User loop function ------------------------------------------*/
void user_loop(void)
{
  UpdateDAC();
  UpdateIO();
  UpdateADC();
  UpdateTemperature();

  delay_ms(150); // ADC takes 149.9 ms to convert maximum.
}

//
//-----------------------------------------------------------------------------
void publishCtlCsr(void) {
    DISABLE_INTERRUPTS;
    user_data.control = rCTL;
    user_data.status  = rSTAT;
    ENABLE_INTERRUPTS;
}

//
//-------------------------------------------------------------
void UpdateIO(void) {
  static char changed=0;
  if (cQpup) {
    changed  = 1;
    if (sQpup) {
     pca_Operation(PCA_INTERNAL_OFF);
     cQpup = 0; sQpup = 0;
    } else {
     pca_Operation(PCA_INTERNAL_ON);
     cQpup = 0; sQpup = 1;
    }
  } else if (cVref) {
    changed  = 1;
    if (sVref) {  // bVrefN
      bVref = 1; cVref = 0; sVref = 0;
    } else {
      bBias = 1; sBias = 0; bVref = 0; cVref = 0; sVref = 1;
    }
  } else if (cBias) {  // bBiasN
    changed  = 1;
    if (sBias) {
      bBias = 1; cBias = 0; sBias = 0;
    } else {
      bVref = 1; sVref = 0; bBias = 0; cBias = 0; sBias = 1;
    }
  }

  // Temperature alert
  if (bAlert) {  // bAlertN
    sAlert = 0;
  } else {
    changed = 1;
    sAlert = 1;    
  }
  if (changed) {
    publishCtlCsr();
  }
}

//
//-------------------------------------------------------------
void UpdateADC(void) {
#ifdef MSCB_DEVICE_ADC_LTC2495_DISABLE
#else
  static char adcChannel = N_ADC_CHANNEL + 1;
  char prevChannel;
  unsigned char range;
  static xdata signed long adc_raw;

  if(adcChannel == (N_ADC_CHANNEL + 1)) {
    adcChannel = CLEAR;
    if (adcChannel < 3)
      LTC2495_StartConversion(LTC2495_ADDR, LTC2495_CMD_DIF, user_data.Igain, adcChannel);
    else
      LTC2495_StartConversion(LTC2495_ADDR, LTC2495_CMD_DIF, LTC2495_GAIN1, adcChannel);
  } else {
    prevChannel = adcChannel;
    adcChannel++;
    adcChannel %= N_ADC_CHANNEL;

    if (adcChannel < 3)
      range = LTC2495_ReadConversion(LTC2495_ADDR, LTC2495_CMD_DIF, user_data.Igain , adcChannel, &adc_raw);
    else 
      range = LTC2495_ReadConversion(LTC2495_ADDR, LTC2495_CMD_DIF, LTC2495_GAIN1 , adcChannel, &adc_raw);
      
    switch(range) {
      case LTC2495_VALIDRANGE:
        adc_raw += ADC_ZERO_OFFSET;
        eAdc = 0;
        break;

      case LTC2495_OVERRANGE:
        eAdc = 1;
        adc_raw = 0xFFFFFFFF;
        break;

      case LTC2495_UNDERRANGE:
        eAdc = 1;
        adc_raw = 0xFFFFFFFF;
        break;
    }
    
    DISABLE_INTERRUPTS;
    if (prevChannel < 8) {
      if (prevChannel == 3) {
        user_data.volt[prevChannel] = (((float)(adc_raw + ADC_OFFSET)) * (float)(ADC_GAIN) * -100.95f);
      } else if (prevChannel < 3) {
        user_data.volt[prevChannel] = (((float)(adc_raw + ADC_OFFSET)) * (float)(ADC_GAIN));
        user_data.volt[prevChannel] -= (offsettable[user_data.Igain]);  // offset
        user_data.volt[prevChannel] /= gaintable[user_data.Igain];
        user_data.volt[prevChannel] *= 1.E6;   // U to I conversion (1KOhm) and in nA

      } else {
        user_data.volt[prevChannel] = (((float)(adc_raw + ADC_OFFSET)) * (float)(ADC_GAIN));
      }
    }

    user_data.adc[prevChannel]  = adc_raw;  // +/- Range 
    ENABLE_INTERRUPTS;

  }
#endif // MSCB_DEVICE_ADC_LTC2489_DISABLE
}

//
//-------------------------------------------------------------
void UpdateDAC(void) {
#ifdef MSCB_DEVICE_DAC_LTC2609_DISABLE
#else
  if (DAC0 || DAC1 || DAC2 || DAC3 || VDAC0 || VDAC1 || VDAC2 || VDAC3) {
    led_blink(LED_GREEN, 1, 150);
  }
  // Set all necessary DAC values, if an interrupt occurs while this is going on
  // everything should be OK, at worst we may update a newer value.
  if(DAC0) { LTC2609_Write(LTC2609_ADDR, LTC2609_WREG_n_UPDATE_n | 0, user_data.dac[0]); DAC0 = 0; }
  if(DAC1) { LTC2609_Write(LTC2609_ADDR, LTC2609_WREG_n_UPDATE_n | 1, user_data.dac[1]); DAC1 = 0; }
  if(DAC2) { LTC2609_Write(LTC2609_ADDR, LTC2609_WREG_n_UPDATE_n | 2, user_data.dac[2]); DAC2 = 0; }
  if(DAC3) { LTC2609_Write(LTC2609_ADDR, LTC2609_WREG_n_UPDATE_n | 3, user_data.dac[3]); DAC3 = 0; }
  if(VDAC0) { LTC2609_Write(LTC2609_ADDR, LTC2609_WREG_n_UPDATE_n | 0, DAC_GAIN * user_data.vdac[0]); VDAC0 = 0; }
  if(VDAC1) { LTC2609_Write(LTC2609_ADDR, LTC2609_WREG_n_UPDATE_n | 1, DAC_GAIN * user_data.vdac[1]); VDAC1 = 0; }
  if(VDAC2) { LTC2609_Write(LTC2609_ADDR, LTC2609_WREG_n_UPDATE_n | 2, DAC_GAIN * user_data.vdac[2]); VDAC2 = 0; }
  if(VDAC3) { LTC2609_Write(LTC2609_ADDR, LTC2609_WREG_n_UPDATE_n | 3, DAC_GAIN * user_data.vdac[3]); VDAC3 = 0; }
#endif // MSCB_DEVICE_DAC_LTC2609_DISABLE
}

//
//-------------------------------------------------------------
void UpdateTemperature(void) {
  float temp;

  temp = ADT7483_ReadTemp(ADT7483_ADDR, ADT7483_CH_READ_LOCAL);

  DISABLE_INTERRUPTS;
  user_data.intemp  = temp; 
  ENABLE_INTERRUPTS;
  
  temp = ADT7483_ReadTemp(ADT7483_ADDR, ADT7483_CH_READ_REMOTE1);

  DISABLE_INTERRUPTS;  
  user_data.temp[0] = temp;
  ENABLE_INTERRUPTS;

  temp = ADT7483_ReadTemp(ADT7483_ADDR, ADT7483_CH_READ_REMOTE2);
  
  DISABLE_INTERRUPTS;
  user_data.temp[1] = temp;  
  ENABLE_INTERRUPTS;
}