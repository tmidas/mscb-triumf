/********************************************************************\

  Name:     Genericio410.h
  $Id$

\********************************************************************/

#ifndef _GENERIC_IO_H
#define _GENERIC_IO_H

#include "../../common/mscbemb.h"

//
//
#define N_VOLT      8
#define N_ADC       8
#define N_DAC       4
#define N_TEMP      3
#define CTL_IDX     2
#define FIRST_VOLT  4
#define FIRST_ADC   12
#define FIRST_DAC   28
#define FIRST_VDAC  32
#define FIRST_TEMP  36
#define N_ADC_CHANNEL   8    // Number of ADC Channels
#define VREF        4.9967f/2.0f
#define ADC_OFFSET  0.0f
#define ADC_ZERO_OFFSET 0  // 65536/2
#define ADC_GAIN	  (VREF / 65536.0f)
#define DACVREF     4.5f
#define DAC_GAIN    (65536.0f / DACVREF)

#define LTC2609_ADDR  0x10 // 16-bit 4 channel DAC
#define LTC2495_ADDR  0x14 // 16-bit 8 channel ADC
#define ADT7483_ADDR  0x18 // Temperature Sensor

char code gaintable[] = {1,4,8,16,32,64,128,264};
float code offsettable[] = {0.0020967, 0.00846304, 0.0169642, 0.0339, 0.06789, 0.1359, 0.2725, 0.5440};

// Global definition
// Global ON / OFF definition
#define ON     1
#define DONE   1
#define SET    1
#define OFF    0
#define CLEAR  0

#define LED_GREEN 1
#define LED_RED   0

unsigned char freqTable[4] = {250, 150, 50 ,1};

//
//--- MSCB structure
typedef struct {
  // System
  unsigned long Igain;
  unsigned int  error;
  unsigned char control;
  unsigned char status;
  
  // Functions
  float         volt[N_VOLT];
  unsigned long adc[N_ADC];
  unsigned int  dac[N_DAC];
  float         vdac[N_DAC];
  float         intemp;     // ADT7486A internal temperature [degree celsius]
  float         temp[2];    // ADT7486A external2 temperature [degree celsius]
  unsigned char freq;
} MSCB_USER_DATA;

void UpdateADC(void);
void UpdateDAC(void);
void UpdateIO(void);
void UpdateTemperature(void);

#endif // _GENERIC_IO.H