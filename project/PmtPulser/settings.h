// $Id$


#ifndef _MSCB_PROJECT_H
#define _MSCB_PROJECT_H

#define LED_0 P0 ^ 2
#define LED_1 P0 ^ 3
#define LED_ON 1
#define MCU_C8051F410
#define RS485_ENABLE_PIN P0 ^ 6

//#define SHT_DATA1    P1 ^ 4
//#define SHT_SCK1     P1 ^ 5

#define DEL_RDBCK    P2 ^ 0
#define DEL_CLK      P2 ^ 1
#define DEL_DAT      P2 ^ 2
#define DEL_LE       P0 ^ 7    


// MSCB Constants
#define MSCB_NUM_SUB_NODES 1

// Used Protocols
#define MSCB_SMBus_HW
//#define MSCB_SHT7x

// Used Devices
//#define MSCB_DEVICE_DAC_LTC2607
//#define MSCB_DEVICE_TUI_LTC2990
#define MSCB_DEVICE_ADC_INTERNAL
#define MCSB_DEVICE_IO_AD5248
// #define MSCB_DEVICE_IO_DS1023

//#define MSCB_DEVICE_HUMIDITY_SHT7x

// Disabled Devices
//#define MCSB_DEVICE_TUI_LTC2990_DISABLE
//#define MCSB_DEVICE_ADC_LTC2493_DISABLE
//#define MSCB_DEVICE_ADC_LTC2489_DISABLE 
//#define MSCB_DEVICE_DAC_LTC2605_DISABLE 
//#define MSCB_DEVICE_IO_PCA9536_DISABLE 
//#define MSCB_DEVICE_TEMP_ADT7483_DISABLE
//#define MSCB_DEVICE_INTERNAL_PCA_DISABLE
//#define MCSB_DEVICE_TUI_LTC2990_DISABLE
//#define MSCB_DEVICE_HUMIDITY_SHT7x_DISABLE
#endif // _MSCB_PROJECT_H
