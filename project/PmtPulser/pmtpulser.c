/********************************************************************\

Name:     pmtpulser.c
Author:   P-A Amaudruz
       pulser for 64 multi-Anode PMT
       Project Geotomo, Cript
$Id$

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pmtpulser.h" 
extern xdata SYS_INFO sys_info;

char    code  MSCB_node_name[] = "pmtpulser410";
char    xdata svn_rev_code[] = "$Rev$";
xdata         MSCB_USER_DATA user_data;

MSCB_INFO_VAR code vars[] = {
  4, UNIT_BYTE,     0, 0, 0,           "SerialN",    &user_data.sn,          // 0
  2, UNIT_BYTE,     0, 0, 0,           "Error",      &user_data.error,       // 1
  1, UNIT_BYTE,     0, 0, 0,           "Control",    &user_data.control,     // 2
  1, UNIT_BYTE,     0, 0, 0,           "Status",     &user_data.status,      // 3

  1, UNIT_BYTE,     0, 0, 0,           "raw0007",    &user_data.raw[0],   // 4
  1, UNIT_BYTE,     0, 0, 0,           "raw0815",    &user_data.raw[1],   // 5
  1, UNIT_BYTE,     0, 0, 0,           "raw1623",    &user_data.raw[2],   // 6
  1, UNIT_BYTE,     0, 0, 0,           "raw2431",    &user_data.raw[3],   // 7
  1, UNIT_BYTE,     0, 0, 0,           "raw3239",    &user_data.raw[4],   // 8
  1, UNIT_BYTE,     0, 0, 0,           "raw4047",    &user_data.raw[5],   // 9
  1, UNIT_BYTE,     0, 0, 0,           "raw4855",    &user_data.raw[6],   // 10
  1, UNIT_BYTE,     0, 0, 0,           "raw5663",    &user_data.raw[7],   // 11

  1, UNIT_BYTE,     0, 0, 0,           "Trim0",      &user_data.trim[0],     // 12
  1, UNIT_BYTE,     0, 0, 0,           "Trim1",      &user_data.trim[1],     // 13
  1, UNIT_BYTE,     0, 0, 0,           "Trim2",      &user_data.trim[2],     // 14
  1, UNIT_BYTE,     0, 0, 0,           "Trim3",      &user_data.trim[3],     // 15
  
  2, UNIT_BYTE,     0, 0, 0,           "Periodic",   &user_data.periodic,        // 16
  2, UNIT_BYTE,     0, 0, 0,           "Duration",   &user_data.duration,        // 17
  // F410 internal ADCs
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "uCT",        &user_data.ucTemp,      // 18
  0
};

code MSCB_INFO_VAR *variables = vars;

/********************************************************************\

Application specific init and inout/output routines

\********************************************************************/

// Change register
volatile unsigned char bdata bChange = 0;
sbit fTrim0 = bChange ^ 0;
sbit fTrim1 = bChange ^ 1;
sbit fTrim2 = bChange ^ 2;
sbit fTrim3 = bChange ^ 3;
sbit fna5   = bChange ^ 4;

// Control register
unsigned char bdata rCTL = 0;
sbit cLatch    = rCTL ^ 0;  // Load pattern 
sbit cmTrig    = rCTL ^ 1;  // Manual Trigger LED
sbit cpTrig    = rCTL ^ 2;  // Enable Frq Trigger LED
sbit cxTrig    = rCTL ^ 3;  // Enable External Trigger LED

// Status Register
unsigned char bdata rSTAT = 0;
sbit sLatch   = rSTAT ^ 0;
sbit smTrig   = rSTAT ^ 1;
sbit spTrig   = rSTAT ^ 2;
sbit sxTrig   = rSTAT ^ 3;

// ERR Error Register
//The low and high bytes are switched in the bdata section of the memory
//This is the reason that the sbit declarations do not appear to match
//the documentation but they actually do.
unsigned int bdata rERR = 0;
sbit eTrim0 = rERR ^ 8;  //0x1  
sbit eTrim1 = rERR ^ 9;  //0x2
sbit eTrim2 = rERR ^ 10; //0x4
sbit eTrim3 = rERR ^ 11; //0x8

sbit eNa12 = rERR ^ 12;  //0x10 
sbit eNa13 = rERR ^ 13;  //0x20 
sbit eNa16 = rERR ^ 14;  //0x40 
sbit eNa15 = rERR ^ 15;  //0x80 

sbit eNa0  = rERR ^ 0;   //0x100
sbit eNa1  = rERR ^ 1;   //0x200
sbit eNa2  = rERR ^ 2;   //0x400
sbit eNa3  = rERR ^ 3;   //0x800

sbit eNa4  = rERR ^ 4;   //0x1000
sbit eNa5  = rERR ^ 5;   //0x2000
sbit eNa6  = rERR ^ 6;   //0x4000
sbit eNa7  = rERR ^ 7;   //0x8000

// Hardware bits
// P0 .7: Sp4   .6:Tx/En   .5:Rx,     .4:Tx,   .3:LED2   .2:LED1   .1:SCL   .0 SDA
// P1 .7:LEDDrv .6:Blk Trg .5:TrgClrn .4:SDI4  .3:LAT4   .2:SDI2   .1:SCK    0:LAT2   
// P2 .7:C2D    .6:SDI1    .5:LAT1    .4:SDI3  .3:LAT3   .2:SP3    .1:SP2   .0:SP1

/*---- User init function ------------------------------------------*/
void user_init(unsigned char init)
{
  char i;
  unsigned char temp;

  /* Format the SVN and store this code SVN revision into the system */
  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  sys_info.svn_revision = (svn_rev_code[6]-'0')*1000+
    (svn_rev_code[7]-'0')*100+
    (svn_rev_code[8]-'0')*10+
    (svn_rev_code[9]-'0');

  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  if (init) {
    // Initialize variables
    user_data.ucTemp = 0;
    user_data.trim[0] = 255;
    user_data.trim[1] = 255;
    user_data.trim[2] = 255;
    user_data.trim[3] = 255;
  }

  // Reference Voltage on P0.0 Enable
  //  REF0CN = 0x00;  // Int Bias off, Int Temp off, VREF input on

  // 0:analog 1:digital
  P0MDIN = 0xFF; // P0 all digital pins 
  P1MDIN = 0xFF; // P1 all digital pins
  P2MDIN = 0xFF; // P2 all digital pins
 
  // 0: open-drain, 1:push-pull
  // P0 .7: Sp4   .6:Tx/En   .5:Rx,   .4:Tx,   .3:LED2   .2:LED1   .1:SCL   .0 SDA
  P0MDOUT = ~(0x21);  //   OD: Rx, SDA

  // 0: open-drain, 1:push-pull
  // P1 .7:LEDDrv .6:Blk Trg .5:TrgClrn .4:SDI4  .3:LAT4   .2:SDI2   .1:SCK    0:LAT2   
  P1MDOUT = 0xFF;  

  // 0: open-drain, 1:push-pull
  // P2 .7:C2D    .6:SDI1    .5:LAT1    .4:SDI3  .3:LAT3   .2:SP3    .1:SP2   .0:SP1
  P2MDOUT = 0xFF;  // 

  // Setting the cross bar
  P0SKIP = 0x0; 
  P1SKIP = 0x0; 
  P2SKIP = 0x0; 

  
  XBR0 |=0x04;	 // Enable SMBus pins P0 .0/SDA .1/SCL
  // CPLD clock input 25MHz
  // XBR0 |=0x08;	 // Enable SYSCLK to Port 1.2

  rCTL  = 0;
  rSTAT = 0;
  rERR  = 0;
   
  UpdateIO();
  LEDDrv = 0;
  adc_internal_init(0x1C);  //  0(1.5V)/1(2.2V)--Vdd-TEMPE-0-nREFBE

  // Trim Digital 
  AD5248_Init();

  // Update trim to current value
  fTrim0 = fTrim1 = fTrim2 = fTrim3 = 1;
  UpdateTrim();
}

/*---- User write function -----------------------------------------*/
void user_write(unsigned char index) reentrant {

  // Set the appropriate DAC update register bit
  // Done this way so we if we can take being interrupted, and avoid a
  // read-write-modify setup. (if done using a byte)
  if (index == CTL_IDX){
    if (user_data.control & 0x01) { cLatch = 1; } 
    if (user_data.control & 0x02) { cmTrig = 1; } 
    if (user_data.control & 0x04) { cpTrig = 1; } 
    if (user_data.control & 0x08) { cxTrig = 1; } 
  } 
 
  if (index == TRIM0) { fTrim0 = 1; }
  if (index == TRIM1) { fTrim1 = 1; }
  if (index == TRIM2) { fTrim2 = 1; }
  if (index == TRIM3) { fTrim3 = 1; }

  return;
}

/*---- User read function ------------------------------------------*/

unsigned char user_read(unsigned char index)
{
  if(index);
  return 0;
}

/*---- User function called via CMD_USER command -------------------*/

unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
  // echo input data
  data_out[0] = data_in[0];
  data_out[1] = data_in[1];
  return 2;
}

//
//-----------------------------------------------------------------------------
void publishCtlCsr(void) {
    DISABLE_INTERRUPTS;
    user_data.control = rCTL;
    user_data.status  = rSTAT;
    user_data.error   = rERR;
    ENABLE_INTERRUPTS;
}

//
//-------------------------------------------------------------
unsigned char setTrim(unsigned char chip, unsigned char ch, unsigned char v) {
  unsigned char value=0;

  AD5248_Write(chip, ch, v);
  AD5248_Read(chip, ch, &value, 1);
  if (v != value) return 1;
  return 0;
}

//
//-------------------------------------------------------------
// 
void UpdateReg(void) {
int i, reg, pos, v;

  memset (Reg, 0, sizeof(Reg));
  for (i=0;i<64;i++) { 
    reg = pmt2reg[i].reg;
    pos = pmt2reg[i].pos;

    v = ((user_data.raw[i/8] & (1<<i%8)) >> (i%8));

    Reg[reg] |= (v << pos);
  }
}

//
//-------------------------------------------------------------
void LatchPattern(void) {

signed char i;

//Reg 0
  LAT1 = 0; SCK = 0; 
  for (i=15;i>=0;i--) {
    SDI1 = (Reg[0] & (1<<i));
    SDI2 = (Reg[1] & (1<<i));
    SDI3 = (Reg[2] & (1<<i));
    SDI4 = (Reg[3] & (1<<i));
    SCK = 1; SCK = 0;
  }
  LAT1 = 1; LAT1 = 0;
  LAT2 = 1; LAT2 = 0;
  LAT3 = 1; LAT3 = 0;
  LAT4 = 1; LAT4 = 0;
}

//
//-------------------------------------------------------------
void UpdateTrim(void) {

  if (fTrim0 || fTrim1 || fTrim2 || fTrim3) {
    led_blink(LED_RED, 1, 50);
  }
  
  // Set Trim
  if (fTrim0) {
     fTrim0 = 0;
     eTrim0 = setTrim(AD5238_ADDR1, 0, user_data.trim[0]);
  }
   if (fTrim1) {
     fTrim1 = 0;
     eTrim1 = setTrim(AD5238_ADDR1, 1, user_data.trim[1]);
  }
   if (fTrim2) {
     fTrim2 = 0;
     eTrim2 = setTrim(AD5238_ADDR2, 0, user_data.trim[2]);
  }
   if (fTrim3) {
     fTrim3 = 0;
     eTrim3 = setTrim(AD5238_ADDR2, 1, user_data.trim[3]);
  }

  DISABLE_INTERRUPTS;
  user_data.error   = rERR;
  ENABLE_INTERRUPTS;
}

//
//-------------------------------------------------------------
void UpdateTrigger(void) {
  unsigned int ltime;
  if (user_data.status & 0x4) {
  // Periodic single shot
    ltime = time();
    if ((ltime - ptime) <= 0) {
      ptime = ltime;
    } else if ((ltime - ptime) > user_data.periodic) {
      led_blink(LED_GREEN, 1, 50);
      LEDDrv = 1; delay_us(user_data.duration); LEDDrv = 0;
      ptime = time();
    }
  }
}

//
//-------------------------------------------------------------
/*---- User loop function -----------------------------------*/
void user_loop(void)
{

  // Command input (dac, on/off, rh)
  UpdateIO();

  // uCTemp
  UpdateiADC();

  // Periodic Trigger
  UpdateTrigger();

  UpdateTrim();

//  delay_us(1);

  // Mark loop
//  led_blink(LED_RED, 1, 50);
}

//
//-------------------------------------------------------------
void UpdateIO(void) {
  static char changed=0;

  if (cLatch) {
    cLatch = 0;
    UpdateReg();
    LatchPattern();
    sLatch = 1;
  }

  if (cmTrig) { 
    cmTrig = 0;
    sLatch = 0;
    sxTrig = 0;
    // Manual single shot
    LEDDrv = 1; delay_us(user_data.duration); LEDDrv = 0;
  }

  // Toggle External
  if (cxTrig) { 
    cxTrig = 0;
    sLatch = 0;
    if (sxTrig) {
      LEDDrv = 0;
      sxTrig = 0;
      cpTrig = 0;
    } else {
      LEDDrv = 1;
      sxTrig = 1;
      cpTrig = 0;
    } 
  }

  // Toggle Periodic
  if (cpTrig) { 
    sLatch = 0;
    sxTrig = 0;
    if (spTrig) {
      spTrig = 0;
      cpTrig = 0;
    } else {
      spTrig = 1;
      cpTrig = 0;
    } 
  }

  publishCtlCsr();
}

//
//-------------------------------------------------------------
void UpdateiADC(void) {
  unsigned int adc_raw;
  float xdata ucAdc;

  adc_raw = adc_read(ADC_INTERNAL_TEMP_CH, 1);
  ucAdc = (((float)(adc_raw)) * IADC2VOLT);
  ucAdc = (ucAdc - 0.9) / 0.00295;
  
  DISABLE_INTERRUPTS;
  user_data.ucTemp = ucAdc;
  ENABLE_INTERRUPTS;
}



