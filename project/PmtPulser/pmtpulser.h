/********************************************************************\

  Name:     pmtpulser.h
  $Id$

\********************************************************************/

#ifndef _GENERIC_IO_H
#define _GENERIC_IO_H

#include "../../common/mscbemb.h"

//
#define CTL_IDX      2
#define TRIM0       12
#define TRIM1       13
#define TRIM2       14
#define TRIM3       15

#define IVREF         2.47f
#define IADC2VOLT     (IVREF / 4096.0f)

// Digital Resistor
#define AD5238_ADDR1  0x2c 
#define AD5238_ADDR2  0x2e 

// Global definition
// Global ON / OFF definition
#define ON     1
#define OFF    0
#define DONE   1
#define SET    1
#define OFF    0
#define CLEAR  0

#define LED_GREEN 1
#define LED_RED   0

// Hardware bits
// P0 .7: Sp4   .6:Tx/En   .5:Rx,     .4:Tx,   .3:LED2   .2:LED1   .1:SCL   .0 SDA
// P1 .7:LEDDrv .6:Blk Trg .5:TrgClrn .4:SDI4  .3:LAT4   .2:SDI2   .1:SCK    0:LAT2   
// P2 .7:C2D    .6:SDI1    .5:LAT1    .4:SDI3  .3:LAT3   .2:SP3    .1:SP2   .0:SP1
sbit LEDDrv = P1 ^ 7;
sbit SCK    = P1 ^ 1;
sbit SDI1   = P2 ^ 6;
sbit LAT1   = P2 ^ 5;
sbit SDI2   = P1 ^ 2;
sbit LAT2   = P1 ^ 0;
sbit SDI3   = P2 ^ 4;
sbit LAT3   = P2 ^ 3;
sbit SDI4   = P1 ^ 4;
sbit LAT4   = P1 ^ 3;

//
// LED assignment
typedef struct {
  unsigned char reg;
  unsigned char pos;
} PMT2REG;

PMT2REG code pmt2reg[64]={{0, 0},{0, 1},{0, 2},{0, 3},{1, 4},{1, 5},{1, 6},{1, 7}   //  0 .. 7    1 ..  8
                         ,{0, 4},{0, 5},{0, 6},{0, 7},{1, 0},{1, 1},{1, 2},{1, 3}   //  8 .. 15   9 .. 16
                         ,{0,15},{0,14},{0,13},{0,12},{1,11},{1,10},{1, 9},{1, 8}   // 15 .. 23  17 .. 24 
                         ,{0,11},{0,10},{0, 9},{0, 8},{1,15},{1,14},{1,13},{1,12}   // 24 .. 31  25 .. 32 
                         ,{2, 0},{2, 1},{2, 2},{2, 3},{3, 4},{3, 5},{3, 6},{3, 7}   // +0 .. 7    1 ..  8
                         ,{2, 4},{2, 5},{2, 6},{2, 7},{3, 0},{3, 1},{3, 2},{3, 3}   //  8 .. 15   9 .. 16
                         ,{2,15},{2,14},{2,13},{2,12},{3,11},{3,10},{3, 9},{3, 8}   // 15 .. 23  17 .. 24 
                         ,{2,11},{2,10},{2, 9},{2, 8},{3,15},{3,14},{3,13},{3,12}}; // 24 .. 31  25 .. 32 
unsigned int Reg[4];
unsigned int ptime=0;

//
//--- MSCB structure
typedef struct {
  // System
  unsigned long sn;
  unsigned int  error;
  unsigned char control;
  unsigned char status;
  
  // Functions
  unsigned char raw[8];
  unsigned char trim[4];
  unsigned int  periodic;
  unsigned int  duration;
  float         ucTemp;        // 410Temp

} MSCB_USER_DATA;


void publishCtlCsr(void);
void publishErr(void);
void UpdateIO(void);
void UpdateiADC(void);
void UpdateTrim(void);
void LatchPattern(void);
void UpdateReg(void);
void UpdateTrigger(void);
void UpdateReg(void);
unsigned char setTrim(unsigned char chip, unsigned char ch, unsigned char v);
#endif // _GENERIC_IO.