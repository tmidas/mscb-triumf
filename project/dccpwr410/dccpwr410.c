/********************************************************************\

Name:     dccpwr410.c   
Author:   Pierre-Andr� Amaudruz 
Function: Control the DCC-Power switch
          Monitor current, voltage and temperature
          for (j=500 ; j>0 ; j--) ; i+= 1;  // ~2us delay

$Id$

\********************************************************************/

#include "dccpwr410.h"

extern xdata SYS_INFO sys_info;

void UpdateADCs(void);
float read_voltage(unsigned char channel, float coeff, float offset);

char code  MSCB_node_name[] = "PWRSWITCH";
char xdata svn_rev_code[]   = "$Rev$";
unsigned char once = 0;
xdata MSCB_USER_DATA user_data;

code MSCB_INFO_VAR vars[] = {
  1, UNIT_BYTE,     0, 0, 0,           "Shutdown"  , &user_data.error,         // 0
  1, UNIT_BYTE,     0, 0, 0,           "Control"   , &user_data.control,       // 1
  1, UNIT_BYTE,     0, 0, 0,           "Status"    , &user_data.status,        // 2 

  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "in Volt1"  , &user_data.invalue[0],     // 3 ADM1176
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "in Volt2"  , &user_data.invalue[1],     // 4    "
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "in Volt3"  , &user_data.invalue[2],     // 5    "

  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "outVolt1"  , &user_data.outvalue[0],    // 6 LTC2991
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "outVolt2"  , &user_data.outvalue[1],    // 7   "
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "outVolt3"  , &user_data.outvalue[2],    // 8   "
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "outVolt4"  , &user_data.outvalue[3],    // 9   "
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "outVolt5"  , &user_data.outvalue[4],    // 10  "
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "outVolt6"  , &user_data.outvalue[5],    // 11  "

  4, UNIT_AMPERE,     0, 0, MSCBF_FLOAT, "outcur1"  ,   &user_data.ivalue[0],   // 12 ADM1176
  4, UNIT_AMPERE,     0, 0, MSCBF_FLOAT, "outCur2"  ,   &user_data.ivalue[1],   // 13   "
  4, UNIT_AMPERE,     0, 0, MSCBF_FLOAT, "outCur3"  ,   &user_data.ivalue[2],   // 14   "
  4, UNIT_AMPERE,     0, 0, MSCBF_FLOAT, "outCur4"  ,   &user_data.ivalue[3],   // 15   "
  4, UNIT_AMPERE,     0, 0, MSCBF_FLOAT, "outCur5"  ,   &user_data.ivalue[4],   // 16   " 
  4, UNIT_AMPERE,     0, 0, MSCBF_FLOAT, "outCur6"  ,   &user_data.ivalue[5],   // 17   " 

  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "Temp1"  ,   &user_data.temperature[0], // 18  LTC2990
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "Temp2"  ,   &user_data.temperature[1], // 19    "
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "Temp3"  ,   &user_data.temperature[2], // 20    "
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "Temp4"  ,   &user_data.temperature[3], // 21    "
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "Temp5"  ,   &user_data.temperature[4], // 22    "
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "Temp6"  ,   &user_data.temperature[5], // 23    "

  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "inTemp1"  ,   &user_data.localtemp[0], // 24  LTC2990
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "inTemp2"  ,   &user_data.localtemp[1], // 25    "
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "inTemp3"  ,   &user_data.localtemp[2], // 26    "
  
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "LTemp1"  ,   &user_data.limit[0], // 27
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "LTemp2"  ,   &user_data.limit[1], // 28 
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "LTemp3"  ,   &user_data.limit[2], // 29
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "LTemp4"  ,   &user_data.limit[3], // 30
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "LTemp5"  ,   &user_data.limit[4], // 31
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "LTemp6"  ,   &user_data.limit[5], // 32

  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "AvgeT1"  ,   &user_data.atemperature[0], // 33  LTC2990
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "AvgeT2"  ,   &user_data.atemperature[1], // 34    "
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "AvgeT3"  ,   &user_data.atemperature[2], // 35    "
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "AvgeT4"  ,   &user_data.atemperature[3], // 36    "
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "AvgeT5"  ,   &user_data.atemperature[4], // 37    "
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "AvgeT6"  ,   &user_data.atemperature[5], // 38    "
//  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,"Navge"   ,   &user_data.navge,           // 39     

  0
};

code MSCB_INFO_VAR *variables = vars;

/********************************************************************\

Application specific init and input/output routines

\********************************************************************/

static signed char xdata lAvge = -1;
// Change register
unsigned char bdata bChange = 0;
sbit fAvge = bChange ^ 0;

// ESR Error Register
unsigned char bdata rESR;
sbit eoverall    = rESR ^ 0;  //0x1
sbit etemplimit  = rESR ^ 1;  //0x2
sbit eOCurrent   = rESR ^ 2;  //0x4
sbit espare1     = rESR ^ 3;  //0x8
sbit espare2     = rESR ^ 4;  //0x10
sbit etempchip1  = rESR ^ 5;  //0x20
sbit etempchip2  = rESR ^ 6;  //0x40
sbit etempchip3  = rESR ^ 7;  //0x80

sbit ON1 = P2 ^ 0;    // 
sbit ON2 = P2 ^ 1;    //  
sbit ON3 = P2 ^ 2;    //  
sbit ON4 = P2 ^ 3;    //  
sbit ON5 = P2 ^ 4;    //  
sbit ON6 = P2 ^ 5;    // 
sbit LED_FAULT = P1 ^ 0;   // 

//
//-----------------------------------------------------------------------------
void publishErr(void) {
    DISABLE_INTERRUPTS;
    user_data.error   = rESR;
    ENABLE_INTERRUPTS;
}

/*---- User init function ------------------------------------------*/
void user_init(unsigned char init)
{
  idata char i;

  /* Format the SVN and store this code SVN revision into the system */
  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }
  sys_info.svn_revision = (svn_rev_code[6]-'0')*1000+
    (svn_rev_code[7]-'0')*100+
    (svn_rev_code[8]-'0')*10+
    (svn_rev_code[9]-'0');

  if (init) {
    user_data.error = 0;
    user_data.control = 0;
    user_data.status = 0;
    for (i=0;i<N_TEMPERATURE;i++) {
     user_data.limit[i] = 50.0;
	  }
  }

  // 0: open-drain, 1:push-pull
  // P0 .7:Tx/En  .6:n/a,    .5:Rx,   .4:Tx,    .3:n/a,   .2:n/a,  .1: SCK, .0 SDA
  P0MDOUT = 0x92;  //   OD: Rx, SDA

  // 0: open-drain, 1:push-pull
  // P1 .7:na/ .6:n/a  .5:n/a .4:n/a  .3:n/a    .2:LED1  .1:LED2,  .0:LED_Fault
  P1MDOUT = 0xFF;  //   

  // 0: open-drain, 1:push-pull
  // P2 .7:C2D    6:n/a  .5:ON6 .4:ON5 .3:ON4  .2:ON3 .1:ON2  .0:ON1
  P2MDOUT = 0xFF;  // 


   // Setting the cross bar
  P0SKIP = 0x0; 
  P1SKIP = 0x0; 
  P2SKIP = 0x0; 

  XBR0 |=0x04;	 // Enable SMBus pins P0 .0/SDA .1/SCL

  // Shutdown all 6 switches
  ON1 = ON2 = ON3 = ON4 = ON5 = ON6 = 0;

  // Fault reset
  LED_FAULT = 0;

  LTC2990TUI_Init();        // Init Temperature
  LTC2991TUI_Init();        // Init Voltage
  ADM1176_Init();           // Init Volt / Current 

  // Initialize variables
  for (i=0;i<6;i++) {
    user_data.outvalue[i] = 0.0;
    user_data.ivalue[i] = 0.0;
    user_data.temperature[i] = 0.0;
  }

  for (i=0;i<3;i++) {
    user_data.invalue[i] = 0.0;
    user_data.temperature[i] = 0.0;
  }

  user_data.control = 0;
  user_data.status  = 0;
  user_data.error   = 0;

//  user_data_navge = 30;
 
  for(i=0; i<N_TEMPERATURE; i++) {
    user_data.temperature[i] = 0.0;
    user_data.atemperature[i]= 0.0;
  }

  // Force a fAvge;
  fAvge = 1;

}

/*---- User write function -----------------------------------------*/
void user_write(unsigned char index) reentrant
{
  if (etempchip1 && etempchip2 && etempchip3) {
  // No Temp sensors operational, don't turn on device
    eoverall = 1;
    LED_FAULT = 1;
    user_data.control = 0;
    led_blink(LED_RED, 5, 20);
  } else if (index == IDX_CONTROL) {
    led_blink(LED_RED, 1, 50);
		ON1 = (user_data.control & 0x01);
		ON2 = (user_data.control & 0x02);
		ON3 = (user_data.control & 0x04);
		ON4 = (user_data.control & 0x08);
		ON5 = (user_data.control & 0x10);
		ON6 = (user_data.control & 0x20);
		user_data.status  = user_data.control;
		user_data.control = 0;
		user_data.error   = 0;
		rESR = 0;
    LED_FAULT = 0;
	} else if (index == IDX_AVGE) {
    fAvge = 1;
  }
}

/*---- User read function ------------------------------------------*/
unsigned char user_read(unsigned char index)
{
  if(index);
  return 0;
}

/*---- User function called vid CMD_USER command -------------------*/
unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
  /* echo input data */
  data_out[0] = data_in[0];
  data_out[1] = data_in[1];
  return 2;
}


//
//----------------------------------------------------------------------------------
//Main user loop
void user_loop(void)
{
  unsigned char i;
  static unsigned char xdata cavg=-1;
  static unsigned char idata channel, avgcount, numavg=0;
  float xdata average;

  // Read temperatures
  for (i=0; i<N_TCHIP; i++) UpdateTemp(i);

  // Reset averaging in case of change
  if (fAvge) {
    fAvge = 0;
    if((user_data_navge <= TAVGMAX) && (user_data_navge > 0)) {
      lAvge = user_data_navge;
      cavg = -1;
      numavg = 0;
    } else {
      lAvge = -1;
    }
  }

  if (lAvge != -1) {
    // Calculating the rolling average
    cavg++;
    if (numavg != lAvge) numavg = cavg+1; else numavg = lAvge;
	  cavg %= lAvge;
  
    // Do average on all the channels
    for(channel=0; channel<N_TEMPERATURE; channel++) {
      Taverage[channel][(cavg)] = user_data.temperature[channel];

      // Reset this channel average    
      average=0;

      // Sum temps
      for(avgcount=0; avgcount<numavg; avgcount++) {
       average += Taverage[channel][avgcount];
       }
    
      // Compute average
      average /= numavg;
    
      DISABLE_INTERRUPTS;
      user_data.atemperature[channel]=average;
      ENABLE_INTERRUPTS;
    }
  }

  // Check switches
  for (i=0; i<N_SWITCH; i++) UpdateSwitch(i);  // input U/I

  UpdateVoltage();  // output U

  // Check if at least one TChip is present otherwise shutdown
  if (etempchip1 && etempchip2 && etempchip3) {
     eoverall = LED_FAULT = 1;
  }

  CheckTemperature();   // Temperature in Range
  if (etemplimit) {
     eoverall = LED_FAULT = 1;
  }

/*  CheckHotSwap();
  if (eOCurrent) {
     eoverall = LED_FAULT = 1;
  }
*/
	
  publishErr();

	// Shutdown board if any of the temperature is too high
	// NO Condition on FAULT YET!
	if(eoverall) {
		ON1 = ON2 = ON3 = ON4 = ON5 = ON6 = 0;
		DISABLE_INTERRUPTS;
		user_data.status = 0;
		ENABLE_INTERRUPTS;
	}

  led_blink(LED_GREEN, 1, 50);
}

//
//-------------------------------------------------------------
void CheckHotSwap(void) {
  unsigned char i, ADM1176_ADDR, status;
  eOCurrent = 0;

  for (i=0;i<N_SWITCH;i++) {
    ADM1176_ADDR = amd1176_addr[i];
    if(ADM1176_ReadStatus(ADM1176_ADDR, &status)) {
      if (status & 0x1) {
        eOCurrent |= 1; 
      }
    }
  }
  publishErr();
}

//
//-------------------------------------------------------------
void CheckTemperature(void) {
  unsigned char i, tmp;
//  etemplimit = 0;
  tmp = rESR;
  for (i=0;i<N_TEMPERATURE;i++) {
    switch (i) {
      case 0:
      case 1:
      if ((etempchip1 == 0) && (user_data.atemperature[i] != -1.)) {
        if (user_data.atemperature[i] >= user_data.limit[i]) {
          etemplimit |= 1; 
        }
      }
      break;
      case 2:
      case 3:
      if ((etempchip2 == 0) && (user_data.atemperature[i] != -1.)) {
        if (user_data.atemperature[i] >= user_data.limit[i]) {
          etemplimit |= 1; 
        }
      }
      break;
      case 4:
      case 5:
      if ((etempchip3 == 0) && (user_data.atemperature[i] != -1.)) {
        if (user_data.atemperature[i] >= user_data.limit[i]) {
          etemplimit |= 1; 
        }
      }
      break;
    }
  }
  publishErr();
}

//
//-------------------------------------------------------------
void UpdateTemp(unsigned char chip) {
#ifdef MCSB_DEVICE_TUI_LTC2990_DISABLE
#else
  char i, LTC2990_ADDR;
  signed int xdata raw[3];

  // Set address of the chip
  LTC2990_ADDR = ltc2990_addr[chip];
  
  // Internal Temperature sensors (on the board)
  LTC2990TUI_WriteRegister(LTC2990_ADDR, LTC2990TUI_CONTROL, LTC2990TUI_SGL_ACQ);
  LTC2990TUI_WriteRegister(LTC2990_ADDR, LTC2990TUI_TRIGGER, 1);
  delay_ms(50);
  i = LTC2990TUI_ReadTemperature(LTC2990_ADDR, LTC2990TUI_INTTEMP, &(raw[2])); // Internal T

  // Remote Temp TR1 and TR2
  LTC2990TUI_WriteRegister(LTC2990_ADDR, LTC2990TUI_CONTROL, 0x5D);  //DegC/Single/mode2/TR1,TR2
  LTC2990TUI_WriteRegister(LTC2990_ADDR, LTC2990TUI_TRIGGER, 1);
  delay_ms(50);
  i = LTC2990TUI_ReadTemperature(LTC2990_ADDR, LTC2990TUI_VOLT1, &(raw[0])); // Tr1
  i = LTC2990TUI_ReadTemperature(LTC2990_ADDR, LTC2990TUI_VOLT3, &(raw[1])); // Tr2

  if (i == LTC2990TUI_VALIDRANGE) {
    DISABLE_INTERRUPTS;
    user_data.temperature[2*chip]   = raw[0] * LTC2990TUI_TEMPCOEFF;
    rESR &= ~(1<<(5+chip));

    user_data.temperature[2*chip+1] = raw[1] * LTC2990TUI_TEMPCOEFF;  
    rESR &= ~(1<<(5+chip));

    user_data.localtemp[chip]       = raw[2] * LTC2990TUI_TEMPCOEFF;
    ENABLE_INTERRUPTS;
  } else {
    DISABLE_INTERRUPTS;
    user_data.temperature[2*chip]   = -1.;
    user_data.temperature[2*chip+1] = -1.;  
    user_data.localtemp[chip]       = -1.;
    rESR |= (1<<(5+chip));
    ENABLE_INTERRUPTS;
  }

  publishErr();

#endif // MCSB_DEVICE_TUI_LTC2990_DISABLE
}

//
//-------------------------------------------------------------
void UpdateSwitch(unsigned char chip) {

#ifdef MCSB_DEVICE_ADM1176_DISABLE
#else
  unsigned int volt, current;
  unsigned char ADM1176_ADDR, status, raw[3];

  // Get address from table (.h)
  ADM1176_ADDR = amd1176_addr[chip];

  if(ADM1176_ReadStatus(ADM1176_ADDR, &status)) {

    // Read Voltage and Current
    if(ADM1176_CombineConversion(ADM1176_ADDR, 0x0A, &raw[0])) {

      volt = raw[0];
      volt = (volt << 4) | (raw[2]>>4);

      current = raw[1];
      current = (current<<4) | (raw[2] & 0x0F);

      // See manual & .h for conversion factor
      DISABLE_INTERRUPTS;
      if (chip < 3) user_data.invalue[chip]  = volt * ADM1176_DAC2VOLT;
      user_data.ivalue[chip]   = current * ADM1176_DAC2CURRENT;  
      ENABLE_INTERRUPTS;
    }
  } else {
    // Failed, notify that device is down
  }


#endif // MCSB_DEVICE_ADM1176_DISABLE
}

//
//-------------------------------------------------------------
unsigned char const voltmap[6] = {2,1,0,5,4,3};
void UpdateVoltage(void) {
#ifdef MCSB_DEVICE_TUI_LTC2991_DISABLE
#else
  unsigned char i;
  signed int raw[N_VOLTAGES];


  // Read Voltages, No filter, Voltage Single Ended
  LTC2991TUI_WriteRegister(LTC2991_ADDR, LTC2991TUI_V1234CTL, 0x0);
  LTC2991TUI_WriteRegister(LTC2991_ADDR, LTC2991TUI_V5678CTL, 0x0);
  // Single Shot
  LTC2991TUI_WriteRegister(LTC2991_ADDR, LTC2991TUI_PWM, 0x0); 
  // Trigger single shot
  LTC2991TUI_WriteRegister(LTC2991_ADDR, LTC2991TUI_TRIGGER, 0xF0);
//  delay_ms(60);

  // Read Voltages
  for (i=0;i<N_VOLTAGES;i++) {
    LTC2991TUI_ReadVoltage(LTC2991_ADDR, LTC2991TUI_VOLT1+(2*i), &(raw[i]));
  }

  DISABLE_INTERRUPTS;
  for (i=0;i<N_VOLTAGES;i++) {
    if (raw[i] > 5) user_data.outvalue[voltmap[i]]  = raw[i] * LTC2991_DAC2VOLT;
    else user_data.outvalue[voltmap[i]] = 0.;
  }
  ENABLE_INTERRUPTS;
  

#endif // MCSB_DEVICE_TUI_LTC2991_DISABLE
}

