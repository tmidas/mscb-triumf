#ifndef _MSCB_PROJECT_H
#define _MSCB_PROJECT_H

// MSCB Project Defines
#define MCU_C8051F410
#define LED_0             P1 ^ 2   // Green
#define LED_1             P1 ^ 1   // Red
#define LED_ON            0
#define RS485_ENABLE_PIN 	P0 ^ 7

// MSCB Constants
#define MSCB_NUM_SUB_NODES 1

// Used Protocols
#define MSCB_SMBus_HW

// Used Devices
//#define MSCB_DEVICE_ADC_INTERNAL
#define MSCB_DEVICE_TUI_LTC2990
#define MSCB_DEVICE_TUI_LTC2991
#define MSCB_DEVICE_ADC_ADM1176
//#define MCSB_DEVICE_TUI_LTC2991_DISABLE
#define MCSB_DEVICE_ADM1176   // _DISABLE
#endif // _MSCB_PROJECT_H
