/********************************************************************\

  Name:     dccpwr410.h
  $Id$

\********************************************************************/

#ifndef POWER_SWITCH_H
#define POWER_SWITCH_H

#include "../../common/mscbemb.h"

#define N_VOLTAGES 6
#define LTC2991_ADDR 0x4f  // Voltage readbacks
#define LTC2991_VREF 2.5f 
#define LTC2991_DAC2VOLT   LTC2991_VREF / 8192 * 2.


#define DAC2VOLT 1.f
#define N_TEMPERATURE  6
#define N_TCHIP 3
unsigned char ltc2990_addr[N_TCHIP] = {0x4c, 0x4d, 0x4e}; // Temperature

#define N_SWITCH 6
unsigned char amd1176_addr[N_SWITCH] = {0x40, 0x42, 0x43, 0x48, 0x4a, 0x4b}; // Volt in, Current out
//#define ADM1176_VREF  6.65f
#define ADM1176_VREF  26.35f         
#define ADM1176_DAC2VOLT       ADM1176_VREF / 4096.
#define SENSE_RESISTOR         0.01  // Ohm
#define ADM1176_DAC2CURRENT    0.10584 / 4096 / SENSE_RESISTOR

// Averging Temperature
#define user_data_navge  50
#define TAVGMAX (int) 50
float xdata Taverage[N_TEMPERATURE][TAVGMAX];

//
//--- MSCB structure
typedef struct user_data_type {
// System
  unsigned char error;
  unsigned char control;
  unsigned char status;
  float         invalue[3];
  float         outvalue[N_VOLTAGES];
  float         ivalue[N_VOLTAGES];
  float         temperature[N_TEMPERATURE];
  float         localtemp[3];
  float         limit[N_TEMPERATURE];
  float         atemperature[N_TEMPERATURE];
  unsigned int  navge;      // Number of samples for averging temperature
} MSCB_USER_DATA;

// Global definition
// Global ON / OFF definition
#define ON     1
#define DONE   1
#define SET    1
#define OFF    0
#define CLEAR  0
#define LED_GREEN 0
#define LED_RED   1

// Indices for user_write functions
#define IDX_CONTROL				1
#define IDX_AVGE         39

void UpdateTemp(unsigned char chip);
void UpdateSwitch(unsigned char chip);
void UpdateVoltage(void);
void publishErr(void);
void CheckTemperature(void);
void CheckHotSwap(void);

#endif // POWER_SWITCH_H