#ifndef _MSCB_PROJECT_H
#define _MSCB_PROJECT_H

// MSCB Project Defines
#define MCU_C8051F310
#define LED_0 						P3 ^ 0
#define LED_ON 						1
#define RS485_ENABLE_PIN 	P0 ^ 3

// MSCB Constants
#define MSCB_NUM_SUB_NODES 1

// Used Protocols and related defines
#define MSCB_SST
#define MSCB_SST1 				P2 ^ 6 //SST1 line  SST_IO (Write/Push-Pull) 
#define SST_ClientResponse1 (char) ((CPT1CN & 0x40) >> 6) //Comparator1 overflow bit

// Used Devices
#define MSCB_DEVICE_ADC_INTERNAL
#define MSCB_DEVICE_TEMP_ADT7486A

#endif // _MSCB_PROJECT_H
