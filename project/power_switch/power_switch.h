/********************************************************************\

  Name:     power_switch.h
  $Id$

\********************************************************************/

#ifndef POWER_SWITCH_H
#define POWER_SWITCH_H

#include "../../common/mscbemb.h"

#define N_POWER_CHANNELS 15
#define SST_LINE1         1
#define VREF         3.293f

//
//--- MSCB structure
typedef struct user_data_type {
// System
  unsigned char error;
  unsigned char control;
  unsigned char status;
  float         value[15];
  float         temperature[6];
  float         localtemp[3];
  float         limit[6];
} MSCB_USER_DATA;

// Global definition
// Global ON / OFF definition
#define ON     1
#define DONE   1
#define SET    1
#define OFF    0
#define CLEAR  0

// Indices for user_write functions
#define IDX_CONTROL				1

#endif // POWER_SWITCH_H