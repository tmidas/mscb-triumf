/********************************************************************\

  Name:         LXe_IVC32.h
  Created by:   Chris Ohlmann    Apr/13/2007


  Contents:     Application specific (user) part of
                Midas Slow Control Bus protocol
                for LXe_IVC32 APD Bias / Intermediate Voltage Control

  $Id$

\********************************************************************/

#ifndef  _LXE_IVC32_H
#define  _LXE_IVC32_H

#include "../../common/mscbemb.h"

// number of HV channels
#define N_CHANNELS       32

#define HV_WARNING_THRESHOLD      24.0f

// ADC0 Channels
#define CH_TEMPERATURE   8
#define CH_HVSUPPLY      0

// maximum voltage in volts 
#define MAX_VOLTAGE      250

// maximum current in nano amps
#define MAX_CURRENT      100
#define TRIP_DELAY       1000     // in milliseconds

#define STEP_VOLTAGE     1
#define STEP_DELAY       100     // in milliseconds

// calibration constants
#define ADC0_N_AVERAGE   1
#define ADC0_VREF        2.4f
#define VREF 		       2.5f
#define VS_GAIN          110.5211f  // 106.96
#define VS_OFFSET        0.0f
#define V_GAIN				 50.8f
#define V_OFFSET			 0.0f
#define I_GAIN				 1000.0f  // for current in nA
#define I_OFFSET			 0.0f

//#define AD7490_CMDWIDTH		16

// HV254 parameters
#define HV254_GAIN       50.0f

/* CSR control bits */
#define CONTROL_ENABLED 	(1<<0)
#define CONTROL_REGULATED	(1<<1)

/* CSR status bits */
#define STATUS_RAMP_DOWN   (1<<0)
#define STATUS_RAMP_UP     (1<<1)
#define STATUS_NEW_VALUE   (1<<2)
#define STATUS_VLIMIT      (1<<6)
#define STATUS_ILIMIT      (1<<7)


//--- MSCB structure
typedef struct user_data_type {
  unsigned char control;       // LXe_IVC32 Control Register
  unsigned char status;        // LXe_IVC32 Status Register
  float         v_set;         // Requested HV [V]
  float         v_measured;    // Requested HV [V]
  float         i_measured;    // Measured current [nA]
  float         i_limit;       // Current limit
  unsigned int  trip_delay;    // Time over-limit for trip [ms]

  unsigned int  ramp_step;     // Voltage step ramp [V]
  unsigned int  ramp_delay;    // Time per ramp step [ms]

  float         v_supply;      // Measured HV Supply Voltage [V]
  float         temperature_c8051;

  float			    v_gain;
  float			    v_offset;
  float			    i_gain;
  float			    i_offset;
} MSCB_USER_DATA; 

/********************************************************************\

  Application specific init and inout/output routines

\********************************************************************/

void user_init(unsigned char init);
void user_loop(void);
void user_write(unsigned char index) reentrant;

float read_hvsupply(void);
float read_temperature(void);

void ramp_voltage(unsigned char channel);
void regulate_voltage(unsigned char channel);
void check_current(unsigned char channel);

void channel_enable(unsigned char channel);
void channel_disable(unsigned char channel);
void check_current(unsigned char channel);

unsigned int read_current(unsigned char channel);
unsigned int read_voltage(unsigned char channel);

#endif