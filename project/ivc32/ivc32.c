/********************************************************************\
  Name:         ivc32.c
  Created by:   Chris Ohlmann                         April 18, 2007
                P. Amaudruz

  Contents:     Application specific (user) part of
                Midas Slow Control Bus protocol
                for LXe_IVC32 APD Bias / Intermediate Voltage Control
\********************************************************************/

//  need to have LXE_IVC32 defined.

#include <stdio.h>
#include <math.h>
#include "ivc32.h"

extern xdata SYS_INFO sys_info;

char code MSCB_node_name[] = "LXe_IVC32";

char xdata svn_rev_code[]   = "$Rev$";

extern bit FREEZE_MODE;
extern bit DEBUG_MODE;

/* 4-bit Hardware Address pins */
sbit A3 = P1 ^ 7;  // MSB
sbit A2 = P1 ^ 6;
sbit A1 = P1 ^ 5;
sbit A0 = P1 ^ 4;  // LSB

/* AD5383 pins - Voltage Control DAC */
sbit DAC_SYNCn  = P2 ^ 4;         // Frame Sync for Serial Clock (Chip Select)
sbit DAC_PD     = P3 ^ 4;         // Power Down
sbit DAC_CLRn   = P3 ^ 5;         // Asynchronous Clear (analog)
sbit DAC_RESETn = P3 ^ 6;         // Asynchronous Reset (digital)
sbit DAC_BUSYn  = P3 ^ 7;         // Busy
sbit DAC_LDACn  = P3 ^ 0;         // Load DAC

/* AD7490 pins - Current/Voltage Monitoring ADCs */
sbit IADC_CS0n   = P2 ^ 0;         // Chip Select (Current Readback ADC for Ch[31..16])
sbit IADC_CS1n   = P2 ^ 1;         // Chip Select (Current Readback ADC for Ch[15..0])
sbit VADC_CS0n   = P2 ^ 2;         // Chip Select (Voltage Readback ADC for Ch[31..16])
sbit VADC_CS1n   = P2 ^ 3;         // Chip Select (Voltage Readback ADC for Ch[15..0])

/* data buffer (mirrored in EEPROM) */

sbit led_red    = P2 ^ 7;
sbit led_yellow = P2 ^ 6;
sbit led_green  = P2 ^ 5;

xdata MSCB_USER_DATA user_data[MSCB_NUM_SUB_NODES];

float         xdata v_dac[MSCB_NUM_SUB_NODES];
unsigned long xdata t_ramp[MSCB_NUM_SUB_NODES];
unsigned long xdata t_trip[MSCB_NUM_SUB_NODES];

code MSCB_INFO_VAR vars[] = {

  1, UNIT_BYTE,            0, 0,           0, "Control",   &user_data[0].control,                  // 0
  1, UNIT_BYTE,            0, 0,           0, "Status",    &user_data[0].status,                   // 1
  4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "Vset",      &user_data[0].v_set,                    // 2
  4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "Vmeas",     &user_data[0].v_measured,               // 3
  4, UNIT_AMPERE,  PRFX_NANO, 0, MSCBF_FLOAT, "Imeas",     &user_data[0].i_measured,               // 4
  4, UNIT_AMPERE,  PRFX_NANO, 0, MSCBF_FLOAT, "Ilimit",    &user_data[0].i_limit,                  // 5
  2, UNIT_SECOND, PRFX_MILLI, 0,           0, "TripTime",  &user_data[0].trip_delay,               // 6

  2, UNIT_VOLT,            0, 0,           0, "RampStep",  &user_data[0].ramp_step,                // 7
  2, UNIT_SECOND, PRFX_MILLI, 0,           0, "StepTime",  &user_data[0].ramp_delay,               // 8

  4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "Vsupply",   &user_data[0].v_supply,                 // 9
  4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "T_CPU",     &user_data[0].temperature_c8051,        // 10

  /* calibration constants */
  4, UNIT_FACTOR,          0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Vgain",    &user_data[0].v_gain,     // 11
  4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Voffset",  &user_data[0].v_offset,   // 12
  4, UNIT_FACTOR,          0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Igain",    &user_data[0].i_gain,     // 13
  4, UNIT_AMPERE,  PRFX_NANO, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Ioffset",  &user_data[0].i_offset,   // 14
  //   4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "VDAC",    &user_data[0].u_dac,     // 15
  0
};


code MSCB_INFO_VAR *variables = vars;

/********************************************************************\

  Application specific init and inout/output routines

\********************************************************************/

/*---- User init function ------------------------------------------*/
void user_init(unsigned char init)
{
  unsigned int i;
  unsigned int address;


  /* initial non-zero eeprom values */
  if (init) {

    for (i=0; i<MSCB_NUM_SUB_NODES; i++) {
      // Set initial calibration constants
      user_data[i].v_gain = V_GAIN;
      user_data[i].v_offset = V_OFFSET;
      user_data[i].i_gain = I_GAIN;
      user_data[i].i_offset = I_OFFSET;

      // Set initial operational parameters
      user_data[i].i_limit = MAX_CURRENT;
      user_data[i].trip_delay = TRIP_DELAY;
      user_data[i].ramp_step = STEP_VOLTAGE;
      user_data[i].ramp_delay = STEP_DELAY;

      user_data[i].control |= CONTROL_REGULATED;
    }
  }

  for (i=0; i<MSCB_NUM_SUB_NODES; i++) {
    v_dac[i] = 0;
    t_trip[i] = time();
    t_ramp[i] = time();
    user_data[i].v_set = 0;
    user_data[i].status |= STATUS_NEW_VALUE;
    channel_enable(i);
  }

  SFRPAGE = CONFIG_PAGE;

  /* configure user I/O ports */
  P1MDOUT = 0x03;  // SCLK, MOSI: PushPull Output
  P2MDOUT = 0xFF;  // LEDs and all Chip Selects: Pushpull Output
  P3MDOUT = 0x71;  // DAC_RESETn, DAC_CLRn, DAC_PD, DAC_LDACn: PushPull Output
  P4MDOUT = 0xFF;  // Channel Enables: Pushpull Outputs
  P5MDOUT = 0xFF;  // Channel Enables: Pushpull Outputs
  P6MDOUT = 0xFF;  // Channel Enables: Pushpull Outputs
  P7MDOUT = 0xFF;  // Channel Enables: Pushpull Outputs

  // set input ports to high-z
  A3 = 1;
  A2 = 1;
  A1 = 1;
  A0 = 1;

  DAC_BUSYn = 1;

  // Configure DAC
  DAC_PD     = 0;   // Disable Powerdown... Set for normal operation
  DAC_CLRn   = 1;
  DAC_RESETn = 1;
  DAC_LDACn  = 0;   // Set LDAC... Load DAC immediately when register value is updated

  // DAC Enable 2.5 Int. Reference and Thermal Monitor
  AD5383_Init();
  DAC_SYNCn = 0;
  AD5383_Write(AD5383_SFR, AD5283_CR, 0xD40);
  DAC_SYNCn = 1;


  SFRPAGE = LEGACY_PAGE;

  /* enable ADC */
  ADC0CN = 0xC0;  // ADC0 Enabled
  // Tracking on request (ADBUSY)
  // left-justified
  ADC0CF  = 0x80; // ADC conversion 16 clocks
  // PGA gain = 1

  REF0CN = 0x07; // Enable Temperature Sensor
  // Enable Internal Bias for ADC/Vref)
  // Enable Internal Reference Buffer

  adc_internal_init(0x0E);  //  0(1.5V)/1(2.2V)-0-TEMPE-0-REFBE

  /* Configure Addressing (16-bit)...  xxxxHHHHxxxCCCCC */
  /* S = MSCB soft address */
  /* H = hard address */
  /* C = Channel Address */

  address = A3;
  address = (address << 1) | A2;
  address = (address << 1) | A1;
  address = (address << 1) | A0;
  address = address << 8;

  address |= (sys_info.node_addr & 0xF000);
  sys_info.node_addr = address;


  /* set default group address */
  if (sys_info.group_addr == 0xFFFF)
  sys_info.group_addr = 600;

}

/*---- User write function -----------------------------------------*/
#pragma NOAREGS

void user_write(unsigned char index) reentrant
{
  if (index == 0) {
    // reset trip condition
    t_trip[cur_sub_addr()] = time();
  }
  else if(index == 2) {
    // reset trip condition
    t_trip[cur_sub_addr()] = time();

    // re-enable the channel
    user_data[cur_sub_addr()].control |= CONTROL_ENABLED;
  }

  if (index == 0 || index == 2) {

    user_data[cur_sub_addr()].status |= STATUS_NEW_VALUE;

    if (user_data[cur_sub_addr()].control & CONTROL_ENABLED)
    channel_enable(cur_sub_addr());
    else
    channel_disable(cur_sub_addr());
  }
}

/*---- User read function ------------------------------------------*/

unsigned char user_read(unsigned char index)
{
  if (index);
  return 0;
}

/*---- User function called vid CMD_USER command -------------------*/

unsigned char user_func(unsigned char *data_in,
unsigned char *data_out)
{
  /* echo input data */
  data_out[0] = data_in[0];
  data_out[1] = data_in[1];
  return 2;
}

/*------------------------------------------------------------------*/
float read_hvsupply()
{
  unsigned int  i;
  unsigned long v_average;
  float         voltage;

  v_average = 0;
  for (i=0 ; i<ADC0_N_AVERAGE ; i++) {
    v_average += adc_read(CH_HVSUPPLY, 1);
    watchdog_refresh(0);
  }

  /* convert to V */
  voltage = (float)  v_average / ADC0_N_AVERAGE;    // averaging
  voltage = (float)  voltage / 1024.0 * ADC0_VREF;   // conversion
  voltage = (voltage * VS_GAIN) + VS_OFFSET;    // correction

  DISABLE_INTERRUPTS;
  for (i=0; i<MSCB_NUM_SUB_NODES; i++) {
    user_data[i].v_supply = voltage;
  }
  ENABLE_INTERRUPTS;

  return voltage;
}

/*------------------------------------------------------------------*/
float read_temperature()
{
  unsigned int  i;
  unsigned long t_average;
  float         temperature;

  t_average = 0;
  for (i=0 ; i<ADC0_N_AVERAGE ; i++) {
    t_average += adc_read(CH_TEMPERATURE, 1);
    watchdog_refresh(0);
  }

  /* convert to V */
  temperature = (float)  t_average / ADC0_N_AVERAGE;    // averaging
  temperature = (float)  temperature / 1024.0 * ADC0_VREF;   // conversion

  /* convert to deg. C */
  temperature = (temperature - 0.775) / 0.00275;  // calibration

  /* strip to 0.1 digits */
  temperature = ((int) (temperature * 10 + 0.5)) / 10.0;

  DISABLE_INTERRUPTS;
  for (i=0; i<MSCB_NUM_SUB_NODES; i++) {
    user_data[i].temperature_c8051 = temperature;
  }
  ENABLE_INTERRUPTS;

  return temperature;
}

/*------------------------------------------------------------------*/
void ramp_voltage(unsigned char channel)
{
  unsigned int value;
  unsigned char load_dac = 0;

  if ((user_data[channel].control & CONTROL_ENABLED) &&
      (user_data[channel].status & STATUS_NEW_VALUE)) {

    // Check if it's time for next voltage step (time resolution is 10ms)
    if (time() >= (t_ramp[channel] + (user_data[channel].ramp_delay / 10))) {

      // Ramp Up Voltage
      //       if (user_data[channel].v_set > v_dac[channel]) {
      if (user_data[channel].v_set > user_data[channel].v_measured) {

        // Check if a step increase will cause an overshoot
        //          if ((v_dac[channel] + user_data[channel].ramp_step) < user_data[channel].v_set) {
        if ((user_data[channel].v_measured + user_data[channel].ramp_step) < user_data[channel].v_set) {
          // Increase DAC voltage by one step
          user_data[channel].status |= STATUS_RAMP_UP;
          v_dac[channel] += user_data[channel].ramp_step;
        }
        else {
          // Ramping complete - DAC voltage to set value
          user_data[channel].status &= ~STATUS_RAMP_UP;
          user_data[channel].status &= ~STATUS_NEW_VALUE;
          //             v_dac[channel] = user_data[channel].v_set;
          v_dac[channel] += user_data[channel].v_set - user_data[channel].v_measured;
        }
        load_dac = 1;
      }

      // Ramp Down Voltage
      //       else if (user_data[channel].v_set < v_dac[channel]) {
      else if (user_data[channel].v_set < user_data[channel].v_measured) {

        // Check if a step decrease will cause an overshoot
        //          if ((v_dac[channel] - user_data[channel].ramp_step) > user_data[channel].v_set) {
        if ((user_data[channel].v_measured - user_data[channel].ramp_step) > user_data[channel].v_set) {
          // Decrease DAC voltage by one step
          user_data[channel].status |= STATUS_RAMP_DOWN;
          v_dac[channel] -= user_data[channel].ramp_step;
        }
        else {
          // Ramping complete - DAC voltage to set value
          user_data[channel].status &= ~STATUS_RAMP_DOWN;
          user_data[channel].status &= ~STATUS_NEW_VALUE;
          //             v_dac[channel] = user_data[channel].v_set;
          v_dac[channel] -= user_data[channel].v_measured - user_data[channel].v_set;
        }
        load_dac = 1;
      }

      else {
        // Voltage was set to current value... Clear flags
        user_data[channel].status &= ~STATUS_NEW_VALUE;
        user_data[channel].status &= ~STATUS_RAMP_UP;
        user_data[channel].status &= ~STATUS_RAMP_DOWN;
        v_dac[channel] = user_data[channel].v_set;
        load_dac = 1;
      }


      // Write to DAC... Only if value changed
      if (load_dac == 1) {

        // Check for ramping overshoot condition (due to low-end offset of HV amp)
        if(v_dac[channel] < 0) {
          user_data[channel].status &= ~STATUS_RAMP_DOWN;
          user_data[channel].status &= ~STATUS_NEW_VALUE;
          v_dac[channel] = 0;
        }
        // Convert set voltage to serial data
        value = (unsigned int) (v_dac[channel] * 4096 / (2*VREF * HV254_GAIN));
        if (value > 0xfff) {
          // Max DAC setting has been reached... stop ramping, raise Vlimit flag
          user_data[channel].status &= ~STATUS_RAMP_UP;
          user_data[channel].status &= ~STATUS_NEW_VALUE;
          user_data[channel].status |= STATUS_VLIMIT;
          value = 0xfff;
        }
        else {
          user_data[channel].status &= ~STATUS_VLIMIT;
        }

        DAC_SYNCn = 0;
        AD5383_Write(AD5383_DATA, channel, value);
        DAC_SYNCn = 1;
        t_ramp[channel] = time();
      }
    }
  }
}

/*------------------------------------------------------------------*/
void regulate_voltage(unsigned char channel)
{
  float LSB;
  unsigned int value;
  unsigned char load_dac = 0;

  if ((user_data[channel].control & CONTROL_ENABLED) &&
      (user_data[channel].control & CONTROL_REGULATED) &&
      !(user_data[channel].status & STATUS_NEW_VALUE)) {

    // Define equivalent voltage of LSB
    LSB = (2*VREF * HV254_GAIN) / 4096;

    // Check for undervoltage condition
    if (user_data[channel].v_measured < (user_data[channel].v_set - LSB/2)) {

      v_dac[channel] += LSB;
      load_dac = 1;
    }

    // Check for overvoltage condition
    else if (user_data[channel].v_measured > (user_data[channel].v_set + LSB/2)) {

      v_dac[channel] -= LSB;
      load_dac = 1;
    }

    // Write to DAC... Only if value changed
    if (load_dac == 1) {
      // Convert set voltage to serial data
      if(v_dac[channel] < 0) {
        v_dac[channel] = 0;
      }
      value = (unsigned int) (v_dac[channel] * 4096 / (2*VREF * HV254_GAIN));
      if (value > 0xfff) {
        user_data[channel].status |= STATUS_VLIMIT;
        value = 0xfff;
      }
      else {
        user_data[channel].status &= ~STATUS_VLIMIT;
      }

      DAC_SYNCn = 0;
      AD5383_Write(AD5383_DATA, channel, value);
      DAC_SYNCn = 1;
    }
  }
}

/*------------------------------------------------------------------*/
unsigned int read_current(unsigned char channel)
{
  unsigned int datatosend;
  unsigned int datareceived;
  unsigned int ichannel;
  float current;

  watchdog_refresh(0);

  ichannel = channel;

  // Check which AD7490 device the channel corresponds to
  if(channel < 16) {

    // Write CR and select channel for conversion
    IADC_CS0n = 0;
    delay_us(SPI_DELAY);
    datatosend = 0x833;      // Set CR... Write, normal mode, REFin (0-2.5V), straight binary coding
    datatosend |= (channel << 6);  // Set channel for next conversion
    datatosend = (datatosend << 4);  // Shift 12-bit CR data for 16-bit transfer
    SPI16_Write(datatosend, 16);
    IADC_CS0n = 1;
    delay_us(SPI_DELAY);

    // Read conversion data
    IADC_CS0n = 0;
    delay_us(SPI_DELAY);
    datatosend = 0;   // Do not write CR
    datareceived = SPI16_Write(datatosend, 16);
    IADC_CS0n = 1;
    delay_us(SPI_DELAY);

  }
  else {

    channel -= 16;

    // Write CR and select channel for conversion
    IADC_CS1n = 0;
    delay_us(SPI_DELAY);
    datatosend = 0x833;      // Set CR... Write, normal mode, REFin (0-2.5V), straight binary coding
    datatosend |= (channel << 6);  // Set channel for next conversion
    datatosend = (datatosend << 4);  // Shift 12-bit CR data for 16-bit transfer
    SPI16_Write(datatosend, 16);
    IADC_CS1n = 1;
    delay_us(SPI_DELAY);

    // Read conversion data
    IADC_CS1n = 0;
    delay_us(SPI_DELAY);
    datatosend = 0;   // Do not write CR
    datareceived = SPI16_Write(datatosend, 16);
    IADC_CS1n = 1;
    delay_us(SPI_DELAY);

  }

  datareceived &= 0xFFF;  // Remove ADC Channel Identifier

  current = (float)  datareceived / 4096.0 * VREF; // conversion
  current = (current * user_data[ichannel].i_gain) + user_data[ichannel].i_offset;  // correction

  user_data[ichannel].i_measured = current;

  return datareceived;
}

/*------------------------------------------------------------------*/
unsigned int read_voltage(unsigned char channel)
{
  unsigned int datatosend;
  unsigned int datareceived;
  unsigned int ichannel;
  float voltage;

  watchdog_refresh(0);

  ichannel = channel;

   // Check which AD7490 device the channel corresponds to
  if(channel < 16) {

    // Write CR and select channel for conversion
    VADC_CS0n = 0;
    delay_us(SPI_DELAY);
    datatosend = 0x831;      // Set CR... Write, normal mode, 2xREFin (0-5V), straight binary coding
    datatosend |= (channel << 6);  // Set channel for next conversion
    datatosend = (datatosend << 4);  // Shift 12-bit CR data for 16-bit transfer
    SPI16_Write(datatosend, 16);
    VADC_CS0n = 1;
    delay_us(SPI_DELAY);

    // Read conversion data
    VADC_CS0n = 0;
    delay_us(SPI_DELAY);
    datatosend = 0;   // Do not write CR
    datareceived = SPI16_Write(datatosend, 16);
    VADC_CS0n = 1;
    delay_us(SPI_DELAY);

  }
  else {

    channel -= 16;

    // Write CR and select channel for conversion
    VADC_CS1n = 0;
    delay_us(SPI_DELAY);
    datatosend = 0x831;      // Set CR... Write, normal mode, 2xREFin (0-5V), straight binary coding
    datatosend |= (channel << 6);  // Set channel for next conversion
    datatosend = (datatosend << 4);  // Shift 12-bit CR data for 16-bit transfer
    SPI16_Write(datatosend, 16);
    VADC_CS1n = 1;
    delay_us(SPI_DELAY);

    // Read conversion data
    VADC_CS1n = 0;
    delay_us(SPI_DELAY);
    datatosend = 0;   // Do not write CR
    datareceived = SPI16_Write(datatosend, 16);
    VADC_CS1n = 1;
    delay_us(SPI_DELAY);

  }

  datareceived &= 0xFFF;  // Remove ADC Channel Identifier

  voltage = (float)  datareceived / 4096.0 * 2 * VREF; // conversion
  voltage = (voltage * user_data[ichannel].v_gain) + user_data[ichannel].v_offset;  // correction

  user_data[ichannel].v_measured = voltage;

  return datareceived;
}

/*------------------------------------------------------------------*/
void channel_enable(unsigned char channel)
{

  SFRPAGE = CONFIG_PAGE;

  user_data[channel].control |= CONTROL_ENABLED;

  // Enable appropriate channel
  if(channel < 8) {
    P4 &= ~ (0x1 << channel);
  }
  else if(channel < 16) {
    channel -= 8;
    P5 &= ~ (0x1 << channel);
  }
  else if(channel < 24) {
    channel -= 16;
    P6 &= ~ (0x1 << channel);
  }
  else if(channel < 32) {
    channel -= 24;
    P7 &= ~ (0x1 << channel);
  }

}

/*------------------------------------------------------------------*/
void channel_disable(unsigned char channel)
{

  SFRPAGE = CONFIG_PAGE;

  v_dac[channel] = 0;

  DAC_SYNCn = 0;
  AD5383_Write(AD5383_DATA, channel, 0);
  DAC_SYNCn = 1;

  user_data[channel].control &= ~CONTROL_ENABLED;

  // Disable appropriate channel
  if(channel < 8) {
    P4 |= 0x1 << channel;
  }
  else if(channel < 16) {
    channel -= 8;
    P5 |= 0x1 << channel;
  }
  else if(channel < 24) {
    channel -= 16;
    P6 |= 0x1 << channel;
  }
  else if(channel < 32) {
    channel -= 24;
    P7 |= 0x1 << channel;
  }

}

/*------------------------------------------------------------------*/
void check_current(unsigned char channel)
{
  if (user_data[channel].i_measured < user_data[channel].i_limit) {

    user_data[channel].status &= ~STATUS_ILIMIT;
    t_trip[channel] = time();    // reset trip time
  }
  else if (user_data[channel].trip_delay < 0xFFFF) {

    // Current is over limit and trip is enabled...
    user_data[channel].status |= STATUS_ILIMIT;

    // check if trip time has expired (10ms resolution)
    if (time() >= (t_trip[channel] + (user_data[channel].trip_delay / 10))) {
      channel_disable(channel);
    }
  }
}

/*---- User loop function ------------------------------------------*/
void user_loop(void)
{
  unsigned char channel;
  float hv_supply, temperature;

  temperature = read_temperature();
  hv_supply = read_hvsupply();

  if(hv_supply > HV_WARNING_THRESHOLD)
  led_red = 1;
  else
  led_red = 0;

  /* loop over all HV channels */
  for (channel=0; channel<MSCB_NUM_SUB_NODES; channel++) {

    read_voltage(channel);
    ramp_voltage(channel);
    regulate_voltage(channel);
    read_current(channel);
    check_current(channel);
    watchdog_refresh(0);
  }

  led_blink(0, 1, 50);

}

