/********************************************************************\

Name:     cPet 
Author:   PA Amaudruz
Date:     
$Id$

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cpet.h" 

extern xdata SYS_INFO sys_info;

char    code  MSCB_node_name[] = "cPet410";
char    xdata svn_rev_code[] = "$Rev$";
xdata         MSCB_USER_DATA user_data;

MSCB_INFO_VAR code vars[] = {
  4, UNIT_BYTE,     0, 0, 0,           "SerialN"  ,   &user_data.error,       // 0
  2, UNIT_BYTE,     0, 0, 0,           "Error"  ,   &user_data.error,         // 1
  1, UNIT_BYTE,     0, 0, 0,           "Control",   &user_data.control,       // 2
  1, UNIT_BYTE,     0, 0, 0,           "Status" ,   &user_data.status,        // 3

  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "HVPVSet1"          ,   &user_data.vset[0],     // 4
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "HVPVSet2"          ,   &user_data.vset[1],     // 5 

  4, UNIT_AMPERE,   PRFX_NANO, 0, MSCBF_FLOAT, "HVPImon1"  ,   &user_data.uimon[0],    // 6 
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "HVPVmon1"          ,   &user_data.uimon[1],    // 7
  4, UNIT_AMPERE,   PRFX_NANO, 0, MSCBF_FLOAT, "HVPImon2"  ,   &user_data.uimon[2],    // 8 
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "HVPVmon2"          ,   &user_data.uimon[3],    // 9 

  4, UNIT_AMPERE,   PRFX_NANO, 0, MSCBF_FLOAT, "Imon"  ,   &user_data.imon,            // 10 
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "cpuAdc"        ,   &user_data.cpuadc,          // 11 
  
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "TuC",     &user_data.ucTemp,      // 12
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "Tint",    &user_data.intTemp,     // 13
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "Tbrd",    &user_data.brdTemp,     // 14
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Vbrd",    &user_data.brdVcc,      // 15

  2, UNIT_BYTE,     0, 0, 0,           "CPUTrig" ,   &user_data.cputrig,  // 16

  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC0"   ,   &user_data.adc[0],      // 17
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC1"   ,   &user_data.adc[1],      // 18
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC2"   ,   &user_data.adc[2],      // 19
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "ADC3"   ,   &user_data.adc[3],      // 20

  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "TUI0"     , &user_data.tui[0],      // 21
  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "TUI1"     , &user_data.tui[1],      // 22
  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "TUIINT"   , &user_data.tui[2],      // 23 
  2, UNIT_BYTE,     0, 0, MSCBF_HIDDEN,           "TUIVCC"   , &user_data.tui[3],      // 24
  0
};

code MSCB_INFO_VAR *variables = vars;

/********************************************************************\

Application specific init and inout/output routines

\********************************************************************/

// Change register
volatile unsigned char bdata bChange = 0;
sbit fCpuTrig      = bChange ^ 0;

// Control register
unsigned char bdata rCTL = 0;
sbit cSw1     = rCTL ^ 0;
sbit cSw2     = rCTL ^ 1;
sbit cBtrig   = rCTL ^ 2;
sbit Cspare2  = rCTL ^ 3;  // 0x08
sbit Cspare3  = rCTL ^ 4;  // 0x10
sbit Cspare4  = rCTL ^ 5;
sbit Cspare5  = rCTL ^ 6;
sbit Cspare6  = rCTL ^ 7;

// Status Register
unsigned char bdata rSTAT = 0;
sbit sSw1     = rSTAT ^ 0;
sbit sSw2     = rSTAT ^ 1;
sbit sBtrig   = rSTAT ^ 2;
sbit sspare3  = rSTAT ^ 3;
sbit sAlert   = rSTAT ^ 4;
sbit sspare5  = rSTAT ^ 5;
sbit sSpare6  = rSTAT ^ 6;
sbit sSpare7  = rSTAT ^ 7;

// ERR Error Register
//The low and high bytes are switched in the bdata section of the memory
//This is the reason that the sbit declarations do not appear to match
//the documentation but they actually do.
unsigned int bdata rERR =0;
sbit euCtemp  = rERR ^ 8;  //0x1  
sbit ebrdTemp = rERR ^ 9;  //0x2
sbit eAdc     = rERR ^ 10; //0x4
sbit eBehlke  = rERR ^ 11; //0x8
sbit eSwitch  = rERR ^ 12; //0x10 
sbit espare5  = rERR ^ 13; //0x20 
sbit espare6  = rERR ^ 14; //0x40 
sbit espare7  = rERR ^ 15; //0x80 

sbit espare8  = rERR ^ 0;  //0x100
sbit espare9  = rERR ^ 1;  //0x200
sbit espare10 = rERR ^ 2;  //0x400
sbit espare11 = rERR ^ 3;  //0x800

sbit espare12 = rERR ^ 4;  //0x1000
sbit espare13 = rERR ^ 5;  //0x2000
sbit espare14 = rERR ^ 6;  //0x4000
sbit espare15 = rERR ^ 7;  //0x8000

// DAC update register
unsigned char bdata rDAC = 0xFF;
sbit DAC0     = rDAC ^ 0;
sbit DAC1     = rDAC ^ 1;

// Hardware bits
// P0 .7:trigger  .6:cnv,    .5:Rx,   .4:Tx,    .3:cex0,   .2:sysclk,  .1: SCK, .0 SDA
sbit bCPUTrigger   = P0 ^ 7;
sbit bCpuCvnStr    = P0 ^ 6;
sbit bCpuCex0      = P0 ^ 3;
sbit bCpuSysClk    = P0 ^ 2;

// P1 .7:led0    .6:led1,    .5:led2, .4:ledc0, .3:ledc1,  .2:DACld,   .1:CpuADC, .0:sw1, 
sbit bLEDStat0     = P1 ^ 7;
sbit bLEDStat1     = P1 ^ 6;
sbit bLEDStat2     = P1 ^ 5;
sbit bDacLDn       = P1 ^ 2;
sbit bCpuAdc       = P1 ^ 1;
sbit bSw1          = P1 ^ 0;

// P2 .7:C2D     .6:swFault, .5:sw2,  .4:485en  .3:behlkF, .2:spiclk, .1:spidout, .0 spidin
sbit bSwFaultn     = P2 ^ 6;
sbit bSw2          = P2 ^ 5;
sbit bBehlkeFaultn = P2 ^ 3;
sbit bCpuSPIClk    = P2 ^ 2;
sbit bCpuSPIDout   = P2 ^ 1;
sbit bCpuSPIDin    = P2 ^ 0;


/*---- User init function ------------------------------------------*/

void user_init(unsigned char init)
{
  char i;
  unsigned char temp;

  /* Format the SVN and store this code SVN revision into the system */
  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  sys_info.svn_revision = (svn_rev_code[6]-'0')*1000+
    (svn_rev_code[7]-'0')*100+
    (svn_rev_code[8]-'0')*10+
    (svn_rev_code[9]-'0');

  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  if (init) {
    // Initialize variables
    for (i=0;i<N_ADC;i++) user_data.uimon[i] = 0;
    for (i=0;i<N_DAC;i++) user_data.vset[i] = 0;
    for (i=0;i<N_TUI;i++) user_data.tui[i] = 0;

    // Temperature initialization
    user_data.ucTemp = 0;
    user_data.brdTemp = 0;
  }

  // Reference Voltage on P0.0 Enable
//  REF0CN = 0x00;  // Int Bias off, Int Temp off, VREF input on

  // 0:analog 1:digital
  P0MDIN = 0xFF; // P0 all digital pins
  P1MDIN = 0xFD; // P1 all digital pins except .1 (ADC)
  P2MDIN = 0xFF; // P2 all digital pins
 
  // 0: open-drain, 1:push-pull
  // P0 .7:trigger  .6:cnv,    .5:Rx,   .4:Tx,    .3:cex0,   .2:sysclk,  .1: SCK, .0 SDA
  P0MDOUT = 0x17;  //  trig, cnv, Rx:OD, SDA

// P1 .7:led0    .6:led1,    .5:led2, .4:ledc0, .3:ledc1,  .2:DACld,  .1:CpuADC, .0:sw1 ,
  P1MDOUT = 0xFD;  // adc:OD

// P2 .7:C2D     .6:swFault, .5:sw2,  .4:485en  .3:behlkF, .2:spiclk, .1:spidout, .0 spidin
  P2MDOUT = 0x36;  // spidout, spiclk, 485, sw2:PP

  // Setting the cross bar
  P0SKIP = 0x0; 
  P1SKIP = 0x0; 
  P2SKIP = 0x0; 

  XBR0 |=0x04;	 // Enable SMBus pins P0 .0/SDA .1/SCL
  // CPLD clock input 25MHz
  XBR0 |=0x08;	 // Enable SYSCLK to Port 1.2

  rCTL  = 0;
  rSTAT = 0;
  rERR  = 0;
   
  // Shutdown switches
  bSw1 = 0; bSw2 = 0;

  UpdateIO();

//  pca_Init();
  adc_internal_init();
  LTC2493_Init();
  LTC2607_Init();
//  LTC2990TUI_Init();
}

/*---- User write function -----------------------------------------*/
void user_write(unsigned char index) reentrant{

//  led_blink(LED_RED, 1, 150);

  // Set the appropriate DAC update register bit
  // Done this way so we if we can take being interrupted, and avoid a
  // read-write-modify setup. (if done using a byte)
  if (index == CTL_IDX){
    if (user_data.control & 0x01) {
     cSw1 = 1;
    } else if (user_data.control & 0x02) {
     cSw2 = 1;
    } else if (user_data.control & 0x04) {
     cBtrig = 1;
    } else if (user_data.control & 0x08) {
     Cspare2 = 1;
    } else if (user_data.control & 0x10) {
     Cspare3 = 1;
    }
  } else if ((index >= FIRST_DAC) && (index < (FIRST_DAC + (N_DAC)))) {
    switch(index - FIRST_DAC) {
      case 0:
        DAC0 = 1;
        break;

      case 1:
        DAC1 = 1;
        break;

    }
  } else if (index == CPUTRIG_IDX) {
    fCpuTrig = 1;
  }

  return;
}

/*---- User read function ------------------------------------------*/

unsigned char user_read(unsigned char index)
{
  if(index);
  return 0;
}

/*---- User function called vid CMD_USER command -------------------*/

unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
  // echo input data
  data_out[0] = data_in[0];
  data_out[1] = data_in[1];
  return 2;
}

//
//----------------------------------------------------------------------------------
//Main user loop
//Read 6 ADT7486A SST diode Temperature

/*---- User loop function ------------------------------------------*/
void user_loop(void)
{
  UpdateDAC();
  UpdateIO();
  UpdateADC();
//  UpdateTUI();
  UpdateiADC();
  delay_ms(100); // ADC takes 149.9 ms to convert maximum.

  bLEDStat0 = 0;
  delay_ms(100); // ADC takes 149.9 ms to convert maximum.
  bLEDStat0 = 1;
}

//
//-----------------------------------------------------------------------------
void publishCtlCsr(void) {
    DISABLE_INTERRUPTS;
    user_data.control = rCTL;
    user_data.status  = rSTAT;
    user_data.error   = rERR;
    ENABLE_INTERRUPTS;
}

//
//-------------------------------------------------------------
void UpdateIO(void) {
  static char changed=0;
  if (cSw1) {
    if (sSw1) {
     cSw1 = 0; sSw1= 0; bSw1 = 0; 
    } else {
     cSw1 = 0; sSw1 = 1; bSw1 = 1;
    }
  }
  if (cSw2) {
    if (sSw2) {
     cSw2 = 0; sSw2= 0; bSw2 = 0;
    } else {
     cSw2 = 0; sSw2 = 1; bSw2 = 1;
    }
  }
  if (fCpuTrig) {
	  fCpuTrig = 0;
  }

// Check fault bits
//  eBehlke = ~bBehlkeFaultn;
//  eSwitch = ~bSwFaultn;
 
  publishCtlCsr();
}

//
//-------------------------------------------------------------
void UpdateiADC(void) {
  unsigned int adc_raw;

  adc_raw = adc_read(CPU_ADC_CH, 0);

  DISABLE_INTERRUPTS;
  user_data.cpuadc = (((float)(adc_raw)) * IADC2VOLT);
  ENABLE_INTERRUPTS;

}

//
//-------------------------------------------------------------
/*
char chmap[4] = {0, 1, 2, 3};
void UpdateADC(void) {
#ifdef MSCB_DEVICE_ADC_LTC2493_DISABLE
#else
  static char adcChannel = N_ADC + 1;
  char prevChannel;
  unsigned char range;
  static xdata signed long adc_raw;

  if(adcChannel == (N_ADC + 1)) {
    adcChannel = CLEAR;
      LTC2493_StartConversion(LTC2493_ADDR, LTC2493_CMD_SGL, 0, adcChannel);
  } else {
    prevChannel = adcChannel;
    adcChannel++;
    adcChannel %= N_ADC;

    range = LTC2493_ReadConversion(LTC2493_ADDR, LTC2493_CMD_SGL, 0 , adcChannel, &adc_raw);
      
    switch(range) {
      case LTC2493_VALIDRANGE:
        eAdc = 0;
        break;

      case LTC2493_OVERRANGE:
        eAdc = 1;
        adc_raw = 0xFFFFFFFF;
        break;

      case LTC2493_UNDERRANGE:
        eAdc = 1;
        adc_raw = 0xFFFFFFFF;
        break;
    }
    
    DISABLE_INTERRUPTS;
    // Ch0:Imon1, Ch1:Imon2, Ch2: Vmon1, Ch3:Vmon2
    if (prevChannel < N_ADC) { // within channel range
  		if (prevChannel > 1) { // Vmon
            user_data.uimon[chmap[prevChannel]] = (((float)(adc_raw)) * ADC2VOLT);
  		} else { // Imon
            user_data.uimon[chmap[prevChannel]] = (((float)(adc_raw)) * ADC2CURRENT);
            user_data.uimon[chmap[prevChannel]] *= 1.E6;   // U to I conversion (1KOhm) and in nA
  		}
    }

  user_data.adc[chmap[prevChannel]]  = adc_raw;  // +/- Range 
  ENABLE_INTERRUPTS;
  }
#endif // MSCB_DEVICE_ADC_LTC2489_DISABLE
}
*/
//
//-------------------------------------------------------------
void UpdateADC(void) {
#ifdef MSCB_DEVICE_ADC_LTC2493_DISABLE
#else
  static char adcChannel = 0;
  char prevChannel;
  unsigned char range;
  static xdata signed long adc_raw;
  
  adcChannel++; // CLEAR;
  if(adcChannel == N_ADC) adcChannel = CLEAR;

  LTC2493_StartConversion(LTC2493_ADDR, LTC2493_CMD_SGL, 0, adcChannel);
  delay_ms(300);
  range = LTC2493_ReadConversion(LTC2493_ADDR, LTC2493_CMD_SGL, 0 , adcChannel, &adc_raw);
    switch(range) {
      case LTC2493_VALIDRANGE:
        eAdc = 0;
        break;

      case LTC2493_OVERRANGE:
        eAdc = 1;
        adc_raw = 0xFFFFFFFF;
        break;

      case LTC2493_UNDERRANGE:
        eAdc = 1;
        adc_raw = 0xFFFFFFFF;
        break;
    }
    
    DISABLE_INTERRUPTS;
		switch (adcChannel) {
      case 0:
      case 2:
      user_data.uimon[adcChannel] = (((float)(adc_raw)) * ADC2CURRENT);
      user_data.uimon[adcChannel] *= 1.E6;   // U to I conversion (1KOhm) and in nA
      user_data.adc[adcChannel]  = adc_raw;  // +/- Range 
      break;
      case 1:   
      case 3:
		  user_data.uimon[adcChannel] = (((float)(adc_raw)) * ADC2VOLT);
      user_data.adc[adcChannel]  = adc_raw;  // +/- Range 
      break;
    }
    ENABLE_INTERRUPTS;

#endif // MSCB_DEVICE_ADC_LTC2489_DISABLE
}

//
//-------------------------------------------------------------
void UpdateDAC(void) {
#ifdef MSCB_DEVICE_DAC_LTC2607_DISABLE
#else
  unsigned int ldac;

  if (DAC0 || DAC1 ) {
    led_blink(LED_GREEN, 1, 150);
  }
  // Set all necessary DAC values, if an interrupt occurs while this is going on
  // everything should be OK, at worst we may update a newer value.
  if(DAC0) {
  	ldac = (unsigned int) user_data.vset[0]; // * HV2VOLT * VOLT2DAC; 
  	LTC2607_Write(LTC2607_ADDR, LTC2607_WREG_n_UPDATE_n | 0x0, ldac);
    DAC0 = 0;
  }

  if(DAC1) {
  	ldac = (unsigned int) user_data.vset[1]; //  * HV2VOLT * VOLT2DAC; 
  	LTC2607_Write(LTC2607_ADDR, LTC2607_WREG_n_UPDATE_n | 0x1, ldac);
    DAC1 = 0;
  }

#endif // MSCB_DEVICE_DAC_LTC2607_DISABLE
}

//
//-------------------------------------------------------------
void UpdateTUI(void) {
#ifdef MCSB_DEVICE_TUI_LTC2990_DISABLE
#else
  char i, stat[N_TUI];
  unsigned int xdata raw[N_TUI];

  LTC2990TUI_ConfigStart(LTC2990_ADDR, LTC2990TUI_CTL_DIFF12T2 | LTC2990TUI_SGL_ACQ);

  stat[0] = LTC2990TUI_ReadConversion(LTC2990_ADDR, LTC2990TUI_VOLT1, &raw[0]);
  
  stat[1] = LTC2990TUI_ReadConversion(LTC2990_ADDR, LTC2990TUI_VOLT3, &raw[1]);
 
  stat[2] = LTC2990TUI_ReadConversion(LTC2990_ADDR, LTC2990TUI_INTTEMP, &raw[2]);
 
  stat[3] = LTC2990TUI_ReadConversion(LTC2990_ADDR, LTC2990TUI_VCC, &raw[3]);
  
  DISABLE_INTERRUPTS;
  for (i=0;i<N_TUI;i++)  user_data.tui[i] = raw[i];  
  ENABLE_INTERRUPTS;

  DISABLE_INTERRUPTS;
//  user_data.ucTemp = 
  user_data.imon    = raw[0] * 1.;  
  user_data.brdTemp = raw[1] * 1.;  
  user_data.intTemp = raw[2] * 1.;  
  user_data.brdVcc  = raw[3] * 1.;  
  ENABLE_INTERRUPTS;
#endif // MCSB_DEVICE_TUI_LTC2990_DISABLE
}