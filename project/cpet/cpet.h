/********************************************************************\

  Name:     cpet.h
  $Id$

\********************************************************************/

#ifndef _GENERIC_IO_H
#define _GENERIC_IO_H

#include "../../common/mscbemb.h"

//
#define CTL_IDX       2
#define FIRST_DAC     4
#define CPUTRIG_IDX  16

#define LTC2493_ADDR  0x14   // 24-bit 2-4  channel ADC
#define N_ADC        4
#define ADCVREF      5.000f/2.f
#define ADC2VOLT     (ADCVREF / 16777216.0f)
#define ADC2CURRENT  (ADC2VOLT * 0.001)

#define LTC2607_ADDR  0x10   // 16-bit Dual channel DAC
#define N_DAC         2
#define DACVREF       5.0f
#define HVMAX         3000.f
#define VOLT2DAC      (65536.0f / DACVREF)
#define HV2VOLT       (DACVREF / HVMAX)

#define CPU_ADC_CH    9
#define IADC2VOLT     (2.5f / 4096.0f)

#define N_TUI         4
#define LTC2990_ADDR  0x4C   // Temp, Voltage, Current monitoring

// Global definition
// Global ON / OFF definition
#define ON     1
#define DONE   1
#define SET    1
#define OFF    0
#define CLEAR  0

#define LED_GREEN 1
#define LED_RED   0


//
//--- MSCB structure
typedef struct {
  // System
  unsigned long SerialN;
  unsigned int  error;
  unsigned char control;
  unsigned char status;
  
  // Functions
  float         vset[N_DAC];
  float         uimon[N_ADC];
  float         imon;
  float         cpuadc;
  float         ucTemp;    
  float         brdTemp; 
  float         intTemp;  
  float         brdVcc;   
  unsigned int  cputrig;

  // Raw data
  unsigned long adc[N_ADC];
  unsigned int  tui[N_TUI];

} MSCB_USER_DATA;

void UpdateADC(void);
void UpdateiADC(void);
void UpdateDAC(void);
void UpdateIO(void);
void UpdateTUI(void);

#endif // _GENERIC_IO.