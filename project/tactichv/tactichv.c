/********************************************************************\

Name:     tactichv 
Author:   Pierre-Andr�
Date:     
$Id$


\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tactichv.h" 

extern xdata SYS_INFO sys_info;

char    code  MSCB_node_name[] = "TACTICHV410";
char    xdata svn_rev_code[] = "$Rev$";
xdata         MSCB_USER_DATA user_data;

MSCB_INFO_VAR code vars[] = {
  4, UNIT_BYTE,     0, 0, 0,            "SerialN", &user_data.SerialN,     // 0
  2, UNIT_BYTE,     0, 0, 0,            "Error"  , &user_data.error,       // 1
  1, UNIT_BYTE,     0, 0, 0,            "Control", &user_data.control,     // 2
  1, UNIT_BYTE,     0, 0, 0,            "Status" , &user_data.status,      // 3
 	4, UNIT_CELSIUS, 	0, 0, MSCBF_FLOAT,  "Temp",		 &user_data.temp,		     // 4
	
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,  "Uinput",	 &user_data.volt[0],	   // 5
	4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,  "UoutRing",&user_data.volt[1],	   // 6
	4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,  "UinRing", &user_data.volt[2],	   // 7
 	4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,  "Uback",	 &user_data.volt[3],	   // 8
	4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,  "UdvGem",	 &user_data.volt[4],     // 9
 	4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,  "Ugem",    &user_data.volt[5],	   // 10
 	4, UNIT_VOLT,     0, 0, MSCBF_FLOAT,  "Umeas",   &user_data.volt[6],	   // 11

 	4, UNIT_AMPERE,     PRFX_MICRO, 0, MSCBF_FLOAT,  "Ibranch",  &user_data.curBranch,// 12
	4, UNIT_AMPERE,     PRFX_MICRO, 0, MSCBF_FLOAT,  "Ioverall", &user_data.curOver,	 // 13

	4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Imeas0",  &user_data.cur[0],	     // 14
	4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Imeas1",  &user_data.cur[1],	     // 15
	4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Imeas2",  &user_data.cur[2],	     // 16
	4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Imeas3",  &user_data.cur[3],	     // 17
	4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Imeas4",  &user_data.cur[4],	     // 18
	4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Imeas5",  &user_data.cur[5],	     // 19
	4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Imeas6",  &user_data.cur[6], 	   // 20
	4, UNIT_AMPERE,     PRFX_NANO, 0, MSCBF_FLOAT, "Imeas7",  &user_data.cur[7], 	   // 21

	4, UNIT_OHM,     0, 0, MSCBF_FLOAT,  "R1",      &user_data.res1,	       // 22
	4, UNIT_OHM,     0, 0, MSCBF_FLOAT,  "R2",      &user_data.res2,	       // 23
	4, UNIT_OHM,     0, 0, MSCBF_FLOAT,  "R3",      &user_data.res3,	       // 24
	4, UNIT_OHM,     0, 0, MSCBF_FLOAT,  "R4",      &user_data.res4,	       // 25
	4, UNIT_OHM,     0, 0, MSCBF_FLOAT,  "Rendcap", &user_data.resTendc,     // 26
	4, UNIT_OHM,     0, 0, MSCBF_FLOAT,  "R5",      &user_data.res5,	       // 27
	4, UNIT_OHM,     0, 0, MSCBF_FLOAT,  "R6",      &user_data.res6,	       // 28
	4, UNIT_OHM,     0, 0, MSCBF_FLOAT,  "R7",      &user_data.res7, 	       // 29
 	4, UNIT_OHM,     0, 0, MSCBF_FLOAT,  "R10k",    &user_data.res10k, 	     // 30
 	4, UNIT_OHM,     0, 0, MSCBF_FLOAT,  "Rmeas",   &user_data.resMeas, 	   // 31

  1, UNIT_BYTE,     0, 0, MSCBF_HIDDEN | MSCBF_SIGNED, "oDAC",    &user_data.oDAC, 	     // 32
  1, 0,     0, 0, MSCBF_HIDDEN, "Gain",    &user_data.gain,        // 33

  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN | MSCBF_SIGNED, "ADC0",    &user_data.adc[0],      // 34
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN | MSCBF_SIGNED, "ADC1",    &user_data.adc[1],      // 35
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN | MSCBF_SIGNED, "ADC2",    &user_data.adc[2],      // 36
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN | MSCBF_SIGNED, "ADC3",    &user_data.adc[3],      // 37
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN | MSCBF_SIGNED, "ADC4",    &user_data.adc[4],      // 38
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN | MSCBF_SIGNED, "ADC5",    &user_data.adc[5],      // 39
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN | MSCBF_SIGNED, "ADC6",    &user_data.adc[6],      // 40
  4, UNIT_BYTE,     0, 0, MSCBF_HIDDEN | MSCBF_SIGNED, "ADC7",    &user_data.adc[7],      // 41
  
  0
};

code MSCB_INFO_VAR *variables = vars;

/********************************************************************\
Application specific init and inout/output routines
\********************************************************************/

// Globals
float xdata fvalue;
float xdata current;

sbit ShutdownIso = ADS1240_SHUTDOWN_PIN;

// Control register
unsigned char bdata rCTL = 0;
sbit cQpup    = rCTL ^ 0;
sbit cVref    = rCTL ^ 1;
sbit cBias    = rCTL ^ 2;
sbit C3       = rCTL ^ 3;
sbit CeeS     = rCTL ^ 4;
sbit CeeR     = rCTL ^ 5;
sbit CeeClr   = rCTL ^ 6;
sbit CmSd     = rCTL ^ 7;

// Status Register
unsigned char bdata rSTAT = 0;
sbit sQpup    = rSTAT ^ 0;
sbit sVref    = rSTAT ^ 1;
sbit sBias    = rSTAT ^ 2;
sbit sAlert   = rSTAT ^ 3;
sbit SeeS     = rSTAT ^ 4;
sbit SeeR     = rSTAT ^ 5;
sbit SsS      = rSTAT ^ 6;
sbit eAdc     = rSTAT ^ 7;

// Flag Register
unsigned char bdata rFLAG = 0xFF;
sbit RES_FLAG   = rFLAG ^ 0;
sbit DAC_FLAG   = rFLAG ^ 1;
sbit GAIN_FLAG  = rFLAG ^ 2;

// ESR Error Register
//The low and high bytes are switched in the bdata section of the memory
unsigned int bdata rERR;
sbit Imon0     = rERR ^ 8;  //0x1
sbit Imon1     = rERR ^ 9;  //0x2
sbit Imon2     = rERR ^ 10; //0x4
sbit Imon3     = rERR ^ 11; //0x8

sbit Imon4     = rERR ^ 12; //0x10
sbit Imon5     = rERR ^ 13; //0x20
sbit Imon6     = rERR ^ 14; //0x40
sbit Imon7     = rERR ^ 15; //0x80

sbit eTemp     = rERR ^ 0;  //0x100
sbit eVgem     = rERR ^ 1;  //0x200
sbit spare1    = rERR ^ 2;  //0x400
sbit spare2    = rERR ^ 3;  //0x800

sbit spare3    = rERR ^ 4;  //0x1000
sbit spare4    = rERR ^ 5;  //0x2000
sbit spare5    = rERR ^ 6;  //0x4000
sbit spare6    = rERR ^ 7;  //0x8000

/*---- User init function ------------------------------------------*/
void user_init(unsigned char init)
{
  char i;

  /* Format the SVN and store this code SVN revision into the system */
  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  sys_info.svn_revision = (svn_rev_code[6]-'0')*1000+
    (svn_rev_code[7]-'0')*100+
    (svn_rev_code[8]-'0')*10+
    (svn_rev_code[9]-'0');

  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  if (init) {
    // Initialize variables
    for (i=0;i<8;i++) {
      user_data.cur[i] = 0;
      user_data.adc[i] = 0.0;
    }
    for (i=0;i<7;i++) {
      user_data.volt[i] = 0.0;
    }

    user_data.oDAC = 0x00;          // offset
    user_data.gain = 0x03;          // gain of 8

    user_data.res1 = 992000;        // Tactic HV Box
    user_data.res2 = 9670000;       // Tactic HV Box
    user_data.res3 = 1;             // Tactic HV Box 
    user_data.res4 = 2600000;       // Tactic HV Box
    user_data.resTendc = 1845000;   // Tactic HV Box
    user_data.res5 = 268200;        // Tactic HV Box
    user_data.res6 = 998300;        // TacticHV Board
    user_data.res7 = 2196100;       // TacticHV Board
    user_data.res10k = 4920;        // TacticHV Board

  }

  // Reference Voltage on P0.0 Enable
  REF0CN = 0x00;  // Int Bias off, Int Temp off, VREF input on

  // 0:analog 1:digital
  P0MDIN = 0xFF; // P0 all digital pins
  P1MDIN = 0xFF; // P1 all digital pins 
  P2MDIN = 0xFF; // P2 all digital pins
 

  // 0: open-drain, 1:push-pull
	P0MDOUT = 0x9F;              	// P0.4:TX, P0.2:RS485 enable Push/Pull
	P1MDOUT	= 0xFF;
	P2MDOUT = 0x66;

  // Setting the cross bar
  P0SKIP = 0x0; 
  P1SKIP = 0x0; 
  P2SKIP = 0x0; 

	XBR0 	= 0x01;                 // Enable RX/TX
	XBR1 	= 0x40;                 // Enable crossbar
	OSCICN 	= 0x87;             	// IOSCEN=1, SYSCLK/1=24.5 MHz

  /* set-up / initialize circuit components */
  // Force IO update
  UpdateIO();

  rCTL  = 0;
  rSTAT = 0;
  rERR  = 0;

// isoPowerShutdown(ON);

	ADS1225_Init();
	ADS1240_Init();
  init_ADS1240();

//  ADS1240_SelfCalibrate();       // could potentially uncalibrate it (no ground reference used)
//  ADS1240_ReadCont();  
}

//
//-----------------------------------------------------------------------------
void init_ADS1240(void) {
 	ADS1240_WriteRegister(ADS1240_REG_MUX, 0x80);   // Positive channels, AINCOM
	ADS1240_WriteRegister(ADS1240_REG_ACR, ADS1240_ACR_BUFEN);     // Enable Buffer, Bipolar
 	//ADS1240_WriteRegister(ADS1240_REG_ACR, 0x14);   // Buffer ON, Range= 1/2 scale
//  ADS1240_ACR_BUFEN | ADS1240_ACR_POL
  GAIN_FLAG = 1;
  Update_Gain();

  DAC_FLAG = 1;
  Update_oDAC();
}
//
//-----------------------------------------------------------------------------
void isoPowerShutdown(unsigned char onoff) {
  if (onoff) {
    ShutdownIso = 1;
  } else {
    ShutdownIso = 0;
  }
}

//
//-----------------------------------------------------------------------------
void publishCtlCsr(void) {
    DISABLE_INTERRUPTS;
    user_data.control = rCTL;
    user_data.status  = rSTAT;
    ENABLE_INTERRUPTS;
}

/*---- User write function -----------------------------------------*/
void user_write(unsigned char index) reentrant{

  led_blink(LED_RED, 1, 150);

  // Set the appropriate update register bit
  // Done this way so we if we can take being interrupted, and avoid a
  // read-write-modify setup. (if done using a byte)

  // Checks if the user has changed any of these values
  if (index == ODAC) {
    DAC_FLAG = 1;
  } else if ((index >= FIRST_RES) && (index <= (FIRST_RES+N_RES))) {
    RES_FLAG = 1;
  } else if (index == GAIN) {
    GAIN_FLAG = 1;
  }

  return;
}

/*---- User read function ------------------------------------------*/
unsigned char user_read(unsigned char index)
{
  if(index);
  return 0;
}

/*---- User function called vid CMD_USER command -------------------*/
unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
  // echo input data
  data_out[0] = data_in[0];
  data_out[1] = data_in[1];
  return 2;
}

//
/*---- User loop function ------------------------------------------*/
void user_loop(void)
{
  static unsigned long xdata itsTime=0;
  
/*  if ((uptime() - itsTime) > MAX_TIME) {
      isoPowerShutdown(ON);
      UpdateVolt();
      init_ADS1240();
      UpdateAllCurrent();
      UpdateTemp();
      itsTime = uptime();
    }
*/
  {
    UpdateIO();
    UpdateVolt();
    UpdateCurrent();
    UpdateTemp();
    Update_oDAC();
    Update_Gain();
  } 

  delay_ms(10); // ADC takes 149.9 ms to convert maximum.
}

//
//-------------------------------------------------------------
void UpdateIO(void) {

}

//
//-------------------------------------------------------------
void publishFloat(float *d, float s)
{
  /* copy float value to user_data without interrupt */
  DISABLE_INTERRUPTS;
  *d = s;
  ENABLE_INTERRUPTS;
}

//
//-------------------------------------------------------------
void Update_oDAC(void) {
#ifdef MSCB_DEVICE_ADC_LTC1240_DISABLE
#else
  if(DAC_FLAG) {  // updates the DAC offset if the user has changed this value
    ADS1240_WriteRegister(ADS1240_REG_ODAC, user_data.oDAC);
    DAC_FLAG = 0;
  }
#endif // MSCB_DEVICE_ADC_LTC1225_DISABLE
}

char code lookupTable2 [] = {1, 2, 4, 8, 16, 32, 64, 128};  // acceptable values for gain
//
//-------------------------------------------------------------
void Update_Gain(void) {
#ifdef MSCB_DEVICE_ADC_LTC1240_DISABLE
#else
  unsigned char gain = 0;
  unsigned char i;
  if(GAIN_FLAG) {   // updates the gain if the user has changed this value
    for (i = 0;i < 7;i++) {      // search lookup table for correct registry entry
      if (lookupTable2[i] == user_data.gain ) { 
         gain = i;
         break;
      } 
    } 
    ADS1240_WriteRegister(ADS1240_REG_SETUP, gain); //0=1, 1=2, 2=4, 3=8, 4=16, 5=32, 6= 64, 7=128 gain
    GAIN_FLAG = 0;
  }
#endif // MSCB_DEVICE_ADC_LTC1225_DISABLE
}

//
//-------------------------------------------------------------
void UpdateTemp(void) {
#ifdef MSCB_DEVICE_ADC_LTC1225_DISABLE
#else

  fvalue = (((((float)ADS1225_ReadTemp() * VOLTAGE_CONVERSION_RATIO) - 0.106)/0.00036) + 25.0);  // 106mV at 25C, 360uV/C
  publishFloat(&user_data.temp, fvalue);

#endif // MSCB_DEVICE_ADC_LTC1225_DISABLE
}

//
//-------------------------------------------------------------
void UpdateVolt(void) {
#ifdef MSCB_DEVICE_ADC_LTC1225_DISABLE
#else

  fvalue = ADS1225_SingleConversion() * VOLTAGE_CONVERSION_RATIO;
  fvalue -= 2.0195; // 1/2 due to V divider 
   
  publishFloat(&user_data.volt[6], fvalue);  //Voltage measured across 10kohm resistor

  current = fvalue/user_data.res10k; // 2 10K in parallel should be ~4.920Ohm
  publishFloat(&user_data.curBranch, current*1E6); // Current for branch (converted to microamps)

  fvalue = fvalue + (current*user_data.res7);
  publishFloat(&user_data.volt[5], fvalue);  //Voltage for Gem
  
  fvalue = fvalue + (current*user_data.res6) + 1.235;
  publishFloat(&user_data.volt[3], fvalue);  //Voltage for Back  

  fvalue = fvalue + (current*(user_data.res5 + user_data.resTendc + user_data.res4));
  publishFloat(&user_data.volt[1], fvalue);  //Voltage for Outer Ring

  fvalue = fvalue + (current*user_data.res3);
  publishFloat(&user_data.volt[0], fvalue);  //Voltage for Input

  current = current + fvalue/(user_data.res1 + user_data.res2);
  publishFloat(&user_data.curOver, current*1E6); //Overall current (converted to microamps)

  fvalue = fvalue*((user_data.res2)/(user_data.res1 + user_data.res2));
  publishFloat(&user_data.volt[2], fvalue);  //Voltage at Inner Ring

  fvalue = user_data.volt[3] - user_data.volt[5];
  publishFloat(&user_data.volt[4], fvalue);  //Voltage difference between Back and Gem


#endif // MSCB_DEVICE_ADC_LTC1225_DISABLE
}

//
//-------------------------------------------------------------
char code lookupTable [] = {7, 6, 2, 3, 5, 4, 0, 1};  //for correcting channel mapping
float code lookupTable3 [] = {1500, 5100, 19500, 56500, 0.0, 0.0, 0.0, 0.0};  //for correcting offsets at different gains

void UpdateCurrent(void) {
#ifdef MSCB_DEVICE_ADC_LTC1240_DISABLE
#else
  unsigned char Channel, RbkChannel, i;
  static unsigned char channel = 0;

  user_data.adc[channel] = ADS1240_ReadConversion();
  if (user_data.adc[channel] & 0x800000) {         //change data from 24 bit to 32 bit
     user_data.adc[channel] |= 0xFF000000;
  }


  for (i = 0;i < 7;i++) {      // search lookup table for user's gain
    if (lookupTable2[i] == user_data.gain ) { 
       //current = i;
       break;
    } 
  }
  current = ((float)user_data.adc[channel] - lookupTable3[i]);  // remove offset
  current = (current * VOLTAGE_CONVERSION_RATIO / (user_data.gain * user_data.resMeas * FULL)); // FULL=full range, HALF=half range
  //current = ((float)user_data.adc[channel] * NANOAMPS_PER_BIT_FULL / user_data.gain); // FULL=full range, HALF=half range
  

  current *= 1e9; // in nanoamp
  publishFloat(&user_data.cur[lookupTable[channel]], current);


  ++channel;

	if(channel == 8) channel = 0;
  Channel = (0x80 | channel);                // Signal negative, Common positive
  // Channel = (channel << 4) | 0x08);          // Signal positive, common negative
  ADS1240_SelectChannel(Channel);       
  delay_ms(1);    
  RbkChannel = ADS1240_ReadRegister(ADS1240_REG_MUX);
  DISABLE_INTERRUPTS;
  user_data.status =  Channel;
  user_data.control = RbkChannel;
  ENABLE_INTERRUPTS;
  delay_ms(50);    

  #endif // MSCB_DEVICE_ADC_LTC1240_DISABLE
}

//
//-------------------------------------------------------------

void UpdateAllCurrent(void) {
#ifdef MSCB_DEVICE_ADC_LTC1240_DISABLE
#else
    unsigned char Channel, RbkChannel;

   unsigned char channel;
   for (channel=0;channel<N_ADC; channel++) {    // loop through all channels
      Channel = (0x80 | channel);                // Signal negative, Common positive
//      Channel = (channel << 4) | 0x08);          // Signal positive, common negative
      ADS1240_SelectChannel(Channel);       
 
      delay_ms(1);    
      RbkChannel = ADS1240_ReadRegister(ADS1240_REG_MUX);
      DISABLE_INTERRUPTS;
      user_data.status =  Channel;
      user_data.control = RbkChannel;
      ENABLE_INTERRUPTS;
      delay_ms(50);    

      user_data.adc[channel] = ADS1240_ReadConversion();
      if (user_data.adc[channel] & 0x800000) {         //change data from 24 bit to 32 bit
         user_data.adc[channel] |= 0xFF000000;
      }
      current = ((float)user_data.adc[channel] * VOLTAGE_CONVERSION_RATIO / (user_data.gain * user_data.resMeas * FULL)); // use FULL for full range, HALF for half range
      current *= 1e9; // in nanoamp
      publishFloat(&user_data.cur[lookupTable[channel]], current);
   }


#endif // MSCB_DEVICE_ADC_LTC1240_DISABLE
}

