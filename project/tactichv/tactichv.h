/********************************************************************\

  Name:     tactichv.h
  $Id$

\********************************************************************/

#ifndef _GENERIC_IO_H
#define _GENERIC_IO_H

#include "../../common/mscbemb.h"

//
//
#define FIRST_VOLT   5
#define N_VOLT       7
#define FIRST_RES   22
#define N_RES       10
#define FIRST_ADC   34
#define N_ADC        8
#define VREF1       4.096f
#define VREF        2.500f
#define ODAC        32
#define GAIN        33        
#define MAX_TIME     1        

// Global definition
#define VOLTAGE_CONVERSION_RATIO	0.000000488281308f  // (VREF / (2^23 - 1))
#define RMEASUREMENT 1000000
#define FULL 1
#define HALF 2
//#define NANOAMPS_PER_BIT_HALF VOLTAGE_CONVERSION_RATIO/RMEASUREMENT/2 // half range
#define NANOAMPS_PER_BIT_FULL VOLTAGE_CONVERSION_RATIO/RMEASUREMENT   // full range

// Global ON / OFF definition
#define ON     1
#define DONE   1
#define SET    1
#define OFF    0
#define CLEAR  0

#define LED_GREEN 1
#define LED_RED   0

//
//--- MSCB structure
typedef struct {
  unsigned long SerialN;
  unsigned int  error;
  unsigned char control;
  unsigned char status;
  
  float temp;

  float volt[N_VOLT];

  float curBranch;
  float curOver;

  float cur[N_ADC];
  
  float res1;
  float res2;
  float res3;
  float res4;
  float resTendc;
  float res5;
  float res6;
  float res7;
  float res10k;
  float resMeas;

  signed char oDAC;
  char gain;

  signed long adc[N_ADC];

} MSCB_USER_DATA;

void Update_oDAC(void);
void Update_Gain(void);
void UpdateTemp(void);
void UpdateVolt(void);
void UpdateCurrent(void);
void UpdateIO(void);
void publishFloat(float*, float);
void publishCtlCsr(void);
void isoPowerShutdown(unsigned char onoff);
void init_ADS1240(void);
void UpdateAllCurrent(void);


#endif // _GENERIC_IO.H