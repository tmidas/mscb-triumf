/********************************************************************\

  Name:     GPIBPOL.h
  $Id$

\********************************************************************/

#ifndef _GPIB_H
#define _GPIB_H

#include "../../common/mscbemb.h"

// Global definition
#define CTL_IDX   2
#define CTL_RUN   0x1
#define CTL_INIT  0x2


// Global ON / OFF definition
#define ON        1
#define DONE      1
#define SET       1
#define OFF       0
#define CLEAR     0

#define LED_GREEN 1
#define LED_RED   0

//
//--- MSCB structure
typedef struct {
  // System
  unsigned long SerialN;
  unsigned int  error;
  unsigned char control;
  unsigned char status;
  unsigned char state;

  char output[64];
  char input[64];
  char gpib_adr;
} MSCB_USER_DATA;

void UpdateLoop(void);
unsigned char send_byte(unsigned char b);
unsigned char send(unsigned char adr, char *str);
unsigned char enter(unsigned char adr, char *str, unsigned char maxlen);

#endif // _GPIB_H