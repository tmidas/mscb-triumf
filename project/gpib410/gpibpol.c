/********************************************************************\

  Name:         gpibpol.c
  Created by:   PAA

  Contents:     Application specific (user) part of
                Midas Slow Control Bus protocol
                for the MSCB-GPIB for F410

  CODE(?PR?UPGRADE?MSCBMAIN (0x6800)) 

  code used for Keithley (Fabrice, TREK)

  http://www.d.umn.edu/~jmaps/phys5061/drivers/K6485_pA_refmanual.pdf

\********************************************************************/

#include <stdio.h>
#include <math.h>
#include <string.h>
#include "gpibpol.h" 

extern bit FREEZE_MODE;
extern bit DEBUG_MODE;

extern xdata SYS_INFO sys_info;

char          code  MSCB_node_name[] = "GPIB410";
char          xdata svn_rev_code[] = "$Rev: 2 $";

/* declare number of sub-addresses to framework */
char xdata mydate[24];
bit output_flag, ctl_flag, loop_flag, control_flag;

xdata MSCB_USER_DATA user_data;

MSCB_INFO_VAR code vars[] = {
  4, UNIT_BYTE,     0, 0, 0,           "SerialN",   &user_data.SerialN,      // 0
  2, UNIT_BYTE,     0, 0, 0,           "Error"  ,   &user_data.error,        // 1
  1, UNIT_BYTE,     0, 0, 0,           "Control",   &user_data.control,      // 2
  1, UNIT_BYTE,     0, 0, 0,           "Status" ,   &user_data.status,       // 3
  1, UNIT_BYTE,     0, 0, 0,           "State",     &user_data.state,        // 4

 64, UNIT_STRING, 0, 0,              0, "Output",   &user_data.output[0],    //  
 64, UNIT_STRING, 0, 0,              0, "Input",    &user_data.input[0],     //  
  1, UNIT_BYTE,   0, 0,   MSCBF_HIDDEN, "GPIB Adr", &user_data.gpib_adr,     //  
  0
};

code MSCB_INFO_VAR *variables = vars;

#define GPIB_DATA P2

/* GPIB control/status bits DB24 */
sbit GPIB_REN  = P1 ^ 0;          // REMOTE ENABLE, 	Pin 9
sbit GPIB_EOI  = P1 ^ 1;          // END-OR-IDENTIFY, 	Pin 10
sbit GPIB_DAV  = P1 ^ 2;          // DATA VALID,		Pin 11
sbit GPIB_NRFD = P1 ^ 3;          // NOT READY FOR DATA,Pin 12
sbit GPIB_NDAC = P1 ^ 4;          // NOT DATA ACCEPTED,	Pin 13
sbit GPIB_IFC  = P1 ^ 5;          // INTERFACE CLEAR,	Pin 14
sbit GPIB_SRQ  = P1 ^ 6;          // SERVICE REQUEST,	Pin 15
sbit GPIB_ATN  = P1 ^ 7;          // ATTENTION,			Pin 16

sbit BUF_CLE   = P0 ^ 0;   // P0.
sbit BUF_DATAE = P0 ^ 3;   // P0.3

/********************************************************************\

  Application specific init and input/output routines

\********************************************************************/

void user_write(unsigned char index) reentrant;
unsigned char send(unsigned char adr, char *str);
unsigned char send_byte(unsigned char b);

// Control register
unsigned char bdata rCTL = 0;
sbit cRun       = rCTL ^ 0;
sbit cInit      = rCTL ^ 1;
sbit cSpare2    = rCTL ^ 2;
sbit cspare3    = rCTL ^ 3;
sbit cspare4    = rCTL ^ 4;
sbit cspare5    = rCTL ^ 5;
sbit cspare6    = rCTL ^ 6;
sbit CmSd       = rCTL ^ 7;

// Status Register
unsigned char bdata rCSR = 0;
sbit sRun     = rCSR ^ 0;
sbit sInit    = rCSR ^ 1;
sbit sspare2  = rCSR ^ 2;
sbit sspare3  = rCSR ^ 3;
sbit sspare4  = rCSR ^ 4;
sbit sspare5  = rCSR ^ 5;
sbit sspare6  = rCSR ^ 6;
sbit sspare7  = rCSR ^ 7;

/*---- User init function ------------------------------------------*/
void user_init(unsigned char init)
{

  char i;
  unsigned char temp;

  /* Format the SVN and store this code SVN revision into the system */
  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  sys_info.svn_revision = (svn_rev_code[6]-'0')*1000+
    (svn_rev_code[7]-'0')*100+
    (svn_rev_code[8]-'0')*10+
    (svn_rev_code[9]-'0');

  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }


// Ports
//	P0MDIN = 0xFF;						// all digital pins
//	P1MDIN = 0xFF;
	P2MDIN = 0xFF;

	P0MDOUT = 0x18;					// TXen, TXD, CNTRL_Oen
	P1MDOUT = 0x00;					// Unknown
	P2MDOUT = 0x00;					// Unknown

	P0 = 0x03;
	P1 = 0xFF;
	P2 = 0xFF;

  /* set initial state of lines */
  GPIB_DATA = 0xFF;
  GPIB_EOI = 1;
  GPIB_DAV = 1;
  GPIB_NRFD = 1;
  GPIB_NDAC = 1;
  GPIB_IFC = 1;
  GPIB_SRQ = 1;
  GPIB_ATN = 1;
  GPIB_REN = 1;

  BUF_CLE = 0;   // For the 121 rev 2 of the GPIB
  BUF_DATAE = 0;


  /* initialize GPIB */
  GPIB_IFC = 0;
  delay_ms(1);
  GPIB_IFC = 1;

  GPIB_ATN = 0;
  send_byte(0x14);             // DCL
  GPIB_ATN = 1;


  /* initial nonzero EEPROM values */
  if (init) {
  /* default most settings to zero */
    memset(&user_data, 0, sizeof(user_data));

    user_data.gpib_adr = 10;
    user_data.control = 0;

   /* set default group address */
   if (sys_info.group_addr == 0xFFFF)
      sys_info.group_addr = 0xFF00;

   if (sys_info.node_addr == 0xFFFF) 
   	  sys_info.node_addr = 0x01;
  }

}

/*---- User write function -----------------------------------------*/
#pragma NOAREGS 
void user_write(unsigned char index) reentrant
{
  if (index == CTL_IDX){ // Turn on function
    if (user_data.control & CTL_RUN) {
      cRun = ON;
    }
    else if (user_data.control & CTL_INIT) { // Run Init function
      cInit = ON;
    }
  }
  if (index == 5) output_flag=1;

  return;
}

/*---- User read function ------------------------------------------*/
unsigned char user_read(unsigned char index)
{
   if (index);
   return 0;
}

/*---- User function called via CMD_USER command -------------------*/
unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
   /* echo input data */
   data_out[0] = data_in[0];
   data_out[1] = data_in[1];
   return 2;
}

/*---- User loop function ------------------------------------------*/
void user_loop(void)
{
  UpdateLoop();
}

/*---- Functions for GPIB port -------------------------------------*/
unsigned char send_byte(unsigned char b)
{
   unsigned int i;

//  yield();

   /* wait for NRFD go high */
   for (i = 0; i < 1000; i++) {
//      delay_us(2);
      if (GPIB_NRFD == 1)
         break;
   }

   if (GPIB_NRFD == 0)
      return 0;

   GPIB_DATA = ~b;              // negate
   delay_us(10);                 // let signals settle
   GPIB_DAV = 0;

   /* wait for NDAC go high */
   for (i = 0; i < 1000; i++) {
      delay_us(10);
      if (GPIB_NDAC == 1)
         break;
   }

   if (GPIB_NDAC == 0) {
      GPIB_DAV = 1;
      GPIB_DATA = 0xFF;
      return 0;                 // timeout
   }

//   delay_us(10);
   GPIB_DAV = 1;
   GPIB_DATA = 0xFF;            // prepare for input

   /* wait for NRFD go high */
   for (i = 0; i < 1000; i++) {
//      delay_us(2);
      if (GPIB_NRFD == 1)
         break;
   }

   if (GPIB_NRFD == 0)
      return 0;

   return 1;
}

/*------------------------------------------------------------------*/
unsigned char send(unsigned char adr, char *str)
{
   unsigned char i;
   char s;

  /*---- address cycle ----*/
   GPIB_REN = 0;
   GPIB_ATN = 0;                // assert attention
   send_byte(0x3F);             // unlisten
   send_byte(0x5F);             // untalk
   send_byte(0x20 | adr);       // listen device
   send_byte(0x40 | 21);        // talk 21
   GPIB_ATN = 1;                // remove attention
 
  /*---- data cycles ----*/
   for (i = 0; str[i] > 0; i++) {
      s = send_byte(str[i]);
      if (s == 0) {
         GPIB_REN = 1;
         return 0;
      }
   }

   GPIB_REN = 0;
   GPIB_ATN = 0;                // assert attention
   send_byte(0x3F);             // unlisten
   send_byte(0x5F);             // untalk
   send_byte(0x20 | adr);       // listen device
   send_byte(0x40 | 21);        // talk 21
   GPIB_ATN = 1;                // remove attention

   GPIB_EOI = 0;
   send_byte(0x0A);             // NL
   GPIB_EOI = 1;

   return i;
}

/*------------------------------------------------------------------*/
unsigned char enter(unsigned char adr, char *str, unsigned char maxlen)
{
   unsigned long t;
   unsigned char i, flag;
   unsigned int j;

  /*---- address cycle ----*/

   GPIB_REN = 0;                // Remote enable
   GPIB_ATN = 0;                // assert attention
   send_byte(0x3F);             // unlisten
   send_byte(0x5F);             // untalk
   send_byte(0x20 | 21);        // listen 21
   send_byte(0x40 | adr);       // talk device
   GPIB_ATN = 1;                // remove attention

  /*---- data cycles ----*/

   GPIB_NDAC = 0;               // init NDAC line

// Reset string
   for(i=0;i<maxlen;i++) str[i] = 0;

// Loop over string length
   for (i = 0; i < maxlen; i++) {

      GPIB_NRFD = 1;            // signal ready for data

      /* wait 1s for DAV go low */
      t = time();
      do {
         if (GPIB_DAV == 0)
            break;
          delay_ms(5);
      } while (time() - t < 100);

      if (GPIB_DAV == 1) {
         GPIB_NDAC = 1;
         GPIB_NRFD = 1;
         GPIB_REN  = 1;
         return 20;              // timeout
      }

      GPIB_NRFD = 0;            // signal busy
      str[i] = ~GPIB_DATA;      // read negated data
      flag = GPIB_EOI;          // read EOI flag
      GPIB_NDAC = 1;            // signal acknowledge

      /* wait for DAV go high */
      for (j = 0; j < 1000; j++) {
         delay_us(10);
         if (GPIB_DAV == 1)
            break;
      }
      GPIB_NDAC = 0;            // remove acknowledge

      if (flag == 0)            // stop if end of data
         break;
   }

   GPIB_NDAC = 1;               // release handshake lines
   GPIB_NRFD = 1;

   /* stop talker */
   GPIB_ATN = 0;                // assert attention
   send_byte(0x3F);             // unlisten
   send_byte(0x5F);             // untalk
   GPIB_ATN = 1;                // remove attention
   GPIB_REN = 1;

   return i;
}


/*---- User loop function ------------------------------------------*/
void UpdateLoop(void)
{
  int i=0;
//  unsigned long int ltime;

	watchdog_refresh(0);
   if (output_flag || loop_flag) {

	 if (output_flag) led_blink(1, 1, 100);     // manual entry 
     output_flag = 0;
     loop_flag = 0;

      /* send buffer */
      i = send(user_data.gpib_adr, user_data.output);
      user_data.error = i; 
      delay_ms(200);

      /* receive buffer */

      i = enter(user_data.gpib_adr, user_data.input, sizeof(user_data.input));
       user_data.SerialN = i;
      // strip NL 
      if (strlen(user_data.input) > 0 &&
          user_data.input[strlen(user_data.input) - 1] == 10)
         user_data.input[strlen(user_data.input) - 1] = 0;

   }

   if (control_flag) {
      control_flag = 0;

      GPIB_ATN = 0;             // assert attention
      send_byte(0x3F);          // unlisten
      send_byte(0x5F);          // untalk
      send_byte(0x20 | user_data.gpib_adr);     // listen device
      send_byte(0x40 | 21);     // talk 21
      send_byte(user_data.control);     // send control
      send_byte(0x3F);          // unlisten
      send_byte(0x5F);          // untalk
      GPIB_ATN = 1;             // remove attention
   }

/* Loop for Voltage measurement */

   if (user_data.state == 0x01) {
     // Manual range 100V@1mV
//     sprintf(user_data.output, "MEAS:VOLT:DC? 100,0.001");
     sprintf(user_data.output, "READ?");
     loop_flag = 1;
//     led_blink(0, 1, 100);     // signal data received
   }
   if (user_data.state == 0x02) {
     // Manual range 0.1V @0.01mV
     sprintf(user_data.output, "MEAS:VOLT:DC? 0.1,.00001,(@101)");
     loop_flag = 1;
//     led_blink(0, 1, 200);     // signal data received
   }

  /*
     ltime = SmaRTCRead();
     user_data.mytime = ltime;
     ascTime(&mydate[0], ltime);
     sprintf(user_data.date, "%s", mydate);
     delay_ms(300);
  */
}
