/********************************************************************\

  Name:         mscb_voltmeter-410.c
  Created by:   Bryerton Shaw


  Contents:     Application specific (user) part of
                Midas Slow Control Bus protocol
                for the MSCB-GPIB for F410

  CODE(?PR?UPGRADE?MSCBMAIN (0x6800)) 
  $Id$

\********************************************************************/

#include <stdio.h>
#include <math.h>
#include <string.h>
//#include "Time.h"
//#include "SmaRTC.h"
#include "gpib410.h" 

extern bit FREEZE_MODE;
extern bit DEBUG_MODE;

extern xdata SYS_INFO sys_info;

char          code  MSCB_node_name[] = "GPIB410";
char          xdata svn_rev_code[] = "$Rev$";

/* declare number of sub-addresses to framework */
char xdata mydate[24];
bit output_flag, ctl_flag, loop_flag, control_flag;

xdata MSCB_USER_DATA user_data;

MSCB_INFO_VAR code vars[] = {
   1,  UNIT_ASCII,  0, 0, MSCBF_DATALESS, "GPIB",                       0, // 0
   64, UNIT_STRING, 0, 0,              0, "Output",  &user_data.output[0], // 1
   64, UNIT_STRING, 0, 0,              0, "Input",    &user_data.input[0], // 2
   1,  UNIT_BYTE,   0, 0,              0, "Control",   &user_data.control, // 3
   1,  UNIT_BYTE,   0, 0,              0, "SRQ",           &user_data.srq, // 4
   1,  UNIT_BYTE,   0, 0,              0, "CTL",           &user_data.ctl, // 5
   1,  UNIT_BYTE,   0, 0,              0, "GPIB Adr", &user_data.gpib_adr, // 6
   4,  UNIT_BYTE,   0, 0,    MSCBF_FLOAT, "Temp1",    &user_data.temp[0],  // 7
   4,  UNIT_BYTE,   0, 0,    MSCBF_FLOAT, "Temp2",    &user_data.temp[1],  // 8
   4,  UNIT_BYTE,   0, 0,    MSCBF_FLOAT, "Temp3",    &user_data.temp[2],  // 9
   4,  UNIT_BYTE,   0, 0,    MSCBF_FLOAT, "Temp4",    &user_data.temp[3],  // 10
   4,  UNIT_BYTE,   0, 0,    MSCBF_FLOAT, "Temp5",    &user_data.temp[4],  // 11
   4,  UNIT_BYTE,   0, 0,    MSCBF_FLOAT, "Temp6",    &user_data.temp[5],  // 12
   4,  UNIT_BYTE,   0, 0,    MSCBF_FLOAT, "Temp7",    &user_data.temp[6],  // 13 
   4,  UNIT_BYTE,   0, 0,    MSCBF_FLOAT, "Temp8",    &user_data.temp[7],  // 14
   24, UNIT_STRING, 0, 0,   MSCBF_HIDDEN, "GMT Date", &user_data.date[0],  // 15
   4,  UNIT_BYTE,   0, 0,   MSCBF_HIDDEN, "GMT Epok" ,    &user_data.mytime, // 16
   0
};

code MSCB_INFO_VAR *variables = vars;

#define GPIB_DATA P2

/* GPIB control/status bits DB24 */
sbit GPIB_REN  = P1 ^ 0;          // REMOTE ENABLE,   	Pin 9
sbit GPIB_EOI  = P1 ^ 1;          // END-OR-IDENTIFY, 	Pin 10
sbit GPIB_DAV  = P1 ^ 2;          // DATA VALID,		    Pin 11
sbit GPIB_NRFD = P1 ^ 3;          // NOT READY FOR DATA,Pin 12
sbit GPIB_NDAC = P1 ^ 4;          // NOT DATA ACCEPTED,	Pin 13
sbit GPIB_IFC  = P1 ^ 5;          // INTERFACE CLEAR,   Pin 14
sbit GPIB_SRQ  = P1 ^ 6;          // SERVICE REQUEST,   Pin 15
sbit GPIB_ATN  = P1 ^ 7;          // ATTENTION,		      Pin 16
 
sbit BUF_CLE   = P0 ^ 0;   // P0.
sbit BUF_DATAE = P0 ^ 3;   // P0.3

/********************************************************************\

  Application specific init and input/output routines

\********************************************************************/

void user_write(unsigned char index) reentrant;
unsigned char send(unsigned char adr, char *str);
unsigned char send_byte(unsigned char b);
void read_temp(unsigned int idx);
void read_Atemp(void);

/*---- User init function ------------------------------------------*/
void user_init(unsigned char init)
{
//	P0MDIN = 0xFF;						// all digital pins
//	P1MDIN = 0xFF;
	P2MDIN = 0xFF;

	P0MDOUT = 0x18;					// TXen, TXD, CNTRL_Oen
	P1MDOUT = 0x00;					// Unknown
	P2MDOUT = 0x00;					// Unknown

	P0 = 0x03;
	P1 = 0xFF;
	P2 = 0xFF;

   /* set initial state of lines */
   GPIB_DATA = 0xFF;
   GPIB_EOI = 1;
   GPIB_DAV = 1;
   GPIB_NRFD = 1;
   GPIB_NDAC = 1;
   GPIB_IFC = 1;
   GPIB_SRQ = 1;
   GPIB_ATN = 1;
   GPIB_REN = 1;

   BUF_CLE = 0;   // For the 121 rev 2 of the GPIB
   BUF_DATAE = 0;


   /* initialize GPIB */
   GPIB_IFC = 0;
   delay_ms(1);
   GPIB_IFC = 1;

   GPIB_ATN = 0;
   send_byte(0x14);             // DCL
   GPIB_ATN = 1;


   /* initial nonzero EEPROM values */
   if (init) {
		/* default most settings to zero */
      memset(&user_data, 0, sizeof(user_data));

      user_data.gpib_adr = 10;
		user_data.control = 0;

	   /* set default group address */
	   if (sys_info.group_addr == 0xFFFF)
	      sys_info.group_addr = 0xFF00;

	   if (sys_info.node_addr == 0xFFFF) 
	   	  sys_info.node_addr = 0x01;
   }

   user_data.ctl = 0x1;
 //  SmaRTCInit();
}

/*---- User write function -----------------------------------------*/
#pragma NOAREGS 
void user_write(unsigned char index) reentrant
{
   if (index == 5)
      ctl_flag = 1;
   if (index == 1)
      output_flag = 1;
   if (index == 3)
      control_flag = 1;
//  if (index == 8)
//     SmaRTCSetTime(user_data.mytime);

		return;
}

/*---- User read function ------------------------------------------*/
unsigned char user_read(unsigned char index)
{
   if (index);
   return 0;
}

/*---- User function called vid CMD_USER command -------------------*/
unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
   /* echo input data */
   data_out[0] = data_in[0];
   data_out[1] = data_in[1];
   return 2;
}

/*---- Functions for GPIB port -------------------------------------*/
unsigned char send_byte(unsigned char b)
{
   unsigned int i;

//  yield();

   /* wait for NRFD go high */
   for (i = 0; i < 1000; i++) {
//      delay_us(2);
      if (GPIB_NRFD == 1)
         break;
   }

   if (GPIB_NRFD == 0)
      return 0;

   GPIB_DATA = ~b;              // negate
   delay_us(10);                 // let signals settle
   GPIB_DAV = 0;

   /* wait for NDAC go high */
   for (i = 0; i < 1000; i++) {
      delay_us(10);
      if (GPIB_NDAC == 1)
         break;
   }

   if (GPIB_NDAC == 0) {
      GPIB_DAV = 1;
      GPIB_DATA = 0xFF;
      return 0;                 // timeout
   }

//   delay_us(10);
   GPIB_DAV = 1;
   GPIB_DATA = 0xFF;            // prepare for input

   /* wait for NRFD go high */
   for (i = 0; i < 1000; i++) {
//      delay_us(2);
      if (GPIB_NRFD == 1)
         break;
   }

   if (GPIB_NRFD == 0)
      return 0;

   return 1;
}

/*------------------------------------------------------------------*/
unsigned char send(unsigned char adr, char *str)
{
   unsigned char i;
   char s;

  /*---- address cycle ----*/

   GPIB_REN = 0;
   GPIB_ATN = 0;                // assert attention
   send_byte(0x3F);             // unlisten
   send_byte(0x5F);             // untalk
   send_byte(0x20 | adr);       // listen device
   send_byte(0x40 | 21);        // talk 21
   GPIB_ATN = 1;                // remove attention
 

  /*---- data cycles ----*/
   for (i = 0; str[i] > 0; i++) {
      s = send_byte(str[i]);
//      GPIB_REN = 1;
      if (s == 0) return 0;
   }

   GPIB_REN = 0;
   GPIB_ATN = 0;                // assert attention
   send_byte(0x3F);             // unlisten
   send_byte(0x5F);             // untalk
   send_byte(0x20 | adr);       // listen device
   send_byte(0x40 | 21);        // talk 21
   GPIB_ATN = 1;                // remove attention

   GPIB_EOI = 0;
   send_byte(0x0A);             // NL
   GPIB_EOI = 1;
//   GPIB_REN = 1;

   return i;
}

/*------------------------------------------------------------------*/
unsigned char enter(unsigned char adr, char *str, unsigned char maxlen)
{
   unsigned long t;
   unsigned char i, flag;
   unsigned int j;

  /*---- address cycle ----*/

   GPIB_REN = 0;
   GPIB_ATN = 0;                // assert attention
   send_byte(0x3F);             // unlisten
   send_byte(0x5F);             // untalk
   send_byte(0x20 | 21);        // listen 21
   send_byte(0x40 | adr);       // talk device
   GPIB_ATN = 1;                // remove attention

  /*---- data cycles ----*/

   GPIB_NDAC = 0;               // init NDAC line

// Reset string
   for(i=0;i<maxlen;i++) str[i] = 0;

// Loop ove rstring length
   for (i = 0; i < maxlen; i++) {
//    yield();

      GPIB_NRFD = 1;            // singal ready for data

      /* wait 1s for DAV go low */
      t = time();
      do {
         if (GPIB_DAV == 0)
            break;
          delay_ms(10);
         yield();

      } while (time() - t < 100);

      if (GPIB_DAV == 1) {
         GPIB_NDAC = 1;
         GPIB_NRFD = 1;
//         GPIB_REN = 1;
         return 0;              // timeout
      }

      GPIB_NRFD = 0;            // signal busy
      str[i] = ~GPIB_DATA;      // read negated data

      flag = GPIB_EOI;          // read EOI flag

      GPIB_NDAC = 1;            // signal acknowledge

      /* wait for DAV go high */
      for (j = 0; j < 1000; j++) {
         delay_us(10);
         if (GPIB_DAV == 1)
            break;
      }
      GPIB_NDAC = 0;            // remove acknowledge

      if (flag == 0)            // stop if end of data
         break;
   }

   GPIB_NDAC = 1;               // release handshake lines
   GPIB_NRFD = 1;

   /* stop talker */
   GPIB_ATN = 0;                // assert attention
   send_byte(0x3F);             // unlisten
   send_byte(0x5F);             // untalk
   GPIB_ATN = 1;                // remove attention
//   GPIB_REN = 1;

   return i;
}

/*---- User loop function ------------------------------------------*/
void read_temp(unsigned int idx)
{
  //  sprintf(user_data.output, "12.0345");
  float value;

  
  sprintf(user_data.output, "KRDG? %i", idx+1);

	watchdog_refresh(0);

  // send buffer 
  send(user_data.gpib_adr, user_data.output);
  delay_ms(1000);

  // receive buffer
  enter(user_data.gpib_adr, user_data.input, sizeof(user_data.input));
  // led_blink(0, 1, 10);

  // strip NL 
  if (strlen(user_data.input) > 0 && user_data.input[strlen(user_data.input) - 1] == 10) {
    user_data.input[strlen(user_data.input) - 1] = 0;
  }

  sscanf(user_data.input, "%f", &value);
  DISABLE_INTERRUPTS;
  user_data.temp[idx] = value;
  ENABLE_INTERRUPTS;

  led_blink(1, 1, 200);     // signal transaction
}


/*---- User loop function ------------------------------------------*/
void read_Atemp(void)
{
  unsigned int idx;
  //  sprintf(user_data.output, "12.0345");
  float value[8];

  
  sprintf(user_data.output, "KRDG?");

	watchdog_refresh(0);

  // send buffer 
  send(user_data.gpib_adr, user_data.output);
  delay_ms(3000);

  // receive buffer
  enter(user_data.gpib_adr, user_data.input, sizeof(user_data.input));
  // led_blink(0, 1, 10);

  // strip NL 
  if (strlen(user_data.input) > 0 && user_data.input[strlen(user_data.input) - 1] == 10) {
    user_data.input[strlen(user_data.input) - 2] = 0;
  }

  sscanf(&(user_data.input[0]), "%f,%f,%f,%f"
            , &value[0], &value[1], &value[2], &value[3]);
  sscanf(&(user_data.input[32]), "%f,%f,%f,%f"
            , &value[4], &value[5], &value[6], &value[7]);
//  sscanf(user_data.input, "%f,%f,%f,%f,%f,%f,%f,%f"
//            , &value[0], &value[1], &value[2], &value[3]
//            , &value[4], &value[5], &value[6], &value[7]);

  if (strlen(user_data.input)) {
    DISABLE_INTERRUPTS;
    for (idx=0;idx < N_LAKES; idx++) {
      user_data.temp[idx] = value[idx];
    }
    ENABLE_INTERRUPTS;
  }
  led_blink(1, 1, 200);     // signal transaction
}


/*---- User loop function ------------------------------------------*/
void user_loop(void)
{
  unsigned int i;

  if (user_data.ctl == 0x8) {
    read_Atemp();
  }
  if (user_data.ctl == 0x4) {
    for (i=0;i<N_LAKES;i++) {
      read_temp(i);
      delay_ms(500);
    }
  } else {
    watchdog_refresh(0);
    if (output_flag || loop_flag) {
      output_flag = 0;
      loop_flag = 0;

      /* send buffer */
      send(user_data.gpib_adr, user_data.output);
      delay_ms(2000);

      /* receive buffer */

      enter(user_data.gpib_adr, user_data.input, sizeof(user_data.input));
      //    led_blink(0, 1, 10);

      // strip NL 
      if (strlen(user_data.input) > 0 && user_data.input[strlen(user_data.input) - 1] == 10) {
        user_data.input[strlen(user_data.input) - 1] = 0;
      }
    }

    if (control_flag) {
      control_flag = 0;

      GPIB_ATN = 0;             // assert attention
      send_byte(0x3F);          // unlisten
      send_byte(0x5F);          // untalk
      send_byte(0x20 | user_data.gpib_adr);     // listen device
      send_byte(0x40 | 21);     // talk 21
      send_byte(user_data.control);     // send control
      send_byte(0x3F);          // unlisten
      send_byte(0x5F);          // untalk
      GPIB_ATN = 1;             // remove attention
    }

    /* Loop for Voltage measurement */
    if (user_data.ctl == 0x01) {
      // Manual range 100V@1mV
      // sprintf(user_data.output, "MEAS:VOLT:DC? 100,0.001");
      sprintf(user_data.output, "READ?");
      loop_flag = 1;
      led_blink(1, 1, 100);     // signal data received
    }
    if (user_data.ctl == 0x02) {
      // Manual range 0.1V @0.01mV
      sprintf(user_data.output, "MEAS:VOLT:DC? 0.1,.00001");
      loop_flag = 1;
      led_blink(1, 1, 200);     // signal data received
    }
  }

/*
     ltime = SmaRTCRead();
     user_data.mytime = ltime;
     ascTime(&mydate[0], ltime);
     sprintf(user_data.date, "%s", mydate);
     delay_ms(300);
*/
  led_blink(0, 1, 100);     // signal loop
}
