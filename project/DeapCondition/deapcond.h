/********************************************************************\

  Name:     deapcond.h
            on F410
  $Id$

\********************************************************************/

#ifndef _GENERIC_IO_H
#define _GENERIC_IO_H

#include "../../common/mscbemb.h"

//
//

#define V2PV    37.4/10.0f
#define V2NV    -3.0f
#define V2A    1.0f/(0.18*50.)
#define ADCVREF       2.225f
#define VDDREF        2.5f
#define A2VCONV12	   (ADCVREF / 4096.f)
#define AI2VCONV12   (VDDREF / 4096.f)

#define N_ADC         4
//                        P5V   P5I   N5V           N5I
char code chmap[N_ADC] = {0x14, 0x15, 0x10        , 0x11};
float code coeff [N_ADC] ={V2PV, V2A, 4.         ,  V2A};
float code offset[N_ADC] ={0.0,  0.0,  -3.*ADCVREF,  0.0};
#define N_TEMPERATURE 4
//                                  T0    T1    T2    IntT
char code  tempmap[N_TEMPERATURE] = {0x16, 0x0E, 0x0F, 0x18};
float code tcoeff [N_ADC] = { 37.244,  37.244,  37.244, 1/0.00295};
float code toffset[N_ADC] = {-21.496, -21.496, -21.496, -0.9/0.00295};
 
// Enables
#define PCA9535_ADD  0x20



// Global definition
#define CTL_IDX       2
#define FIRST_ADC     5
#define FIRST_TEMP    9

// Global ON / OFF definition
#define ON     1
#define DONE   1
#define SET    1
#define OFF    0
#define CLEAR  0

#define LED_GREEN 1
#define LED_RED   0

//
//--- MSCB structure
typedef struct {
  // System
  unsigned long SerialN;
  unsigned int  error;
  unsigned char control;
  unsigned char status;
  unsigned int  enable;
  float         adc[N_ADC];
  float         temp[N_TEMPERATURE];
  unsigned int  radc[N_ADC];
  unsigned int  rtemp[N_TEMPERATURE];
} MSCB_USER_DATA;

void publishCtlCsr(void);
void publishErr(bit errbit);
void publishAll();
void UpdateEnable();
void UpdateIO();
void UpdateADC();
void UpdateTemperature();

#endif // _GENERIC_IO.H