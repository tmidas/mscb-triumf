/********************************************************************\

Name:     deapcond 
Author:   Pierre-Andr�
Date:     Jan 28/2011
$Id$
\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "deapcond.h" 

extern xdata SYS_INFO sys_info;

char    code  MSCB_node_name[] = "DEAPCOND410";
char    xdata svn_rev_code[] = "$Rev$";

xdata         MSCB_USER_DATA user_data;
//-----------------------------------------------------------------------------
// User Data structure declaration
MSCB_INFO_VAR code vars[] = {
  4, UNIT_BYTE,     0, 0, 0,           "SerialN",   &user_data.SerialN,    // 0
  2, UNIT_BYTE,     0, 0, 0,           "Error"  ,   &user_data.error,      // 1
  1, UNIT_BYTE,     0, 0, 0,           "Control",   &user_data.control,    // 2
  1, UNIT_BYTE,     0, 0, 0,           "Status" ,   &user_data.status,     // 3

  2, UNIT_BYTE,     0, 0, 0,           "Enable"  ,  &user_data.enable,     // 4

  4, UNIT_VOLT,              0, 0, MSCBF_FLOAT, "P5Vmon"  ,   &user_data.adc[0],     // 5   
  4, UNIT_AMPERE,   PRFX_MILLI, 0, MSCBF_FLOAT, "P5Imon"  ,   &user_data.adc[1],     // 6
  4, UNIT_VOLT,              0, 0, MSCBF_FLOAT, "N5Vmon"  ,   &user_data.adc[2],     // 7
  4, UNIT_AMPERE,   PRFX_MILLI, 0, MSCBF_FLOAT, "N5Imon"  ,   &user_data.adc[3],     // 8

  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "Temp0"   ,   &user_data.temp[0],    // 9
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "Temp1"   ,   &user_data.temp[1],    // 10
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "Temp2"   ,   &user_data.temp[2],    // 11
  4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT, "IntTemp" ,   &user_data.temp[3],    // 12

  2, UNIT_BYTE,     0, 0, 0,           "rp5vmon" ,   &user_data.radc[0],    // 13       
  2, UNIT_BYTE,     0, 0, 0,           "rp5imon" ,   &user_data.radc[1],    // 14       
  2, UNIT_BYTE,     0, 0, 0,           "rn5vmon" ,   &user_data.radc[2],    // 15       
  2, UNIT_BYTE,     0, 0, 0,           "rn5imon" ,   &user_data.radc[3],    // 16

  2, UNIT_BYTE,     0, 0, 0,           "rtemp0" ,    &user_data.rtemp[0],   // 17       
  2, UNIT_BYTE,     0, 0, 0,           "rtemp1" ,    &user_data.rtemp[1],   // 18       
  2, UNIT_BYTE,     0, 0, 0,           "rtemp2" ,    &user_data.rtemp[2],   // 19       
  2, UNIT_BYTE,     0, 0, 0,           "rintemp" ,   &user_data.rtemp[3],   // 20       
  0
  };

code MSCB_INFO_VAR *variables = vars;

/********************************************************************\

Application specific init and inout/output routines

\********************************************************************/

// Change register
volatile unsigned char bdata bChange = 0;
sbit IO_Flag      = bChange ^ 0;

// Control register
unsigned char bdata rCTL = 0;
sbit cPup       = rCTL ^ 0;
sbit cEnable    = rCTL ^ 1;
sbit cspare2    = rCTL ^ 2;
sbit cspare3    = rCTL ^ 3;
sbit cspare4    = rCTL ^ 4;
sbit cspare5    = rCTL ^ 5;
sbit cspare6    = rCTL ^ 6;
sbit CmSd       = rCTL ^ 7;

// Status Register
unsigned char bdata rCSR = 0;
sbit sPup     = rCSR ^ 0;
sbit sEnable  = rCSR ^ 1;
sbit sspare2  = rCSR ^ 2;
sbit sspare3  = rCSR ^ 3;
sbit sspare4  = rCSR ^ 4;
sbit sspare5  = rCSR ^ 5;
sbit sspare6  = rCSR ^ 6;
sbit sspare7  = rCSR ^ 7;

// ESR Error Register
//The low and high bytes are switched in the bdata section of the memory
//This is the reason that the sbit declarations do not appear to match
//the documentation but they actually do.
unsigned int bdata rESR;
sbit eAlert     = rESR ^ 8;  //0x1
sbit eAdc       = rESR ^ 9;  //0x2

// unused bunch
sbit AV33mon   = rESR ^ 10; //0x4
sbit AV25mon   = rESR ^ 11; //0x8

sbit DV15mon   = rESR ^ 12; //0x10
sbit DV18mon   = rESR ^ 13; //0x20
sbit DV25mon   = rESR ^ 14; //0x40
sbit DV33mon   = rESR ^ 15; //0x80

sbit uCT       = rESR ^ 0;  //0x100
sbit FPGAssTT  = rESR ^ 1;  //0x200
sbit Vreg1ssTT = rESR ^ 2;  //0x400
sbit Vreg2ssTT = rESR ^ 3;  //0x800

sbit RdssT     = rESR ^ 4;  //0x1000
sbit EEPROM    = rESR ^ 5;  //0x2000
sbit AsumLock  = rESR ^ 6;  //0x4000 1= any of 4 not lock
sbit V4_OC     = rESR ^ 7;  //0x8000

// Hardware bits
sbit bPowerDis = P0 ^ 7;
sbit bVref = P1 ^ 2;

/*---- User init function ------------------------------------------*/
void user_init(unsigned char init)
{
  char i;
  unsigned char temp;

  /* Format the SVN and store this code SVN revision into the system */
  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  sys_info.svn_revision = (svn_rev_code[6]-'0')*1000+
    (svn_rev_code[7]-'0')*100+
    (svn_rev_code[8]-'0')*10+
    (svn_rev_code[9]-'0');

  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  if (init) {
    // Initialize variables
    for (i=0;i<N_ADC;i++) user_data.adc [i] = 0;
  }


  // 0:analog 1:digital
  P0MDIN = 0xFF; // P0 all digital pins
  P1MDIN = 0xF8; // P1 .2:Vref
  P2MDIN = 0x8C; // P2 
 
  // 0: open-drain, 1:push-pull
  P0MDOUT = 0xD0; // .7: PowerEn .6:RS485EN .5:Rx .4:Tx 
  bPowerDis = 1;
  XBR0 |=0x04;	 // Enable SMBus pins P0 .0/SDA .1/SCL


  // 
 // REG0CN = 0x90;  // disable VREG

  // Drive Vref out on P1.2
  REF0CN = 0x18;  // Vref:2.2 out, Vdd internal, Drive Vref out, Int Bias off, Int Temp off, Int Buf Ref on
  // 0: open-drain, 1:push-pull
  P1MDOUT = 0x04;  // .7:Temp2 .6:Temp1 .2:Vref 
  bVref = 1;

  // 0: open-drain, 1:push-pull
  P2MDOUT = 0x0C;  // .0 ALERTn (temp)

  // Setting the cross bar
  P0SKIP = 0x0; 
  P1SKIP = 0x1; // Vref out on P1.2
  P2SKIP = 0x0; 


  IO_Flag = 1;

  rCTL = 0;
  rCSR = 0;
  rESR = 0;
  // PCA
  PCA9535_Init();
  PCA9535_WriteWord(PCA9535_ADD, PCA9535_CONFIG, PCA9535_ALL_OUTPUT);
  PCA9535_WriteWord(PCA9535_ADD, PCA9535_POLINV, PCA9535_NO_INVERT);

  // Restore mscb mask
  PCA9535_WriteWord(PCA9535_ADD, PCA9535_OUTPUT, user_data.enable);

  adc_internal_init(0x18);  //  0(1.5V)/1(2.2V)-0-TEMPE-0-REFBE
  UpdateIO();
}

/*---- User write function -----------------------------------------*/
void user_write(unsigned char index) reentrant{

  if (index == CTL_IDX){ // Turn on/off Qpump
    if (user_data.control & 0x1) {
      cPup = 1;
    }
    else if (user_data.control & 0x2) { // Enable channels
      cEnable = 1;
    }
    else if (user_data.control & 0x4) { // Enable Current offfset subtraction
      cspare2 = 1;
    }
    else if (user_data.control & 0x8) { // Copy current offset to offset array
      cspare3 = 1;
    }
  }
  return;
}

/*---- User read function ------------------------------------------*/
unsigned char user_read(unsigned char index)
{
  if(index);
  return 0;
}

/*---- User function called vid CMD_USER command -------------------*/
unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
  // echo input data
  data_out[0] = data_in[0];
  data_out[1] = data_in[1];
  return 2;
}

/*---- User loop function ------------------------------------------*/
void user_loop(void)
{
  char i;

  UpdateEnable();
  UpdateADC();
  UpdateTemperature();
  UpdateIO();

  delay_ms(150); // sleep for a while.
}

//-----------------------------------------------------------------------------
//
void publishCtlCsr(void) {
    DISABLE_INTERRUPTS;
    user_data.control = rCTL;
    user_data.status  = rCSR;
    ENABLE_INTERRUPTS;
}

//-----------------------------------------------------------------------------
//
#if 0
void publishErr(bit errbit) {
    DISABLE_INTERRUPTS;
    errbit = SET;
    user_data.error   = rESR;
    ENABLE_INTERRUPTS;
}

//-----------------------------------------------------------------------------
//
void publishAll() {
  DISABLE_INTERRUPTS;
  user_data.control = rCTL;
  user_data.status  = rCSR;
  user_data.error   = rESR;
  ENABLE_INTERRUPTS;
}
#endif

//
//-------------------------------------------------------------
void UpdateIO(void) {
  char i;

  if (cPup) {
    if (sPup) {
     cPup = 0; sPup = 0;
     bPowerDis = 1;
    } else {
     cPup = 0; sPup = 1;
     bPowerDis = 0;
    }
  }

  publishCtlCsr();
}

//
//-------------------------------------------------------------
void UpdateEnable(void) {

#ifdef MSCB_DEVICE_PCA9535_DISABLE
#else

  if (cEnable) {
    if (sEnable) {
     cEnable = 0; sEnable = 0;
    } else {
     cEnable = 0; sEnable = 1;
    }
  }

  publishCtlCsr();

  if (sEnable) {
    PCA9535_WriteWord(PCA9535_ADD, PCA9535_OUTPUT, user_data.enable);
  }
 
#endif // MSCB_DEVICE_PCA9535_DISABLE
}

//
//-------------------------------------------------------------
void UpdateADC(void) {

#ifdef MSCB_DEVICE_ADC_INTERNAL_DISABLE
#else

    DISABLE_INTERRUPTS;
    // Voltages
    adc_internal_init(0x15);   //  VREF:2.2V/ADCREF=VREF/TEMPE/0/REFBE
    user_data.radc[0] = adc_read(chmap[0], 1);
    user_data.adc[0] = (user_data.radc[0] * A2VCONV12 * coeff[0]) + offset[0];
    user_data.radc[2] = adc_read(chmap[2], 1);
    user_data.adc[2] = (user_data.radc[2] * A2VCONV12 * coeff[2]) + offset[2];
    //
    // Currents
    adc_internal_init(0x18);  //  VREF:2.2V/ADCREF=VDD/TEMPE/0/REFBE
    delay_ms(1);
    user_data.radc[1] = adc_read(chmap[1], 1);
    user_data.adc[1] = (user_data.radc[1] * AI2VCONV12 * coeff[1]) + offset[1];
    user_data.adc[1] *= 1E3;  // milli
    user_data.radc[3] = adc_read(chmap[3], 1);
    user_data.adc[3] = (user_data.radc[3] * AI2VCONV12 * coeff[3]) + offset[3];
    user_data.adc[3] *= 1E3;  // milli
    ENABLE_INTERRUPTS;

#endif // MSCB_DEVICE_ADC_LTC2489_DISABLE
}

//
//-------------------------------------------------------------
void UpdateTemperature(void) {
  unsigned int temp;

#ifdef MSCB_DEVICE_ADC_INTERNAL_DISABLE
#else
  char i;

  adc_internal_init(0x15);  //  (2.2V)-internal ref Vref-TEMPE-0-REFBE
  for (i=0;i<N_TEMPERATURE;i++) {
    temp = adc_read(tempmap[i], 1);
    DISABLE_INTERRUPTS;
    user_data.rtemp[i] = temp;
    user_data.temp[i] = ((user_data.rtemp[i] * A2VCONV12) * tcoeff[i]) + toffset[i]; // In degC
    ENABLE_INTERRUPTS;
  }

#endif
}