/********************************************************************\

  Name:     giocpet410.h
  $Id$

\********************************************************************/

#ifndef _GIOCPET_410_H
#define _GIOCPET_410_H

#include "../../common/mscbemb.h"
#include "../../device/dac/ad5535.h"
//
//
#define CTL_IDX    0

#define N_DAC       32
#define FIRST_DAC    2 

#define PCA9536_ADDR  0x41 // MUX

// Global definition
// Global ON / OFF definition
#define ON     1
#define DONE   1
#define SET    1
#define OFF    0
#define CLEAR  0

#define LED_GREEN 0
#define LED_RED   1

//
//--- MSCB structure
typedef struct {
  unsigned char control;
  unsigned char status;

  // Functions
//  float              volt[N_VOLT];
  unsigned long int  dac[N_DAC];
} MSCB_USER_DATA;

void UpdatePCA(void);
void UpdateDAC(void);

#endif // _GIOCPET_410.H