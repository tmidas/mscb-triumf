/********************************************************************\

Name:     giocpet410    
Author:   Pierre-Andr�
Date:     
$Id$      Control of the AD5535 32DAC@14bit for MPET 
\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "giocpet410.h" 

extern xdata SYS_INFO sys_info;

char           code  MSCB_node_name[] = "GIOCPET410";
char           xdata svn_rev_code[] = "$Rev$";
char           xdata bdac[N_DAC];
MSCB_USER_DATA xdata user_data;
unsigned long int xdata mirdac[N_DAC];
unsigned long int jtest=0;

MSCB_INFO_VAR code vars[] = {
// 0x1 : Force DAC updates on all 32 channels now
// 0X2 : Enable Global Update (toggle) and force operation with CSR == 0x1
// 0x4 : Enable a slow sweep on all channels (bypass other DAC settings (no dispay on _data
// Default is update as they come with new value different then previous

  1, UNIT_BYTE,     0, 0, 0,           "Control",   &user_data.control,     // 0
  1, UNIT_BYTE,     0, 0, 0,           "Status" ,   &user_data.status,      // 1
  
  4, UNIT_BYTE,     0, 0, 0,           "DAC0"   ,   &user_data.dac[0],      // 2
  4, UNIT_BYTE,     0, 0, 0,           "DAC1"   ,   &user_data.dac[1],      // 32
  4, UNIT_BYTE,     0, 0, 0,           "DAC2"   ,   &user_data.dac[2],      // 33
  4, UNIT_BYTE,     0, 0, 0,           "DAC3"   ,   &user_data.dac[3],      // 34
  4, UNIT_BYTE,     0, 0, 0,           "DAC4"   ,   &user_data.dac[4],      // 35
  4, UNIT_BYTE,     0, 0, 0,           "DAC5"   ,   &user_data.dac[5],      // 36
  4, UNIT_BYTE,     0, 0, 0,           "DAC6"   ,   &user_data.dac[6],      // 37
  4, UNIT_BYTE,     0, 0, 0,           "DAC7"   ,   &user_data.dac[7],      // 38
  4, UNIT_BYTE,     0, 0, 0,           "DAC8"   ,   &user_data.dac[8],      // 37
  4, UNIT_BYTE,     0, 0, 0,           "DAC9"   ,   &user_data.dac[9],      // 38
  4, UNIT_BYTE,     0, 0, 0,           "DAC10"  ,   &user_data.dac[10],      // 31
  4, UNIT_BYTE,     0, 0, 0,           "DAC11"  ,   &user_data.dac[11],      // 32
  4, UNIT_BYTE,     0, 0, 0,           "DAC12"  ,   &user_data.dac[12],      // 33
  4, UNIT_BYTE,     0, 0, 0,           "DAC13"  ,   &user_data.dac[13],      // 34
  4, UNIT_BYTE,     0, 0, 0,           "DAC14"  ,   &user_data.dac[14],      // 35
  4, UNIT_BYTE,     0, 0, 0,           "DAC15"  ,   &user_data.dac[15],      // 36
  4, UNIT_BYTE,     0, 0, 0,           "DAC16"  ,   &user_data.dac[16],      // 37
  4, UNIT_BYTE,     0, 0, 0,           "DAC17"  ,   &user_data.dac[17],      // 38
  4, UNIT_BYTE,     0, 0, 0,           "DAC18"  ,   &user_data.dac[18],      // 37
  4, UNIT_BYTE,     0, 0, 0,           "DAC19"  ,   &user_data.dac[19],      // 38
  4, UNIT_BYTE,     0, 0, 0,           "DAC20"  ,   &user_data.dac[20],      // 31
  4, UNIT_BYTE,     0, 0, 0,           "DAC21"  ,   &user_data.dac[21],      // 32
  4, UNIT_BYTE,     0, 0, 0,           "DAC22"  ,   &user_data.dac[22],      // 33
  4, UNIT_BYTE,     0, 0, 0,           "DAC23"  ,   &user_data.dac[23],      // 34
  4, UNIT_BYTE,     0, 0, 0,           "DAC24"  ,   &user_data.dac[24],      // 35
  4, UNIT_BYTE,     0, 0, 0,           "DAC25"  ,   &user_data.dac[25],      // 36
  4, UNIT_BYTE,     0, 0, 0,           "DAC26"  ,   &user_data.dac[26],      // 37
  4, UNIT_BYTE,     0, 0, 0,           "DAC27"  ,   &user_data.dac[27],      // 38
  4, UNIT_BYTE,     0, 0, 0,           "DAC28"  ,   &user_data.dac[28],      // 37
  4, UNIT_BYTE,     0, 0, 0,           "DAC29"  ,   &user_data.dac[29],      // 38
  4, UNIT_BYTE,     0, 0, 0,           "DAC30"  ,   &user_data.dac[30],      // 31
  4, UNIT_BYTE,     0, 0, 0,           "DAC31"  ,   &user_data.dac[31],      // 32
  
 0
};

code MSCB_INFO_VAR *variables = vars;

/********************************************************************\

Application specific init and inout/output routines

\********************************************************************/

// Change register
volatile unsigned char bdata bChange = 0;
sbit PCA9536_Flag   = bChange ^ 0;
sbit CMD_REQ        = bChange ^ 1;
sbit bSweep         = bChange ^ 2;
sbit bEnable        = bChange ^ 3;

// Control register
unsigned char bdata rCTL = 0;
sbit cGlbTrig  = rCTL ^ 0;
sbit cGTEnable = rCTL ^ 1;
sbit cSweep    = rCTL ^ 2;
sbit C3       = rCTL ^ 3;
sbit CeeS     = rCTL ^ 4;
sbit CeeR     = rCTL ^ 5;
sbit CeeClr   = rCTL ^ 6;
sbit CmSd     = rCTL ^ 7;

// Status Register
unsigned char bdata rSTAT = 0;
sbit sGlbTrig  = rSTAT ^ 0;
sbit sGTEnable = rSTAT ^ 1;
sbit sSweep    = rSTAT ^ 2;
sbit S3       = rSTAT ^ 3;
sbit SeeS     = rSTAT ^ 4;
sbit SeeR     = rSTAT ^ 5;
sbit SsS      = rSTAT ^ 6;
sbit eAdc     = rSTAT ^ 7;


//
//-----------------------------------------------------------------------------
void publishCtlCsr(void) {
    DISABLE_INTERRUPTS;
    user_data.control = rCTL;
    user_data.status  = rSTAT;
    ENABLE_INTERRUPTS;
}

/*---- User init function ------------------------------------------*/
void user_init(unsigned char init)
{
  char i, temp=0;

  /* Format the SVN and store this code SVN revision into the system */
  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  sys_info.svn_revision = (svn_rev_code[6]-'0')*1000+
    (svn_rev_code[7]-'0')*100+
    (svn_rev_code[8]-'0')*10+
    (svn_rev_code[9]-'0');

  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  if (init) {
    // Initialize variables
    for (i=0;i<N_DAC;i++) {
      user_data.dac [i] = 0;
    }
  }
  i = cur_sub_addr();

  // Reference Voltage on P0.0 Enable
  REF0CN = 0x00;  // Int Bias off, Int Temp off, VREF input on

  // 0:analog 1:digital
  P0MDIN = 0xFF; // P0 all digital pins
  P1MDIN = 0xFF; // P1 all digital pins 
  P2MDIN = 0xFF; // P2 all digital pins
 
  // 0: open-drain, 1:push-pull
  P0MDOUT = 0x50; // P0 .6/Ren, .4/Tx

  P1MDOUT = 0x00; 
  P2MDOUT = 0x00; 
  P1 = 0x00;
  P2 = 0x00;

  // Setting the cross bar
  P0SKIP = 0x0; 
  P1SKIP = 0x0; 
  P2SKIP = 0x0; 

  XBR0 |=0x04;	 // Enable SMBus pins P0 .0/SDA .1/SCL

  rCTL  = 0;
  rSTAT = 0;

  /* set-up / initialize circuit components (order is important) */

  // CONFIG : 0 for output
//  PCA9536_Init();
//  PCA9536_WriteByte (PCA9536_ADDR, PCA9536_CONFIG, PCA9536_ALL_OUTPUT);
//  PCA9536_WriteByte (PCA9536_ADDR, PCA9536_POLINV, PCA9536_NO_INVERT);
//  PCA9536_WriteByte (PCA9536_ADDR, PCA9536_OUTPUT, 0x0);
  // Read back the value, and use that to propogate the user_data.mux fields
//  PCA9536_Read (PCA9536_ADDR, PCA9536_INPUT, &temp ,1);

  for (i=0;i<N_DAC;i++) {
     mirdac[i] = 0;
     user_data.dac[i] = i;
     bdac[i] = 1;
  }
  AD5535_Init();

  PCA9536_Init();
  PCA9536_WriteByte (PCA9536_ADDR, PCA9536_CONFIG, PCA9536_ALL_OUTPUT);
  PCA9536_WriteByte (PCA9536_ADDR, PCA9536_POLINV, PCA9536_INVERT);
  PCA9536_WriteByte (PCA9536_ADDR, PCA9536_OUTPUT, 0xf);
//  PCA9536_Read (PCA9536_ADDR, PCA9536_INPUT, &temp ,1);

}

/*---- User write function -----------------------------------------*/
void user_write(unsigned char index) reentrant{


  if ((index >= FIRST_DAC) && (index < (FIRST_DAC + N_DAC))) {
    if (mirdac[index-FIRST_DAC] !=  user_data.dac[index-FIRST_DAC]) {
       bdac[index-FIRST_DAC] = 1;
    }
  } else if(index == CTL_IDX) {
    rCTL = user_data.control;
    if (cSweep) bSweep = 1;
    CMD_REQ = 1;
  }

  return;
}

/*---- User read function ------------------------------------------*/

unsigned char user_read(unsigned char index)
{
  if(index);
  return 0;
}

/*---- User function called vid CMD_USER command -------------------*/

unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
  // echo input data
  data_out[0] = data_in[0];
  data_out[1] = data_in[1];
  return 2;
}

//
//----------------------------------------------------------------------------------
//Main user loop
/*---- User loop function ------------------------------------------*/
void user_loop(void)
{
  char i;
  if (CMD_REQ) {
    CMD_REQ = 0;
    if (bSweep) {
      if (sSweep) {
        sSweep = 0; cSweep = 0; bSweep = 0;
      } else if (!sSweep) {
        sSweep = 1; cSweep = 0; bSweep = 0;
      }
    } else if (cGlbTrig) {
      sGlbTrig = 1; cGlbTrig = 0;
    } else if (sGTEnable) {
      sGTEnable = 0; cGTEnable = 0;
    } else if (!sGTEnable) {
      sGTEnable = 1; cGTEnable = 0;
    }
    
  }
  publishCtlCsr();
  if (sSweep) {
    for (i=0;i<N_DAC;i++) {
      AD5535_Write(i, jtest);
    }
    jtest = (jtest+100) % 0x3FFF;

    DISABLE_INTERRUPTS;
    for (i=0;i<N_DAC;i++) {
     user_data.dac[i] = jtest;
    }
    ENABLE_INTERRUPTS;
    led_blink(LED_GREEN, 1, 50);
  } else {
    UpdateDAC();
  }
//  led_blink(LED_GREEN, 1, 50);
  delay_ms(50); // for now.
}

//
//---------------------------------------------------------------
void UpdateDAC(void) {
#ifdef MSCB_DEVICE_DAC_AD5535_DISABLE
#else
  char i;

  if (sGlbTrig) {
// Do all of them on request
    for (i=0;i<N_DAC;i++) {
      led_blink(LED_RED, 1, 50);
      AD5535_Write(i, user_data.dac[i]);
      mirdac[i] = user_data.dac[i];
      bdac[i] = 0;
    }
    sGlbTrig = 0;
  } else if (!sGTEnable) {
// Do them as they change
    for (i=0;i<N_DAC;i++) {
      if (bdac[i]) {
        if (mirdac[i] != user_data.dac[i]) {
          led_blink(LED_RED, 1, 50);
          mirdac[i] = user_data.dac[i];
          AD5535_Write(i, mirdac[i]);
        }
        bdac[i] = 0;
      }
    }
  }
#endif // MSCB_DEVICE_DAC_AD5535_DISABLE
}



