// $Id$

#ifndef _MSCB_PROJECT_H
#define _MSCB_PROJECT_H

#define LED_0 P0 ^ 3
#define LED_1 P0 ^ 2
#define LED_ON 1
#define MCU_C8051F410
#define RS485_ENABLE_PIN P0 ^ 6

// MSCB Constants
#define MSCB_NUM_SUB_NODES 1

// Used Protocols
#define MSCB_SPI_CLK  P2 ^ 2
#define MSCB_SPI_MOSI P2 ^ 0
#define MSCB_SPI_SYNC P2 ^ 1
#define MSCB_SPI_MISO P2 ^ 3

#define MSCB_SMBus_HW
//#define MSCB_SHT7x

// Used Devices
#define MSCB_DEVICE_IO_PCA9536
#define MSCB_DEVICE_DAC_AD5535

//#define MSCB_DEVICE_DAC_LTC2605
//#define MSCB_DEVICE_ADC_LTC2489
//#define MSCB_DEVICE_TEMP_ADT7483
//#define MSCB_DEVICE_HUMIDITY_SHT7x

// Disabled Devices
//#define MCSB_DEVICE_ADC_LTC2489_DISABLE 
//#define MCSB_DEVICE_DAC_LTC2605_DISABLE 
//#define MCSB_DEVICE_IO_PCA9536_DISABLE 
//#define MSCB_DEVICE_TEMP_ADT7483_DISABLE

#endif // _MSCB_PROJECT_H
