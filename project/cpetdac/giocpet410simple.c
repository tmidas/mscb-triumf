/********************************************************************\

Name:     giocpet410    
Author:   Pierre-Andr�
Date:     
$Id$      Control of the AD5535 32DAC@14bit for MPET 
\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "giocpet410.h" 

extern xdata SYS_INFO sys_info;

char           code  MSCB_node_name[] = "GIOCPET410";
char           xdata svn_rev_code[] = "$Rev$";
char           xdata bvolt[N_VOLT];
MSCB_USER_DATA xdata user_data;
float          xdata mirvolt[N_VOLT];

MSCB_INFO_VAR code vars[] = {
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt0"  ,   &user_data.volt[0],     // 15
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt1"  ,   &user_data.volt[1],     // 16
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt2"  ,   &user_data.volt[2],     // 17
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt3"  ,   &user_data.volt[3],     // 18
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt4"  ,   &user_data.volt[4],     // 19
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt5"  ,   &user_data.volt[5],     // 20
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt6"  ,   &user_data.volt[6],     // 21
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt7"  ,   &user_data.volt[7],     // 22
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt8"  ,   &user_data.volt[8],     // 19
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt9"  ,   &user_data.volt[9],     // 20
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt10"  ,  &user_data.volt[10],     // 21
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt11"  ,  &user_data.volt[11],     // 16
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt12"  ,  &user_data.volt[12],     // 17
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt13"  ,  &user_data.volt[13],     // 18
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt14"  ,  &user_data.volt[14],     // 19
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt15"  ,  &user_data.volt[15],     // 20
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt16"  ,  &user_data.volt[16],     // 21
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt17"  ,  &user_data.volt[17],     // 22
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt18"  ,  &user_data.volt[18],     // 19
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt19"  ,  &user_data.volt[19],     // 20
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt20"  ,  &user_data.volt[20],     // 21
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt21"  ,  &user_data.volt[21],     // 16
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt22"  ,  &user_data.volt[22],     // 17
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt23"  ,  &user_data.volt[23],     // 18
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt24"  ,  &user_data.volt[24],     // 19
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt25"  ,  &user_data.volt[25],     // 20
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt26"  ,  &user_data.volt[26],     // 21
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt27"  ,  &user_data.volt[27],     // 22
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt28"  ,  &user_data.volt[28],     // 19
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt29"  ,  &user_data.volt[29],     // 20
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt30"  ,  &user_data.volt[30],     // 21
  4, UNIT_VOLT,     0, 0, MSCBF_FLOAT, "Volt31"  ,  &user_data.volt[31],     // 16
  
  
  4, UNIT_BYTE,     0, 0, 0,           "DAC0"   ,   &user_data.dac[0],      // 31
  4, UNIT_BYTE,     0, 0, 0,           "DAC1"   ,   &user_data.dac[1],      // 32
  4, UNIT_BYTE,     0, 0, 0,           "DAC2"   ,   &user_data.dac[2],      // 33
  4, UNIT_BYTE,     0, 0, 0,           "DAC3"   ,   &user_data.dac[3],      // 34
  4, UNIT_BYTE,     0, 0, 0,           "DAC4"   ,   &user_data.dac[4],      // 35
  4, UNIT_BYTE,     0, 0, 0,           "DAC5"   ,   &user_data.dac[5],      // 36
  4, UNIT_BYTE,     0, 0, 0,           "DAC6"   ,   &user_data.dac[6],      // 37
  4, UNIT_BYTE,     0, 0, 0,           "DAC7"   ,   &user_data.dac[7],      // 38
  4, UNIT_BYTE,     0, 0, 0,           "DAC8"   ,   &user_data.dac[8],      // 37
  4, UNIT_BYTE,     0, 0, 0,           "DAC9"   ,   &user_data.dac[9],      // 38
  4, UNIT_BYTE,     0, 0, 0,           "DAC10"  ,   &user_data.dac[10],      // 31
  4, UNIT_BYTE,     0, 0, 0,           "DAC11"  ,   &user_data.dac[11],      // 32
  4, UNIT_BYTE,     0, 0, 0,           "DAC12"  ,   &user_data.dac[12],      // 33
  4, UNIT_BYTE,     0, 0, 0,           "DAC13"  ,   &user_data.dac[13],      // 34
  4, UNIT_BYTE,     0, 0, 0,           "DAC14"  ,   &user_data.dac[14],      // 35
  4, UNIT_BYTE,     0, 0, 0,           "DAC15"  ,   &user_data.dac[15],      // 36
  4, UNIT_BYTE,     0, 0, 0,           "DAC16"  ,   &user_data.dac[16],      // 37
  4, UNIT_BYTE,     0, 0, 0,           "DAC17"  ,   &user_data.dac[17],      // 38
  4, UNIT_BYTE,     0, 0, 0,           "DAC18"  ,   &user_data.dac[18],      // 37
  4, UNIT_BYTE,     0, 0, 0,           "DAC19"  ,   &user_data.dac[19],      // 38
  4, UNIT_BYTE,     0, 0, 0,           "DAC20"  ,   &user_data.dac[20],      // 31
  4, UNIT_BYTE,     0, 0, 0,           "DAC21"  ,   &user_data.dac[21],      // 32
  4, UNIT_BYTE,     0, 0, 0,           "DAC22"  ,   &user_data.dac[22],      // 33
  4, UNIT_BYTE,     0, 0, 0,           "DAC23"  ,   &user_data.dac[23],      // 34
  4, UNIT_BYTE,     0, 0, 0,           "DAC24"  ,   &user_data.dac[24],      // 35
  4, UNIT_BYTE,     0, 0, 0,           "DAC25"  ,   &user_data.dac[25],      // 36
  4, UNIT_BYTE,     0, 0, 0,           "DAC26"  ,   &user_data.dac[26],      // 37
  4, UNIT_BYTE,     0, 0, 0,           "DAC27"  ,   &user_data.dac[27],      // 38
  4, UNIT_BYTE,     0, 0, 0,           "DAC28"  ,   &user_data.dac[28],      // 37
  4, UNIT_BYTE,     0, 0, 0,           "DAC29"  ,   &user_data.dac[29],      // 38
  4, UNIT_BYTE,     0, 0, 0,           "DAC30"  ,   &user_data.dac[30],      // 31
  4, UNIT_BYTE,     0, 0, 0,           "DAC31"  ,   &user_data.dac[31],      // 32
  
 0
};

code MSCB_INFO_VAR *variables = vars;

/********************************************************************\

Application specific init and inout/output routines

\********************************************************************/

// Change register
volatile unsigned char bdata bChange = 0;
sbit PCA9536_Flag = bChange ^ 0;

// Control register
unsigned char bdata rCTL = 0;
sbit C0       = rCTL ^ 0;
sbit C1       = rCTL ^ 1;
sbit C2       = rCTL ^ 2;
sbit C3       = rCTL ^ 3;
sbit CeeS     = rCTL ^ 4;
sbit CeeR     = rCTL ^ 5;
sbit CeeClr   = rCTL ^ 6;
sbit CmSd     = rCTL ^ 7;

// Status Register
unsigned char bdata rSTAT = 0;
sbit S0       = rSTAT ^ 0;
sbit S1       = rSTAT ^ 1;
sbit S2       = rSTAT ^ 2;
sbit S3       = rSTAT ^ 3;
sbit SeeS     = rSTAT ^ 4;
sbit SeeR     = rSTAT ^ 5;
sbit SsS      = rSTAT ^ 6;
sbit eAdc     = rSTAT ^ 7;

/*---- User init function ------------------------------------------*/

void user_init(unsigned char init)
{
  char i, temp=0;

  /* Format the SVN and store this code SVN revision into the system */
  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  sys_info.svn_revision = (svn_rev_code[6]-'0')*1000+
    (svn_rev_code[7]-'0')*100+
    (svn_rev_code[8]-'0')*10+
    (svn_rev_code[9]-'0');

  for (i=0;i<4;i++) {
    if (svn_rev_code[6+i] < 48) {
      svn_rev_code[6+i] = '0';
    }
  }

  if (init) {
    // Initialize variables
    for (i=0;i<8;i++) {
      user_data.dac [i] = 0;
      user_data.volt[i] = 0.0;
    }
  }
  i = cur_sub_addr();

  // Reference Voltage on P0.0 Enable
  REF0CN = 0x00;  // Int Bias off, Int Temp off, VREF input on

  // 0:analog 1:digital
  P0MDIN = 0xFF; // P0 all digital pins
  P1MDIN = 0xFF; // P1 all digital pins 
  P2MDIN = 0xFF; // P2 all digital pins
 
  // 0: open-drain, 1:push-pull
  P0MDOUT = 0x50; // P0 .6/Ren, .4/Tx

  P1MDOUT = 0x00; 
  P2MDOUT = 0x00; 
  P1 = 0x00;
  P2 = 0x00;

  // Setting the cross bar
  P0SKIP = 0x0; 
  P1SKIP = 0x0; 
  P2SKIP = 0x0; 

  XBR0 |=0x04;	 // Enable SMBus pins P0 .0/SDA .1/SCL

  /* set-up / initialize circuit components (order is important) */

  // CONFIG : 0 for output
//  PCA9536_Init();
//  PCA9536_WriteByte (PCA9536_ADDR, PCA9536_CONFIG, PCA9536_ALL_OUTPUT);
//  PCA9536_WriteByte (PCA9536_ADDR, PCA9536_POLINV, PCA9536_NO_INVERT);
//  PCA9536_WriteByte (PCA9536_ADDR, PCA9536_OUTPUT, 0x0);
  // Read back the value, and use that to propogate the user_data.mux fields
//  PCA9536_Read (PCA9536_ADDR, PCA9536_INPUT, &temp ,1);

  for (i=0;i<N_VOLT;i++) {
     mirvolt[i] = 0;
     user_data.dac[i] = i;
     bvolt[i] = 1;
  }
  AD5535_Init();

  PCA9536_Init();
  PCA9536_WriteByte (PCA9536_ADDR, PCA9536_CONFIG, PCA9536_ALL_OUTPUT);
  PCA9536_WriteByte (PCA9536_ADDR, PCA9536_POLINV, PCA9536_NO_INVERT);
  PCA9536_WriteByte (PCA9536_ADDR, PCA9536_OUTPUT, 0xf);
//  PCA9536_Read (PCA9536_ADDR, PCA9536_INPUT, &temp ,1);

}

/*---- User write function -----------------------------------------*/
void user_write(unsigned char index) reentrant{


  if ((index >= FIRST_VOLT) && (index < (FIRST_VOLT + N_VOLT))) {
    if (mirvolt[index] !=  user_data.volt[index]) {
       bvolt[index] = 1;
    }
  }

  return;
}

/*---- User read function ------------------------------------------*/

unsigned char user_read(unsigned char index)
{
  if(index);
  return 0;
}

/*---- User function called vid CMD_USER command -------------------*/

unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
  // echo input data
  data_out[0] = data_in[0];
  data_out[1] = data_in[1];
  return 2;
}

//
//----------------------------------------------------------------------------------
//Main user loop
/*---- User loop function ------------------------------------------*/
void user_loop(void)
{
  UpdateDAC();
  led_blink(LED_GREEN, 1, 50);
  delay_ms(50); // for now.
}

//
//---------------------------------------------------------------
void UpdateDAC(void) {
#ifdef MSCB_DEVICE_DAC_AD5535_DISABLE
#else
  float alpha = 1.;
  float beta  = 0.;
  char i;

  for (i=0;i<N_VOLT;i++) {
    if (bvolt[i]) {
      DISABLE_INTERRUPTS;
      user_data.dac[i] = (int) (user_data.volt[i] * alpha + beta);
      ENABLE_INTERRUPTS;        
      led_blink(LED_RED, 1, 50);
      AD5535_Write(i, user_data.dac[i]);
      mirvolt[i] = user_data.volt[i];
      if (i!=5) bvolt[i] = 0;
    }
  }
#endif // MSCB_DEVICE_DAC_AD5535_DISABLE
}



