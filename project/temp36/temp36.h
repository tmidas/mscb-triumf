/********************************************************************\

  Name:         temp36.h
  Created by:   Noel Wu

  Contents:     Application specific (user) part of
                Midas Slow Control Bus protocol
                for temp 36 board
   $Id: temp36.h 260 2012-11-27 20:56:36Z midas $
\********************************************************************/

#ifndef  _TEMP36_H_
#define  _TEMP36_H_

#include "../../common/mscbemb.h"

/*****USER DEFINED DATA*****/
typedef struct user_data_type {
  unsigned long SerialN;    // Serial Number
  unsigned int  error;      // General Error register
  unsigned char control;    // Writing/Reading the EEPROM
  unsigned char status;     // Displaying the status of the EEPROM command
  unsigned char eepage;     // EEPROM page number
  unsigned int  navge;      // Number of samples for averging temperature
  unsigned char terror[5];  // Status for reading the temperature
  float         Temp[36];   // ADT7486A external temperature [degree celsius]
  float         ref;
  float         AT[36];
  float         SHT_temp;
  float         SHT_humidity;
#ifdef MORETHANONEHUM
  float         SHT_temp2;
  float         SHT_humidity2;
#endif
  unsigned char SHT_on;
} MSCB_USER_DATA;


#define AVGE_IDX    5 

/*****TEMPERATURE VARIABLES*****/
#define SST_LINE1 1
#define SST_LINE2 2

//Every element in Terrorclear indicates a particular binary bit is 0
unsigned char xdata Terrorclear[]={0xFE, 0xFD, 0xFB, 0xF7, 0xEF, 0xDF, 0xBF, 0x7F};

//Every element in Terrorclear indicates a particular binary bit is 1
unsigned char xdata Terrorset[]={0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};
#define TAVGMAX (int) 30
float xdata Taverage[36][TAVGMAX];

/*****GENERAL*****/
//Control Register
//unsigned char bdata rCTL;
//sbit Cref     = rCTL ^ 1;
//sbit CHum     = rCTL ^ 2;
//sbit CeeS     = rCTL ^ 4;
//sbit CeeR     = rCTL ^ 5;
//sbit CeeClr   = rCTL ^ 6;

// Status Register
//unsigned char bdata rCSR;
//sbit Sref     = rCSR ^ 1;
//sbit SeeS     = rCSR ^ 4;
//sbit SeeR     = rCSR ^ 5;

// ESR Error Register
//The low and high bytes are switched in the bdata section of the memory
//This is the reason that the sbit declarations do not appear to match
//the documentation but they actually do.
//unsigned int bdata rESR;
//sbit err1      = rESR ^ 8;  //0x1
//sbit err2      = rESR ^ 9;  //0x2
//sbit err3      = rESR ^ 10; //0x4
//sbit err4      = rESR ^ 11; //0x8

//sbit err5      = rESR ^ 12; //0x10
//sbit err6      = rESR ^ 13; //0x20
//sbit err7      = rESR ^ 14; //0x40
//sbit err8      = rESR ^ 15; //0x80

//sbit uCT       = rESR ^ 0;  //0x100
//sbit err10     = rESR ^ 1;  //0x200
//sbit err11     = rESR ^ 2;  //0x400
//sbit err12     = rESR ^ 3;  //0x800

//sbit RdssT     = rESR ^ 4;  //0x1000
//sbit EEPROM    = rESR ^ 5;  //0x2000
//sbit err15     = rESR ^ 6;  //0x4000 1= any of 4 not lock
//sbit err16     = rESR ^ 7;  //0x8000

// Global ON / OFF definition
#define DONE   1
#define FAILED 0
#define SET    1
#define CLEAR  0
#endif



