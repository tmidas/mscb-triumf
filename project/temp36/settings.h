#ifndef _MSCB_PROJECT_H
#define _MSCB_PROJECT_H

#define TEMP36

// MSCB Project Defines
#define MCU_C8051F120
#define LED_0 						P2 ^ 7
#define LED_1 						P2 ^ 6
#define LED_ON 						1
#define RS485_ENABLE_PIN 	P0 ^ 2

// MSCB Constants
#define MSCB_NUM_SUB_NODES 1

#define MORE_THAN_ONE_LINE   // SST Line

// Used Protocols and related defines
#define MSCB_SST
#define MSCB_SST1 				P0 ^ 3 //SST1 line  SST_IO (Write/Push-Pull) 
#define SST_ClientResponse1 (char) ((CPT1CN & 0x40) >> 6) //Comparator1 overflow bit

#define MSCB_SST
#define MSCB_SST2 				P0 ^ 4 //SST1 line  SST_IO (Write/Push-Pull) 
#define SST_ClientResponse2 (char) ((CPT0CN & 0x40) >> 6) //Comparator1 overflow bit

// Used Devices
#define MSCB_DEVICE_TEMP_ADT7486A

#define MSCB_SHT7x
#define MSCB_DEVICE_HUMIDITY_SHT7x

#define MORETHANONEHUM

#define SHT_DATA1 P1 ^ 3  
#define SHT_SCK1  P1 ^ 0

#define SHT_DATA2 P1 ^ 7  
#define SHT_SCK2  P1 ^ 4


#endif // _MSCB_PROJECT_H
