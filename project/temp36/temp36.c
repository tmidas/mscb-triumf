/********************************************************************\
 Name:         temp36.c
 Created by:   Noel Wu                May/07/2008


 Contents:     Application specific (user) part of
 Midas Slow Control Bus protocol for temp36 board
 
 $Id: temp36.c 260 2012-11-27 20:56:36Z midas $
 \********************************************************************/

#include "temp36.h"

extern xdata SYS_INFO sys_info;

void UpdateADT7486A(const unsigned char sst_line, unsigned char block);

char code  MSCB_node_name[] = "TEMP36";
char idata svn_rev_code[]   = "$Rev: 260 $";
 
xdata MSCB_USER_DATA user_data;

 // User Data structure declaration
 //-----------------------------------------------------------------------------
 code MSCB_INFO_VAR vars[] = {
   4, UNIT_BYTE,            0, 0,           0, "SerialN",    &user_data.SerialN,       //0
   2, UNIT_BYTE,            0, 0,           0, "Error",      &user_data.error,         //1
   1, UNIT_BYTE,            0, 0,           0, "Control",    &user_data.control,       //2
   1, UNIT_BYTE,            0, 0,           0, "Status",     &user_data.status,        //3
   1, UNIT_BYTE,            0, 0,           0, "EEPage",     &user_data.eepage,        //4
   2, UNIT_BYTE,            0, 0,           0, "Navge",        &user_data.navge,       //5     
   1, UNIT_BYTE,            0, 0,           0, "TE01-08",      &user_data.terror[0],   //6 
   1, UNIT_BYTE,            0, 0,           0, "TE09-16",      &user_data.terror[1],   //7 
   1, UNIT_BYTE,            0, 0,           0, "TE17-24",      &user_data.terror[2],   //8 
   1, UNIT_BYTE,            0, 0,           0, "TE25-32",      &user_data.terror[3],   //9 
   1, UNIT_BYTE,            0, 0,           0, "TE33-36",      &user_data.terror[4],   //10 
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp01",       &user_data.Temp[0],     //11
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp02",       &user_data.Temp[1],     //12
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp03",       &user_data.Temp[2],     //13
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp04",       &user_data.Temp[3],     //14
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp05",       &user_data.Temp[4],     //15
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp06",       &user_data.Temp[5],     //16
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp07",       &user_data.Temp[6],     //17
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp08",       &user_data.Temp[7],     //18
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp09",       &user_data.Temp[8],     //19
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp10",       &user_data.Temp[9],     //20
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp11",       &user_data.Temp[10],    //21
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp12",       &user_data.Temp[11],    //22
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp13",       &user_data.Temp[12],    //23
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp14",       &user_data.Temp[13],    //24
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp15",       &user_data.Temp[14],    //25
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp16",       &user_data.Temp[15],    //26
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp17",       &user_data.Temp[16],    //27
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp18",       &user_data.Temp[17],    //28
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp19",       &user_data.Temp[18],    //29
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp20",       &user_data.Temp[19],    //30
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp21",       &user_data.Temp[20],    //31
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp22",       &user_data.Temp[21],    //32
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp23",       &user_data.Temp[22],    //33
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp24",       &user_data.Temp[23],    //34
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp25",       &user_data.Temp[24],    //35
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp26",       &user_data.Temp[25],    //36
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp27",       &user_data.Temp[26],    //37
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp28",       &user_data.Temp[27],    //38
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp29",       &user_data.Temp[28],    //39
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp30",       &user_data.Temp[29],    //40
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp31",       &user_data.Temp[30],    //41
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp32",       &user_data.Temp[31],    //42
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp33",       &user_data.Temp[32],    //43
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp34",       &user_data.Temp[33],    //44
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp35",       &user_data.Temp[34],    //45
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Temp36",       &user_data.Temp[35],    //46

   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "REF",          &user_data.ref   ,      //47

   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg01",       &user_data.AT[0],       //48
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg02",       &user_data.AT[1],       //49
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg03",       &user_data.AT[2],       //50
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg04",       &user_data.AT[3],       //51
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg05",       &user_data.AT[4],       //52 
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg06",       &user_data.AT[5],       //53
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg07",       &user_data.AT[6],       //54
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg08",       &user_data.AT[7],       //55
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg09",       &user_data.AT[8],       //56
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg10",       &user_data.AT[9],       //57
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg11",       &user_data.AT[10],      //58
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg12",       &user_data.AT[11],      //59
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg13",       &user_data.AT[12],      //60
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg14",       &user_data.AT[13],      //61
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg15",       &user_data.AT[14],      //62
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg16",       &user_data.AT[15],      //63
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg17",       &user_data.AT[16],      //64
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Tavg18",       &user_data.AT[17],      //65
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg19",       &user_data.AT[18],      //66
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg20",       &user_data.AT[19],      //67
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg21",       &user_data.AT[20],      //68
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg22",       &user_data.AT[21],      //69
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg23",       &user_data.AT[22],      //70
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg24",       &user_data.AT[23],      //71
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg25",       &user_data.AT[24],      //72
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg26",       &user_data.AT[25],      //73
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg27",       &user_data.AT[26],      //74
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg28",       &user_data.AT[27],      //75
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg29",       &user_data.AT[28],      //76
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg30",       &user_data.AT[29],      //77
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg31",       &user_data.AT[30],      //78
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg32",       &user_data.AT[31],      //79
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg33",       &user_data.AT[32],      //80
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg34",       &user_data.AT[33],      //81
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg35",       &user_data.AT[34],      //82
   4, UNIT_CELSIUS,         0, 0, MSCBF_HIDDEN|MSCBF_FLOAT, "Tavg36",       &user_data.AT[35],      //83
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "SHTtemp",   &user_data.SHT_temp,                    //84
   4, UNIT_PERCENT,         0, 0, MSCBF_FLOAT, "SHThum",    &user_data.SHT_humidity,                //85
#ifdef MORETHANONEHUM
   4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "SHTtemp2",   &user_data.SHT_temp2,                  //86
   4, UNIT_PERCENT,         0, 0, MSCBF_FLOAT, "SHThum2",    &user_data.SHT_humidity2,              //87
#endif
   1, UNIT_BYTE,            0, 0, MSCBF_HIDDEN,"SHTon",     &user_data.SHT_on,                      //86 or 88
   0
 };

code MSCB_INFO_VAR *variables = vars;
 
/********************************************************************\

Application specific init and inout/output routines

\********************************************************************/

static signed char xdata lAvge = -1;
// Change register
unsigned char bdata bChange = 0;
sbit fAvge = bChange ^ 0;
sbit HUM_FLAG = bChange ^1;

/*---- User init function ------------------------------------------*/
void user_init(unsigned char init)
{
   unsigned char idata i;

   /* Format the SVN and store this code SVN revision into the system */
   for (i=0;i<4;i++) {
      if (svn_rev_code[6+i] < 48) {
       svn_rev_code[6+i] = '0';
     }
   }

  i = cur_sub_addr();
  sys_info.svn_revision = (svn_rev_code[6]-'0')*1000+
                           (svn_rev_code[7]-'0')*100+
                           (svn_rev_code[8]-'0')*10+
                           (svn_rev_code[9]-'0');

  if(init) {}

// Initial setting for communication and overall ports 
//-----------------------------------------------------------------------------

// ADT7486A Init
   SFRPAGE  = CONFIG_PAGE;
   P0MDOUT |= 0x1C; // Setting the SST_DRV (SST1/SST2) to push pull
   SFRPAGE  = CPT1_PAGE;
   CPT1CN  |= 0x80; // Enable the Comparator 1
   CPT1MD   = 0x03; //Comparator1 Mode Selection
   SFRPAGE  = CPT0_PAGE;
   CPT0CN  |= 0x80; // Enable the Comparator 0
   CPT0MD   = 0x03; //Comparator0 Mode Selection
   //Use default, adequate TYP (CP1 Response Time, no edge triggered interrupt)   
   ADT7486A_Init(SST_LINE1); //Temperature measurements related initialization
   ADT7486A_Init(SST_LINE2); 

// User variables initialization
   user_data.terror[0]=0;
   user_data.terror[1]=0;
   user_data.terror[2]=0;
   user_data.terror[3]=0;
   user_data.terror[4]=0;
   user_data.status=0;

   user_data.navge=20;
   user_data.ref=0;
 
  for(i=0; i<36; i++) {
    user_data.Temp[i] = 0.0;
    user_data.AT[i]=0;
  }
  
  // Humidity initilaization
  SFRPAGE  = CONFIG_PAGE;
  P1MDIN = 0xFF;    // Not Analog
// ports provide VCC+GND
//  P1MDOUT = 0x0E; // 0=Open-drain, 1=pushpull
//  P1 = 0x04;
//
// With the SHT adaptor board
// CLKx  P1^0, P1^4   Pushpull
// DATAx P1^3, P1^7   Open-Drain
  P1MDOUT = 0x11; // 0=Open-drain, 1=pushpull (clock only)
  P1 = 0x00;      // not used
//  user_data.SHT_on = 1;
  HUM_FLAG = 1;
  // Force a fAvge;
  fAvge = 1;
}

/*---- User write function -----------------------------------------*/
void user_write(unsigned char index) reentrant
{
  if (index == AVGE_IDX) { // Update #avge
    fAvge = 1;
  }  
}

/*---- User read function ------------------------------------------*/
unsigned char user_read(unsigned char index)
{
  if(index);
  return 0;
}

/*---- User function called vid CMD_USER command -------------------*/
unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
   /* echo input data */
   data_out[0] = data_in[0];
   data_out[1] = data_in[1];
   return 2;
}


//------------------------------------------------------------------
// Temperature reading
void UpdateADT7486A_LINE1(void)
{
  // Block=0 refers to channels  0..17

  //Address for all the ADT7486A sensors on the SST_LINE1/SST_LINE2
  static unsigned char code ADT7486A_addrArray[] = {0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0x50};
  signed char status;
  static unsigned char i = 0;
  unsigned char j;
  float xdata temperature[3];

  if(i == 9) { i = 0; }
  j = (i * 2);

  // Read the external temperature from each chip that is connected to SST_LINE1
  // Corresponds to Temp01,03,05...17
  status = ADT7486A_GetAllTemp(SST_LINE1, ADT7486A_addrArray[i], &temperature);
  if (status == ADT7486A_SUCCESS) {
    DISABLE_INTERRUPTS;
    user_data.Temp[j] 	= temperature[0];
    user_data.Temp[j+1] = temperature[1];
    ENABLE_INTERRUPTS;
  }
  i++;
}    

//------------------------------------------------------------------
// Temperature reading
void UpdateADT7486A_LINE2(void)
{
  // Block=0 refers to channels  0..17
  // Block=1 refers to channels 18..35

  //Address for all the ADT7486A sensors on the SST_LINE1/SST_LINE2
  static unsigned char code ADT7486A_addrArray[] = {0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0x50};
  signed char status;
  static unsigned char i = 0;
  unsigned char j;
  float xdata temperature[3];

  if(i == 9) { i = 0; }
  j = 18 + (i * 2);

  // Read the external temperature from each chip that is connected to SST_LINE1
  // Corresponds to Temp01,03,05...17
  status = ADT7486A_GetAllTemp(SST_LINE2, ADT7486A_addrArray[i], &temperature);
  if (status == ADT7486A_SUCCESS) {
    DISABLE_INTERRUPTS;
    user_data.Temp[j] 	= temperature[0];
    user_data.Temp[j+1] = temperature[1];
    ENABLE_INTERRUPTS;
  }
  i++;
}    

//
//-----------------------------------------------------------
void UpdateHumidity(void) {

  //Humidity variables
  static unsigned long xdata currentTime=0;
  static unsigned char xdata status;
  static float         xdata humidity, htemperature;
  static unsigned int  xdata rSHTtemp1, rSHThumi1;
  static unsigned char xdata FCSorig1, FCSdevi1;

  if(user_data.SHT_on >= 1) {
    if(HUM_FLAG) {
      // Initializing the SHTxx communication
      HumiSensor_Init(1);  
      HUM_FLAG = 0;
    }
    //Measuring the humidity and temperature
    if ((uptime() - currentTime) > 1) {
    led_blink(0, 1, 50);
      status = HumidSensor_Cmd (&rSHThumi1
                               ,&rSHTtemp1
                              ,&humidity
                              ,&htemperature
                              ,&FCSorig1
                              ,&FCSdevi1
                              ,1);
      if (status == DONE){	 
        DISABLE_INTERRUPTS;
        user_data.SHT_humidity = humidity;
        user_data.SHT_temp = htemperature;
        ENABLE_INTERRUPTS;
      } else {
        DISABLE_INTERRUPTS;
        user_data.SHT_humidity = -1;
        user_data.SHT_temp = -1;
        ENABLE_INTERRUPTS;
      }
      currentTime = uptime();
    } // uptime()-
  } else { // SHT off
    DISABLE_INTERRUPTS;
    user_data.SHT_humidity = 0;
    user_data.SHT_temp = 0;
    ENABLE_INTERRUPTS;
  }
}

#ifdef MORETHANONEHUM
//
//-----------------------------------------------------------
void UpdateHumidity2(void) {

  //Humidity variables
  static unsigned long xdata currentTime=0;
  static unsigned char xdata status;
  static float         xdata humidity, htemperature;
  static unsigned int  xdata rSHTtemp1, rSHThumi1;
  static unsigned char xdata FCSorig1, FCSdevi1;

  if(user_data.SHT_on == 2) {
    if(HUM_FLAG) {
      // Initializing the SHTxx communication
      HumiSensor_Init(2);  
      HUM_FLAG = 0;
    }
    //Measuring the humidity and temperature
    if ((uptime() - currentTime) > 1) {
    led_blink(0, 1, 50);
      status = HumidSensor_Cmd (&rSHThumi1
                               ,&rSHTtemp1
                              ,&humidity
                              ,&htemperature
                              ,&FCSorig1
                              ,&FCSdevi1
                              ,2);
      if (status == DONE){	 
        DISABLE_INTERRUPTS;
        user_data.SHT_humidity2 = humidity;
        user_data.SHT_temp2 = htemperature;
        ENABLE_INTERRUPTS;
      } else {
        DISABLE_INTERRUPTS;
        user_data.SHT_humidity2 = -1;
        user_data.SHT_temp2 = -1;
        ENABLE_INTERRUPTS;
      }
      currentTime = uptime();
    } // uptime()-
  } else { // SHT off
    DISABLE_INTERRUPTS;
    user_data.SHT_humidity2 = 0;
    user_data.SHT_temp2 = 0;
    ENABLE_INTERRUPTS;
  }
}
#endif

/*---- User loop function ------------------------------------------*/
void user_loop(void)
{
 // Temperature variables
 static signed char xdata cavg=-1;
 static unsigned char idata channel, avgcount, numavg=0;
 float xdata average;

  UpdateHumidity();
#ifdef MORETHANONEHUM
  UpdateHumidity2();
#endif
  UpdateADT7486A_LINE1();
//  led_blink(0, 1, 50);
  UpdateADT7486A_LINE2();
  led_blink(1, 1, 50);

  if (fAvge) {
    fAvge = 0;
    if((user_data.navge <= TAVGMAX) && (user_data.navge > 0)) {
      lAvge = user_data.navge;
      cavg = -1;
      numavg = 0;
    } else {
      lAvge = -1;
    }
  }

  if (lAvge == -1) return;
  
  // Calculating the rolling average
	cavg++;
  if (numavg != lAvge) numavg = cavg+1; else numavg = lAvge;
	cavg %= lAvge;
  
  // Do averaging on all the channels
  for(channel=0; channel<36; channel++) {
    Taverage[channel][(cavg)] = user_data.Temp[channel];

    // Reset average for that channel
    average = 0;

    // Sum of temps
    for(avgcount=0; avgcount<numavg ; avgcount++) {
      average += Taverage[channel][avgcount];
    }

  // Compute average
  average /= numavg;

  DISABLE_INTERRUPTS;
  user_data.AT[channel] = average;
  ENABLE_INTERRUPTS; 
  }

  delay_ms(500);
}